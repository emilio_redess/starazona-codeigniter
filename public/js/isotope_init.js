


$(function(){
	var filtro_text = "";
	switch (filtro) {

		case 0:
			filtro_text = '*';
		break;

		case 1:
			filtro_text = '.particulares';
		break;

		case 2:
			filtro_text = '.empresas';
		break;

		case 3:
			filtro_text = '.vida';
		break;

		case 4:
			filtro_text = '.espectaculos';
		break;								
	}


  
  var $container = $('#isotope_container'),
      $filterLinks = $('#filters a');
  
  $container.isotope({
    itemSelector: '.item',
    filter: filtro_text
  });
  
  $filterLinks.click(function(){
    var $this = $(this);
    
    // don't proceed if already selected
    if ( $this.hasClass('active') ) {
      return;
    }
    
    $filterLinks.filter('.active').removeClass('active');
    $this.addClass('active');
    
    // get selector from data-filter attribute
    selector = $this.data('filter');
    
    $container.isotope({
      filter: selector
    });
    
    
  });
  
});