
      
<!-- Page Content-->
<main class="page-content">
	<section class="bg-image-06">
    	<div class="breadcrumb-wrapper">
      		<div class="shell context-dark section-30 section-lg-top-100">
		       
		        <h1 class="offset-top-20 text-ubold">Reglamento de funcionamiento</h1>
		       
      		</div>
    	</div>
  	</section>

	<section class="section-80 section-md-120">
	  	<div class="shell shell-wide text-lg-left">
		    <div class="range">
		        <div class="inset-lg-left-80">




				<div class="col-sm-12 section-md-100" style="line-height: 1.7;">


      <h3 id="titulo">Reglamento de funcionamiento del servicio de atención al cliente de Salvador Tarazona, correduría de seguros S.L.</h3>
      <ul id="inicio">
        <li>CAPÍTULO I: <a href="#capitulo1">Marco Legal</a></li>
        <ul>
          <li><a href="#marcolegal">Artículo 1: Marco Legal</a></li>
        </ul>
        <li>CÁPITULO II: <a href="#capitulo2">Servicios de Atención al Cliente</a></li>
        <ul>
          <li><a href="#regulacion">Artículo 2: Regulación, designación, principios e independencia del cargo.</a></li>
          <li><a href="#duracion">Artículo 3: Duración y causas de su terminación.</a></li>
          <li><a href="#funciones">Artículo 4: Funciones y materias excluidas.</a></li>
          <li><a href="#asuntos">Artículo 5: Asuntos de los que puede conocer el Servicio de Atención al Cliente.</a></li>
          <li><a href="#materia">Artículo 6: Materias excluidas de las competencias del servicio de atención al cliente.</a></li>
        </ul>
        <li>CAPÍTULO III: <a href="#capitulo3">Obligaciones de la Correduría</a></li>
        <ul>
          <li><a href="#obligaciones">Artículo 7: Obligaciones de la correduría.</a></li>
        </ul>
        <li>CAPÍTULO IV: <a href="#capitulo4">Reclamaciones y Tramitación.</a></li>
        <ul>
          <li><a href="#objeto">Artículo 8: Objeto, forma y plazo para la presentación de las reclamaciones.</a></li>
          <li><a href="#tramitacion">Artículo 9: Tramitación.</a></li>
          <li><a href="#efectos">Artículo 10: Efectos de la resolución del Servicio de Atención al Cliente.</a></li>
          <li><a href="#presentacion">Artículo 11: Presentación de reclamaciones o cuestiones por la Correduría.</a></li>
          <li><a href="#relacion">Artículo 12: Relación con Comisionados.</a></li>
          <li><a href="#presentacion2">Artículo 13: Presentación de Informes.</a></li>
        </ul>
       
      </ul><br /><br />
      <h4 id="capitulo1">CAPÍTULO I: Marco Legal (<a href="#inicio"> Subir</a> )</h4>
      <h5 id="marcolegal">Artículo 1: Marco Legal</h5>
      <p>La Ley 44/2002, de 22 de noviembre, de medidas de reforma del sistema financiero, adopta en su capítulo V una serie de medidas protectoras de los clientes de servicios financieros. Una de las medidas que se establecen es la obligación para las entidades financieras, entre las que se encuentran las sociedades de correduría de seguros, de atender y resolver las quejas y reclamaciones que sus clientes puedan presentar, relacionadas con sus intereses legalmente reconocidos. A estos efectos las entidades aseguradoras deberán contar con un Departamento o Servicio de Atención al Cliente. Además, las entidades contarán con la posibilidad de designar un defensor del cliente, a quien corresponderá atender y resolver los tipos de reclamaciones que determine su reglamento de funcionamiento correspondiente, el cual habrá de ser una entidad o experto independiente. Asimismo, se regula en el capítulo V de la Ley 44/2002, de 22 de noviembre, la existencia de unos órganos de nueva creación, los Comisionados para la Defensa de los Clientes de Servicios Financieros, en cuyo reglamento aprobado por el Real Decreto 303/2004, de 20 de febrero, se establece que tendrán por objeto la protección de los derechos del usuario de servicios financieros, siendo competentes para resolver las quejas y reclamaciones que los usuarios hayan formulado previamente ante los servicios creados a tal efecto por las entidades afectadas por la queja o reclamación y que no se hayan resuelto, se haya denegado su admisión, o se hubieren desestimado.</p>
      <p>La Orden ECO 734/2004, de 11 de marzo, sobre los departamentos y servicios de Atención al Cliente y el defensor del cliente de las entidades financieras, tiene por objeto regular los requisitos que deben cumplir los mismos. Son sus objetivos:</p>
      <ul>
        <li>Que todas las corredurías de seguros estén obligadas a atender y resolver las quejas y reclamaciones que presenten tantos sus clientes, como los terceros perjudicados en caso de seguros de responsabilidad civil.</li>
        <li>Que las corredurías de seguros dispongan de un departamento especializado de Atención al Cliente que tenga por objeto atender y resolver las quejas y reclamaciones que presenten sus clientes. Dicho departamento estará separado de los restantes departamentos comerciales u operativos de la entidad.</li>
        <li>Que al frente de dicho departamento se designe a una persona con honorabilidad comercial y profesional, y con conocimiento y experiencia adecuados para ejercer sus funciones.</li>
        <li>Que las corredurías de seguros contarán con un Reglamento para la Defensa del Cliente, aprobado por su Consejo de Administración y ratificado en Junta General de Accionistas, que regulará la actividad del departamento o servicio de Atención al Cliente; así como el procedimiento para la presentación, tramitación y resolución de las quejas y reclamaciones.</li>
        <li>Que las corredurías de seguros pondrán a disposición de sus clientes, en todas y cada una de las oficinas abiertas al público, así como en sus páginas web en el caso de que los contratos se hubieran celebrado por medios telemáticos, la siguiente información:</li>
        <ul>
          <li>La existencia de un departamento o servicio de Atención al Cliente y, en su caso, de un defensor del cliente, con indicación de su dirección postal y electrónica.</li>
          <li>La obligación por parte de la entidad de atender y resolver las quejas y reclamaciones presentadas por sus clientes, en el plazo de dos meses desde su presentación en el departamento o servicio de Atención al Cliente.</li>
          <li>Referencia al Comisionado para la Defensa del Cliente de Servicios Aseguradores, con especificación de su dirección postal y electrónica, y de la necesidad de agotar la vía del departamento o servicio de Atención al Cliente para poder formular las quejas y reclamaciones ante ellos.</li>
          <li>El Reglamento de funcionamiento del departamento o servicio de Atención al Cliente.</li>
          <li>Referencias a la normativa de transparencia y protección del cliente de servicios financieros.</li>
          <li>Que cada año, los departamentos y servicios de Atención al Cliente, presenten ante el consejo de administración un informe explicativo del desarrollo de su función durante el ejercicio precedente.</li>
        </ul>
      </ul>
      <br /><br />







      <h4 id="capitulo2">CÁPITULO II: Servicios de Atención al Cliente (<a href="#inicio"> Subir</a> )</h4>
      <h5 id="regulacion">Artículo 2: Regulación, designación, principios e independencia del cargo.</h5>
      <ul>
        <li>a) Regulación. El presente reglamento regula la actividad a desarrollar por el Servicio de Atención al Cliente, de conformidad con las características y funciones señaladas en el presente reglamento.</li>
        <li>b) Designación. Ha sido designada por la Dirección General de SALVADOR TARAZONA CORREDURÍA DE SEGUROS, S.L. (en adelante Correduría), con fecha 14 de julio de 2004 como titular del Servicio de Atención al Cliente a la entidad HEBRERO Y ASOCIADOS S.L., con domicilio en Madrid, calle Barceló, nº 1, 1º izqda. Al frente del mismo se encuentra D. José Ignacio Hebrero Álvarez, con D.N.I 70.796.499, doctor en Derecho, el cual cuenta con una carrera profesional de reconocido prestigio dentro del ámbito jurídico, con una dilatada trayectoria profesional en compañías del sector asegurador, tal y como se acredita en el Curriculum Vitae adjunto.</li>
        <li>c) Principios: El Servicio de Atención al Cliente se realizará por la firma HEBRERO Y ASOCIADOS S.L., que a todos los efectos será considerado un Departamento más de la Correduría de Seguros.Tanto la entidad que asume el Servicio de Atención al Cliente como la persona que asume su dirección cuentan con experiencia y conocimientos sólidos y adecuados para ejercer profesionalmente, con independencia y objetividad sus funciones como Servicio de Atención al Cliente, su actuación garantizará un servicio eficiente, a fin de resolver los conflictos que pudieran generarse en las relaciones entre la Correduría y los usuarios, y en general proteger los derechos de los usuarios.</li>
        <li>d) Independencia. Tanto los miembros como el titular del Servicio de Atención al Cliente son expertos independientes, que actuarán con independencia respecto de la Correduría y total autonomía en cuanto a los criterios y directrices a aplicar en el ejercicio de sus funciones. Es una persona ajena a la organización de la Correduría para la que presta sus servicios. Las decisiones del Servicio de Atención al Cliente que sean favorables al usuario vincularán a la Correduría.</li>
      </ul>
      <h5 id="duracion">Artículo 3: Duración y causas de su terminación.</h5>
      <ul>
        <li>3.1. Duración del cargo. El nombramiento del Servicio de Atención al Cliente tendrá una duración de DOS años y podrá ser renovado por iguales periodos de tiempo, cuantas veces lo considere oportuno la Correduría.</li>
        <li>3.2. Terminación del cargo. El titular del Servicio de Atención al Cliente cesará en su cargo por cualquiera de las causas siguientes:</li>
        <ul>
          <li>a) Expiración del plazo para el que fue nombrado, salvo que la Correduría acordara su renovación.</li>
          <li>b) Incapacidad sobrevenida.</li>
          <li>c) Renuncia.</li>
          <li>d) Cese por actuación notoriamente negligente en el desempeño de su cargo. Una vez que el titular del Servicio de Atención al Cliente haya cesado en sus funciones y el cargo esté vacante y sin perjuicio del cumplimiento de las resoluciones adoptadas por el Servicio de Atención al Cliente cesado, la Correduría podrá proceder al nombramiento de un nuevo titular del Servicio de Atención al Cliente.</li>
        </ul>
      </ul>
      <h5 id="funciones">Artículo 4: Funciones y materias excluidas.</h5>
      <p>En el cumplimiento de sus funciones corresponde al Servicio de Atención al Cliente:</p>
      <ul>
        <li>a) Actuar como portavoz de los usuarios de la Correduría para contribuir a la solución de sus conflictos.</li>
        <li>b) El Servicio de Atención al Cliente estará obligado a atender y resolver las quejas y reclamaciones que los clientes de la Correduría le presenten, directamente o mediante representación, por todas las personas físicas o jurídicas, españolas o extranjeras, que reúnan la condición de usuario de los servicios prestados por las sociedades de corredurías de seguros, siempre que tales quejas estén relacionadas con sus intereses y derechos legalmente reconocidos, ya se deriven de los contratos, de la normativa de transparencia y protección de la clientela o de las buenas prácticas y usos financieros y, en particular, del derecho de equidad.</li>
        <li>c) En los contratos de seguro tendrán también la consideración de usuarios, los terceros perjudicados.</li>
        <li>d) Atender y resolver las quejas o reclamaciones presentadas por los usuarios, en el plazo de dos meses a contar desde la presentación ante Servicio de Atención al Cliente la queja o reclamación, para dictar un pronunciamiento.</li>
        <li>e) Podrá intervenir como mediador entre usuarios y la Correduría, con objeto de llegar a una solución amistosa.</li>
        <li>f) Generar recomendaciones hacia la Correduría para mejorar el servicio al cliente.</li>
        <li>g) El Servicio de Atención al Cliente presentará un informe anual dentro del primer trimestre de cada año, que consistirá en un informe explicativo del desarrollo de su función durante el ejercicio precedente, el cual deberá incluir como contenido mínimo:</li>
        <ul>
          <li>Resumen estadístico de las quejas y reclamaciones atendidas, con información sobre: su número, admisión a trámite y razones de inadmisión, motivos y cuestiones planteadas en las quejas y reclamaciones, y cuantías e importes afectados.</li>
          <li>Resumen de las decisiones dictadas, con indicación del carácter favorable o desfavorable para el reclamante.</li>
          <li>Criterios generales contenidos en las decisiones.</li>
          <li>Recomendaciones o sugerencias derivadas de su experiencia, con vistas a una mejor consecución de los fines del departamento o servicio de Atención al Cliente.</li>
        </ul>
        <li>h) Elaborar una Memoria que remita al Presidente y al Consejo de Administración de la Correduría, así como a los organismos interesados en sus actuaciones. Se integrará dentro de la memoria anual al menos un informe anual.</li>
      </ul>
      <h5 id="asuntos">Artículo 5: Asuntos de los que puede conocer el Servicio de Atención al Cliente.</h5>
      <ol>
        <li>Incidencias o errores que sean achacables a la correduría de seguros en la tarificación de un contrato de seguro.</li>
        <li>Incidencias o errores achacables a la correduría de seguros en la emisión de un contrato de seguro.</li>
        <li>Incidencias o errores achacables a la correduría de seguros en la renovación de contratos de seguros.</li>
        <li>Incidencias o errores achacables a la correduría de seguros en el cobro de la prima de un contrato de seguro o de sus suplementos.</li>
        <li>Incidencias o errores achacables a la correduría de seguros derivados de la anulación de un contrato de seguro.</li>
        <li>Reclamaciones por el retraso en el pago de un siniestro si la correduría de seguros no ha cumplido con celeridad los trámites que le marca la entidad aseguradora.</li>
      </ol>
      <h5 id="materias">Artículo 6: Materias excluidas de las competencias del servicio de atención al cliente.</h5>
      <p>En todo caso, quedan excluidos de la competencia del Servicio de Atención al Cliente:</p>
      <ul>
        <li>a) Las relativas a la gestión societaria de la correduría de seguros</li>
        <li>b) Las relaciones entre la Correduría y sus empleados.</li>
        <li>c) Las relaciones entre la Correduría y sus socios.</li>
        <li>d) Aquellas cuestiones que correspondan a la decisión sobre la celebración de un contrato y a la vinculación o admisión como cliente.</li>
        <li>e) Las que se refieren a temas que se encuentren en tramitación o hayan sido ya resueltos en vía judicial o arbitral, o por, la Dirección General de Seguros y Fondos de Pensiones del Ministerio de Economía, o que tengan por objeto impedir, dilatar o entorpecer el ejercicio de cualquier derecho de la Correduría frente a sus clientes.</li>
        <li>f) Las quejas que tengan por objeto los mismos hechos y afecten a las mismas partes y que hayan sido objeto de decisión previa por parte del Servicio de Atención al Cliente.</li>
        <li>g) Las quejas o reclamaciones que se refieran a hechos de los que el cliente tuviera conocimiento de su existencia, con dos años o más de antelación a la fecha de presentación de la queja.</li>
      </ul>
      <br />
      <br /><br />






      <h4 id="capitulo3">CAPÍTULO III: Obligaciones de la Correduría (<a href="#inicio"> Subir</a> )</h4>
      <h5 id="obligaciones">Artículo 7: Obligaciones de la correduría.</h5>
      <p>La Correduría deberá adoptar las medidas necesarias para colaborar con el Servicio de Atención al Cliente en todo aquello que favorezca al mejor ejercicio de su cargo. En particular corresponde a la Correduría:</p>
      <ul>
        <li>a) Que el Servicio de Atención al Cliente tome las decisiones referentes al ámbito de su actividad de manera autónoma e independiente de manera que se eviten los conflictos de intereses que puedan surgir.</li>
        <li>b) Que los procedimientos previstos para la transmisión de la información requerida por el Servicio de Atención al Cliente al resto de los servicios de la Correduría, respondan a los principios de rapidez, seguridad, eficacia y coordinación.</li>
        <li>c) Poner a disposición de sus clientes, en todas y cada una de las oficinas abiertas al público, así como en sus páginas web, en el caso de que los contratos se hubieran celebrado por medios telemáticos, pero cumpliendo con las exigencias de la Dirección General de Seguros y fondos de Pensiones, de la existencia y funciones del Servicio de Atención al Cliente, con expresa indicación de la dirección postal y electrónica, así como del contenido de presente reglamento y de los derechos que les asisten para presentar sus reclamaciones y del procedimiento para su formulación.</li>
        <li>d) Recibir, estudiar y valorar las quejas que puedan formularse respecto a la actuación del Servicio de Atención al Cliente en orden a adoptar o no la decisión a que se refiere la letra d) del apartado 3.2. del Artículo 3 del presente reglamento.</li>
        <li>e) Informar a los usuarios de la facultad de acudir al Comisionado para la Defensa del Cliente de la Dirección General de Seguros y Fondo de Pensiones, indicando la dirección postal y de correo electrónico de este, para en caso de disconformidad con el resultado de las decisiones que finalicen los procedimientos de tramitación de quejas y reclamaciones, con apercibimiento del requisito imprescindible de haber formulado las quejas y reclamaciones previamente ante el Servicio de Atención al Cliente, para su posterior admisión y tramitación ante el Comisionado o Comisionados.</li>
        <li>f) Informar al Servicio de Atención al Cliente de las decisiones que hayan sido aceptadas expresamente por las partes, con indicación de la cuantía, modo, tiempo y lugar de cumplimiento de las obligaciones pactadas, dentro de los cinco días hábiles siguientes a la fecha de notificación de la decisión del Servicio de Atención al Cliente.</li>
        <li>g) La designación del titular del Servicio de Atención al Cliente, será comunicada al Comisionado o Comisionados para la Defensa del Cliente de Servicios Financieros y a la autoridad o autoridades supervisoras que correspondan por razón de su actividad.</li>
      </ul>
      <br /><br />





      <h4 id="capitulo4">CAPÍTULO IV: Reclamaciones y Tramitación. (<a href="#inicio"> Subir</a> )</h4>
      <h5 id="objeto">Artículo 8: Objeto, forma y plazo para la presentación de las reclamaciones.</h5>
      <ul>
        <li>8.1 Objeto. Los clientes podrán dirigirse al Servicio de Atención al Cliente cuando estimen que en sus relaciones con la Correduría han sufrido un tratamiento incorrecto, negligente, no ajustado a Derecho o a las buenas prácticas y usos, salvo en los casos excluidos por el Artículo 6 de este reglamento.</li>
        <li>8.2 Forma. La presentación de las quejas y reclamaciones podrá efectuarse, personalmente o mediante representación. En soporte papel o por medios informáticos, telemáticos o electrónicos, siempre que estos permitan la lectura, impresión y conservación de los documentos y que la utilización de los medios informáticos, telemáticos y electrónicos se ajuste a lo previsto en la Ley 59/2003 de 19 de diciembre de firma electrónica.</li>
        <li>8.3 Iniciación y contenido:</li>
        <ol>
          <li>Las reclamaciones o quejas se podrán presentar en un plazo de dos años desde que el reclamante tuviera conocimiento de los hechos causantes de la queja o reclamación.</li>
          <li>El procedimiento se iniciará mediante la presentación de un documento en el que se hará constar:</li>
          <ul>
            <li>a) Nombre, apellidos y domicilio del interesado y, en su caso, de la persona que lo represente debidamente acreditada. Número del documento nacional de identidad para las personas físicas y datos referidos a registro público para las personas jurídicas.</li>
            <li>b) Motivo de la queja o reclamación, con especificación clara de las cuestiones sobre las que se solicita un pronunciamiento.</li>
            <li>c) Oficina u oficinas, departamento o servicio dónde se hubieran producido los hechos objeto de la queja o reclamación.</li>
            <li>d) Que el reclamante no tiene conocimiento de que el asunto objeto de la queja o reclamación está siendo sustanciada a través de un procedimiento administrativo, arbitral o judicial.</li>
            <li>e) Las pruebas documentadas que obren en su poder en que se fundamente su queja o reclamación.</li>
            <li>f) Lugar, fecha y firma 3º. La queja o reclamación se presentará una sola vez por el interesado, sin que pueda exigirse su reiteración ante distintos órganos de la Correduría</li>
          </ul>
        </ol>
        <li>8.4 Lugar: Las quejas y reclamaciones dirigidas al Servicio de Atención al Cliente podrán ser presentadas:</li>
        <ul>
          <li>a) Ante el Servicio de Atención al Cliente, en la calle Barceló, Nº1, Planta 1º, Puerta Izda. C.P. 28004 de Madrid, Teléfono 91.532.00.09 Fax 91.522.78.94</li>
          <li>b) Ante cualquier oficina abierta al público de la Correduría.</li>
          <li>c) Ante, atencionalcliente@hebreroyasociados.com, dirección de correo electrónico habilitada para este fin.</li>
        </ul>
        <li>8.5. Cómputo del Plazo: El cómputo del plazo máximo para dictar un pronunciamiento por parte del Servicio de Atención al Cliente será de dos meses y comenzará a contar desde la presentación de la queja o reclamación ante el mismo. En todo caso se deberá acusar recibo por escrito y dejar constancia de la fecha de presentación a efectos del cómputo de dicho plazo.</li>
      </ul>
      <p>La presentación de reclamaciones por parte de los clientes ante el Servicio de Atención al Cliente deberá realizarse dentro del plazo de dos años a contar desde que el cliente tuviera conocimiento de su existencia Las reclamaciones presentadas fuera del plazo anterior serán rechazadas de pleno.</p>
      <h5 id="tramitacion">Artículo 9: Tramitación.</h5>
      <ul>
        <li>9.1 Admisión a Trámite y Tramitación:</li>
        <ul>
          <li>a) Una vez recibida la queja o reclamación por el Servicio de Atención al Cliente se procederá a la apertura del expediente. Si no se encontrase suficientemente acreditada la identidad del reclamante, o no se pudieran establecer con claridad los hechos objeto de la queja o reclamación, se requerirá al firmante para completar la documentación remitida en el plazo de diez días naturales, con el apercibimiento de que si así no lo hiciese se archivará la queja o reclamación sin más trámite.</li>
          <li>b) Dicho plazo empleado para subsanar los errores a que se refiere el párrafo anterior no se incluirá en cómputo de dos meses al que se hace referencia en el apartado d), del artículo 4 del presente reglamento.</li>
          <li>c) El Servicio de Atención al Cliente podrá recabar en el curso de la tramitación los expedientes, tanto del reclamante como de los distintos departamentos y servicios de la Correduría, cuantos datos, aclaraciones, informes o elementos de prueba considere pertinentes para adoptar su decisión.</li>
          <li>d) A la vista de la queja o reclamación, la Correduría podrá rectificar su situación con el reclamante a satisfacción de este, entonces deberá comunicarlo a la instancia competente y justificarlo documentalmente, salvo que existiere desistimiento expreso del interesado. En tales casos se procederá al archivo de la queja o reclamación sin más trámite.</li>
          <li>e) Los reclamantes podrán desistir de sus quejas y reclamaciones en cualquier momento, el desistimiento da lugar a la finalización inmediata del procedimiento.</li>
          <li>f) La tramitación del expediente deberá finalizar en el plazo máximo de dos meses, desde la fecha de presentación de la queja o reclamación ante el Servicio de Atención al Cliente.</li>
        </ul>
        <li>9.2 No admisión a trámite:</li>
        <ul>
          <li>a) Sólo podrá rechazarse la admisión a trámite de las quejas y reclamaciones en los casos siguientes:</li>
          <ol>
            <li>Cuando se omitan datos esenciales para la tramitación no subsanables, incluidos los supuestos en que no se concrete el motivo de la queja o reclamación.</li>
            <li>Cuando se pretenda tramitar como queja o reclamación, recursos o acciones cuyo conocimiento sea competencia de los órganos administrativos, arbitrales o judiciales, o la misma se encuentre pendiente de resolución o litigio o el asunto haya sido resuelto en aquellas instancias.</li>
            <li>Cuando los hechos, razones y solicitud objeto de la reclamación no se refieran a los intereses y derechos legalmente reconocidos de los usuarios de los servicios financieros.</li>
            <li>Cuando se formulen quejas o reclamaciones que reiteren otras anteriores resueltas, presentadas por el mismo cliente con relación a los mismos hechos.</li>
            <li>5º. Cuando hubiera transcurrido el plazo para la presentación de quejas y reclamaciones que establezca el artículo 8.3.1º y 8.5 párrafo Segundo del presente reglamento.</li>
          </ol>
          <li>b) Cuando se esté tramitando simultáneamente sobre el mismo asunto una queja o reclamación y un procedimiento administrativo, arbitral o judicial, deberá abstenerse de tramitar la primera.</li>
          <li>c) Se pondrá de manifiesto al reclamante, mediante decisión motivada la no admisión a trámite, por alguna de las causas indicadas, de su queja o reclamación y se le dará un plazo de diez días naturales para que presente sus alegaciones. Cuando el reclamante presente sus alegaciones y se mantengan las causas de inadmisión, se le comunicará la decisión final adoptada.</li>
        </ul>
        <li>9.3. Expedientes en Tramitación:</li>
        <ul>
          <li>Los expedientes de quejas y reclamaciones que se encuentren en tramitación por la Correduría a la entrada en vigor de la Orden ECO 734/2004,del 11 de marzo, y se remitan al Servicio de Atención al Cliente para que se haga cargo de su continuación, se sustanciarán por conforme a lo establecido en el presente reglamento.</li>
        </ul>
      </ul>
      <h5 id="efectos">Artículo 10: Efectos de la resolución del Servicio de Atención al Cliente.</h5>
      <ul>
        <li>10.1 Efectos para el cliente. El cliente no está obligado a aceptar la resolución del Servicio de Atención al Cliente. Caso de no aceptarla podrá ejercitar las actuaciones administrativas y/o las acciones judiciales que estime oportunas. Si la aceptase deberá comunicarlo por escrito al Servicio de Atención al Cliente en el plazo de treinta días, pasado el cual sin respuesta se entenderá que la rechaza.<br />
          La aceptación se hará en los propios términos de la resolución e irá acompañada de la renuncia expresa a cualquier otra acción reclamatoria, ya sea judicial, administrativa, o de cualquier otra índole.</li>
        <li>10.2 Efectos para la Correduría. Está obligada a aceptar la resolución del Servicio de Atención al Cliente, siempre que lo haga el cliente, en la forma y con los requisitos establecidos en el presente reglamento. A estos efectos, el Servicio de Atención al Cliente comunicará a la Correduría, la aceptación del cliente. Cuando la resolución obligara a pagar una cantidad al cliente, o realizar cualquier acto en su favor, la Correduría la ejecutará en el plazo máximo de un mes a partir del día en que se le notifique la aceptación del cliente, salvo que en la propia resolución, ya aceptada, se estableciera un plazo distinto.<br />
          La decisión será siempre motivada y contendrá unas conclusiones claras sobre la solicitud planteada en cada queja o reclamación, y será notificada en el plazo de diez días naturales a contar desde su fecha, según la forma que haya designado expresamente el reclamante o en defecto de aquella a través del mismo medio en que hubiera sido presentada la queja o reclamación, ya sea por escrito o por medios informáticos, electrónicos o telemáticos, siempre que estos permitan su lectura, impresión y conservación, y cumplan los requisitos exigidos en la Ley 59/2003, de 19 de diciembre, de firma electrónica.</li>
      </ul>
      <h5 id="presentacion">Artículo 11: Presentación de reclamaciones o cuestiones por la Correduría.</h5>
      <p>La Correduría podrá someter a la consideración y resolución del Servicio de Atención al Cliente cualquier reclamación que le hayan dirigido sus clientes, así como cualquier cuestión que pudiera ser objeto de reclamación por éstos, aunque no haya sido interpuesta todavía.</p>
      <h5 id="relacion">Artículo 12: Relación con Comisionados.</h5>
      <p>La Correduría a través del responsable del Servicio de Atención al Cliente atenderá los requerimientos que los Comisionados para la Defensa del Cliente de Servicios Financieros puedan efectuarles en el ejercicio de sus funciones, en los plazos que éstos determinen de conformidad con lo establecido en su Reglamento.</p>
      <h5 id="presentacion2">Artículo 13: Presentación de Informes</h5>
      <p>Dentro del primer trimestre de cada año, el Servicio de Atención al Cliente presentará ante el Consejo de Administración de la correduría un Informe explicativo del desarrollo de su función durante el ejercicio precedente, que contendrá:</p>
      <ol>
        <li>Un resumen estadístico de las quejas y reclamaciones atendidas, con información sobre su número, admisión a trámite y razones de inadmisión, motivos y cuestiones planteadas en las quejas y reclamaciones y cuantías e importes afectados.</li>
        <li>Un resumen de las decisiones adoptadas, con indicación del carácter favorable o desfavorable para el reclamante.</li>
        <li>Los criterios generales contenidos en las decisiones.</li>
        <li>Las recomendaciones o sugerencias derivadas de su experiencia, con vistas a una mejor consecución de las fines que informan su actuación.</li>
        <li>Un resumen de dicho Informe se integrará en la memoria anual de la correduría.</li>
      </ol>






				</div>

				</div>
			</div>







    	</div>
	</section>
</main>
