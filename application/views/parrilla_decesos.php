      <!-- Page Content-->
      <main class="page-content">
              <section class="bg-image-06">
                <div class="breadcrumb-wrapper">
                  <div class="shell context-dark section-30 section-lg-top-120">
                    <h5>Seguros</h5>
                    <h1 class="offset-top-20 text-ubold">Seguros de decesos</h1>
                    <ol class="breadcrumb">
                      <li><?php echo anchor('inicio','Inicio'); ?></li>
                      <li>Seguros</li>
                      <li>Seguro de decesos
                      </li>
                    </ol>
                  </div>
                </div>
              </section>

        <section class="section-80 section-md-120">
          <div class="shell shell-wide text-lg-left">
            <div class="range">


  <div class="container">
  <input id="product" type="hidden" value="Parrilla de precios de Seguro de decesos" />
  <input id="ref" type="hidden" value="<?php echo $codigoTarificacion; ?>" />
  <input id="cia" type="hidden" value="" />


<div class="table-responsive">
  <table class="table table-striped table-hover table-bordered">
  <caption>Precios anuales por persona (impuestos incluídos)</caption>
  <thead>
    <tr style="background:linear-gradient(to bottom, #1e5799 0%,#dadada 0%,#bdbdbd 100%);">
      <th class='precio_parrilla'>Compañía</th>
      <th class='precio_parrilla'>Prima anual</th>
      <th class='precio_parrilla'>Capital asegurado</th>
      <th class='precio_parrilla'>Contratación</th>
    </tr>
    </thead>
<?php

    foreach ($primas as $k => $v) {

        if ($v["nombreProducto"] == NULL) $prima_celda = "-<br>";
        else $prima_celda = $v["prima_12m"] . "&euro;<br>";
      

      
       

      echo "<tr>";
      echo "<td>";
      echo img(array('src'=>'public/images/aseguradoras/' . $v["logoProducto"], 'alt'=> $v['nombreCia'] . '->' . $v['nombreProducto']));
      echo "</td>";
      echo "<td class='precio_parrilla'><span>";
      echo $prima_celda . "</span>";
      echo "</td>";
      echo "<td class='precio_parrilla'><span>";
      echo $v["capital"] . "&euro;</span>";
      echo "</td>";     
      echo "<td class='precio_parrilla'>";
      $ruta = 'seguros/contratar/decesos/' . $codtar . '/' . $v["nombreCia"];
      $boton_contratar = '<button type="button" class="btn btn-warning contratar_pop">Contratar</button>';
      echo anchor($ruta,$boton_contratar);
      echo "</td>";
      echo "</tr>";     
    }
?>
  </table>
</div>

<div class="table-responsive">
  <table class="table table-striped table-hover table-bordered">
  <caption>Datos de la tarificación</caption>
  <tr>
    <td><b>Referencia</b></td>
    <td><?php echo $codigoTarificacion; ?></td>
  </tr> 
  <tr>
    <td><b>Nombre y apellidos</b></td>
    <td><?php echo $nombreApellidos; ?></td>
  </tr>

  <tr>
    <td><b>Email</b></td>
    <td><?php echo $email; ?></td>
  </tr>

  <tr>
    <td><b>Teléfono</b></td>
    <td><?php echo $telefono; ?></td>
  </tr>
  </table>
</div>
  </div>              




            </div>
          </div>
        </section>
      </main>