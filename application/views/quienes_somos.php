
      
<!-- Page Content-->
<main class="page-content">
	<section class="bg-image-06">
    	<div class="breadcrumb-wrapper">
      		<div class="shell context-dark section-30 section-lg-top-100">
		        <!--<h5>Sobre nosotros</h5>-->
		        <h1 class="offset-top-20 text-ubold">Quienes somos</h1>
		        <!--
		        <ol class="breadcrumb">
		         	<li><?php echo anchor('inicio','Inicio'); ?></li>
		          	<li>Sobre nosotros</li>
		          	<li>Quienes somos</li>
		        </ol>-->
      		</div>
    	</div>
  	</section>

	<section class="section-80 section-md-120">
	  	<div class="shell shell-wide text-lg-left">
		    <div class="range">
		        <div class="inset-lg-left-80">
		          	<h5 class="text-info-dr">DANDO SOLUCIONES, CONFIANZA Y TRANQUILIDAD DESDE 1985</h5>
		          	<hr class="divider divider-lg-left divider-primary divider-80">
		          	<div class="col-sm-12">
						<?php echo img(array('src'=>'public/images/quienes_somos.png','alt'=> '','class' => 'img-responsive center-block')); ?>
					</div>
					
			         <div class="col-sm-12 section-md-100">

					<p style="margin-bottom: 25px;">La Empresa <strong>Salvador Tarazona Correduría de Seguros S.L.</strong> fue fundada como sociedad limitada en el año 1997, aunque su inicio real se remonta al día 2 de Mayo de 1985 con el nombre de la persona física que corresponde al actual Administrador Único D. Salvador Francisco Tarazona Baixauli. En el año 1986 obtuvo la Diplomatura como Corredor de Seguros y en 1997 la forma jurídica de la empresa paso de ser persona física a Sociedad Limitada obteniendo la Autorización Nacional por parte de la Dirección General de Seguros y Fondos de Pensión con el número J. 1672. En 2006 la empresa ha sido adaptada a la nueva Ley de Mediación de 2006 por la Dirección General de Seguros y Fondos de Pensión en Madrid, en tiempo y forma.</p>


			        <h5 style="margin-bottom: 25px;"><strong>TITULACIONES PROFESIONALES:</strong></h5>
			        
			        <ul class="lista_datos"  style="margin-bottom: 25px;">
			        	<li>Diplomado en Mediación de Seguros. Obtenida en la Facultad de Derecho de Barcelona.1984. Autorización de la Dirección General de Seguros y Fondos de Pensión. Ministerio de Economía 1985.</li>

			        	<li>Diploma de Especialista / Broker en Bolsa por la Universitaria de Estadísticas / Complutense de Madrid. Año 2000.</li>

			        	<li>Doctorado en Seguros y Teoría Aseguradora. Cosmopolitan University de Jefferson City ( Missouri ) EEUU. Primera Titulación de esta categoría que se da a un español. Año 2002</li>

			        	<li>Curso de Gerencia de Riesgos para empresas. Colegio de Corredores de Valencia. Año 2003</li>

			        	<li>Título de Perito Judicial en Seguros, Selfos Madrid. Año 2004.</li>

			        	<li>Curso de Responsabilidad civil General. Colegio Corredores de Seguros de Valencia. Año 2005</li>

			        	<li>Curso de Responsabilidad Civil Medioambiental. Año 2008</li>

			        	<li>Desde 2008 es miembro, por segunda vez, de la Junta de Gobierno del COLEGIO DE MEDIADORES DE VALENCIA.</li>

			        	<li>Nuestra empresa tiene dedicación exclusiva en el sector de la mediación de seguro y reaseguro, siendo hoy en día una de las primeras Corredurías de Seguros autóctonas, en cuanto a facturación, número de clientes y en la rápida resolución de los siniestros, habiendo puestas nuestras técnicas y sistemas de gestión en la mayoría de Compañías Aseguradoras y Corredurías de Seguros de España y Europa. </li>
			        </ul>
			        
			        <p>Contamos con unos recursos humanos, materiales, patrimoniales y financieros que nos permite estar a la vanguardia de las técnicas aseguradoras e innovaciones del sector, contando con un sistema informático propio el cual nos permite estar conectados en tiempo real entre todas las oficinas y las diversas entidades aseguradoras con las que trabajamos, dándonos una respuesta plena a las actuales y futuras necesidades en el mundo de la información. De igual manera los cliente a través de nuestra pagina web pueden pedir y recibir en tiempo real respuesta de los presupuestos o preguntas que puedan formularnos.</p>

			        <p>La totalidad de las oficinas que disponemos en la actualidad son en régimen de Propiedad.</p>
			        
			        <p>Mediamos en casi todos los ramos de seguros existentes en la actualidad, al contar con un importante apoyo del sector del reaseguros debido a nuestra contrastada reputación y fiabilidad en el sector. También trabajamos con los mayores brokers de reaseguros del mundo.</p>

			        <p>El día 1 de Enero de 2001 esta sociedad entró a formar parte de la <strong>Agrupación de Corredores COJEBRO</strong>, asociación  que aglutina a las más importante corredurías de España repartido  por provincias. </p>

			        <p>En el año 2001, 26 de abril, se entrego a esta correduría el premio a la Mejor Correduría de Seguros de la Comunidad Valenciana, Murcia y Baleares. El premio fue entregado por Director General de Zurich Financial Service España y C.E.O. para Europa del Sur D. José Cela Martínez, estando presente en el acto numerosas Personalidades del sector asegurador como por Profesionales venidos de toda España, para acompañarnos en este importante acto.</p>

			        <p><strong>Nueva Web  en 2018.  Hemos creado una nueva web, de última generación, compatible para todos los ordenadores, tablets y teléfonos móviles.</strong></p>
			        <br>
			        
			        

			        <div class="row">
						<div class="col-sm-6">
					        <h5 style="margin-bottom: 25px;"><u>Especialidades de la Correduría y nichos de negocio.</u></h5>		       				      
					        <ul class="lista_datos">
					        	<li>Responsabilidad Civil General y profesional.</li>
					        	<li>Responsabilidad civil para Fuegos Artificiales y Pirotecnias.</li>
					        	<li>Fallas, Correfocs, Trabucaires, Cordàes, Toros de fuego y manifestaciones festivas similares, utilizando fuego o no.</li> 
					        	<li>Responsabilidad Civil Empresas de Seguridad y Vigilancia.</li> 
					        	<li>Feriantes,  Responsabilidad Civil, Daños a las  atracciones y cobertura a todos sus vehículos.</li> 
					        	<li>Responsabilidad Civil Eventos, Conciertos, Congresos.</li> 
					        	<li>Seguro de Suspensión de Espectáculos.</li> 
					        	<li>Seguro de Responsabilidad Civil "Bous al Carrer".</li> 
					        	<li>Seguro de Responsabilidad Empresas de Efectos Especiales para rodajes cinematográficos, TV, teatro.</li> 
					        	<li>Seguro de Responsabilidad Civil para Topógrafos.</li> 
					        	<li>Póliza para Divorciados.</li> 
					        	<li>Seguro de Secuestro.</li> 
				
					        </ul>
						</div>

						<div class="col-sm-6">
							<?php echo img(array('src'=>'public/images/fallas-de-valencia-correduria-de-seguros.png','alt'=> 'Seguro de fallas','width' => '380','class' => 'img-responsive center-block')); ?>
						</div>
			        </div>
			        

			        <br><br>

			        
			        <p><a href="index.php" title="Correduria de seguros">Starazona</a> esta compuesto por:</p>
				    <ul class="lista_datos">
			            <li><strong>Departamento de Gerencia</strong>: Compuesto por Salvador Tarazona Baixauli, el cual marca las directivas, objetivos a seguir por la empresa independientemente de ser el encargado de las negociaciones en las compañías de seguros y reaseguros como de la creación de nuevos productos.</li>

			            <li><strong>Departamento Comercial</strong>: Compuesto por seis personas de las que dos se encargan de atender a las personas que acuden a las oficinas para pedir información y contratar sus riesgos y dos más son las que se dedican de realizar las visitas a domicilio de las Empresas y Clientes que nos reclaman tipo de seguros más especializados y a los cuales hay que dedicarles más tiempo y en un clima más personal.</li>

			            <li><strong>Departamento de Siniestros</strong>: Lo componen tres personas las cuales se encargan de que la gestión de siniestros sea lo más rápida posible como de igual forma de la negociación con las Compañías de Seguros para que las indemnizaciones sean de acorde a lo reclamado por los clientes.</li>

			            <li><strong>Departamento Administrativo y contabilidad</strong>: Compuesto por un responsable de área y del personal administrativo, que se encarga del movimiento de recibos, póliza de la correduría, asuntos fiscales e inversión de la propia empresa.</li>

			            <li><strong>Departamento de informática</strong>: Compuesto por un responsable de área que se encarga de la gestión de los equipos informáticos y del sistema informático.</li>

			            <li><strong>Multitarificador</strong>: El último proyecto ha sido la creación de una web de venta de seguros a través de internet, propiedad totalmente de esta sociedad.</li>
			        </ul>
			                
			        <br><br><h5>Pertenecemos como miembros de Honor:</h5>
			        <ul>
	                	<li>Asociación Valenciana de Empresas de Seguridad.</li>
	                	<li>A.F.A.P.E. ( Asociación Nacional de Pirotecnia ).</li>
	                	<li>FVAAC ( Federación Valencia de Amics del Coet ).</li>
	                </ul>
	                <br><br>
					<p>También en diversos Congresos realizados en numerosos países del mundo se nos ha hecho entrega de diversos premios y diplomas en reconocimiento de nuestra labor realizada en el sector de la mediación, en apoyo de los derechos de los asegurados y el estudio y análisis de nuevos productos aseguradores.</p>

			        <p>Constantemente estamos estudiando nuevas maneras de aseguramiento para los clientes , creando nichos de negocio para poder realizar pólizas a la medida de sus necesidades, no cejando en ningún momento en  innovar y crear un ambiente cómodo de aceptación de riesgos  para los actuales y futuros clientes.</p>
				</div>
			</div>
			</div>
    	</div>
	</section>
</main>
