

      <!-- Page Content-->
      <main class="page-content">
              <section class="bg-image-07">
                <div class="breadcrumb-wrapper">
                  <div class="shell context-dark section-30 section-lg-top-100">
                    <!--<h5>Contact or Visit Us</h5>-->
                    <h1 class="offset-top-20 text-ubold">Colaboradores</h1>
                   
                  </div>
                </div>
              </section>
        <section>
          <div class="shell"></div>
        </section>
        <section class="section-top-80 section-md-top-0">
          <div class="shell shell-wide text-md-left">
            <div class="range">
              <div class="cell-md-12 cell-lg-12 section-md-80 section-lg-120">
                <h2 class="text-ubold">Colabora con nosotros</h2>
                <hr class="divider divider-md-left divider-primary divider-80">
                <p class="offset-top-20 offset-md-top-40">Si deseas iniciarte en el sector de la mediación de seguros de forma definitiva o puntual o si ya siendo mediador de seguros quieres ampliar tu actual actividad profesional, mediante la comercialización y venta de seguros específicos o de nichos debes de apoyarte con los mejores profesionales del sector de la mediación.</p>
                <p>Nosotros contamos con más de 30 años de experiencia y deseamos compartirla contigo, dándote a su vez seguridad en tu labor diaria.</p>
              </div>
            </div>

            <div class="range">
              <div class="cell-md-12 cell-lg-12">
                <h3 style="text-align: center;font-weight: bold;">Trabaja con nosotros como Auxiliar o Colaborador en Seguros.</h3>

              </div>
            </div>
<div class="range">
                <div class="cell-md-1 cell-lg-1"></div>

                <div class="cell-md-4 cell-lg-4 contacto_bordered">
                  De la misma forma si eres el Director de Riesgos de tu empresa y quieres que la misma cuente con un Departamento Especializado en Seguros y a su vez que se ahorre un dinero, por medio un contrato de colaboración se puede beneficiar por un importe menor de la prima al revertir el importe de la comisión a cobrar.
                </div>

                <div class="cell-md-1 cell-lg-1"></div>

                <div class="cell-md-4 cell-lg-4 contacto_bordered">
                  Si eres titular de una Correduría y deseas aumentar tus ingresos y deseas que nosotros nos ocupemos de la gestión de tus siniestros, engorrosas tareas de administración, negociación con compañías, colocación de riesgos, nosotros somos lo que estás buscando y a su vez queremos contar contigo.
                </div>

                <div class="cell-md-1 cell-lg-1"></div>
              </div>

<div class="range">
  <div class="cell-md-12 cell-lg-12 section-md-80 contacto_bordered">

    <p>También, si deseas tener tu propio negocio con una oficina con puertas abiertas, puedes transformarte en <b>Franquiciado o Auxiliar Externo de Salvador Tarazona Correduría de Seguros.</b></p>
    <p>Nuestra forma singular y práctica de trabajar te permitirá tener más tiempo para dedicarte a la prospección de nuevos clientes, producción de pólizas y un contante reciclaje por medio de recibir formación y asistir a reuniones de entidades aseguradoras.</p>
    <p>Consecuentemente este tiempo te proporcionara mayores ingresos, todo esto encuadrado en un ambiente de profesionalidad, amistad, apoyo mutuo, honestidad, seguridad y confianza.</p>

  </div>
</div>

<div class="range">
  <div class="cell-md-12 cell-lg-12 section-md-80 contacto_bordered">

    <h3 style="text-align: center;font-weight: bold;">Te queremos ofrecer:</h3>
    <ul>
      <li>Formación Específica en Seguros y su Legislación</li>
      <li>Apoyo técnico en la búsqueda de las mejores primas y mayores coberturas</li>
      <li>Conocimiento de las nuevas tecnologías y redes sociales</li>
      <li>Ayuda para la mejor realización de tu actividad</li>
      <li>Exoneración de las cargas administrativas y gestión de siniestros</li>
      <li>Poder constituir una cartera de forma ilimitada en el tiempo</li>
      <li>Aprovecharte de unas excelentes condiciones económicas al negociar toda la cartera de la correduría en tu favor</li>
      <li>Contar con un contrato de colaboración desde el primer día</li>
    </ul>

  </div>
</div>





              
<div class="range">
              <div class="cell-md-9 cell-lg-7">
                <p>Cumplimenta tus datos en este formulario y nos pondremos en contacto contigo. Todos los campos son obligatorios.</p>
                <!-- RD Mailform-->
               <?php echo form_open_multipart('contacto', array('class' => 'offset-top-30 range text-left')); ?>
                  <div class="cell-sm-6">
                    <div class="form-group">
                      <label for="contact-name" class="form-label form-label-outside">Nombre</label>
                      <input id="contact-name" type="text" name="name" class="form-control form-control-gray">
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-20 offset-sm-top-0">
                    <div class="form-group">
                      <label for="contact-surname" class="form-label form-label-outside">Apellidos</label>
                      <input id="contact-surname" type="text" name="surname" class="form-control form-control-gray">
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-20">
                    <div class="form-group">
                      <label for="contact-email" class="form-label form-label-outside">E-mail</label>
                      <input id="contact-email" type="email" name="email" class="form-control form-control-gray">
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-20">
                    <div class="form-group">
                      <label for="contact-phone" class="form-label form-label-outside">Teléfono</label>
                      <input id="contact-phone" type="text" name="phone" class="form-control form-control-gray">
                    </div>
                  </div>

                  <div class="cell-sm-6 offset-top-20">
                    <div class="form-group">
                      <label for="contact-localidad" class="form-label form-label-outside">Localidad</label>
                      <input id="contact-localidad" type="text" name="localidad" class="form-control form-control-gray">
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-20">
                    <div class="form-group">
                      <label for="contact-provincia" class="form-label form-label-outside">Provincia</label>
                      <input id="contact-provincia" type="text" name="provincia" class="form-control form-control-gray">
                    </div>
                  </div>


                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="contact-message" class="form-label form-label-outside">Mensaje</label>
                      <textarea id="contact-message" name="message"  class="form-control form-control-gray"></textarea>
                    </div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>
                </form>
              </div>

<!--
              <div class="cell-lg-3 cell-lg-preffix-1 rd-google-map-abs offset-top-40 offset-md-top-0">

                <div data-zoom="16" data-y="39.423022" data-x="-0.385206" class="rd-google-map rd-google-map__model">
                  <ul class="map_locations">
                    <li data-x="-0.385206" data-y="39.423022">
                      <p>Plaza Horticultor Corset nº 12 - 46008 Valencia</p>
                    </li>
                  </ul>
                </div>
              </div>
-->


            </div>
          </div>
        </section>
      </main>

