
        <!-- Swiper-->
        <div class="swiper-slide-wrapper">

          <div data-simulate-touch="false" data-autoplay="5500" data-slide-effect="fade" class="swiper-container swiper-slider">
            <div class="swiper-wrapper">
              <div data-slide-bg="<?php echo base_url();?>public/images/carousel/carousel0b.jpg" class="swiper-slide"></div>
              <!--<div data-slide-bg="<?php echo base_url();?>public/images/carousel/carousel1.jpg" class="swiper-slide"></div>
              <div data-slide-bg="<?php echo base_url();?>public/images/carousel/carousel2.jpg" class="swiper-slide"></div>
              <div data-slide-bg="<?php echo base_url();?>public/images/carousel/carousel3.jpg" class="swiper-slide"></div>-->
            </div>
          </div>

          <div class="swiper-onSlider">


            <div class="shell shell-wide section-70 section-xl-top-200 section-xl-bottom-220">
              <div class="range range-xs-center">
                <div class="cell-xl-12 cell-lg-12 cell-md-12" style="opacity: 1;">

                  
<div class="container pe-home-productos">
    <div class="row pe-zona-productos">
      <div class="col-md-4">

        <a href="<?php echo site_url('seguros/coche') ?>"><div class="pe-seguro-box">
            <h2><i class="fas fa-car"></i> Seguros de Coches</h2>
           <!-- <a id="link_condicional_coche" class="btn btn-producto" href="<?php echo site_url('seguros/coche') ?>"><i class="fas fa-hand-point-right"></i> Calcular Precio</a>   -->
            <a id="link_condicional_coche" href="<?php echo site_url('seguros/coche') ?>" class="myButton">Calcular</a>         
        </div></a>


        <a href="<?php echo site_url('seguros/salud') ?>"><div class="pe-seguro-box">
            <h2><i class="fas fa-ambulance"></i> Seguros de Salud</h2>
           <!-- <a class="btn btn-producto " href="<?php echo site_url('seguros/salud') ?>"><i class="fas fa-hand-point-right"></i> Calcular Precio</a>-->
            <a href="<?php echo site_url('seguros/salud') ?>" class="myButton">Calcular</a>
        </div>        
      </div></a>

      <div class="col-md-4">

        <a href="<?php echo site_url('seguros/hogar') ?>"><div class="pe-seguro-box">
            <h2><i class="fas fa-home"></i> Seguros de Hogar</h2>
            <!--<a class="btn btn-producto " href="<?php echo site_url('seguros/hogar') ?>"><i class="fas fa-hand-point-right"></i> Calcular Precio</a>-->
            <a href="<?php echo site_url('seguros/hogar') ?>" class="myButton">Calcular</a>
        </div></a>

        <a href="<?php echo site_url('seguros/feriantes') ?>"><div class="pe-seguro-box">
            <h2><i class="fab fa-fort-awesome"></i> Seguros para Feriantes</h2>
           <!-- <a class="btn btn-producto " href="<?php echo site_url('seguros/feriantes') ?>"><i class="fas fa-hand-point-right"></i> Calcular Precio</a>-->
           <a href="<?php echo site_url('seguros/feriantes') ?>" class="myButton">Calcular</a>
        </div></a>

      </div>
      <div class="col-md-4">
        <div class="pe-otros-seguros-box">
          
          <?php echo anchor('seguros','<h2>Otros Seguros</h2>'); ?>
          <a class="btn btn-otros-productos btn-sm" href="<?php echo site_url('seguros/filtro/1') ?>"><i class="fas fa-hand-point-right"></i> Particulares</a>
          <a class="btn btn-otros-productos btn-sm" href="<?php echo site_url('seguros/filtro/2') ?>"><i class="fas fa-hand-point-right"></i> Empresas</a>
          <a class="btn btn-otros-productos btn-sm" href="<?php echo site_url('seguros/filtro/4') ?>"><i class="fas fa-hand-point-right"></i> Espectáculos</a>
          <a class="btn btn-otros-productos btn-sm" href="<?php echo site_url('seguros/filtro/3') ?>"><i class="fas fa-hand-point-right"></i> Vida-inversión</a>
        </div>
      </div>        
    </div>
  </div>

                </div>
              </div>
            </div>
          </div>
        </div>