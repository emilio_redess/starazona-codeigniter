
        <!-- Swiper-->
        <div class="swiper-slide-wrapper">
          <div data-simulate-touch="false" data-autoplay="5500" data-slide-effect="fade" class="swiper-container swiper-slider">
            <div class="swiper-wrapper">
             
              <div data-slide-bg="<?php echo base_url();?>public/images/dummy2/backgrounds-salud3.jpg" class="swiper-slide"></div>
              <div data-slide-bg="<?php echo base_url();?>public/images/dummy2/backgrounds-decesos3.jpg" class="swiper-slide"></div>
              <div data-slide-bg="<?php echo base_url();?>public/images/dummy2/backgrounds-vida3.jpg" class="swiper-slide"></div>
              <!--<div data-slide-bg="<?php echo base_url();?>public/images/background-08.jpg" class="swiper-slide"></div>-->
            </div>
          </div>

          <div class="swiper-onSlider">


            <div class="shell shell-wide section-70 section-xl-top-200 section-xl-bottom-220">
              <div class="range range-xs-right">
                <div class="cell-xl-6 cell-lg-9 cell-md-11" style="opacity: 0.8;">

            <!--<button class="btn btn-primary btn-sm btn-naira btn-naira-up" type="submit"><span class="icon mdi mdi-human"></span><span>Comparar</span></button>-->

                  <div data-type="horizontal" class="responsive-tabs text-md-left nav-custom-dark view-animate fadeInUpSmall">
                    <!-- Responsive-tabs-->
                    <ul class="nav-custom-tabs resp-tabs-list">
                      <li class="nav-item"><span class="icon fa-heartbeat"></span><span>Salud</span></li>
                      <li class="nav-item"><span class="icon mdi mdi-human"></span><span>Vida</span></li>
                      <li class="nav-item"><span class="icon mdi mdi-flower"></span><span>Decesos</span></li>
                    </ul>
                    <div class="resp-tabs-container nav-custom-tab nav-custom-wide">                  
          
<!--  ----------------------------------------------- FORMULARIO 1 --------------------------------------------------------------------------- -->
                      <div> 
                        <?php echo form_open_multipart('seguros/salud/1',array('class' => 'small')); ?>
                          <div class="range">

                            <div class="range offset-top-15">
                            <div class="cell-xs">
                              <?php echo validation_errors(); ?>
                              </div>
                            </div>
                            <div class="range offset-top-15">
                              <div class="cell-xs">
                                <div class="form-group">
                                  <label class="form-group-label">Nombre y apellidos</label>
                                  <input type="text" name="nombreApellidos" id="nombreApellidos" class="form-control">
                                </div>
                              </div>

                              <div class="cell-xs offset-top-15 offset-xs-top-0">
                                <div class="form-group">
                                  <label class="form-group-label">Email</label>
                                  <input type="text" name="email" id="email" class="form-control">
                                </div>
                              </div>

                              <div class="cell-xs offset-top-15 offset-xs-top-0">
                                <div class="form-group">
                                  <label class="form-group-label">Teléfono</label>
                                  <input type="text" name="telefono" id="telefono" class="form-control">
                                </div>
                              </div>
                            </div>

                            <div class="range offset-top-15">
                              <div class="cell-xs">
                                <div class="form-group">
                                  <label class="form-group-label">Provincia del asegurado</label>
                                  <!--Select 2-->
                                  <select data-minimum-results-for-search="Infinity" class="form-control select-filter" id="provincia" name="provincia">
                                    <option value="">Selecciona una opción</option>
                                    <option value="01">Álava</option>
                                    <option value="02">Albacete</option>
                                    <option value="03">Alicante</option>
                                    <option value="04">Almería</option>
                                    <option value="33">Asturias</option>
                                    <option value="05">Ávila</option>
                                    <option value="06">Badajoz</option>
                                    <option value="07">Baleares (Islas)</option>
                                    <option value="08">Barcelona</option>
                                    <option value="09">Burgos</option>
                                    <option value="10">Cáceres</option>
                                    <option value="11">Cádiz</option>
                                    <option value="39">Cantabria</option>
                                    <option value="12">Castellón</option>
                                    <option value="51">Ceuta</option>
                                    <option value="13">Ciudad real</option>
                                    <option value="14">Córdoba</option>
                                    <option value="15">Coruña (A)</option>
                                    <option value="16">Cuenca</option>
                                    <option value="17">Girona</option>
                                    <option value="18">Granada</option>
                                    <option value="19">Guadalajara</option>
                                    <option value="20">Guipúzcoa</option>
                                    <option value="21">Huelva</option>
                                    <option value="22">Huesca</option>
                                    <option value="23">Jaén</option>
                                    <option value="24">León</option>
                                    <option value="25">Lleida</option>
                                    <option value="27">Lugo</option>
                                    <option value="28">Madrid</option>
                                    <option value="29">Málaga</option>
                                    <option value="52">Melilla</option>
                                    <option value="30">Murcia</option>
                                    <option value="31">Navarra</option>
                                    <option value="32">Orense</option>
                                    <option value="34">Palencia</option>
                                    <option value="35">Palmas (las)</option>
                                    <option value="36">Pontevedra</option>
                                    <option value="26">Rioja (la)</option>
                                    <option value="37">Salamanca</option>
                                    <option value="40">Segovia</option>
                                    <option value="41">Sevilla</option>
                                    <option value="42">Soria</option>
                                    <option value="38">Sta. Cruz Tenerife</option>
                                    <option value="43">Tarragona</option>
                                    <option value="44">Teruel</option>
                                    <option value="45">Toledo</option>
                                    <option value="46">Valencia</option>
                                    <option value="47">Valladolid</option>
                                    <option value="48">Vizcaya</option>
                                    <option value="49">Zamora</option>
                                    <option value="50">Zaragoza</option>
                                  </select>
                                </div>
                              </div>

                              <div class="cell-xs offset-top-15 offset-xs-top-0">
                                <div class="form-group">
                                  <label class="form-group-label">Contratante del seguro</label>
                                  <!--Select 2-->
                                  <select data-minimum-results-for-search="Infinity" class="form-control select-filter" id="autonomo" name="autonomo">
                                    <option value="">Selecciona una opción</option>
                                    <option value="1">Empresa</option>
                                    <option value="2">Autónomo</option>
                                    <option value="3">Autónomo con empleados</option>
                                    <option value="4">Particular</option>
                                  </select>
                                </div>
                              </div>
                              <div class="cell-xs offset-top-15 offset-xs-top-0">
                                <div class="form-group">
                                  <label class="form-group-label">Número de personas</label>
                                  <!--Select 2-->
                                  <select data-minimum-results-for-search="Infinity" class="form-control select-filter" id="numeroPersonas" name="numeroPersonas">
                                    <option value="">Selecciona una opción</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                  </select>
                                </div>
                              </div>                              
                            </div>
                            <div class="range offset-top-15">
                              <div class="cell-xs">
                                  <div class="form-group edad_hide">
                                      <label class="form-group-label">Edad 1</label>
                                      <input type="number" min="0" max="99" value="50" class="form-control" name="edad-1" id="edad-1" data-indice="1">
                                  </div>
                              </div>
                              <div class="cell-xs offset-top-15 offset-xs-top-0">
                                  <div class="form-group edad_hide">
                                      <label class="form-group-label">Edad 2</label>
                                      <input type="number" min="0" max="99" value="50" class="form-control" name="edad-2" id="edad-2" data-indice="2">
                                  </div>
                              </div>

                              <div class="cell-xs offset-top-15 offset-xs-top-0">
                                  <div class="form-group edad_hide">
                                      <label class="form-group-label">Edad 3</label>
                                      <input type="number" min="0" max="99" value="50" class="form-control" name="edad-3" id="edad-3" data-indice="3">
                                  </div>
                              </div>

                              <div class="cell-xs offset-top-15 offset-xs-top-0">
                                  <div class="form-group edad_hide">
                                      <label class="form-group-label">Edad 4</label>
                                      <input type="number" min="0" max="99" value="50" class="form-control" name="edad-4" id="edad-4" data-indice="4">
                                  </div>
                              </div>                                                            

                              <div class="cell-lg-clear-flex cell-sm-bottom cell-lg text-center text-lg-right offset-top-15 offset-lg-top-0">

                                <button class="btn btn-primary btn-sm btn-naira btn-naira-up" type="submit"><span class="icon fa-heartbeat"></span><span>Calcula tu precio</span></button>
                              </div>

                              <div class="cell-lg-clear-flex cell-sm-bottom cell-lg text-center text-lg-right offset-top-15 offset-lg-top-0">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="salud_politica" id="salud_politica">
                                      Acepto la política de protección de datos <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
                                </label>
                              </div>  
                            </div>
                          </div>
                        </form>
                      </div>
<!--  ----------------------------------------------- END FORMULARIO 1 --------------------------------------------------------------------------- -->

<!--  ----------------------------------------------- FORMULARIO 2  VIDA--------------------------------------------------------------------------- -->
                      <div>                       
                        <?php echo form_open_multipart('seguros/vida/1',array('class' => 'small')); ?>
                          <div class="range">

                            <div class="range offset-top-15">
                            <div class="cell-xs">
                              <?php echo validation_errors(); ?>
                              </div>
                            </div>





                            <div class="range offset-top-15">
                              <div class="cell-xs">
                                <div class="form-group">
                                  <label class="form-group-label">Nombre y apellidos</label>
                                  <input type="text" name="vida_nombreApellidos" id="vida_nombreApellidos" class="form-control">
                                </div>
                              </div>

                              <div class="cell-xs offset-top-15 offset-xs-top-0">
                                <div class="form-group">
                                  <label class="form-group-label">Email</label>
                                  <input type="text" name="vida_email" id="vida_email" class="form-control">
                                </div>
                              </div> 

                              <div class="cell-xs offset-top-15 offset-xs-top-0">
                                <div class="form-group">
                                  <label class="form-group-label">Teléfono</label>
                                  <input type="text" name="vida_telefono" id="vida_telefono" class="form-control">
                                </div>
                              </div>                                                             
                            </div>

                            <div class="range offset-top-15">
        

                              <div class="cell-xs offset-top-15 offset-xs-top-0">
                                <div class="form-group">
                                  <label class="form-group-label">Capital para asegurar</label>
                                  <!--Select 2-->
                                  <select data-minimum-results-for-search="Infinity" class="form-control select-filter" id="vida_capital" name="vida_capital">
                                    <option value="">Selecciona una opción</option>
                                    <option value="50000">50.000 €</option>
                                    <option value="100000">100.000 €</option>
                                    <option value="150000">150.000 €</option>
                                    <option value="200000">200.000 €</option>
                                    <option value="250000">250.000 €</option>
                                    <option value="300000">300.000 €</option>
                                    <option value="350000">350.000 €</option>
                                    <option value="400000">400.000 €</option>
                                    <option value="450000">450.000 €</option>
                                    <option value="500000">500.000 €</option>
                                   
                                  </select>
                                </div>
                              </div>

                              <div class="cell-xs offset-top-15 offset-xs-top-0">
                                <div class="form-group">
                                  <label class="form-group-label">Edad del asegurado</label>
                                  <!--Select 2-->
                                  <input type="number" min="0" max="99" value="50" class="form-control" name="vida_edad" id="vida_edad">
                                </div>
                              </div>


                               <div class="cell-lg-clear-flex cell-sm-bottom cell-lg text-center text-lg-right offset-top-15 offset-lg-top-0">
                                <button class="btn btn-primary btn-sm btn-naira btn-naira-up" type="submit"><span class="icon mdi mdi-human"></span><span>Calcula tu precio</span></button>
                              </div>                             
                              <div class="cell-lg-clear-flex cell-sm-bottom cell-lg text-center text-lg-right offset-top-15 offset-lg-top-0">
                                                          <label class="form-check-label">
                                                                <input type="checkbox" class="form-check-input" name="salud_politica" id="salud_politica">
                                                                Acepto la política de protección de datos <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
                                                          </label>
                              </div>  
                            </div>
                          </div>
                        </form>
                      </div>
<!--  ----------------------------------------------- END FORMULARIO 2 --------------------------------------------------------------------------- -->

<!--  ----------------------------------------------- FORMULARIO 3 --------------------------------------------------------------------------- -->
                      <div>                       
                        <?php echo form_open_multipart('seguros/decesos/1',array('class' => 'small')); ?>
                          <div class="range">

                            <div class="range offset-top-15">
                            <div class="cell-xs">
                              <?php echo validation_errors(); ?>
                              </div>
                            </div>
                            <div class="range offset-top-15">
                              <div class="cell-xs">
                                <div class="form-group">
                                  <label class="form-group-label">Nombre y apellidos</label>
                                  <input type="text" name="decesos_nombreApellidos" id="decesos_nombreApellidos" class="form-control">
                                </div>
                              </div>

                              <div class="cell-xs offset-top-15 offset-xs-top-0">
                                <div class="form-group">
                                  <label class="form-group-label">Email</label>
                                  <input type="text" name="decesos_email" id="decesos_email" class="form-control">
                                </div>
                              </div> 

                              <div class="cell-xs offset-top-15 offset-xs-top-0">
                                <div class="form-group">
                                  <label class="form-group-label">Teléfono</label>
                                  <input type="text" name="decesos_telefono" id="decesos_telefono" class="form-control">
                                </div>
                              </div>                                                             
                            </div>

                            <div class="range offset-top-15">
                              <div class="cell-xs">
                                <div class="form-group">
                                  <label class="form-group-label">Provincia del asegurado</label>
                                  <!--Select 2-->
                                  <select data-minimum-results-for-search="Infinity" class="form-control select-filter" id="decesos_provincia" name="decesos_provincia">
                                    <option value="">Selecciona una opción</option>
                                    <option value="01">Álava</option>
                                    <option value="02">Albacete</option>
                                    <option value="03">Alicante</option>
                                    <option value="04">Almería</option>
                                    <option value="33">Asturias</option>
                                    <option value="05">Ávila</option>
                                    <option value="06">Badajoz</option>
                                    <option value="07">Baleares (Islas)</option>
                                    <option value="08">Barcelona</option>
                                    <option value="09">Burgos</option>
                                    <option value="10">Cáceres</option>
                                    <option value="11">Cádiz</option>
                                    <option value="39">Cantabria</option>
                                    <option value="12">Castellón</option>
                                    <option value="51">Ceuta</option>
                                    <option value="13">Ciudad real</option>
                                    <option value="14">Córdoba</option>
                                    <option value="15">Coruña (A)</option>
                                    <option value="16">Cuenca</option>
                                    <option value="17">Girona</option>
                                    <option value="18">Granada</option>
                                    <option value="19">Guadalajara</option>
                                    <option value="20">Guipúzcoa</option>
                                    <option value="21">Huelva</option>
                                    <option value="22">Huesca</option>
                                    <option value="23">Jaén</option>
                                    <option value="24">León</option>
                                    <option value="25">Lleida</option>
                                    <option value="27">Lugo</option>
                                    <option value="28">Madrid</option>
                                    <option value="29">Málaga</option>
                                    <option value="52">Melilla</option>
                                    <option value="30">Murcia</option>
                                    <option value="31">Navarra</option>
                                    <option value="32">Orense</option>
                                    <option value="34">Palencia</option>
                                    <option value="35">Palmas (las)</option>
                                    <option value="36">Pontevedra</option>
                                    <option value="26">Rioja (la)</option>
                                    <option value="37">Salamanca</option>
                                    <option value="40">Segovia</option>
                                    <option value="41">Sevilla</option>
                                    <option value="42">Soria</option>
                                    <option value="38">Sta. Cruz Tenerife</option>
                                    <option value="43">Tarragona</option>
                                    <option value="44">Teruel</option>
                                    <option value="45">Toledo</option>
                                    <option value="46">Valencia</option>
                                    <option value="47">Valladolid</option>
                                    <option value="48">Vizcaya</option>
                                    <option value="49">Zamora</option>
                                    <option value="50">Zaragoza</option>
                                  </select>
                                </div>
                              </div>

     
                              <div class="cell-xs offset-top-15 offset-xs-top-0">
                                <div class="form-group">
                                  <label class="form-group-label">Número de personas</label>
                                  <!--Select 2-->
                                  <select data-minimum-results-for-search="Infinity" class="form-control select-filter" id="decesos_numeroPersonas" name="decesos_numeroPersonas">
                                    <option value="">Selecciona una opción</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                  </select>
                                </div>
                              </div>                              
                            </div>
                            <div class="range offset-top-15">
                              <div class="cell-xs">
                                  <div class="form-group decesos_edad_hide">
                                      <label class="form-group-label">Edad 1</label>
                                      <input type="number" min="0" max="99" value="50" class="form-control" name="decesos_edad-1" id="decesos_edad-1" data-indice="1">
                                  </div>
                              </div>
                              <div class="cell-xs offset-top-15 offset-xs-top-0">
                                  <div class="form-group decesos_edad_hide">
                                      <label class="form-group-label">Edad 2</label>
                                      <input type="number" min="0" max="99" value="50" class="form-control" name="decesos_edad-2" id="decesos_edad-2" data-indice="2">
                                  </div>
                              </div>

                              <div class="cell-xs offset-top-15 offset-xs-top-0">
                                  <div class="form-group decesos_edad_hide">
                                      <label class="form-group-label">Edad 3</label>
                                      <input type="number" min="0" max="99" value="50" class="form-control" name="decesos_edad-3" id="decesos_edad-3" data-indice="3">
                                  </div>
                              </div>

                              <div class="cell-xs offset-top-15 offset-xs-top-0">
                                  <div class="form-group decesos_edad_hide">
                                      <label class="form-group-label">Edad 4</label>
                                      <input type="number" min="0" max="99" value="50" class="form-control" name="decesos_edad-4" id="decesos_edad-4" data-indice="4">
                                  </div>
                              </div>                                                            

                              <div class="cell-lg-clear-flex cell-sm-bottom cell-lg text-center text-lg-right offset-top-15 offset-lg-top-0">
                                <button class="btn btn-primary btn-sm btn-naira btn-naira-up" type="submit"><span class="icon mdi mdi-flower"></span><span>Calcula tu precio</span></button>
                              </div>
                              <div class="cell-lg-clear-flex cell-sm-bottom cell-lg text-center text-lg-right offset-top-15 offset-lg-top-0">
                                                          <label class="form-check-label">
                                                                <input type="checkbox" class="form-check-input" name="decesos_politica" id="decesos_politica">
                                                                Acepto la política de protección de datos <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
                                                              </label>
                              </div>                                
                            </div>
                          </div>
                        </form>
                      </div>
<!--  ----------------------------------------------- END FORMULARIO 3 --------------------------------------------------------------------------- -->

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>