      <footer class="page-footer-widget">
        <div class="shell shell-wide text-sm-left">
          <div class="range section-60 section-md-90">
            <div class="cell-sm-6 cell-lg-3"  style="padding-left: 40px;">
              <h5 class="text-bold">Contáctanos</h5>
              <hr class="divider divider-50 divider-info-dr divider-sm-left offset-top-12">



              <div class="offset-top-30 p">
                <div class="range">

             <!-- <div class="cell-sm-8 cell-lg-8">-->


<!--
                <div class="unit unit-horizontal unit-spacing-15 text-left">
                  <div class="unit-left"><span class="text-primary-dr icon"><i class="fas fa-map-marker"></i></span></div>
                  <div class="unit-body"><a href="#" class="text-gray"><?php echo DIRECCION_CONTACTO_PARTE1; ?><br class="veil reveal-lg-block"><?php echo DIRECCION_CONTACTO_PARTE2; ?></a></div>
                </div>


                <div class="unit unit-horizontal unit-spacing-15 offset-top-10 text-left">
                  <div class="unit-left"><span class="text-primary-dr icon"><i class="fas fa-phone"></i></span></div>
                  <div class="unit-body"><a href="callto:<?php echo TELEFONO_CONTACTO; ?>" class="text-gray"><?php echo TELEFONO_CONTACTO; ?></a></div>
                </div>


                <div class="unit unit-horizontal unit-spacing-15 offset-top-10 text-left">
                  <div class="unit-left"><span class="text-primary-dr icoN"><i class="fas fa-envelope"></i></span></div>
                  <div class="unit-body"><a href="mailto:<?php echo EMAIL_CONTACTO ?>" class="text-gray"><?php echo EMAIL_CONTACTO ?></a></div>
                </div>
-->


                <div class="unit unit-horizontal unit-spacing-15 offset-top-10 text-left">
                  <div class="unit-left"><span class="text-primary-dr icon"><i class="fas fa-phone"></i></span></div>
                  <div class="unit-body"><a href="<?php echo site_url('contacto/loc/MAS') ?>" class="text-gray">Massanassa: <span style="color: #ca5717;"><?php echo TELEFONO_CONTACTO_OFICINA_MASSANASSA; ?></span></a></div>
                </div>

                <div class="unit unit-horizontal unit-spacing-15 offset-top-10 text-left">
                  <div class="unit-left"><span class="text-primary-dr icon"><i class="fas fa-phone"></i></span></div>
                  <div class="unit-body"><a href="<?php echo site_url('contacto/loc/SED') ?>" class="text-gray">Sedaví particulares: <span style="color: #ca5717;"><?php echo TELEFONO_CONTACTO_OFICINA_SEDAVI; ?></span></a></div>
                </div>

                <div class="unit unit-horizontal unit-spacing-15 offset-top-10 text-left">
                  <div class="unit-left"><span class="text-primary-dr icon"><i class="fas fa-phone"></i></span></div>
                  <div class="unit-body"><a href="<?php echo site_url('contacto/loc/SED') ?>" class="text-gray">Sedaví especialidades: <span style="color: #ca5717;"><?php echo TELEFONO_CONTACTO_OFICINA_VALENCIA; ?></span></a></div>
                </div>

                <div class="unit unit-horizontal unit-spacing-15 offset-top-10 text-left">
                  <div class="unit-left"><span class="text-primary-dr icon"><i class="fas fa-phone"></i></span></div>
                  <div class="unit-body"><a href="<?php echo site_url('contacto/loc/SIL') ?>" class="text-gray">Silla: <span style="color: #ca5717;"><?php echo TELEFONO_CONTACTO_OFICINA_SILLA; ?></span></a></div>
                </div>

                <div class="unit unit-horizontal unit-spacing-15 offset-top-10 text-left">
                  <div class="unit-left"><span class="text-primary-dr icon"><i class="fas fa-phone"></i></span></div>
                  <div class="unit-body"><a href="<?php echo site_url('contacto/loc/MAS') ?>" class="text-gray">Siniestros: <span style="color: #ca5717;"><?php echo TELEFONO_CONTACTO_OFICINA_MASSANASSA; ?></span></a></div>
                </div>    

                <div class="unit unit-horizontal unit-spacing-15 offset-top-10 text-left">
                 
                  <a href="<?php echo DIRECCION_FACEBOOK; ?>" target="_blank"><?php echo img(array('src'=>'public/images/facebook.png','alt'=> 'Facebook','class' => 'img-responsive center-block facebook_logo')); ?></a>

                </div>                                                                            
      
</div>
</div>





            </div>
            <div class="cell-sm-6 cell-lg-3">
              <div class="inset-xl-right-65">
                <h5 class="text-bold">Somos miembros de</h5>
                <hr class="divider divider-50 divider-info-dr divider-sm-left offset-top-12">

              <div class="offset-top-30 gallery">



                <div class="gallery-item"><?php echo anchor('http://www.cojebro.com',img(array('src'=>'public/images/colaboradores/cojebro.jpg','alt'=> 'Cojebro','class' => 'img-responsive')),array('class' => 'reveal-inline-block thumbnail-img','target' =>'_blank')); ?></div>
                <div class="gallery-item"><?php echo anchor('http://www.fecor.es',img(array('src'=>'public/images/colaboradores/fecor.jpg','alt'=> 'Fecor','class' => 'img-responsive')),array('class' => 'reveal-inline-block thumbnail-img','target' =>'_blank')); ?></div>
                <div class="gallery-item"><?php echo anchor('http://www.valenciaseguros.com/',img(array('src'=>'public/images/colaboradores/logo-col-mediadors.jpg','alt'=> 'Colegio de mediadores de seguros de Valencia','class' => 'img-responsive')),array('class' => 'reveal-inline-block thumbnail-img','target' =>'_blank')); ?></div>

                <div class="gallery-item"><?php echo anchor('http://fvaac.es/',img(array('src'=>'public/images/colaboradores/fvaac.jpg','alt'=> 'FVAAC','class' => 'img-responsive')),array('class' => 'reveal-inline-block thumbnail-img','target' =>'_blank')); ?></div>                
             
                <div class="gallery-item"><?php echo anchor('http://esacv.es/',img(array('src'=>'public/images/colaboradores/esacv.png','alt'=> 'ESACV','class' => 'img-responsive')),array('class' => 'reveal-inline-block thumbnail-img','target' =>'_blank')); ?></div>      
                
                <div class="gallery-item"><?php echo anchor('http://www.afape.org.es/',img(array('src'=>'public/images/colaboradores/afape.jpg','alt'=> 'AFAPE','class' => 'img-responsive')),array('class' => 'reveal-inline-block thumbnail-img','target' =>'_blank')); ?></div>                           
              
              </div>

              </div>
            </div>
            <div class="cell-sm-6 cell-lg-3 offset-top-40 offset-lg-top-0">


              <h5 class="text-bold">Asistencia</h5>
              <hr class="divider divider-50 divider-info-dr divider-sm-left offset-top-12">
              <div class="offset-top-20 offset-md-top-40">

              <div class="offset-top-30 p">

                <?php echo anchor('faq#tlfs',img(array('src'=>'public/images/asistencia24h_ext2.png','alt'=> 'asistencia 24 horas','class' => 'img-responsive', 'width' => '100px')),array('class' => 'reveal-inline-block thumbnail-img')); ?>

             


              </div>



              </div>


            </div>
            <div class="cell-sm-6 cell-lg-3 offset-top-40 offset-lg-top-0">
              <h5 class="text-bold">Soporte Telefónico</h5>
              <hr class="divider divider-50 divider-info-dr divider-sm-left offset-top-12">
     
              
              <div class="">


              <div class="cell-sm-24 cell-lg-12">
                <?php echo img(array('src'=>'public/images/te_llamamos3.png','alt'=> 'Te llamamos','class' => 'img-responsive')) ?>
              </div>
              <div class="cell-sm-24 cell-lg-12" style="margin-top:10px;">
                <button class="btn btn-primary btn-sm btn-naira btn-naira-up" type="button" data-toggle="modal" data-target="#modal_call_me"><span class="icon fas fa-phone"></span><span>Te llamamos</span></button>
              </div>



            </div>


            </div>
          </div>
        </div>
        <div class="shell shell-wide page-footer-min text-md-left">
          <p>&#169; <span id="copyright-year"></span> Todos los derechos reservados.<?php echo anchor('aviso_legal','Aviso legal'); ?> - <?php echo anchor('politica_privacidad','política de privacidad'); ?>
          </p>
        </div>
      </footer>
    </div>
    <!-- Global Mailform Output-->
    <div id="form-output-global" class="snackbars"></div>
    <!-- PhotoSwipe Gallery-->
    <div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
            <button title="Share" class="pswp__button pswp__button--share"></button>
            <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
            <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
          <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div>





<!------------------------------------------------------ modal dialog para llamame ------------------------------------------------------------------------------- -->
<div class="modal fade" id="modal_call_me"  role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>Llamadme vosotros</h4>
      </div>
      <div class="modal-body">
        <div class="animated bounceInUp box_call">
          <h4 class="uppercase">Te llamamos <b>gratis</b></h4>
          <h5>Y te informamos sin compromiso:</h5>
          <form class="feedback" name="feedback">
            <div class="form-group">
              <input type="text" name="nombre" placeholder="Escribe tu nombre" class="form-control input_nombre">
            </div>              
            <div class="form-group">
              <input type="tel" name="telefono" placeholder="Escribe tu nº de teléfono" pattern="[0-9]{9}" maxlength="9" class="form-control input_phone only_numbers">
            </div>

          
            <div class="form-group">
              <select name="horario_llamada" id="horario_llamada" class="form-control select_horario">
                <option value="">selecciona una opcion...</option>
                <option value="">De 09:00 a 13:00</option>
                <option value="">De 13:00 a 19:00</option>
                <option value="">De 19:00 a 21:00</option>
              </select>

            </div>
        
            <div class="form-group">
              <label>
                <input type="checkbox" class="form-check-input"> He leído y acepto la <a href="#" data-toggle="modal" data-target="#modal_privacy_policy">política de privacidad</a>
              </label>
            </div>
            <button type="button" class="btn btn-warning btn-block callme_button">Quiero que me llamen</button>
          </form>
        </div>
        <div class="form_feedback" style="display:none;">
          <h4 class="uppercase">Solicitud enviada.</h4>
          <hr>
          <p>En breve un asesor se pondrá en contacto contigo para informarte.</p>
        </div>        
      </div>
    </div>
  </div>
</div>
<!------------------------------------------------------ end modal dialog para llamame ------------------------------------------------------------------------------- -->


<!------------------------------------------------------ modal dialog para politica de privacidad -------------------------------------------------------------------- -->
<div class="modal fade" id="modal_privacy_policy" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>Política de privacidad</h4>
      </div>
      <div class="modal-body">
        <p>De conformidad con la Ley 15/1999 de Protección de Datos de Carácter Personal, le informamos de que los datos recabados pasarán a formar parte de un fichero automatizado con la única finalidad de poder realizar, de manera efectiva, la gestión de los contratos intermediados, y que tales datos serán utilizados por nuestra parte, siempre que sea estrictamente necesario para la finalidad antes indicada, así como para remitirle información promocional y publicitaria con relación a la misma.
        Todos los campos del formulario tienen carácter obligatorio. La negativa a facilitar los datos solicitados impedirá su registro en el sistema, no conservándose ninguno de los datos que hubiera informado.</p>
      </div>
    </div>
  </div>
</div>
<!------------------------------------------------------ end modal dialog para politica de privacidad ---------------------------------------------------------------- -->


    <!-- Filtro usado para inicializar el plugin isotope en una pestaña determinada -->
    <script>var filtro = <?php echo isset($filtro) ? $filtro : '0'; ?>;</script>


    <!-- Java script-->
    <script type="text/javascript">var JS_BASE_URL = '<?php echo base_url(); ?>';</script>
    <script type="text/javascript" src="<?php echo base_url();?>public/js/core.min.js" ></script>
    <script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>public/js/script.js" ></script>
    <script type="text/javascript" src="<?php echo base_url();?>public/js/cookies.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment-with-locales.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <?php
      if (isset($jsArray) && is_array($jsArray)) {
        foreach ($jsArray as $value) {
          echo '<script type="text/javascript" src="' . base_url() .  $value . '"></script>' . PHP_EOL;
        }
      }
    ?>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-25541245-16', 'starazona.com');
  ga('send', 'pageview');

</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-25541245-17', 'starazona.com');
  ga('send', 'pageview');

</script>





<script>
  $(document).ready(function(){



    // en caso de usar resoluciones menores de 1024, cargamos formularios en vez de los iframes en seguros de coches y motos
    if (screen.width < 1024) {
      document.getElementById('link_condicional_coche').setAttribute('href', "<?php echo site_url('seguros/coche_mb') ?>");
      document.getElementById('link_condicional_coche2').setAttribute('href', "<?php echo site_url('seguros/coche_mb') ?>");
      document.getElementById('link_condicional_moto').setAttribute('href', "<?php echo site_url('seguros/moto_mb') ?>");
    }




  $('.callme_button').click(function(){
    sendCallmeForm($(this).closest("form"));
    return false;
  });



  function sendCallmeForm(current_form)
  {
    if(!$('.form-check-input',current_form).is(':checked'))
    {
      if(confirm('Debe aceptar la política de privacidad.')){
        $('.form-check-input',current_form).attr("checked", "checked");
            }
      return false;
    } 
      
    var nombre      = $('.input_nombre',current_form).val();      
    var telefono    = $('.input_phone',current_form).val();
    var horario    = $('#horario_llamada option:selected',current_form).text();

    
    $('.callme_button').hide();
      
    $.ajax({
      url: JS_BASE_URL + "/helper/ajax_llamame/",
      type: 'POST',
      data: { nombre: nombre, telefono: telefono, horario: horario},
      dataType: 'json',
      success: function(data)
      {
        if(data.status)
        {
          $('.box_call').hide();
          $('.form_feedback').show();
        }
        else
        {
          alert(data.msg);
          $('.callme_button').show();
        }
      },

    // código a ejecutar si la petición falla;
    // son pasados como argumentos a la función
    // el objeto de la petición en crudo y código de estatus de la petición
    error : function(xhr, status) {
        alert('Disculpe, existió un problema');
    }




    }); 
    return false;   
  }  




  });







</script>
  </body>
</html>

