
      
<!-- Page Content-->
<main class="page-content">
	<section class="bg-image-06">
    	<div class="breadcrumb-wrapper">
      		<div class="shell context-dark section-30 section-lg-top-100">
		        
		        <h1 class="offset-top-20 text-ubold">Seguro de <?php echo $nombre_seguro; ?></h1>

      		</div>
    	</div>
  	</section>

	<section class="section-30 section-md-30">
	  	<div class="shell shell-wide text-lg-left">


<div class="range">

		          	<div class="col-md-10">

                  <h5 class="text-info-dr">Calcula tu precio</h5>
                  <hr class="divider divider-lg-left divider-primary divider-80">

						
						<?php echo form_open_multipart('seguros/' . $callback, array('class' => 'range text-left')); ?>

                  <div class="cell-sm-12">
                    <div class="form-group">
                      <?php echo validation_errors(); ?>
                    </div>
                  </div>

                  <div class="cell-sm-12">
                    <div class="form-group">
                      <label for="nombreApellidos" class="form-group-label">Nombre y apellidos</label>
                      <input id="nombreApellidos" type="text" name="nombreApellidos" value="<?php echo set_value('nombreApellidos'); ?>" class="form-control form-control-gray">
                    </div>
                  </div>

                  <div class="cell-sm-6 offset-top-20">
                    <div class="form-group">
                      <label for="telefono" class="form-group-label">Teléfono</label>
                      <input id="telefono" type="text" name="telefono" value="<?php echo set_value('telefono'); ?>" class="form-control form-control-gray">
                    </div>
                  </div>                  

                  <div class="cell-sm-6 offset-top-20">
                    <div class="form-group">
                      <label for="email" class="form-group-label">E-mail</label>
                      <input id="email" type="email" name="email" value="<?php echo set_value('email'); ?>" class="form-control form-control-gray">
                    </div>
                  </div>