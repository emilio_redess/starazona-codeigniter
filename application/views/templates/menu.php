      <header class="page-header">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap bg-gray-dark">
          <nav data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-sm-stick-up-offset="1px" data-md-stick-up-offset="1px" data-lg-stick-up-offset="1px" class="rd-navbar">
            <div class="rd-navbar-inner">
              <!-- RD Navbar Panel-->
              <div class="rd-navbar-panel">
                <!-- RD Navbar Toggle-->
                <button data-rd-navbar-toggle=".rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
                <!-- RD Navbar Brand-->
                <div class="rd-navbar-brand">


                  <?php echo anchor('inicio',img(array('src'=>'public/images/logo.png','alt'=> 'Starazona','class' => 'img-responsive center-block logo-site')),array('class' => 'reveal-inline-block brand-name')); ?>

                </div>
              </div>
              <div class="rd-navbar-nav-wrap">
                <!-- RD Navbar Nav-->
                <ul class="rd-navbar-nav">
                  <li class="<?php echo $opciones_menu['inicio'] == '1' ? 'active' : '' ?>"><?php echo anchor('inicio','Inicio'); ?></li>
               

                  <li class="<?php echo $opciones_menu['productos'] == '1' ? 'active' : '' ?>"><?php echo anchor('seguros','Productos'); ?>


                  </li>


                <!--<li class="<?php echo $opciones_menu['casos exito'] == '1' ? 'active' : '' ?>"><?php echo anchor('http://www.starazona.com/casosExito/casos-de-exito.php','Casos de éxito'); ?></li>-->

                  <li><a href="#">Sobre nosotros</a>
                    <!-- RD Navbar Dropdown-->
                    <ul class="rd-navbar-dropdown">
                        <li class="<?php echo $opciones_menu['sobre nosotros'] == '1' ? 'active' : '' ?>"><?php echo anchor('quienes_somos','Quienes somos'); ?></li>
                        <li class="<?php echo $opciones_menu['sobre nosotros'] == '1' ? 'active' : '' ?>"><?php echo anchor('faq','Preguntas más frecuentes'); ?></li>
                        <li class="<?php echo $opciones_menu['sobre nosotros'] == '1' ? 'active' : '' ?>"><?php echo anchor('porque_elegirnos','Por qué elegirnos'); ?></li>
                    </ul>
                  </li>   

       

                  <li><a href="#">Contacto</a>
                    <!-- RD Navbar Dropdown-->
                    <ul class="rd-navbar-dropdown">
                        <li class="<?php echo $opciones_menu['contacto'] == '1' ? 'active' : '' ?>"><?php echo anchor('contacto/loc','Contacto'); ?></li>
                        <li class="<?php echo $opciones_menu['contacto'] == '1' ? 'active' : '' ?>"><?php echo anchor('colabora','Colabora con nosotros'); ?></li>
                        <li class="<?php echo $opciones_menu['contacto'] == '1' ? 'active' : '' ?>"><?php echo anchor('att_cliente','Atención al cliente'); ?></li>
                    </ul>
                  </li>


                  <li style="font-size: 1.4em;font-weight: bold;"><i class="fas fa-phone-square" style="color:red;"></i> <?php echo TELEFONO_CONTACTO; ?></li>

                  

                  
                </ul>
                <?php echo anchor('faq#tlfs',img(array('src'=>'public/images/asistencia24h_ext2.png','alt'=> 'asistencia 24 horas','class' => 'img-responsive', 'width' => '70px')),array('class' => '')); ?>
                <!--RD Navbar Search-->

              </div>
            </div>
          </nav>
        </div>
      </header>