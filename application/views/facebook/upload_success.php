<main>
	<div class="container">
		<h3>Imagen subida correctamente!</h3>

		<ul>
		<?php foreach ($upload_data as $item => $value):?>
		<li><?php echo $item;?>: <?php echo $value;?></li>
		<?php endforeach; ?>
		</ul>

		<p><?php echo anchor('facebook/ver_enlaces/' . $articulo_id, 'Volver al articulo'); ?></p>
	</div>
</main>	