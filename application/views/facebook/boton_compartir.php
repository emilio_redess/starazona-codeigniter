<html>
<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
  		<title></title>
  		<meta name="description" content="" />
		<meta name="keywords" content="" />
	    <!-- You can use Open Graph tags to customize link previews.
	    Learn more: https://developers.facebook.com/docs/sharing/webmasters -->
		<meta property="og:url"           content="<?php echo $noticia_url; ?>" />
		<meta property="og:type"          content="article" />
		<!-- <meta property="og:title"         content="" />
		<meta property="og:description"   content="" />
		<meta property="og:image"         content="" />-->
<?php 
			echo link_tag('public/css/bootstrap.min.css');
			echo link_tag('public/css/main.css');
			echo link_tag('public/css/cookiecuttr.css');
?>
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.9";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>



	<header>
	<div class="header">
		<div class="container">
			<div class="col-sm-4 col-sm-push-8 col-xs-12 box_phone_container">
				<div class="box_phone">
					<p><span class="hidden-xs">Atención al cliente</span><!-- <span class="visible-xs">Atención al cliente</span> --></p>
					<p class="hidden-xs"><i class="icon-phone"></i><?php echo TELEFONO_CONTACTO; ?></p>
				<!--	<p class="visible-xs"><i class="icon-phone"></i><a href="tel:<?php echo TELEFONO_CONTACTO; ?>"><?php echo TELEFONO_CONTACTO; ?></a></p>
					<p class="visible-xs"><button type="button" class="btn btn-warning callme_pop">Quiero que me llamen</button></p> -->
				</div>
			</div>
			<div class="col-sm-8 col-sm-pull-4 col-xs-12 clearfix no_padding_left hidden-xs">
					<?php echo anchor('http://www.starazona.com',img(array('src'=>'public/images/logotipo.png', 'alt'=> 'Starazona')),array('class' => 'logo')); ?>
			</div>
		</div>


			<div class="container">
			<nav class="navbar navbar-default">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar_collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="toggle_text" data-toggle="collapse" data-target="#navbar_collapse">
						<b>Menú</b> Todos nuestros productos
					</a>
				</div>
				<div class="collapse navbar-collapse" id="navbar_collapse">
					<ul class="nav nav-justified">
						<li class="dropdown">
							<a href="http://www.starazona.com/index.php" class="dropdown-toggle" role="button" target="_blank">Web Starazona<br><b>página principal</b></a>
						</li>
						<li class="dropdown">
							<a href="http://www.starazona.com/seguro-de-hogar/seguro-de-hogar" class="dropdown-toggle" role="button" target="_blank">Starazona<br><b>seguro hogar</b></a>
						</li>						
						<li class="dropdown ">
							<a href="http://www.starazona.com/seguro-de-coches/seguros-de-coches" class="dropdown-toggle" role="button" target="_blank">Starazona<br><b>seguro coche</b></a>
						</li>
	
						<li class="dropdown ">
							<a href="http://www.starazona.com/seguro-de-vida/seguros-de-vida" class="dropdown-toggle" role="button" target="_blank">Starazona<br><b>seguro de vida</b></a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
</header>






<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h5><b>La URL generada es:</b> </h5>
			<h5>http://www.starazona.com/productos/facebook/articulo/<?php echo $articulo_id; ?></h5>
			<h5>Para terminar el proceso de inserción del artículo, haz click en el botón "compartir" de facebook:<div class="fb-share-button" data-href="http://www.starazona.com/productos/facebook/articulo/<?php echo $articulo_id; ?>" data-layout="button" data-size="large" data-mobile-iframe="false"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.starazona.com%2Fproductos%2Ffacebook%2Farticulo%2F<?php echo $articulo_id; ?>&amp;src=sdkpreparse">Compartir</a></div>
			</h5>
		</div>
	</div>
	<div class="row" style="margin-top: 150px;">
		<div class="col-sm-6">
			<p><?php echo anchor('facebook/insertar','<button class="btn btn-primary btn-facebookapp">Nueva publicación</button>'); ?></p>
		</div>
		<div class="col-sm-6">
			<p><?php echo anchor('facebook/agregar_imagen/' . $articulo_id,'<button class="btn btn-primary btn-facebookapp">Agregar imagen de previsualización</button>'); ?></p>
		</div>
	</div>


	

	
</div>




<script type="text/javascript">var JS_BASE_URL = '<?php echo site_url(); ?>';</script>
<script type="text/javascript" src="<?php echo base_url();?>public/js/jquery-2.2.1.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>public/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>public/js/main.js?v=1468652261" ></script>
	
</body>
</html>