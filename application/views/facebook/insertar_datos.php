<main>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">

				<?php echo validation_errors(); ?>
				<?php echo form_open_multipart('facebook/insertar'); ?>

					<div class="form-group">
						<label for="noticia_url">URL de la noticia original</label>
						<input type="text" name="noticia_url" id="noticia_url" class="form-control" value="<?php echo set_value('noticia_url'); ?>">
					</div>
					<button type="submit" class="btn btn-primary">Enviar datos</button>
				</form>
			</div>
		</div>


		<div class="row">
			<div class="col-sm-12">
				<div style="margin-top: 100px;" class="enlaces_fb">
					<h2>Enlaces de interés</h2>
				</div>
			</div>
			<div class="col-sm-12">
				<a href="http://www.starazona.com/casosExito/casos-de-exito.php" target="_blank"><button class="btn btn-primary">Casos de éxito</button></a><br><br>
				<a href="http://www.starazona.com/casosExito/controlpanel.php" target="_blank"><button class="btn btn-primary">Panel de control de casos de éxito</button></a>
				<?php //echo anchor('http://www.starazona.com/casosExito/casos-de-exito.php','Casos de éxito','target="_blank"'); ?><br>
				<?php //echo anchor('http://www.starazona.com/casosExito/controlpanel.php','Panel de control de casos de éxito','target="_blank"'); ?><br>

			</div>
		</div>


		<div class="row">
			<div class="col-sm-12">
				<div style="margin-top: 100px;" class="recientes">
					<h2>Ultimos articulos insertados</h2>
				</div>
			</div>
		</div>
		
		
			
				<?php 
					$primer_elemento = true;
					//$columna = 1;
					foreach ($recientes->result() as $row){
						if ($primer_elemento === true) 
						{
							echo "<div class='row'>";
							echo "<div class='col-sm-12'>";
							if (!is_null($row->imagen)){
								echo img(array('src'=>'public/uploads/' . $row->imagen, 'width'=> '100%'));
								echo "</div></div>";
								echo "<div class='row' style='margin-top:75px;'>";
								echo "<div class='col-sm-12'>";								
							}

							
							
							echo "<ul>";
							$primer_elemento = false;
						}

						echo "<li>";
						echo $row->articulo_url;
						echo "</li>";

						
/* 
						if ($columna === 1)
							echo '<div class="row" style="margin-top: 30px;">';

						echo '<div class="col-md-4">';
						echo '<div style="border-style: solid;padding:5px;height:280px;">';

						if (!is_null($row->imagen))							
							echo img(array('src'=>'public/uploads/' . $row->imagen, 'height'=> '250'));
						else
							echo $row->articulo_url;
						echo '</div></div>';
						if ($columna === 3){
							echo '</div>';	
							$columna = 1;					
						}
						else
							$columna++;
*/
							
						}
						echo "</ul>";
						echo "</div></div>";
				?>
			</div>
		</div>		




	</div>
</main>