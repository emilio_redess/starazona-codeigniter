
      
<!-- Page Content-->
<main class="page-content">
	<section class="bg-image-06">
    	<div class="breadcrumb-wrapper">
      		<div class="shell context-dark section-30 section-lg-top-100">
		       
		        <h1 class="offset-top-20 text-ubold">CV D. José Ignacio Hebrero Álvarez</h1>
		       
      		</div>
    	</div>
  	</section>

	<section class="section-80 section-md-120">
	  	<div class="shell shell-wide text-lg-left">
		    <div class="range">
		        <div class="inset-lg-left-80">




				<div class="col-sm-12 section-md-100">
      <h5 style="margin-bottom: 15px;">Curriculum vitae</h5>
      <h3 style="margin-bottom: 15px;">José Ignacio Hebrero Álvarez</h3>
      <h5>FORMACION ACADEMICA</h5>
      <ul class="lista_datos" style="margin-bottom: 25px;">
        <li>Doctor en Derecho, por ICADE.</li>
        <li>Licenciado en Derecho por la Universidad Autónoma de Madrid.</li>
        <li>M. B.A. por el CINDE (Madrid).</li>
        <li>Graduado en Empresas Cooperativas por la Universidad Politécnica de Madrid.</li>
        <li>Magister Scientiae por la Universidad de los Andes, Mérida (Venezuela).</li>
        <li>Diplomado en Derecho Privado, en el Instituto de Estudios Jurídicos.<br />
        </li>
      </ul>
      <h5>ACTIVIDAD PROFESIONAL ACTUAL</h5>
      <ul class="lista_datos" style="margin-bottom: 25px;">
        <li>ABOGADO. Socio Director de H. &amp; A. ABOGADOS - ASESORES DE RESPONSABILIDAD CIVIL Y CONSULTORES DE SEGUROS.</li>
        <li>VICESECRETARIO GENERAL DE LA SECCION ESPAÑOLA DE LA ASOCIACION INTERNACIONAL DE DERECHO DE SEGUROS (SEAIDA).</li>
        <li>MIEMBRO DEL COMITÉ DIRECTIVO Y ASESOR JURIDICO DE LA ASOCIACION ESPAÑOLA DE MEDICINA DEL SEGURO.</li>
        <li>MIEMBRO DE LA ASOCIACION ESPAÑOLA DE ABOGADOS ESPECIALIZADOS EN RESPONSABILIDAD CIVIL.</li>
        <li>MIEMBRO DEL CONSEJO EDITORIAL DE LA REVISTA DE RESPONSABILIDAD CIVIL, CIRCULACION Y SEGURO.</li>
        <li>ASESOR EN MATERIA DE DERECHO Y ECONOMIA DE LOS SEGUROS PRIVADOS PARA DIVERSOS GOBIERNOS DE PAISES DE LATINOAMÉRICA Y DEL ESTE DE EUROPA.<br />
        </li>
      </ul>
      <h5>EXPERIENCIA PROFESIONAL</h5>
      <ul class="lista_datos" style="margin-bottom: 25px;">
        <li>Entre 2.001 y 2.003, DIRECTOR DE LA CIA. DE SEGUROS WINTERTHUR EN PORTUGAL.</li>
        <li>Entre 1.992-1.997, DIRECTOR GENERAL DEL GRUPO ASEGURADOR ALEMÁN GRUPO AACHENER UND MUNCHENER.</li>
        <li>Entre 1.991-1.992, DIRECTOR DE SINIESTROS DE AURORA POLAR, CIA. DE SEGUROS.</li>
        <li>En 1.991 MIEMBRO DEL CONSEJO DE OFESAUTO.</li>
        <li>Entre 1986-1.991, DIRECTOR DEL DEPARTAMENTO DE RESPONSABILIDAD CIVIL DE SWISS REASUNRANCE.</li>
        <li>Entre 1.984-1.986. DIRECTOR DE LA ASESORÍA JURÍDICA DEL GRUPO ASSITALIA.</li>
        <li>Entre 1.981-1.983, COORDINADOR GENERAL DEL CONVENIO DE COLABORACION ENTRE EL REINO DE ESPAÑA Y LA REPUBLICA DE VENEZUELA.<br />
        </li>
      </ul>
      <h5>OTRAS ACTIVIDADES PROFESIONALES</h5>
      <ul class="lista_datos" style="margin-bottom: 25px;">
        <li>Ponente en más de doscientos Congresos en España, Alemania, Suiza, Italia, Francia, Reino Unido, Puerto Rico, Venezuela, Colombia.</li>
        <li>Profesor De Derecho de Seguros.</li>
        <li>Tres librospublicados sobre el Seguro de Responsabilidad Civil, así como numerosos artículos y estudios monográficos en Revistas de Seguros y de Información Económica.</li>
      </ul>

				
				
					
				
				</div>



				</div>
			</div>







    	</div>
	</section>
</main>
