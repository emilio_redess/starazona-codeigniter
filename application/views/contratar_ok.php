
 
      <!-- Page Content-->
      <main class="page-content">
              <section class="bg-image-06">
                <div class="breadcrumb-wrapper">
                  <div class="shell context-dark section-30 section-lg-top-120">
                    <h5>Seguros</h5>
                    <h1 class="offset-top-20 text-ubold">Seguros de <?php echo $ramo; ?></h1>
                    <ol class="breadcrumb">
                      <li><?php echo anchor('inicio','Inicio'); ?></li>
                      <li>Seguros</li>
                      <li>Seguro de <?php echo $ramo; ?>
                      </li>
                    </ol>
                  </div>
                </div>
              </section>

        <section class="section-30 section-md-30">
          <div class="shell shell-wide text-lg-left">
            <div class="range">


  <div class="container">

              <div class="cell-md-7 cell-lg-5 section-md-80 section-lg-120">
                <h2 class="text-ubold">Contrata tu seguro</h2>
                <hr class="divider divider-md-left divider-primary divider-80">
                <p class="offset-top-20 offset-md-top-40">
              
                  Hemos recibido los datos del formulario. En breve, uno de nuestros asesores se pondrá en contacto contigo a través del número de teléfono facilitado.</p>
              </div>







  </div>              




            </div>
          </div>
        </section>
      </main>