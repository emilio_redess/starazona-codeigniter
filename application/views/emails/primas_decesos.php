<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body style="margin: 0; padding: 0;">



<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <table align="center" border="1" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                <tr>
                    <td align="center" bgcolor="#C4BEBE" style="padding: 40px 0 30px 0;">
                        <?php echo img(array('src'=>EMAIL_RUTA_IMAGENES . 'logotipo.png', 'alt'=> 'Starazona. Correduría de seguros.', 'style'=>'display:block;')); ?>
                    </td>
                </tr>
                <tr>
 
                  <td bgcolor="#ffffff" style="padding: 40px 10px 40px 10px;">

                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tr>
                              <td>
                                  Apreciado usuario/a:<br>Gracias por mostrar interés en comparar tu seguro de Decesos a través de nuestra web.
                              </td>
                          </tr>
                          <tr>
                              <td style="padding: 20px 0 10px 0;text-align:center;font-size:1.3em;color:#002d59;font-weight:bold;">
                                  TE MEJORAMOS EL PRECIO DE LA COMPETENCIA
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                      <tr>                    
                                          <td style="color:#002d59;font-weight:bold;padding: 0px 0 30px 0;">                         
                                              Comparación de seguros de decesos                        
                                          </td>                         
                                          <td style="font-size: 0; line-height: 0;" width="20">                         
                                              &nbsp;                         
                                          </td>                         
                                          <td style="color:#002d59;font-weight:bold;padding: 0px 0 30px 0;">                         
                                              Código de referencia: <?php echo $codtar; ?>                        
                                          </td>                         
                                      </tr>                         
                                  </table>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <table align="center" border="1" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                                      <tr>
                                        <th>Compañía</th>
                                        <th>Prima anual</th>
                                        <th>Capital asegurado</th>
                                      </tr>
                                  <?php





                                      foreach ($primas as $k => $v) {

                                        if ($v["nombreProducto"] == NULL) $prima_celda = "-<br>";
                                        else $prima_celda = $v["prima_12m"] . "€<br>";

                                             


                                        echo '<tr>';
                                        echo '<td style="text-align:center;">';                                      
                                        echo img(array('src'=>EMAIL_RUTA_IMAGENES . 'aseguradoras/' . $v["logoProducto"], 'alt'=> $v['nombreCia'] . '->' . $v['nombreProducto']));
                                        echo '</td>';

                                        echo '<td style="text-align:center;">';
                                        echo $prima_celda;
                                        echo "</td>";
                                        echo '<td style="text-align:center;">';
                                        echo $v["capital"] . "€<br>";
                                        echo "</td>";     
                                        echo "</tr>";     

                                        
                                      }
                                  ?>
                                  </table>
                              </td>
                          </tr>
                          <tr>                          
                              <td style="padding: 20px 0 30px 0;">
                                  Precios anuales por persona (impuestos incluídos).
                              </td>
                          </tr>  
                          <tr>
                              <td style="padding: 20px 0 30px 0;">
                                Los precios visualizados en el siguiente presupuesto pueden sufrir una variación en función de la veracidad de la información que nos has facilitado.
                              </td>
                          </tr>                              
                      </table>
                  </td>
 
    </tr>
 
    <tr>
 
        <td bgcolor="#ffffff" style="padding: 0px 10px 40px 10px;">
 
            <p style="font-weight:bold;color:#002d59;">LOS DATOS DE TU COMPARATIVA</p>
            Nombre: <?php echo $nombreApellidos; ?><br>      
            Email: <?php echo $email; ?><br>      
            Telefono: <?php echo $telefono; ?><br>      
        </td>
 
    </tr>

    <tr>
 
        <td bgcolor="#ffffff" style="padding: 0px 10px 40px 10px;">
          <p style="font-weight:bold;color:#002d59;">COMO CONTRATAR TU SEGURO</p>
          <p>Puedes contratar tu seguro de decesos poniéndote en contacto con nuestros asesores por teléfono.</p>
            <p>Utilizando tu numero de referencia <?php echo $codtar; ?>, nuestros asesores están a tu disposición.</p>
            <p><strong>Teléfono:</strong> <?php echo TELEFONO_CONTACTO; ?></p>
            <p><strong>Email:</strong> <?php echo EMAIL_CONTACTO; ?></p>

 
        </td>
 
    </tr>

    <tr>
 
        <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
 
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
             <tr>
            <td width="75%" style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">            
             &reg; <?php echo anchor("http://www.starazona.com/",'Starazona',array('style'=>'color: #ffffff;')) ?>, <?php echo date("Y"); ?><br/>  

            </td>             
            <td align="right">             
          
            </td>             
             </tr>             
            </table> 
        </td> 
    </tr>         
</table>
        </td>
    </tr>
</table>

</body>
</html>