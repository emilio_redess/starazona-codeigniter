<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body style="margin: 0; padding: 0;">



<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <table align="center" border="1" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                <tr>
                    <td align="center" bgcolor="#C4BEBE" style="padding: 40px 0 30px 0;">
                        <?php echo img(array('src'=>EMAIL_RUTA_IMAGENES . 'logo.png', 'alt'=> '','width'=> '300', 'style'=>'display:block;')); ?>
                    </td>
                </tr>

 
                <tr>
             
                    <td bgcolor="#ffffff" style="padding: 0px 10px 40px 10px;">
             
                        <p style="font-weight:bold;color:#002d59;">DATOS DEL FORMULARIO DE CONTACTO</p>
                        Nombre: <?php echo $nombre . " " . $apellidos; ?><br>      
                        Email: <?php echo $email; ?><br>      
                        Telefono: <?php echo $telefono; ?><br>
                        Localidad: <?php echo $localidad; ?><br>
                        Provincia: <?php echo $provincia; ?><br>
                        Mensaje: <?php echo $mensaje; ?><br>           
                      
                   
             
                    </td>
             
                </tr>

    

                <tr>
             
        <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
 
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
             <tr>
            <td width="75%" style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">            
             &reg; <?php echo anchor("http://www.starazona.com/",'Starazona',array('style'=>'color: #ffffff;')) ?>, <?php echo date("Y"); ?><br/>  

            </td>             
            <td align="right">             
             <table border="0" cellpadding="0" cellspacing="0">
              <tr>               
               
               <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>               
               <td>               
              

                 <?php echo anchor(DIRECCION_FACEBOOK,img(array('src'=>EMAIL_RUTA_IMAGENES . 'social-facebook.png', 'alt'=> 'Facebook','width'=> '38', 'height' =>'38', 'style'=>'display: block;','border'=> '0'))); ?>             
                </a>               
               </td>               
              </tr>               
               </table>            
            </td>             
             </tr>             
            </table> 
        </td> 
                </tr>         
            </table>
        </td>
    </tr>
</table>

</body>
</html>