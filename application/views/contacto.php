<!-- Page Content-->
<main class="page-content">
        <section class="bg-image-06">
          <div class="breadcrumb-wrapper">
            <div class="shell context-dark section-30 section-lg-top-100">
              <!--<h5>Contact or Visit Us</h5>-->
              <h1 class="offset-top-20 text-ubold">Contacto</h1>
            
            </div>
          </div>
        </section>
  <section>
    <div class="shell"></div>
  </section>
  <section class="section-top-80 section-md-top-0">
    <div class="shell shell-wide text-md-left">
      <div class="range">
        <div class="cell-md-7 cell-lg-5 section-md-80 section-lg-120">
          <h2 class="text-ubold">Contacta con nosotros</h2>
          <hr class="divider divider-md-left divider-primary divider-80">
          <p class="offset-top-20 offset-md-top-40">Puedes contactar con nosotros de la forma que mejor te convenga, ya sea a traves del siguiente formulario, por teléfono, o visitando nuestras oficinas.</p>
          <!-- RD Mailform-->
         <?php echo form_open_multipart('contacto', array('class' => 'offset-top-30 range text-left')); ?>
            <div class="cell-sm-6">
              <div class="form-group">
                <label for="contact-name" class="form-label form-label-outside">Nombre</label>
                <input id="contact-name" type="text" name="name" class="form-control form-control-gray">
              </div>
            </div>
            <div class="cell-sm-6 offset-top-20 offset-sm-top-0">
              <div class="form-group">
                <label for="contact-surname" class="form-label form-label-outside">Apellidos</label>
                <input id="contact-surname" type="text" name="surname" class="form-control form-control-gray">
              </div>
            </div>
            <div class="cell-sm-6 offset-top-20">
              <div class="form-group">
                <label for="contact-email" class="form-label form-label-outside">E-mail</label>
                <input id="contact-email" type="email" name="email" class="form-control form-control-gray">
              </div>
            </div>
            <div class="cell-sm-6 offset-top-20">
              <div class="form-group">
                <label for="contact-phone" class="form-label form-label-outside">Teléfono</label>
                <input id="contact-phone" type="text" name="phone" class="form-control form-control-gray">
              </div>
            </div>
            <div class="cell-md-12 offset-top-20">
              <div class="form-group">
                <label for="contact-message" class="form-label form-label-outside">Mensaje</label>
                <textarea id="contact-message" name="message" class="form-control form-control-gray"></textarea>
              </div>
              <div class="offset-top-20 text-center text-md-left">
                <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
              </div>
            </div>
          </form>



        <section class="section-80 section-lg-120 bg-gray-lighter">
          <div class="shell shell-wide">
            <div class="range range-xs-center">
              <div class="cell-md-8 cell-lg-6">
                
                 <div id="formcontact">               
                    
                    <a name="tlfs"><h3 class="verde">Teléfonos de asistencia 24h</h3></a><br><br>

                    <h5>Auto, Hogar, Comercio, Comunidades</h5><br><br>

                    <table border="0" style="text-align: left;">
                   
                      <tbody><tr>
                        <td width="250px"><span class="negrofuerte">ZURICH</span></td>
                        <td width="130px"><?php echo ASISTENCIA_ZURICH; ?></td>
                      
                      </tr>

                      <tr>
                        <td><span class="negrofuerte">GENERALI</span></td>
                        <td><?php echo ASISTENCIA_GENERALI; ?></td>
                       
                      </tr>

                      <tr>
                        <td><span class="negrofuerte">PELAYO</span></td>
                        <td><?php echo ASISTENCIA_PELAYO; ?></td>
                      
                      </tr>

                      <tr>
                        <td><span class="negrofuerte">ALLIANZ</span></td>
                        <td><?php echo ASISTENCIA_ALLIANZ; ?></td>
                      
                      </tr>

                      <tr>
                        <td><span class="negrofuerte">MAPFRE</span></td>
                        <td><?php echo ASISTENCIA_MAPFRE; ?></td>
                      
                      </tr>

                      <tr>
                        <td><span class="negrofuerte">AXA</span></td>
                        <td><?php echo ASISTENCIA_AXA; ?></td>
                      
                      </tr>

                      <tr>
                        <td><span class="negrofuerte">REALE</span></td>
                        <td><?php echo ASISTENCIA_REALE; ?></td>
                       
                      </tr>

                      <tr>
                        <td><span class="negrofuerte">CASER</span></td>
                        <td><?php echo ASISTENCIA_CASER; ?></td>
                      
                      </tr>

                      <tr>
                        <td><span class="negrofuerte">PLUS ULTRA</span></td>
                        <td><?php echo ASISTENCIA_PLUS_ULTRA; ?></td>
                      
                      </tr>

                      <tr>
                        <td><span class="negrofuerte">CATALANA OCCIDENTE</span></td>
                        <td><?php echo ASISTENCIA_CATALANA_OCCIDENTE; ?></td>
                       
                      </tr>

                    

                      <tr>
                        <td><span class="negrofuerte">SEGUROS BILBAO</span></td>
                        <td><?php echo ASISTENCIA_SEGUROS_BILBAO; ?></td>
                      
                      </tr>


                      <tr>
                        <td><span class="negrofuerte">HELVETIA</span></td>
                        <td><?php echo ASISTENCIA_HELVETIA; ?></td>
                      
                      </tr>

                      <tr>
                        <td><span class="negrofuerte">FIATC</span></td>
                        <td><?php echo ASISTENCIA_FIATC; ?></td>
                       
                      </tr>

                      <tr>
                        <td><span class="negrofuerte">LIBERTY</span></td>
                        <td><?php echo ASISTENCIA_LIBERTY; ?></td>
                        <td></td>
                      </tr>


                    </tbody></table>
                    

                    <br><br>
                    <h5>Decesos</h5>
                    <br><br>
                    <table border="0" style="text-align: left;">
                    
                     
                      <tbody><tr>
                        <td width="250px"><span class="negrofuerte">PREVENTIVA-EXPERTIA</span></td>
                        <td width="130px"><?php echo ASISTENCIA_PREVENTIVA; ?></td>                
                      </tr>

                      <tr>
                        <td><span class="negrofuerte">SANTALUCIA</span></td>
                        <td><?php echo ASISTENCIA_SANTALUCIA; ?></td>                
                      </tr>

                      <tr>
                        <td><span class="negrofuerte">HELVETIA</span></td>
                        <td><?php echo ASISTENCIA_HELVETIA; ?></td>                
                      </tr>

                      <tr>
                        <td><span class="negrofuerte">ACTIVE</span></td>
                        <td><?php echo ASISTENCIA_ACTIVE; ?></td>                
                      </tr>


                  </tbody></table>

                    <br><br>
                    <h5 style="text-align: left;">Servicio de emergencia</h5>
                    <br><br>
                    <table border="0" style="text-align: left;">
                    
                     
                      <tbody><tr>
                        <td width="250px"><span class="negrofuerte">POLICIA</span></td>
                        <td width="130px"><?php echo ASISTENCIA_POLICIA; ?></td>                
                      </tr>

                      <tr>
                        <td><span class="negrofuerte">GUARDIA CIVIL</span></td>
                        <td><?php echo ASISTENCIA_GUARDIA_CIVIL; ?></td>                
                      </tr>

                  </tbody></table>

                </div>
                
              </div>
            </div>
          </div>
        </section>







          
        </div>
        <div class="cell-xl-2 cell-lg-3 cell-md-4 cell-xl-preffix-1 section-md-80 section-lg-120">
          <div class="range text-left">
            <div class="cell-md-12 cell-xs-6">
              <h5 class="text-bold hr-title">Teléfono</h5>

              <div class="media">
                <div class="media-left"><span class="icon icon-sm text-info-dr mdi mdi-phone"></span></div>
                <div class="media-body">                
                  <div><a href="callto:<?php echo TELEFONO_CONTACTO_OFICINA_VALENCIA; ?>" class="text-gray"><?php echo TELEFONO_CONTACTO_OFICINA_VALENCIA; ?> - Productos&nbsp;especializados</a></div>                                   
                </div> 
              </div>



              <div class="media">

                <div class="media-left"><span class="icon icon-sm text-info-dr mdi mdi-phone"></span></div>
                <div class="media-body">
                  <div><a href="callto:<?php echo TELEFONO_CONTACTO_OFICINA_SEDAVI; ?>" class="text-gray"><?php echo TELEFONO_CONTACTO_OFICINA_SEDAVI; ?> - Sedaví</a></div>                               
                </div>
              </div>

              <div class="media">
                <div class="media-left"><span class="icon icon-sm text-info-dr mdi mdi-phone"></span></div>
                <div class="media-body">                
                  <div><a href="callto:<?php echo TELEFONO_CONTACTO_OFICINA_MASSANASSA; ?>" class="text-gray"><?php echo TELEFONO_CONTACTO_OFICINA_MASSANASSA; ?> - Massanassa</a></div>                                   
                </div> 
              </div>

              <div class="media">
                <div class="media-left"><span class="icon icon-sm text-info-dr mdi mdi-phone"></span></div>
                <div class="media-body">                
                  <div><a href="callto:<?php echo TELEFONO_CONTACTO_OFICINA_SILLA; ?>" class="text-gray"><?php echo TELEFONO_CONTACTO_OFICINA_SILLA; ?> - Silla</a></div>                                   
                </div>                 

              </div>



            </div>
            <div class="cell-md-12 cell-xs-6 offset-top-40 offset-xs-top-0 offset-md-top-60">
              <h5 class="text-bold hr-title">E-mail</h5>
              <div class="media">
                <div class="media-left"><span class="icon icon-sm text-info-dr mdi mdi-email-outline"></span></div>
                <div class="media-body">
                  <div><a href="mailto:<?php echo EMAIL_CONTACTO; ?>" class="text-gray"><?php echo EMAIL_CONTACTO; ?></a></div>
                </div>
              </div>
            </div>

        


            <div class="cell-md-12 cell-xs-6 offset-top-40 offset-md-top-60">
              <h5 class="text-bold hr-title">Cómo ir a nuestras oficinas</h5>





              <div class="media">
                <div class="media-left"><span class="icon icon-sm text-info-dr mdi mdi-map-marker"></span></div>
                <div class="media-body">
                  <div><a href="<?php echo site_url('contacto/loc/SED') ?>" class="text-gray"><?php echo DIRECCION_CONTACTO_SEDAVI; ?></a></div>
                </div>
              </div>

              <div class="media">
                <div class="media-left"><span class="icon icon-sm text-info-dr mdi mdi-map-marker"></span></div>
                <div class="media-body">
                  <div><a href="<?php echo site_url('contacto/loc/MAS') ?>" class="text-gray"><?php echo DIRECCION_CONTACTO; ?></a></div>
                </div>
              </div>              

              <div class="media">
                <div class="media-left"><span class="icon icon-sm text-info-dr mdi mdi-map-marker"></span></div>
                <div class="media-body">
                  <div><a href="<?php echo site_url('contacto/loc/SIL') ?>" class="text-gray"><?php echo DIRECCION_CONTACTO_SILLA; ?></a></div>
                </div>
              </div>                            




            </div>
            <div class="cell-md-12 cell-xs-6 offset-top-40 offset-md-top-60">
              <h5 class="text-bold hr-title">Horario</h5>
              <div class="media">
                <div class="media-left"><span class="icon icon-sm text-info-dr mdi mdi-calendar-clock"></span></div>
                <div class="media-body">
                  <div>Lunes–Viernes<br> 9:00h–14:00h<br>16:00h-19:30h</div>
                </div>
              </div>
            </div>
            <div class="cell-md-12 offset-top-40 offset-md-top-60">
              <h5 class="text-bold hr-title">Redes sociales</h5>
              <ul class="list-inline offset-top-24">                
                <!--<li><a href="<?php echo DIRECCION_FACEBOOK; ?>" target="_blank"><i class="fab fa-facebook-square fa-2x"></i></a></li>-->
                <li><a href="<?php echo DIRECCION_FACEBOOK; ?>" target="_blank"><?php echo img(array('src'=>'public/images/facebook.png','alt'=> 'Facebook','class' => 'img-responsive center-block facebook_logo')); ?></a></li>
                <!--<li><a href="<?php echo DIRECCION_LINKEDIN; ?>" target="_blank" class="icon text-gray icon-xxs fa-linkedin"></a></li>-->
          
              </ul>
            </div>
          </div>
        </div>
        <div class="cell-lg-3 cell-lg-preffix-1 rd-google-map-abs offset-top-40 offset-md-top-0">


<div id="map"></div>
<script>
      function initMap() {
        var uluru = {lat: <?php echo $lat; ?>, lng: <?php echo $lng; ?>};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: uluru,
          mapTypeId: 'hybrid'
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_MAPS_API_KEY; ?>&callback=initMap">
</script>
   

          <!-- RD Google Map-->
          <!--
          <div data-zoom="16" data-y="<?php echo $y; ?>" data-x="<?php echo $x; ?>" class="rd-google-map rd-google-map__model">
            <ul class="map_locations">
              <li data-x="<?php echo $x; ?>" data-y="<?php echo $y; ?>">
                <p>Plaza Horticultor Corset nº 12 - 46008 Valencia</p>
              </li>
            </ul>
          </div>
        -->







        </div>
      </div>
    </div>
  </section>
</main>

