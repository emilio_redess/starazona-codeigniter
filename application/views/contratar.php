
     
      <!-- Page Content-->
      <main class="page-content">
              <section class="bg-image-06">
                <div class="breadcrumb-wrapper">
                  <div class="shell context-dark section-30 section-lg-top-120">
                    <h5>Seguros</h5>
                    <h1 class="offset-top-20 text-ubold">Seguros de <?php echo $ramo; ?></h1>
                    <ol class="breadcrumb">
                      <li><?php echo anchor('inicio','Inicio'); ?></li>
                      <li>Seguros</li>
                      <li>Seguro de <?php echo $ramo; ?>
                      </li>
                    </ol>
                  </div>
                </div>
              </section>

        <section class="section-30 section-md-30">
          <div class="shell shell-wide text-lg-left">
            <div class="range">


  <div class="container">

              <div class="cell-md-7 cell-lg-5 section-md-80 section-lg-120">
                <h2 class="text-ubold">Contrata tu seguro</h2>
                <hr class="divider divider-md-left divider-primary divider-80">
                <p class="offset-top-20 offset-md-top-40">Contacta con nuestros asesores expertos. Puedes hacerlo llamando al <?php echo TELEFONO_CONTACTO; ?> o bien mediante el siguiente formulario para que te llamemos en el horario que nos indiques:</p>
                <!-- RD Mailform-->
              
                  <?php echo form_open_multipart('seguros/contratar/' . $ramo . '/' . $codtar . '/' . $nombreCia,array('class' => 'offset-top-30 range text-left')); ?>
                  <div class="cell-sm-12">
                    <div class="form-group">
                      <label for="nombre" class="form-label form-label-outside">Nombre y apellidos</label>
                      <input id="nombre" type="text" name="nombre" data-constraints="@Required" class="form-control form-control-gray">
                    </div>
                  </div>
     
                  <div class="cell-sm-6 offset-top-20">
                    <div class="form-group">
                      <label for="horario" class="form-label form-label-outside">Horario de llamada</label>
                      <select data-minimum-results-for-search="Infinity" class="form-control select-filter" id="horario" name="horario">
                        <option value="09:00h - 12:00h">09:00h - 12:00h</option>
                        <option value="12:00h - 15:00h">12:00h - 15:00h</option>
                        <option value="15:00h - 19:00h">15:00h - 19:00h</option>                                     
                      </select>                      
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-20">
                    <div class="form-group">
                      <label for="telefono" class="form-label form-label-outside">Teléfono</label>
                      <input id="telefono" type="text" name="telefono" data-constraints="@Required @Integer" class="form-control form-control-gray">
                    </div>
                  </div>
                  <div class="cell-md-12 offset-top-20">
        
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>
                </form>
              </div>







  </div>              




            </div>
          </div>
        </section>
      </main>