
      
      <!-- Page Content-->
      <main class="page-content">
              <section class="bg-image-06">
                <div class="breadcrumb-wrapper">
                  <div class="shell context-dark section-30 section-lg-top-120">
                   
                    <h1 class="offset-top-20 text-ubold">Términos de uso</h1>
                   
                  </div>
                </div>
              </section>


        <!-- Privacy Policy-->
        <section class="text-md-left section-80 section-md-120">
          <div class="shell shell-wide">
            <div class="range range-xs-center">
              <div class="cell-md-9 cell-lg-7 cell-xl-6">
                <!-- Terms-list-->
                <dl class="list-terms">
                  <dt class="h5">Información general</dt>
                  <dd>

            De conformidad con en el Reglamento 2016/679 del Parlamento Europeo y del Consejo de 27 de Abril de 2016 y con la Ley 15/1999 de Protección de Datos de Carácter Personal, le informamos de que los datos recabados serán tratados por Salvador Tarazona Correduría de Seguros SL con la única finalidad de poder realizar, de manera efectiva, la gestión de los contratos intermediados, y que tales datos serán utilizados por nuestra parte, siempre que sea estrictamente necesario para la finalidad antes indicada, así como para remitirle información promocional y publicitaria con relación a la misma. Todos los campos del formulario tienen carácter obligatorio. La negativa a facilitar los datos solicitados impedirá su registro en el sistema, no conservándose ninguno de los datos que hubiera informado.             
                  </dd>

                                                                                                                                                         
                  
              </div>
            </div>
          </div>
        </section>

      </main>
