
      
      <!-- Page Content-->
      <main class="page-content">
              <section class="bg-image-06">
                <div class="breadcrumb-wrapper">
                  <div class="shell context-dark section-30 section-lg-top-120">
                    <h5>Política de privacidad</h5>
                    <h1 class="offset-top-20 text-ubold">Términos de uso</h1>
                    <ol class="breadcrumb">
                      
                      <li><?php echo anchor('inicio','Inicio'); ?></li>
                      <li>términos de uso
                      </li>
                    </ol>
                  </div>
                </div>
              </section>


        <!-- Privacy Policy-->
        <section class="text-md-left section-80 section-md-120">
          <div class="shell shell-wide">
            <div class="range range-xs-center">
              <div class="cell-md-9 cell-lg-7 cell-xl-6">
                <!-- Terms-list-->
                <dl class="list-terms">
                  <dt class="h5">Información general</dt>
                  <dd>
<h3>Derecho de Información</h3>
<p>Si el usuario decide registrarse en Salvador Tarazona Correduría de Seguros SL, se solicitarán los datos estrictamente necesarios para la consecución de los fines y finalidades a los que está destinado el sitio Web. A tales efectos se requiere que los usuarios cumplimenten un formulario electrónico, lo cual conlleva facilitar datos de carácter personal necesarios para las funciones y finalidades descritas. El tratamiento de los datos de los usuarios está enfocado a la consecución de estos fines, siempre dentro del marco normativo establecido.
Los datos que facilite el usuario serán tratados por Salvador Tarazona Correduría de Seguros SL</p>

<h3>Consentimiento del Usuario</h3>
<p>Al pulsar en el formulario de Alta "ENVIAR Y SIGUIENTE" el usuario da su consentimiento inequívoco y expreso al tratamiento de sus datos personales conforme a las finalidades y servicios que presta Salvador Tarazona Correduría de Seguros SL.
Los usuarios autorizan a Salvador Tarazona Correduría de Seguros SL a acceder a sus datos personales, independientemente de su nivel de privacidad, a fin y efecto de poder informarles de ofertas de seguros concretas e idóneas a su perfil.</p>
<h3>Comunicaciones</h3>
<p>Salvador Tarazona Correduría de Seguros SL, establece el mecanismo mediante el cual los usuarios que lo soliciten puedan excluirse de los servicios de comunicaciones electrónicas de forma sencilla y gratuita. Para ello, podrán excluirse de este servicio siguiendo las instrucciones que se indican en el pie del cuerpo de los correos electrónicos.</p>
<h3>Cookies e IPs</h3>
<p>El usuario acepta el uso de cookies y seguimientos de IPs. Nuestro analizador de tráfico del site utiliza cookies y seguimientos de IPs que nos permiten recoger datos a efectos estadísticos: fecha de la primera visita, número de veces que se ha visitado, fecha de la última visita, URL y dominio de la que proviene, explorador utilizado y resolución de la pantalla. No obstante, el usuario si lo desea puede desactivar y/o eliminar estas cookies siguiendo las instrucciones de su navegador de Internet.</p>
<h3>Derecho de, Oposición, Acceso, Rectificación, Cancelación, Portabilidad y Derecho al Olvido</h3>
<p>El usuario tiene derecho a acceder a sus datos personales, a rectificarlos si los datos son erróneos y a darse de baja de los servicios de Salvador Tarazona Correduría de Seguros SL. Así, el usuario puede revocar su consentimiento en cualquier momento por causa justificada, pero no se podrán atribuir efectos retroactivos.</p>

<h3>Departamento de atención al Usuario</h3>
<p>seguros@starazona.com</p>


<p>o en la siguiente dirección de correo postal:</p>
<p>Avda. de la Albufera 33, 46910 Sedaví (Valencia)</p>
<p>Salvador Tarazona Correduría de Seguros SL cumple íntegramente con la legislación vigente en materia de protección de datos de carácter personal, y con los compromisos de confidencialidad propios de su actividad.</p>
<p>El tratamiento de los datos personales y el envío de comunicaciones por medios electrónicos están ajustados a la normativa establecida en materia de Protección de Datos de Carácter Personal y en la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y de Comercio Electrónico (B.O.E. 12/07/2002).<p>
<h4>SALVADOR TARAZONA CORREDURIA DE SEGUROS SL</h4>
<p>B-96673041</p>
<p>Inscrito Registro Dirección General de Seguros y Fondo de Pensiones del Ministerio de Economía y Hacienda con el n.º J-1672 y en el Registro Mercantil de Valencia Tomo 5893 Sección 8 Folio 133 Hoja V56482 Inscripción 1</p>
<p>DIRECCION Avda. Blasco Ibañez 127, 46910 Massanassa (Valencia)</p>
<p>Salvador Tarazona Correduría de Seguros S.L. Inscrito Registro Dirección General de Seguros y Fondos de Pensiones del Ministerio y Hacienda con el n. J-1672. .Corredor titulado n. 11.608 27/02/1986 Puede contactar con los citados organismos para cualquier comprobación en el Teléfono 913397059. Cumpliendo lo establecido en la Ley 26/2006. Inicio actividad y titulación como corredor de seguros 1985. Constitución como sociedad de correduría de seguros en 1997.</p>




                  </dd>

                                                                                                                                                         
                  
              </div>
            </div>
          </div>
        </section>

      </main>
