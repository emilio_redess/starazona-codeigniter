
      <!-- Page Content-->
      <main class="page-content">
      	<!--
              <section class="bg-image-06">
                <div class="breadcrumb-wrapper">
                  <div class="shell context-dark section-30 section-lg-top-100">
                   
                    <h1 class="offset-top-20 text-ubold">Preguntas más frecuentes</h1>
                   
                  </div>
                </div>
              </section> -->
        <section class="section-80 section-md-120 bg-image-02">
          <div class="shell shell-wide text-lg-left">
            <div class="range">
              <div class="cell-md-7 cell-lg-5 cell-lg-preffix-6 cell-md-preffix-5">
                <h2 class="text-ubold">Preguntas más frecuentes en caso de accidente</h2>
                <hr class="divider divider-md-left divider-primary divider-80">
                <!-- Responsive-tabs-->
                <div data-type="accordion" class="offset-top-50 offset-lg-top-90 responsive-tabs responsive responsive-tabs-classic">
                  <ul class="resp-tabs-list tabs-1 text-center tabs-group-default">
                    <li>¿Si tengo un accidente, ¿cómo os lo comunico?</li>
                    <li>¿Cómo puedo dar un parte en caso de accidente?</li>
                    <li>¿Dónde puedo reparar mi vehículo?</li>
                    <li>¿Cómo se perita mi coche?</li>
                    <li>En caso de siniestro total, ¿cuál será el valor de mi vehículo?</li>
                    <li>Y en caso de robo?</li>
                    <li>¿A que velocidad saltan los radares de carretera?</li>
                    <li>Si voy a conducir, ¿cuantas bebidas alcoholicas puedo tomar?</li>
                  </ul>
                  <div class="resp-tabs-container text-sm-left tabs-group-default">
                    <div>
                      <p>Llámanos al <?php echo TELEFONO_CONTACTO_OFICINA_MASSANASSA; ?> (Servicio de Gestión de Accidentes). Nuestro personal especializado te atenderá de forma personalizada y te Informará y guiará de manera muy sencilla, para una pronta resolución de tu caso.</p>
                    </div>
                    <div>
                      <p>En nuestro Pack de Bienvenida encontrarás el modelo de parte que debes utilizar (declaración amistosa de accidente). Es importante que lo rellenes bien. Señala la causa del accidente y dibuja un pequeño croquis reflejando cómo se produjo la colisión; no olvides incluir los datos y la firma de los dos conductores, y envíanoslo lo antes posible. La utilización del parte amistoso agiliza los trámites entre las compañías de seguros.</p>
                    </div>
                    <div>

					<p>Al tener libre elección donde o con quien quieras realizar tu reparación,  puedes elegir el taller donde quieras llevarlo o sino tienes ningún interés en especial  te facilitaremos una lista de talleres concertados y de probada experiencia que estén lo más próximo a tu domicilio. Llámanos al <?php echo TELEFONO_CONTACTO_OFICINA_MASSANASSA; ?>  (Servicio de Gestión de Accidentes) y te daremos  una pronta y satisfactoria reparación de tu vehículo, daños en tu hogar, comercio o comunidad de vecinos.</p>


                    </div>
                    <div>
                      <p>En 24 h. A través de una de las mejores redes de peritación de nuestro país. Tú nos indicas el día y hora de peritación, así como el taller que has elegido, y los técnicos se desplazan en 24 horas laborables (24 horas de lunes a jueves; si nos avisas el viernes te atenderán el lunes, como en cualquier compañía que garantiza este servicio en 24 h).</p>
                    </div>
                    <div>
                      <p>Durante los 2 primeros años tras la primera matriculación, la indemnización es a valor nuevo (lo que te costaría comprarte otro vehículo nuevo, igual). Si hace más de 2 años de la primera matriculación, a valor venal (lo que valía tu vehículo antes del accidente).</p>
                    </div>
                    <div>
                      <p>Si te roban el vehículo, lo primero que tienes que hacer es acudir a la Comisaría más cercana para presentar la denuncia, solicita una copia y envíanosla rápidamente al Servicio de Gestión de Accidentes, por fax o por <a href="mailto:<?php echo EMAIL_CONTACTO2; ?>">email</a>.</p>
                    </div>  
                    <div>
                    	<?php echo img(array('src'=>'public/images/faq_tolerancia_radares.png','alt'=> '','class' => 'img-responsive center-block')); ?>
                    	<p>* Información proporcionada por la DGT</p>

                    </div>

                    <div>
                    	<p>La siguiente tabla discrimina por sexo y peso los niveles de alcoholemia según el tipo y cantidad de alcohol ingerido. Y las etiquetas señalan varias opciones:
                    		<ul>
                    			<li><b><span style="color:green;">Verde:</span></b> Alcoholemia positiva poco probable.</li>
                    			<li><b><span style="color:orange;">Naranja:</span></b> Alcoholemia positiva bastante probable.</li>
                    			<li><b><span style="color:red;">Rojo:</span></b> Alcoholemia positiva muy probable.</li>
                    		</ul></p>

                    		<p><sup>1</sup> Así, por ejemplo, <b>un hombre de entre 70 y 90 kilos podría tomar un tercio de cerveza</b>, o dos copas de vino o vermú y la alcoholemia positiva sería poco probable. En el caso de una mujer de entre 50 y 70 kilos, los límites para no dar positivo serían: un tercio de cerveza, una copa de vino o una copa de vermú.</p>
                    	
                    	<?php echo img(array('src'=>'public/images/faq_alcohol.png','alt'=> '','class' => 'img-responsive center-block')); ?>
                    	<br><br>
                    	<p><sup>1</sup> Datos estimados. Puede haber otros factores que afecten a estas cantidades. No se deben usar nunca como referencia exacta.</p>
                    </div>                  
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="section-80 section-lg-120 bg-gray-lighter">
          <div class="shell shell-wide">
            <div class="range range-xs-center">
              <div class="cell-md-8 cell-lg-6">
                
				         <div id="listado_asistencia">				        
				            
				            <a name="tlfs"><h3 class="verde">Teléfonos de asistencia 24h</h3></a><br><br>

				            <h5  style="text-align: left;">Auto, Hogar, Comercio, Comunidades</h5><br><br>

				            <table border="0" style="text-align: left;">
				           
				              <tbody><tr>
				                <td class="telefonos_ancho"><span class="negrofuerte">ZURICH</span></td>
				                <td ><?php echo ASISTENCIA_ZURICH; ?></td>
				              
				              </tr>

				              <tr>
				                <td><span class="negrofuerte">GENERALI</span></td>
				                <td><?php echo ASISTENCIA_GENERALI; ?></td>
				               
				              </tr>

				              <tr>
				                <td><span class="negrofuerte">PELAYO</span></td>
				                <td><?php echo ASISTENCIA_PELAYO; ?></td>
				              
				              </tr>

				              <tr>
				                <td><span class="negrofuerte">ALLIANZ</span></td>
				                <td><?php echo ASISTENCIA_ALLIANZ; ?></td>
				              
				              </tr>

				              <tr>
				                <td><span class="negrofuerte">MAPFRE</span></td>
				                <td><?php echo ASISTENCIA_MAPFRE; ?></td>
				              
				              </tr>

				              <tr>
				                <td><span class="negrofuerte">AXA</span></td>
				                <td><?php echo ASISTENCIA_AXA; ?></td>
				              
				              </tr>

				              <tr>
				                <td><span class="negrofuerte">REALE</span></td>
				                <td><?php echo ASISTENCIA_REALE; ?></td>
				               
				              </tr>

				              <tr>
				                <td><span class="negrofuerte">CASER</span></td>
				                <td><?php echo ASISTENCIA_CASER; ?></td>
				              
				              </tr>

				              <tr>
				                <td><span class="negrofuerte">PLUS ULTRA</span></td>
				                <td><?php echo ASISTENCIA_PLUS_ULTRA; ?></td>
				              
				              </tr>

				              <tr>
				                <td><span class="negrofuerte">CATALANA OCCIDENTE</span></td>
				                <td><?php echo ASISTENCIA_CATALANA_OCCIDENTE; ?></td>
				               
				              </tr>

			              

				              <tr>
				                <td><span class="negrofuerte">SEGUROS BILBAO</span></td>
				                <td><?php echo ASISTENCIA_SEGUROS_BILBAO; ?></td>
				              
				              </tr>


				              <tr>
				                <td><span class="negrofuerte">HELVETIA</span></td>
				                <td><?php echo ASISTENCIA_HELVETIA; ?></td>
				              
				              </tr>

				              <tr>
				                <td><span class="negrofuerte">FIATC</span></td>
				                <td><?php echo ASISTENCIA_FIATC; ?></td>
				               
				              </tr>

				              <tr>
				                <td><span class="negrofuerte">LIBERTY</span></td>
				                <td><?php echo ASISTENCIA_LIBERTY; ?></td>
				                <td></td>
				              </tr>


				            </tbody></table>
				            

				            <br><br>
				            <h5 style="text-align: left;">Decesos</h5>
				            <br><br>
				            <table border="0" style="text-align: left;">
				            
				             
				              <tbody><tr>
				                <td class="telefonos_ancho"><span class="negrofuerte">PREVENTIVA-EXPERTIA</span></td>
				                <td><?php echo ASISTENCIA_PREVENTIVA; ?></td>                
				              </tr>

				              <tr>
				                <td><span class="negrofuerte">SANTALUCIA</span></td>
				                <td><?php echo ASISTENCIA_SANTALUCIA; ?></td>                
				              </tr>

				              <tr>
				                <td><span class="negrofuerte">HELVETIA</span></td>
				                <td><?php echo ASISTENCIA_HELVETIA; ?></td>                
				              </tr>

				              <tr>
				                <td><span class="negrofuerte">ACTIVE</span></td>
				                <td><?php echo ASISTENCIA_ACTIVE; ?></td>                
				              </tr>


				          </tbody></table>


				            <br><br>
				            <h5 style="text-align: left;">Servicio de emergencia</h5>
				            <br><br>
				            <table border="0" style="text-align: left;">
				            
				             
				              <tbody><tr>
				                <td class="telefonos_ancho"><span class="negrofuerte">POLICIA</span></td>
				                <td ><?php echo ASISTENCIA_POLICIA; ?></td>                
				              </tr>

				              <tr>
				                <td><span class="negrofuerte">GUARDIA CIVIL</span></td>
				                <td><?php echo ASISTENCIA_GUARDIA_CIVIL; ?></td>                
				              </tr>

				          </tbody></table>
				        </div>
              </div>
            </div>
          </div>
        </section>
      </main>
