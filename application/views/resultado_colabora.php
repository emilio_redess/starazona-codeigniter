

      <!-- Page Content-->
      <main class="page-content">
              <section class="bg-image-06">
                <div class="breadcrumb-wrapper">
                  <div class="shell context-dark section-30 section-lg-top-120">
                    <!--<h5>Contact or Visit Us</h5>-->
                    <h1 class="offset-top-20 text-ubold">Contacto</h1>
                    <ol class="breadcrumb">
                      
                      <li><?php echo anchor('inicio','Inicio'); ?></li>
                      <li>Colabora con nosotros</li>               
                    </ol>
                  </div>
                </div>
              </section>
        <section>
          <div class="shell"></div>
        </section>
        <section class="section-top-80 section-md-top-0">
          <div class="shell shell-wide text-md-left">
            <div class="range">
              <div class="cell-md-7 cell-lg-5 section-md-80 section-lg-120">
                <h2 class="text-ubold">Colabora con nosotros</h2>
                <hr class="divider divider-md-left divider-primary divider-80">
                <p class="offset-top-20 offset-md-top-40">Hemos recibido los datos del formulario. En breve nos pondremos en contacto.</p>
                <!-- RD Mailform-->
   


              </div>
              <div class="cell-xl-2 cell-lg-3 cell-md-4 cell-xl-preffix-1 section-md-80 section-lg-120">
                <div class="range text-left">
                  <div class="cell-md-12 cell-xs-6">
                    <h5 class="text-bold hr-title">Teléfono</h5>
                    <div class="media">
                      <div class="media-left"><span class="icon icon-sm text-info-dr mdi mdi-phone"></span></div>
                      <div class="media-body">
                        <div><a href="callto:<?php echo TELEFONO_CONTACTO; ?>" class="text-gray"><?php echo TELEFONO_CONTACTO; ?></a></div>
                        <!--<div><a href="callto:#" class="text-gray">1-800-9876-543</a></div>-->
                      </div>
                    </div>
                  </div>
                  <div class="cell-md-12 cell-xs-6 offset-top-40 offset-xs-top-0 offset-md-top-60">
                    <h5 class="text-bold hr-title">E-mail</h5>
                    <div class="media">
                      <div class="media-left"><span class="icon icon-sm text-info-dr mdi mdi-email-outline"></span></div>
                      <div class="media-body">
                        <div><a href="mailto:<?php echo EMAIL_CONTACTO; ?>" class="text-gray"><?php echo EMAIL_CONTACTO; ?></a></div>
                      </div>
                    </div>
                  </div>

              


                  <div class="cell-md-12 cell-xs-6 offset-top-40 offset-md-top-60">
                    <h5 class="text-bold hr-title">Dirección</h5>
                    <div class="media">
                      <div class="media-left"><span class="icon icon-sm text-info-dr mdi mdi-map-marker"></span></div>
                      <div class="media-body">
                        <div><a href="#" class="text-gray"><?php echo DIRECCION_CONTACTO; ?></a></div>
                      </div>
                    </div>
                  </div>
                  <div class="cell-md-12 cell-xs-6 offset-top-40 offset-md-top-60">
                    <h5 class="text-bold hr-title">Horario</h5>
                    <div class="media">
                      <div class="media-left"><span class="icon icon-sm text-info-dr mdi mdi-calendar-clock"></span></div>
                      <div class="media-body">
                        <div>Lunes–Viernes<br> 9:00h–14:00h<br>16:00h-19:00h</div>
                      </div>
                    </div>
                  </div>
                  <div class="cell-md-12 offset-top-40 offset-md-top-60">
                    <h5 class="text-bold hr-title">Redes sociales</h5>
                    <ul class="list-inline offset-top-24">
                      <li><a href="<?php echo DIRECCION_FACEBOOK; ?>" target="_blank" class="icon text-gray icon-xxs fa-facebook"></a></li>
                      <li><a href="<?php echo DIRECCION_LINKEDIN; ?>" target="_blank" class="icon text-gray icon-xxs fa-linkedin"></a></li>
                
                    </ul>
                  </div>
                </div>
              </div>
              <div class="cell-lg-3 cell-lg-preffix-1 rd-google-map-abs offset-top-40 offset-md-top-0">
                <!-- RD Google Map-->
                <div data-zoom="16" data-y="39.454112" data-x="-0.358096" class="rd-google-map rd-google-map__model">
                  <ul class="map_locations">
                    <li data-x="-0.358096" data-y="39.454112">
                      <p>Plaza Horticultor Corset nº 12 - 46008 Valencia</p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>

