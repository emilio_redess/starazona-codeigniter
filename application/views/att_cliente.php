
      
<!-- Page Content-->
<main class="page-content">
	<section class="bg-image-06">
    	<div class="breadcrumb-wrapper">
      		<div class="shell context-dark section-30 section-lg-top-100">
		       
		        <h1 class="offset-top-20 text-ubold">Atención al cliente</h1>
		       
      		</div>
    	</div>
  	</section>

	<section class="section-80 section-md-120">
	  	<div class="shell shell-wide text-lg-left">
		    <div class="range">
		        <div class="inset-lg-left-80">

		          	<div class="col-sm-12">
						<?php echo img(array('src'=>'public/images/foto-martillo.png','alt'=> '','class' => 'img-responsive center-block')); ?>
					</div>


				<div class="col-sm-12 section-md-100">
					<p>En cumplimiento de la Orden ECO 734/2004 del 11 de marzo, sobre los departamentos y servicios de atención al cliente de las entidades financieras, esta correduría de seguros no ha querido escatimar medios económicos a la hora de nombrar a una de las mayores y prestigiosas firmas y a un gran profesional de renombre nacional e Internacional en representación de nuestro Departamento de Atención al Cliente y Defensor del Cliente, servicio que se ha querido exteriorizar para así asegurar aún más la total independencia en el veredicto de las resoluciones de las distintas reclamaciones que se pueden plantear, servicio que estará a disposición de nuestro cliente en la siguiente dirección:</p>

				
				
					
					<p>D. José Ignacio Hebrero Álvarez</br>
					C/ Barceló n° 1,1 Izda.</br>
					28004 Madrid</br>
					Telf. 91 532 00 09</br>
					Fax.: 91.522 78 94</br>
					<a href="http://www.hebreroyasociados.com/">www.hebreroyasociados.com</a></br>
					<a href="mailto:atencionalcliente@hebreroyasociados.com">atencionalcliente@hebreroyasociados.com</a></p>
				</div>

		          	<div class="col-sm-12">
						<?php echo img(array('src'=>'public/images/atencion_reglamento.jpg','alt'=> '','class' => 'img-responsive left-block')); ?><?php echo anchor('att_cliente/reglamento','Reglamento de funcionamiento'); ?>
						<?php echo img(array('src'=>'public/images/atencion_cv.jpg','alt'=> '','class' => 'img-responsive left-block')); ?><?php echo anchor('att_cliente/cv','CV D. José Ignacio Hebrero Álvarez'); ?>
					</div>


				</div>
			</div>







    	</div>
	</section>
</main>
