     <!-- Page Content-->
      <main class="page-content">
              <section class="bg-image-06">
                <div class="breadcrumb-wrapper">
                  <div class="shell context-dark section-30 section-lg-top-120">
                    <h5>Seguros</h5>
                    <h1 class="offset-top-20 text-ubold">Seguros de salud</h1>
                    <ol class="breadcrumb">
                      <li><?php echo anchor('inicio','Inicio'); ?></li>
                      <li>Seguros</li>
                      <li>Seguro de salud
                      </li>
                    </ol>
                  </div>
                </div>
              </section>

        <section class="section-80 section-md-120">
          <div class="shell shell-wide text-lg-left">
            <div class="range">


  <div class="container">
  <input id="product" type="hidden" value="Parrilla de precios de Seguro de salud" />
  <input id="ref" type="hidden" value="<?php echo $codigoTarificacion; ?>" />
  <input id="cia" type="hidden" value="" />


<div class="table-responsive">
  <table class="table table-striped table-hover table-bordered">
  <caption>Precios por persona y mes (impuestos incluídos)</caption>
  <thead>
    <tr style="background:linear-gradient(to bottom, #1e5799 0%,#dadada 0%,#bdbdbd 100%);">
      <th class='precio_parrilla'>Compañía</th>
      <th class='precio_parrilla'>Completo (prima mensual)</th>
      <th class='precio_parrilla'>Seguro médico (prima mensual)</th>
      <th class='precio_parrilla'>Contratación</th>
    </tr>
    </thead>
<?php

    foreach ($primas as $k => $v) {

        if ($v["nombreProducto_cm"] == NULL) $CM_celda = "-<br>";
        else $CM_celda = $v["prima_cm"] . "&euro;<br>". "Seguro médico";
        //else $CM_celda = $v["prima_cm"] . "&euro;<br>". $v["nombreProducto_cm"];

        if ($v["nombreProducto_re"] == NULL) $RE_celda = "-<br>";
        else $RE_celda = $v["prima_re"] . "&euro;<br>" . "Completo";    
        //else $RE_celda = $v["prima_re"] . "&euro;<br>" . $v["nombreProducto_re"];     

      echo "<tr>";
      echo "<td>";
      echo img(array('src'=>'public/images/aseguradoras/' . $v["logoProducto"], 'alt'=> $v['nombreCia'] . '->' . $v['nombreProducto']));
      echo "</td>";
      echo "<td class='precio_parrilla'><span>";
      echo $RE_celda . "</span>";
      echo "</td>";
      echo "<td class='precio_parrilla'><span>";
      echo $CM_celda . "</span>";
      echo "</td>";     
      echo "<td class='precio_parrilla'>";
      //echo "<button type='button' class='btn btn-warning contratar_pop' data-logo='" . $v["logoProducto"] . "' data-cia='" . $v["nombreCia"] . '->' . $v['nombreProducto'] . "'>Contratar</button>";
      $ruta = 'seguros/contratar/salud/' . $codtar . '/' . $v["nombreCia"];
      $boton_contratar = '<button type="button" class="btn btn-warning contratar_pop">Contratar</button>';
      echo anchor($ruta,$boton_contratar);      
      echo "</td>";
      echo "</tr>";     
    }
?>
  </table>
</div>

<div class="table-responsive">
  <table class="table table-striped table-hover table-bordered">
  <caption>Datos de la tarificación</caption>
  <tr>
    <td><b>Referencia</b></td>
    <td><?php echo $codigoTarificacion; ?></td>
  </tr> 
  <tr>
    <td><b>Nombre y apellidos</b></td>
    <td><?php echo $nombreApellidos; ?></td>
  </tr>

  <tr>
    <td><b>Email</b></td>
    <td><?php echo $email; ?></td>
  </tr>

  <tr>
    <td><b>Teléfono</b></td>
    <td><?php echo $telefono; ?></td>
  </tr>
  </table>
</div>
  </div>              




            </div>
          </div>
        </section>
      </main>