      <!-- Page Content-->
      <main class="page-content">

        <section class="bg-gray-lighter">
          <div class="shell shell-wide section-top-10 section-bottom-0">
            <div data-nav="false" data-stage-padding="15" data-items="2" data-xs-items="3" data-autoplay="true" data-sm-items="4" data-lg-items="5" data-xl-items="6" data-margin="30" data-mouse-drag="true" class="owl-nav-offset-30 owl-nav-variant-1 owl-dots-lg owl-carousel">

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/adeslas.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div>

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/aegon.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div>

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/allianz.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div>

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/antares.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div>  

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/arag.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div> 

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/asefa.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div> 
 
              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/asisa.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div> 

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/aviva.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div> 

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/axa.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div> 

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/divinaPastora.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div> 

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/dkv.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div> 

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/fiatc.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div> 

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/generali.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div> 

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/helvetia.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div> 

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/ivida.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div> 

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/liberty.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div> 

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/mapfre.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div> 

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/plus_ultra.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div> 

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/preventiva.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div> 

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/previsora_general.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div> 

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/reale.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div> 

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/sanitas.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div> 

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/santa_lucia.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div> 

              <div class="section-30"><div class="thumbnail-variant-2"><a href="#" class="reveal-inline-block"><img src="<?php echo base_url();?>public/images/aseguradoras/zurich.png" width="150" alt="" class="img-responsive center-block"></a></div>
              </div> 
                                                                                                                                                                                                                                                                            
            </div>
          </div>
        </section>

        <section class="section-120">
          <div class="shell shell-wide">

<h3 class="text-center text-ubold active" style="font-size: 2.3em;">Productos destacados</h3>
<hr class="divider divider-80 divider-primary active">



            <div class="range text-md-left">
              <div class="cell-sm-6 cell-md-3 view-animate fadeInUpSmall delay-06">
                <div class="unit unit-lg-horizontal">
                  <div class="unit-left"><i class="icon icon-info icon-lg fas fa-car"></i></div>
                  <div class="unit-body">
                    <h5 class="inset-xl-right-120 text-ubold"><a id="link_condicional_coche2" href="<?php echo site_url('seguros/coche') ?>">Seguros de coche</a></h5>
                    <p class="offset-top-10">Nuestra póliza de seguros de coche, entre los más baratos del mercado y con las mejores compañías y coberturas.</p>
                  </div>
                </div>
              </div>
              <div class="cell-sm-6 cell-md-3 offset-top-50 offset-sm-top-0 view-animate fadeInUpSmall delay-06">
                <div class="unit unit-lg-horizontal">
                  <div class="unit-left"><i class="icon icon-info icon-lg fas fa-users"></i></div>
                  <div class="unit-body">
                    <h5 class="inset-xl-right-120 text-ubold"><a href="<?php echo site_url('seguros/vida') ?>">Seguro de vida</a></h5>
                    <p class="offset-top-10">Si buscas un seguro de vida, no busques más. Siéntete bien cubierto con nuestro seguro y disfruta con los tuyos.</p>
                  </div>
                </div>
              </div>
              <div class="cell-sm-6 cell-md-3 offset-top-50 offset-md-top-0 view-animate fadeInUpSmall delay-06">
                <div class="unit unit-lg-horizontal">
                  <div class="unit-left"><i class="icon icon-info icon-lg fas fa-home"></i></div>
                  <div class="unit-body">
                    <h5 class="inset-xl-right-120 text-ubold"><a href="http://starazonaseguroalquiler.com">Seguro de alquiler</a></h5>
                    <p class="offset-top-10">Nuestra póliza de seguros de alquiler, entre los más baratos del mercado y con las mejores compañías y coberturas.</p>
                  </div>
                </div>
              </div>
              <div class="cell-sm-6 cell-md-3 offset-top-50 offset-md-top-0 view-animate fadeInUpSmall delay-06">
                <div class="unit unit-lg-horizontal">
                  <div class="unit-left"><i class="icon icon-info icon-lg fas fa-home"></i></div>
                  <div class="unit-body">
                    <h5 class="inset-xl-right-120 text-ubold"><a href="<?php echo site_url('seguros/hogar') ?>">Seguro de hogar</a></h5>
                    <p class="offset-top-10">Nuestra póliza de seguros de hogar, entre los más baratos del mercado y con las mejores compañías y coberturas.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="shell shell-wide" style="margin-top: 40px;">
            <div class="range text-md-left">
              <div class="cell-sm-6 cell-md-3 view-animate fadeInUpSmall delay-06">
                <div class="unit unit-lg-horizontal">
                  <div class="unit-left"><i class="icon icon-info icon-lg fab fa-tencent-weibo"></i></div>
                  <div class="unit-body">
                    <h5 class="inset-xl-right-120 text-ubold"><a href="<?php echo site_url('seguros/decesos') ?>">Seguros de decesos</a></h5>
                    <p class="offset-top-10">En los momentos más duros necesitas a los mejores profesionales a tu lado. Contrata un Seguro de Decesos con Totales Garantías.</p>
                  </div>
                </div>
              </div>
              <div class="cell-sm-6 cell-md-3 offset-top-50 offset-sm-top-0 view-animate fadeInUpSmall delay-06">
                <div class="unit unit-lg-horizontal">
                  <div class="unit-left"><i class="icon icon-info icon-lg fas fa-heartbeat"></i></div>
                  <div class="unit-body">
                    <h5 class="inset-xl-right-120 text-ubold"><a href="<?php echo site_url('seguros/feriantes') ?>">Seguro para feriantes</a></h5>
                    <p class="offset-top-10">Cubra todos sus riesgos a través de nuestras pólizas especializadas en el sector de los industriales feriantes y ferias.</p>
                  </div>
                </div>
              </div>
              <div class="cell-sm-6 cell-md-3 offset-top-50 offset-md-top-0 view-animate fadeInUpSmall delay-06">
                <div class="unit unit-lg-horizontal">
                  <div class="unit-left"><i class="icon icon-info icon-lg fas fa-industry"></i></div>
                  <div class="unit-body">
                    <h5 class="inset-xl-right-120 text-ubold"><a href="<?php echo site_url('seguros/empresas') ?>">Seguro de empresa</a></h5>
                    <p class="offset-top-10">El Seguro que necesitas para Tu Empresa. No corras riesgos innecesarios y protégete ante cualquier adversidad.</p>
                  </div>
                </div>
              </div>
              <div class="cell-sm-6 cell-md-3 offset-top-50 offset-md-top-0 view-animate fadeInUpSmall delay-06">
                <div class="unit unit-lg-horizontal">
                  <div class="unit-left"><i class="icon icon-info icon-lg fas fa-ship"></i></div>
                  <div class="unit-body">
                    <h5 class="inset-xl-right-120 text-ubold"><a href="<?php echo site_url('seguros/barcos') ?>">Seguros de embarcaciones</a></h5>
                    <p class="offset-top-10">Incomparables los servicios que te podemos prestar en tu seguro de embarcación, independientemente del tamaño y uso de esta.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>          
        </section>
      </main>