
      
<!-- Page Content-->
<main class="page-content">
        <section class="section-30 section-md-30 bg-gray-lighter">
          <div class="wow fadeInUp shell shell-wide text-left">

        <div class="row isotope-wrap text-center">
          
          <!-- Isotope Filters-->
          <div class="col-lg-12">
            <div class="isotope-filters isotope-filters-horizontal">

              <ul id="filters" class="nav-custom">

                <li><a href="#" data-filter="*" class="<?php echo $filtro_selected['todos'] == '1' ? 'active' : '' ?>">Todos los productos</a></li>
                <li><a href="#" data-filter=".particulares" class="<?php echo $filtro_selected['particulares'] == '1' ? 'active' : '' ?>">Seguro Particulares</a></li>
                <li><a href="#" data-filter=".empresas" class="<?php echo $filtro_selected['empresas'] == '1' ? 'active' : '' ?>">Seguros para Empresas</a></li>
                <li><a href="#" data-filter=".vida" class="<?php echo $filtro_selected['vida'] == '1' ? 'active' : '' ?>">Seguros Vida e inversión</a></li>
                <li><a href="#" data-filter=".espectaculos" class="<?php echo $filtro_selected['espectaculos'] == '1' ? 'active' : '' ?>">Seguros para Espectáculos</a></li>
              </ul>

            </div>
          </div>

          <!-- Isotope Content-->
          <div class="col-lg-12 offset-top-60">
            <div id="isotope_container" class="row row-no-gutter isotope isotope-no-padding">

<!-- ---------------------------------------------- CATEGORIA 2 - PARTICULARES ------------------------------------------------- -->
<?php $prefijo = "Seguro de <br>"; ?>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item particulares"><a href="<?php echo site_url('seguros/decesos/0') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/decesos.jpg" alt="Decesos" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Decesos</h3>
                    <p>Consigue el mejor precio y coberturas en tu seguro de decesos y el de toda tu familia.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item particulares"><a href="<?php echo site_url('seguros/salud/0') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/salud.jpg" alt="Salud" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Salud</h3>
                    <p>Conoce en pocos segundos los mejores precios con las más importantes compañías de seguros.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>                  


              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item particulares"><a href="<?php echo site_url('seguros/mascotas') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/mascotas.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Mascotas</h3>
                    <p>Seguro para animales domésticos, peligrosos, exóticos y caballos.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>  


              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item particulares"><a id="link_condicional_coche" href="<?php echo site_url('seguros/coche') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/coche.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Coche</h3>
                    <p>Compara tu seguro con más de 12 compañías.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item particulares"><a href="<?php echo site_url('seguros/hogar') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/hogar.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Hogar</h3>
                    <p>Un seguro de hogar a tu medida.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>                            


              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item particulares"><a href="http://starazonaseguroalquiler.com" class="thumbnail-variant-4" target="_blank"><img src="<?php echo base_url();?>public/images/productos/alquiler.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Alquiler</h3>
                    <p>Alquila tu propiedad con la certeza de que cobrarás tu mensualidad en todos los casos. Estudiamos al inquilino y te asesoramos en las gestiones.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>


              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item particulares"><a href="<?php echo site_url('seguros/barcos') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/barcos.jpg" alt="barcos" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Barcos</h3>
                    <p>Contrata tu seguro de embarcación y navega tranquilamente. No pagues más por menos.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>



              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item particulares"><a href="<?php echo site_url('seguros/comunidades') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/comunidades.jpg" alt="Comunidades" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Comunidades</h3>
                    <p>Asegura tu comunidad con un precio económico.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>


              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item particulares"><a id="link_condicional_moto" href="<?php echo site_url('seguros/moto_mb') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/moto.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Moto</h3>
                    <p>Contrátanos tu seguro de moto y deja de pagar más por menos.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>

<!-- ----------------------------------------------- END CATEGORIA 2 - PARTICULARES ------------------------------------------------- -->


<!-- ----------------------------------------------- CATEGORIA 3 - EMPRESAS ------------------------------------------------- -->
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item empresas"><a href="<?php echo site_url('seguros/empresas') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/empresas.jpg" alt="Empresas" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Empresas/Pyme</h3>
                    <p>No hagas más números, hazte tu seguro de empresa con nosotros, relajate y dejalo en nuestras manos.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item empresas"><a href="<?php echo site_url('seguros/joyeria') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/joyeria.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Joyería</h3>
                    <p>Tu negocio 24 horas cubierto con tu seguro para la joyería.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>      

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item empresas"><a href="<?php echo site_url('seguros/rc') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/rc.jpg" alt="RC" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Responsabilidad<br> Civil</h3>
                    <p>Tu seguro de Responsabilidad Civil para afrontar daños involuntarios.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>    

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item empresas"><a href="<?php echo site_url('seguros/riesgos_ciberneticos') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/riesgos_ciberneticos.png" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Riesgos<br> Cibernéticos</h3>
                    <p>Protege los datos de tu empresa.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>  

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item empresas"><a href="<?php echo site_url('seguros/comercio') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/comercio.jpg" alt="Comercio" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Comercio</h3>
                    <p>Asegura tu comercio con el mejor precio y condiciones. No esperes más y obtenlo ya.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>       

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item empresas"><a href="<?php echo site_url('seguros/directivos_altos_cargos') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/directivos_altos_cargos.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Directivos<br>Altos Cargos D&O</h3>
                    <p>El seguro D&O tu mejor inversión empresarial</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>   

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item empresas"><a href="<?php echo site_url('seguros/empresas_seguridad') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/empresa_seguridad.jpg" alt="Empresas de seguridad" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Empresas de<br>Seguridad</h3>
                    <p>El seguro más completo para empresas de seguridad.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>  

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item empresas"><a href="<?php echo site_url('seguros/secuestro_extorsion') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/secuestro.jpg" alt="Secuestro y extorsión" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Secuestro<br>y Extorsión</h3>
                    <p>¡No te dejes atar! Con tu seguro de secuestro y extorsión.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>    

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item empresas"><a href="<?php echo site_url('seguros/accidentes') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/accidentes.png" alt="accidentes" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Accidentes</h3>
                    <p>El futuro de todos cubiertos en caso de un imprevisto.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>      

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item empresas"><a href="<?php echo site_url('seguros/caucion') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/caucion.jpg" alt="caucion" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Caución</h3>
                    <p>El seguro de Caución, garantía de cobro de una deuda.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>                                      
<!-- ----------------------------------------------- END CATEGORIA 3 - EMPRESAS ------------------------------------------------- -->

            
<!-- ----------------------------------------------- CATEGORIA 4 - VIDA E INVERSION ------------------------------------------------- -->

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item vida"><a href="<?php echo site_url('seguros/vida/0') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/vida.jpg" alt="Vida" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Vida</h3>
                    <p>Tu seguro de vida es el futuro de tu familia, consigue el mejor precio y coberturas.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>  

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item vida"><a href="<?php echo site_url('seguros/vida/0/3') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/vida-policia.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Vida Policía</h3>
                    <p>Consigue el mejor precio con nuestro comparador de seguros de vida especialmente concebido para el Cuerpo de la Policia Nacional.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>   

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item vida"><a href="<?php echo site_url('seguros/vida/0/4') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/vida-guardia-civil.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Vida Guardia Civil</h3>
                    <p>Consigue el mejor precio con nuestro comparador de seguros de vida especialmente concebido para el Cuerpo de la Guardia Civil.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>   

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item vida"><a href="<?php echo site_url('seguros/vida/0/2') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/vida_bomberos.png" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Vida Bomberos</h3>
                    <p>Consigue el mejor precio con nuestro comparador de seguros de vida especialmente concebido para Bomberos.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>     

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item vida"><a href="<?php echo site_url('seguros/vida/0/1') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/vida-hipoteca.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Vida Hipoteca</h3>
                    <p>El mejor precio de seguro de vida para tu hipoteca.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>  

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item vida"><a href="<?php echo site_url('seguros/dependencia') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/dependencia.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Dependencia</h3>
                    <p>Porque tu futuro comienza ahora mismo, piensa en los tuyos.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div> 

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item vida"><a href="<?php echo site_url('seguros/jubilacion') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/jubilacion.jpg" alt="jubilacion y pensiones" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Jubilación<br>y Pensiones</h3>
                    <p>Sácale el mejor rendimiento a a tu dinero y con las mayores desgravaciones fiscales.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>  

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item vida"><a href="<?php echo site_url('seguros/fusiones') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/fusiones.jpg" alt="Fusiones y adquisiciones" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Fusiones<br>y Adquisiciones</h3>
                    <p>Con nosotros puedes disponer del mejor servicioy prestaciones en el seguro de fusiones y adquisiciones.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>  

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item vida"><a href="<?php echo site_url('seguros/dacion_pago') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/dacion.jpg" alt="Dacion en pago" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Dación en Pago</h3>
                    <p>Hipoteca tu casa, pero no tu vida.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>                    


<!-- ----------------------------------------------- END CATEGORIA 4 - VIDA E INVERSION ------------------------------------------------- -->        


<!-- ----------------------------------------------- CATEGORIA 6 - ESPECTACULOS ------------------------------------------------- -->          
                  

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item espectaculos"><a href="<?php echo site_url('seguros/feriantes') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/feriantes.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Feriantes</h3>
                    <p>Cubra todos sus riesgos a través de nuestras pólizas especializadas en el sector de los industriales feriantes y ferias.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div> 

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item espectaculos"><a href="<?php echo site_url('seguros/tienda_pirotecnia') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/tienda_pirotecnia.jpg" alt="Tienda pirotecnia" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Tienda y Kiosco<br>Pirotécnia</h3>
                    <p>Asegura tu tienda y/o kiosco de pirotécnia con la mejor póliza.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div> 


              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item espectaculos"><a href="<?php echo site_url('seguros/correfocs') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/correfocs.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Correfocs, Bèsties<br>y Gigantes</h3>
                    <p>Cubra todos sus riesgos a través de nuestras pólizas especializadas en el sector de los industriales feriantes y ferias.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div> 

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item espectaculos"><a href="<?php echo site_url('seguros/fallas') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/fallas.jpg" alt="Fallas y festejos" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Fallas y Festejos</h3>
                    <p>La mejor calidad - precio para el seguro de fallas, seguros para todo tipo de festejo popular.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>  

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item espectaculos"><a href="<?php echo site_url('seguros/cinematografia') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/cinematografia.jpg" alt="cinematografia" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Cinematografía</h3>
                    <p>Que no te cuenten películas y mira las coberturas de seguro de cinematografía.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div> 

                              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item espectaculos"><a href="<?php echo site_url('seguros/efectos_especiales') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/efectos_especiales.jpg" alt="Efectos especiales" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>RC Efectos<br>Especiales</h3>
                    <p>Consigue el mejor precio en nuestro seguro de efectos especiales.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>  
                  

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item espectaculos"><a href="<?php echo site_url('seguros/pirotecnia') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/pirotecnia.jpg" alt="pirotecnia" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Pirotécnia</h3>
                    <p>Asegura tus espectáculos con el seguro para pirotécnia.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div> 

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item espectaculos"><a href="<?php echo site_url('seguros/organizacion_pirotecnia') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/organizacion_pirotecnia.jpg" alt="organizacion pirotecnia" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Organización<br>Pirotécnia</h3>
                    <p>Asegura tus fuegos artificiales con las mejores coberturas.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>  

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item espectaculos"><a href="<?php echo site_url('seguros/bous_carrer') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/bous_carrer.jpg" alt="bous al carrer" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Bous al Carrer</h3>
                    <p>El seguro de bous al carrer más completo y seguro.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>                   



              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item espectaculos"><a href="<?php echo site_url('seguros/artistas_falleros') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/artistas_falleros.png" alt="Artistas falleros" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Artistas Falleros</h3>
                    <p>Seguro para empresas artistas falleros o foguerers.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>     

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item espectaculos"><a href="<?php echo site_url('seguros/rc_espectaculos') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/rc_espectaculos.jpg" alt="RC espectaculos" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>RC Espectáculos</h3>
                    <p>El seguro de RC espectáculos y eventos con más facilidades.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>



              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 item espectaculos"><a href="<?php echo site_url('seguros/suspension_espectaculos') ?>" class="thumbnail-variant-4"><img src="<?php echo base_url();?>public/images/productos/suspension_espectaculos.jpg" alt="suspension de espectaculos" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold"><?php echo $prefijo; ?>Suspensión de<br>Espectáculos</h3>
                    <p>Tendrás aseguradas las perdidas de tus espectáculos musicales, deportivos y de cualquier clase.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>       

<!-- ----------------------------------------------- END CATEGORIA 6 - ESPECTACULOS ------------------------------------------------- -->  

            </div>
          </div>
        </div>

        <hr class="divider divider-80 divider-primary">

            <p class="text-center offset-top-20 offset-md-top-40">Nuestra labor profesional consiste entre otras cosas en conseguir la satisfacción del cliente y por ello te ofrecemos un amplio abanico de productos donde podrás asegurar lo que más te preocupe o precises. Con la información que nos facilites, las coberturas que elijas y todo ello junto a nuestra experiencia profesional, te daremos la mejor oferta aseguradora por su precio, coberturas y servicio.</p>

          </div>
        </section>
</main>