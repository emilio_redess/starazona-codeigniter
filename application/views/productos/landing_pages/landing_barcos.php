


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">

			<div class="cell-sm-12">
				<div class="form-group">
					<label for="uso_animal" class="form-group-label">Clase de embarcación</label>
				
					<select name="clase_embarcacion" id="clase_embarcacion" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="Motor">Motor</option>
						<option value="Velero">Velero</option>
						<option value="Neumática">Neumática</option>
						<option value="Semirígida">Semirígida</option>
						<option value="Solo remo">Solo remo</option>
						<option value="Megayate">Megayate</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
			</div>	

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="velocidad" class="form-group-label">Velocidad en nudos (de 1 a 99)</label>
					<input type="number" min="1" max="99" value="1" class="form-control" name="velocidad" id="velocidad">
				</div>
			</div>

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="nombre_embarcacion" class="form-group-label">Nombre de la embarcación</label>
				
					<input id="nombre_embarcacion" type="text" name="nombre_embarcacion" value="<?php echo set_value('nombre_embarcacion'); ?>" class="form-control form-control-gray">
				</div>
			</div>	

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="marca_y_modelo" class="form-group-label">Marca y modelo</label>
					<input id="marca_y_modelo" type="text" name="marca_y_modelo" value="<?php echo set_value('marca_y_modelo'); ?>" class="form-control form-control-gray">
				</div>
			</div>



			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="eslora" class="form-group-label">Eslora</label>
					<input id="eslora" type="text" name="eslora" value="<?php echo set_value('eslora'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="manga" class="form-group-label">Manga</label>
					<input id="manga" type="text" name="manga" value="<?php echo set_value('manga'); ?>" class="form-control form-control-gray">
				</div>
			</div>	


			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="velocidad" class="form-group-label">Número de motores</label>
					<input type="number" min="1" max="6" value="1" class="form-control" name="velocidad" id="velocidad">
				</div>
			</div>



			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="puerto_de_atraque" class="form-group-label">Puerto de atraque</label>
					<input id="puerto_de_atraque" type="text" name="puerto_de_atraque" value="<?php echo set_value('puerto_de_atraque'); ?>" class="form-control form-control-gray">
				</div>
			</div>	


			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="potencia" class="form-group-label">Potencia total (CV)</label>
					<input id="potencia" type="text" name="potencia" value="<?php echo set_value('potencia'); ?>" class="form-control form-control-gray">
				</div>
			</div>


			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="anyo_construccion" class="form-group-label">Año de construcción</label>
					<input id="anyo_construccion" type="text" name="anyo_construccion" value="<?php echo set_value('anyo_construccion'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="valor_embarcacion" class="form-group-label">Valor de embarcación</label>
					<input id="valor_embarcacion" type="text" name="valor_embarcacion" value="<?php echo set_value('valor_embarcacion'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="valor_accesorios" class="form-group-label">Valor de accesorios</label>
					<input id="valor_accesorios" type="text" name="valor_accesorios" value="<?php echo set_value('valor_accesorios'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="bandera" class="form-group-label">Bandera</label>
					<input id="bandera" type="text" name="bandera" value="<?php echo set_value('bandera'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="numero_plazas" class="form-group-label">Número de plazas</label>
					<input id="numero_plazas" type="text" name="numero_plazas" value="<?php echo set_value('numero_plazas'); ?>" class="form-control form-control-gray">
				</div>
			</div>															




			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="lugar_hibernacion" class="form-group-label">Lugar de hibernación</label>
				
					<select name="lugar_hibernacion" id="lugar_hibernacion" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="En tierra">En tierra</option>
						<option value="En agua">En agua</option>
					</select>
				</div>
			</div>		



			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="sitio_de_navegacion" class="form-group-label">Sitio de navegación</label>
				
					<select name="sitio_de_navegacion" id="sitio_de_navegacion" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
		                <option value="Aguas territoriales o portuguesas">Aguas territoriales o portuguesas</option>
						<option value="Hasta 12 millas L.E.">Hasta 12 millas del litoral español</option>
		                <option value="Hasta 200 millas del litoral español">Hasta 200 millas del litoral español</option>
		                <option value="Navegación en aguas interiores">Navegación en aguas interiores</option>
		                <option value="Otras areas de navegación">Otras areas de navegación</option>				
		                <option value="Sin limite de millas">Sin limite de millas</option>
					</select>
				</div>
			</div>	


			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="condiciones_de_aseguramiento" class="form-group-label">Condiciones de aseguramiento</label>
				
					<select name="condiciones_de_aseguramiento" id="condiciones_de_aseguramiento" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="Condiciones españolas">Condiciones españolas</option>
						<option value="Condiciones inglesas">Condiciones inglesas</option>
						<option value="Sin daños">Sin daños</option>
						<option value="Solo seguro obligatorio">Solo seguro obligatorio</option>
					</select>
				</div>
			</div>															



			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="combustible" class="form-group-label">Combustible</label>
				
					<select name="combustible" id="combustible" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
	                    <option value="Diesel">Diesel</option>
	                    <option value="Gasolina">Gasolina</option>
					</select>
				</div>
			</div>															



			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="tipo_motor" class="form-group-label">Tipo de motor</label>
				
					<select name="tipo_motor" id="tipo_motor" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
	                    <option value="Intra-borda">Intra-borda</option>
	                    <option value="Fuera borda">Fuera borda</option>
						<option value="Mixto">Mixto</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
			</div>		

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="material_del_casco" class="form-group-label">Material del casco</label>
				
					<select name="material_del_casco" id="material_del_casco" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="Aluminio">Aluminio</option>
						<option value="Fibra de vidrio">Fibra de vidrio</option>
						<option value="Madera">Madera</option>
						<option value="Poliester">Poliéster</option>
						<option value="Sandwich">Sandwich</option>
						<option value="Neopreno">Neopreno</option>
						<option value="Zodiac">Zodiac</option>
						<option value="Goma">Goma</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
			</div>	

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="uso_embarcacion" class="form-group-label">Uso embarcación</label>
				
					<select name="uso_embarcacion" id="uso_embarcacion" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="embarcacion recreo">Embarcación recreo</option>
						<option value="deportiva">Deportiva</option>
						<option value="profesional">Profesional</option>
						<option value="alquiler">Alquiler</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
			</div>																				
		
		</div>
	</fieldset>

                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>


					<div class="offset-top-100">
						<h5 class="text-info-dr">DESCARGA DE DOCUMENTOS</h5>
						<hr class="divider divider-lg-left divider-primary divider-80">
						<div class="range">
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/barcos/gobierno-embarcaciones-de-recreo-orden-fom-3200-2007.pdf', img('public/images/pdf_file.png') . ' Gobierno embarcación de recreo orden from 3200/2007', 'target="_blank"'); ?></div>
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/barcos/boe-patron-profesional.pdf', img('public/images/pdf_file.png') .' Obtención de patrón de barco profesional', 'target="_blank"'); ?></div>
							<div class="col-md-4"><?php echo anchor('http://patronesdeyate.org', ' Prácticas para la obtención del título', 'target="_blank"'); ?></div>
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/barcos/requisitos-seguridad-y-escape-embarcaciones.pdf', img('public/images/pdf_file.png') .' Requisitos de seguridad', 'target="_blank"'); ?></div>
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/barcos/LEY DE NAVEGACION MARITIMA.pdf', img('public/images/pdf_file.png') .' Ley navegación marítima 07/2014', 'target="_blank"'); ?></div>
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/barcos/Ley271993.pdf', img('public/images/pdf_file.png') .' Puertos del estado', 'target="_blank"'); ?></div>

							
						</div>
					</div>

					</div>


					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/barcos2.jpg', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de barcos'); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

						<p class="offset-top-30">Ten bien asegurado tu barco, embarcación de recreo, velero, pesquero, moto acuática, a través de las diversas opciones de coberturas que le ofrecemos a través de nuestra comparativa de seguros para barcos que te haremos llegar inmediatamente.</p>
						
						<p class="text-red">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
