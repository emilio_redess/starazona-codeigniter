


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de coche" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">



      <div class="cell-sm-12 offset-top-20">
        <div class="form-group">
          <label for="fecha_nacimiento" class="form-group-label">Fecha de nacimiento</label>
          <input id="fecha_nacimiento" type="text" name="fecha_nacimiento" value="<?php echo set_value('fecha_nacimiento'); ?>" class="form-control form-control-gray">
        </div>
      </div>

      <div class="cell-sm-12 offset-top-20">
        <div class="form-group">
          <label for="fecha_carnet" class="form-group-label">Fecha del carnet de conducir</label>
          <input id="fecha_carnet" type="text" name="fecha_carnet" value="<?php echo set_value('fecha_carnet'); ?>" class="form-control form-control-gray">
        </div>
      </div>

      <div class="cell-sm-12 offset-top-20">
        <div class="form-group">
          <label for="matricula_vehiculo" class="form-group-label">Matrícula del vehículo</label>
          <input id="matricula_vehiculo" type="text" name="matricula_vehiculo" value="<?php echo set_value('matricula_vehiculo'); ?>" class="form-control form-control-gray">
        </div>
      </div>   

			<div class="cell-sm-12 offset-top-20">
				<div class="form-group">
					<label for="segundo_conductor" class="form-group-label">Segundo conductor</label>
				
					<select name="segundo_conductor" id="segundo_conductor" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="Sí">Sí</option>
						<option value="No">No</option>
					</select>
				</div>
			</div>	               
  
																			
		</div>
	</fieldset>
                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>  
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>


					</div>

					<div class="col-md-2">
						<?php //echo img(array('src'=>'public/images/productos/dacion.jpg', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de coche'); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

						<p class="text-red offset-top-30">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
