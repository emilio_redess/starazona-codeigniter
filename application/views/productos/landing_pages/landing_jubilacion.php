


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">


			<div class="cell-sm-6">
                <div class="form-group">
                  <label class="form-group-label">Clase de seguro de su interés</label>
                  <!--Select 2-->
                  <select data-minimum-results-for-search="Infinity" class="form-control select-filter" id="clase_de_seguro" name="clase_de_seguro">
                    <option value="">Selecciona una opción &#x25BC;</option>
                    <option value="Plan de Jubilación">Plan de Jubilación</option>
                    <option value="Prima Única">Prima Única</option>
                    <option value="PIAS">PIAS</option>
                    <option value="Renta Valenciana">Renta Valenciana</option>
                    <option value="Hipoteca Inversa">Hipoteca Inversa</option>
                    <option value="Unit Linked">Unit Linked</option>
                    <option value="Prima Unica">Prima Unica</option>
                  </select>
                </div>
			</div>

			<div class="cell-sm-6">
                <div class="form-group">
                  <label class="form-group-label">Duración de la póliza/Inversión, en Años</label>
                  <!--Select 2-->
                  <input type="number" min="1" max="30" value="1" class="form-control" name="duracion_poliza" id="duracion_poliza">
                </div>
			</div>

      <div class="cell-sm-6 offset-top-20">
                <div class="form-group">
                  <label class="form-group-label">Perfil Riesgo de inversión</label>
                  <!--Select 2-->
                  <select data-minimum-results-for-search="Infinity" class="form-control select-filter" id="perfil_riesgo_de_inversion" name="perfil_riesgo_de_inversion">
                    <option value="">Selecciona una opción &#x25BC;</option>
                    <option value="Renta Fija">Renta Fija</option>
                    <option value="Renta Variable">Renta Variable</option>
                    <option value="Mixto">Mixto</option>
                  </select>
                </div>
      </div>      

      <div class="cell-sm-6 offset-top-20">
                <div class="form-group">
                  <label class="form-group-label">Forma de pago</label>
                  <!--Select 2-->
                  <select data-minimum-results-for-search="Infinity" class="form-control select-filter" id="forma_de_pago" name="forma_de_pago">
                    <option value="">Selecciona una opción &#x25BC;</option>
                    <option value="Mensual">Mensual</option>
                    <option value="Trimestral">Trimestral</option>
                    <option value="Semestral">Semestral</option>
                    <option value="anual">Anual</option>
                    <option value="Pago unico">Pago unico</option>   
                  </select>
                </div>
      </div>         
																			
		</div>
	</fieldset>
                    <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>

          <div class="offset-top-100">
            <h5 class="text-info-dr">DESCARGA DE DOCUMENTOS</h5>
            <hr class="divider divider-lg-left divider-primary divider-80">
            <div class="range">
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/jubilacion/fiscalidad-2013.pdf', img('public/images/pdf_file.png') . ' Fiscalidad 2013', 'target="_blank"'); ?></div>
           

            </div>
          </div>
					</div>

					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/jubilacion.jpg', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de ' . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

            <p class="offset-top-30">Sácale el mejor rendimiento a a tu dinero y con las mayores desgravaciones fiscales.</p>
            <p class="offset-top-30">Beneficiate de la alta solvencia de las Compañias de Seguros, al ser el fondo de garantias cubierto al 100 % del capital por el Estado.</p>
            <p class="text-red offset-top-30">Utiliza este formulario para obtener rapidamente nuestra mejor oferta de seguro. Todos los campos hay que cumplimentarlos para obtener el mejor presupuesto de seguros.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
