


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">


      <div class="cell-sm-6">
                <div class="form-group">
                  <label class="form-group-label">Número de tiendas</label>
                  <!--Select 2-->
                  <input type="number" min="1" max="10" value="50" class="form-control" name="numero_de_tiendas" id="numero_de_tiendas">
                </div>
      </div>


			<div class="cell-sm-6">
                <div class="form-group">
                  <label class="form-group-label">Capital asegurado</label>
                  <!--Select 2-->
                  <select data-minimum-results-for-search="Infinity" class="form-control select-filter" id="capital" name="capital">
                    <option value="">Selecciona una opción &#x25BC;</option>
                    <option value="150000">150.000 €</option>
                    <option value="300000">300.000 €</option>
                    <option value="600000">600.000 €</option>
                    <option value="1200000">1.200.000 €</option>
                   
                  </select>
                </div>
			</div>


																			
		</div>
	</fieldset>
                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>  
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox" class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>

          <div class="offset-top-100">
            <h5 class="text-info-dr">DESCARGA DE DOCUMENTOS</h5>
            <hr class="divider divider-lg-left divider-primary divider-80">
            <div class="range">
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/tienda_pirotecnia/pirotecnia-telefonos-emergencia.pdf', img('public/images/pdf_file.png') . ' Teléfonos de emergencia', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/tienda_pirotecnia/categorias-productos-pirotecnicos.pdf', img('public/images/pdf_file.png') .' Categoria de productos pirotécnicos', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/tienda_pirotecnia/venta-y-uso-productos-pirotecnicos.pdf', img('public/images/pdf_file.png') .' Venta y uso de productos pirotécnicos', 'target="_blank"'); ?></div>

              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/tienda_pirotecnia/decreto_pirotecnia.pdf', img('public/images/pdf_file.png') .' Autorizaciones a menores en la CV', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/tienda_pirotecnia/RD-1335-2012.pdf', img('public/images/pdf_file.png') .' Real Decreto 1335/2012', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/tienda_pirotecnia/BOE-A-2015-12054.pdf', img('public/images/pdf_file.png') .' Real Decreto 563/2010', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/tienda_pirotecnia/RD-230-1998.pdf', img('public/images/pdf_file.png') .' Real Decreto 230/1998', 'target="_blank"'); ?></div>
           
            </div>
          </div>
					</div>

					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/tienda_pirotecnia.jpg', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de ' . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

						<p class="offset-top-30">Seguro de responsabilidad civil que cubrirá los daños que ocasiones a terceros o empleados en el ejercicio de tu actividad pudiendo hacer coberturas anuales como temporales para tiendas y kioskos.</p>

            <p class="text-red offset-top-30">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
