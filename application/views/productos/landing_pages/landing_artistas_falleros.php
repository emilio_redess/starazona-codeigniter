


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">


      <div class="cell-sm-6">
        <div class="form-group">
          <label for="poblacion_de_la_empresa" class="form-group-label">Población de la empresa</label>
          <input id="poblacion_de_la_empresa" type="text" name="poblacion_de_la_empresa" value="<?php echo set_value('poblacion_de_la_empresa'); ?>" class="form-control form-control-gray">
        </div>
      </div>


      <div class="cell-sm-6">
        <div class="form-group">
          <label for="facturacion_estimada" class="form-group-label">Facturación estimada</label>
          <input id="facturacion_estimada" type="text" name="facturacion_estimada" value="<?php echo set_value('facturacion_estimada'); ?>" class="form-control form-control-gray">
        </div>
      </div>


      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="numero_empleados" class="form-group-label">Número de empleados</label>
          <input id="numero_empleados" type="number" min="1" max="200" name="numero_empleados" value="<?php echo set_value('numero_empleados'); ?>" class="form-control form-control-gray">
        </div>
      </div>    

      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="valor_edificio" class="form-group-label">Valor del edificio</label>
          <input id="valor_edificio" type="text" name="valor_edificio" value="<?php echo set_value('valor_edificio'); ?>" class="form-control form-control-gray">
        </div>
      </div>     

      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="valor_mercancia_y_maquinaria" class="form-group-label">Valor de la mercancía y maquinaria</label>
          <input id="valor_mercancia_y_maquinaria" type="text" name="valor_mercancia_y_maquinaria" value="<?php echo set_value('valor_mercancia_y_maquinaria'); ?>" class="form-control form-control-gray">
        </div>
      </div>            



			<div class="cell-sm-6 offset-top-20">
				<div class="form-group">
					<label for="gremiado" class="form-group-label">Gremiado</label>
				
					<select name="gremiado" id="gremiado" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
	                	<option value="Si">Sí</option>                    
	                	<option value="No">No</option> 
					</select>
				</div>
			</div>	
			



    

																			
		</div>
	</fieldset>
                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>  
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>

						

					</div>

					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/artistas_falleros.png', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de ' . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

						<p class="offset-top-30">Seguro de las instalaciones.</p>
						<p class="offset-top-30">Seguro responsabilidad civil del artista fallero.</p>
						<p class="offset-top-30">Seguro transporte, carga y descarga.</p>
						<p class="offset-top-30">Seguro vehículos empresa y particulares.</p>
						<p class="offset-top-30">Seguro buen fin o entrega del monumento.</p>
					
						<p class="text-red offset-top-30">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
