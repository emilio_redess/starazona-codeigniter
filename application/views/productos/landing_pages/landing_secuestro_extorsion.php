


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">







			<div class="cell-sm-6">
				<div class="form-group">
					<label for="clase_de_solicitante" class="form-group-label">Clase de solicitante</label>
				
					<select name="clase_de_solicitante" id="clase_de_solicitante" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
	                	<option value="Empresa">Empresa</option>
	                	<option value="Familia">Familia</option>
	                	<option value="ONG">ONG</option>  
					</select>
				</div>
			</div>	

      <div class="cell-sm-6">
        <div class="form-group">
          <label for="suma_a_asegurar" class="form-group-label">Suma a asegurar por siniestro</label>
          <input id="suma_a_asegurar" type="text" name="suma_a_asegurar" value="<?php echo set_value('suma_a_asegurar'); ?>" class="form-control form-control-gray">
        </div>
      </div>


    

																			
		</div>
	</fieldset>
                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>  
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>

					<div class="offset-top-100">
						<h5 class="text-info-dr">DESCARGA DE DOCUMENTOS</h5>
						<hr class="divider divider-lg-left divider-primary divider-80">
						<div class="range">
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/secuestro_extorsion/seguro-de-secuestro-y-extorsion.pdf', img('public/images/pdf_file.png') . ' Seguro de extorsión y secuestro', 'target="_blank"'); ?></div>
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/secuestro_extorsion/manual-antisecuestro-de-la-ONU.pdf', img('public/images/pdf_file.png') .' Como evitar un secuestro', 'target="_blank"'); ?></div>

							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/secuestro_extorsion/Cuestionario-Corporaciones-AIG.pdf', img('public/images/pdf_file.png') .' Cuestionario Empresas - ONG', 'target="_blank"'); ?></div>

							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/secuestro_extorsion/Cuestionario-Familia-AIG.pdf', img('public/images/pdf_file.png') .' Cuestionario Familiar', 'target="_blank"'); ?></div>														
																		
													
							

							
						</div>
					</div>						


					</div>

					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/secuestro2.png', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de ' . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

						<p class="offset-top-30">Póliza de seguros especialmente importante para empresas y ONGs que tengan su actividad o parte de ella, realizan visitas a países o zonas conflictivas. También puede ser utilizado por turistas o familias particulares.</p>
						<p class="offset-top-30">Las empresas y organizaciones sin fines de lucro que operan en zonas amenazadas se enfrentan a riesgos de seguridad para su personal y a pérdidas financieras en caso de una crisis.</p>
						<p class="offset-top-30">Por ello, es importante que los individuos y negocios que se enfrentan a estos desafíos tengan una mayor comprensión de su entorno de seguridad, cuenten con planes de contingencia y se asocien con una firma de seguridad internacional con una trayectoria probada en la administración y manejo de crisis</p>
						<p class="text-red offset-top-30">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
