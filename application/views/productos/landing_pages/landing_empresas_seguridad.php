


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">



      <div class="cell-sm-6">
        <div class="form-group">
          <label for="numero_empleados" class="form-group-label">Nº de empleados</label>
          <input id="numero_empleados" type="text" name="numero_empleados" value="<?php echo set_value('numero_empleados'); ?>" class="form-control form-control-gray">
        </div>
      </div>

      <div class="cell-sm-6">
        <div class="form-group">
          <label for="facturacion" class="form-group-label">Facturación</label>
          <input id="facturacion" type="text" name="facturacion" value="<?php echo set_value('facturacion'); ?>" class="form-control form-control-gray">
        </div>
      </div>     



			<div class="cell-sm-6 offset-top-20">
				<div class="form-group">
					<label for="actividad" class="form-group-label">Actividad</label>
				
					<select name="actividad" id="actividad" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
		                <option value="Empresa instaladora">Empresa instaladora</option>
		                <option value="Vigilancia">Vigilancia</option>
		                <option value="Central receptora de alarmas">Central receptora de alarmas</option>
		                <option value="Planificación y asesoramiento">Planificación y asesoramiento</option>
		                <option value="Empresa de servicios auxiliares">Empresa de servicios auxiliares</option>
		                <option value="Agencia de detectives">Agencia de detectives</option>  
					</select>
				</div>
			</div>	

			<div class="cell-sm-6 offset-top-20">
				<div class="form-group">
					<label for="informacion_seguro_de_caucion" class="form-group-label">Información seguro de caución</label>
				
					<select name="informacion_seguro_de_caucion" id="informacion_seguro_de_caucion" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
	                    <option value="Si">Si</option>
	                    <option value="No">No</option> 
					</select>
				</div>
			</div>				



																			
		</div>
	</fieldset>
                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>  
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>

					<div class="offset-top-100">
						<h5 class="text-info-dr">DESCARGA DE DOCUMENTOS</h5>
						<hr class="divider divider-lg-left divider-primary divider-80">
						<div class="range">
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/empresas_seguridad/seguro-empresas-seguridad-formulario.pdf', img('public/images/pdf_file.png') . ' Formulario Extendido', 'target="_blank"'); ?></div>
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/empresas_seguridad/seguro-empresas-seguridad-condiciones.pdf', img('public/images/pdf_file.png') .' Condiciones del seguro', 'target="_blank"'); ?></div>

							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/empresas_seguridad/Ley 5-2014 de Seguridad Privada.pdf', img('public/images/pdf_file.png') .' Ley Seguridad Privada 2014', 'target="_blank"'); ?></div>

							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/empresas_seguridad/COMENTARIOS A LA LEY 5_2014 DE SEGURIDAD PRIVADA.pdf', img('public/images/pdf_file.png') .' Comentario ley S.P. 2014', 'target="_blank"'); ?></div>														
																		
													
							

							
						</div>
					</div>						


					</div>

					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/empresa_seguridad.jpg', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de ' . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

						<p class="offset-top-30">Ten cubierta la Responsabilidad Civil profesional de tu empresa y de tus empleados, con unas coberturas muy importantes.</p>
						<p class="text-red offset-top-30">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
