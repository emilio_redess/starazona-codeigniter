


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">

			<div class="cell-sm-6">
				<div class="form-group">
					<label for="codigo_postal" class="form-group-label">Código postal</label>
					<input id="codigo_postal" type="text" name="codigo_postal" value="<?php echo set_value('codigo_postal'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-6">
				<div class="form-group">
					<label for="poblacion" class="form-group-label">Población</label>
					<input id="poblacion" type="text" name="poblacion" value="<?php echo set_value('poblacion'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-6 offset-top-20">
				<div class="form-group">
					<label for="actividad" class="form-group-label">Actividad</label>
					<input id="actividad" type="text" name="actividad" value="<?php echo set_value('actividad'); ?>" class="form-control form-control-gray">
				</div>
			</div>

		
															




			<div class="cell-sm-6 offset-top-20">
				<div class="form-group">
					<label for="tipo_de_seguro" class="form-group-label">Tipo de seguro</label>
				
					<select name="tipo_de_seguro" id="tipo_de_seguro" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="Multiriesgo industrial">Multiriesgo industrial</option>
						<option value="Responsabilidad Civil">Responsabilidad Civil</option>
						<option value="Transportes">Transportes</option>
						<option value="Colectivo vida/accidentes">Colectivo vida/accidentes</option>
						<option value="Flota Vehiculos">Flota Vehículos</option>
						<option value="Averia de maquinaria">Averia de maquinaria</option>
						<option value="D & O">D & O</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
			</div>		


																			
		
		</div>
	</fieldset>

                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 240px;min-height:50px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>




					</div>


					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/empresas.jpg', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de ' . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

						<p class="offset-top-30">Un ámplio abanico de coberturas para poder tener bien cubierta tu empresa a través de una póliza todo riesgo. Utiliza este formulario para obtener rapidamente nuestra mejor oferta de seguro.</p>
						
						<p class="text-red">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>



