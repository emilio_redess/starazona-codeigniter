


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">
			<div class="cell-sm-4">
				<div class="form-group">
					<label for="nombre_empresa" class="form-group-label">Nombre de la empresa</label>
					<input id="nombre_empresa" type="text" name="nombre_empresa" value="<?php echo set_value('nombre_empresa'); ?>" class="form-control form-control-gray">
				</div>
			</div>





			<div class="cell-sm-4">
				<div class="form-group">
					<label for="localidad" class="form-group-label">Localidad</label>
					<input id="localidad" type="text" name="localidad" value="<?php echo set_value('localidad'); ?>" class="form-control form-control-gray">
				</div>
			</div>




																						
			<div class="cell-sm-4">
				<div class="form-group">
					<label for="provincia" class="form-group-label">Provincia</label>
				
					<select name="provincia" id="provincia" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
		                <option value="Álava">Álava</option>
		                <option value="Albacete">Albacete</option>
		                <option value="Alicante">Alicante</option>
		                <option value="Almería">Almería</option>
		                <option value="Asturias">Asturias</option>
		                <option value="Ávila">Ávila</option>
		                <option value="Badajoz">Badajoz</option>
		                <option value="Baleares (Islas)">Baleares (Islas)</option>
		                <option value="Barcelona">Barcelona</option>
		                <option value="Burgos">Burgos</option>
		                <option value="Cáceres">Cáceres</option>
		                <option value="Cádiz">Cádiz</option>
		                <option value="Cantabria">Cantabria</option>
		                <option value="Castellón">Castellón</option>
		                <option value="Ceuta">Ceuta</option>
		                <option value="Ciudad real">Ciudad real</option>
		                <option value="Córdoba">Córdoba</option>
		                <option value="Coruña (A)">Coruña (A)</option>
		                <option value="Cuenca">Cuenca</option>
		                <option value="Girona">Girona</option>
		                <option value="Granada">Granada</option>
		                <option value="Guadalajara">Guadalajara</option>
		                <option value="Guipúzcoa">Guipúzcoa</option>
		                <option value="Huelva">Huelva</option>
		                <option value="Huesca">Huesca</option>
		                <option value="Jaén">Jaén</option>
		                <option value="León">León</option>
		                <option value="Lleida">Lleida</option>
		                <option value="Lugo">Lugo</option>
		                <option value="Madrid">Madrid</option>
		                <option value="Málaga">Málaga</option>
		                <option value="Melilla">Melilla</option>
		                <option value="Murcia">Murcia</option>
		                <option value="Navarra">Navarra</option>
		                <option value="Orense">Orense</option>
		                <option value="Palencia">Palencia</option>
		                <option value="Palmas (las)">Palmas (las)</option>
		                <option value="Pontevedra">Pontevedra</option>
		                <option value="Rioja (la)">Rioja (la)</option>
		                <option value="Salamanca">Salamanca</option>
		                <option value="Segovia">Segovia</option>
		                <option value="Sevilla">Sevilla</option>
		                <option value="Soria">Soria</option>
		                <option value="Sta. Cruz Tenerife">Sta. Cruz Tenerife</option>
		                <option value="Tarragona">Tarragona</option>
		                <option value="Teruel">Teruel</option>
		                <option value="Toledo">Toledo</option>
		                <option value="Valencia">Valencia</option>
		                <option value="Valladolid">Valladolid</option>
		                <option value="Vizcaya">Vizcaya</option>
		                <option value="Zamora">Zamora</option>
		                <option value="Zaragoza">Zaragoza</option>
					</select>
				</div>
			</div>



			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="empresas_filiales" class="form-group-label">Tiene empresas filiales</label>
				
					<select name="empresas_filiales" id="empresas_filiales" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="Sí">Sí</option>
						<option value="No">No</option>
					</select>
				</div>
			</div>	

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="empresas_participadas" class="form-group-label">Tiene empresas participadas</label>
				
					<select name="empresas_participadas" id="empresas_participadas" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="Sí">Sí</option>
						<option value="No">No</option>
					</select>
				</div>
			</div>		

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="cotiza_en_mercados_valores" class="form-group-label">Cotiza en mercado de valores</label>
				
					<select name="cotiza_en_mercados_valores" id="cotiza_en_mercados_valores" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="Sí">Sí</option>
						<option value="No">No</option>
					</select>
				</div>
			</div>				

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="tipo_empresa" class="form-group-label">Tipo de empresa</label>
				
					<select name="tipo_empresa" id="tipo_empresa" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="Privada">Privada</option>
						<option value="Publica">Pública</option>
					</select>
				</div>
			</div>					




			

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="actividad_empresa" class="form-group-label">Actividad Empresa / Objeto Social</label>
					<input id="actividad_empresa" type="text" name="actividad_empresa" value="<?php echo set_value('actividad_empresa'); ?>" class="form-control form-control-gray">
				</div>
			</div>	


			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="cargo" class="form-group-label">Cargo</label>
					<input id="cargo" type="text" name="cargo" value="<?php echo set_value('cargo'); ?>" class="form-control form-control-gray">
				</div>
			</div>		

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="facturacion" class="form-group-label">Facturación último año</label>
					<input id="facturacion" type="text" name="facturacion" value="<?php echo set_value('facturacion'); ?>" class="form-control form-control-gray">
				</div>
			</div>	

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="capital_a_asegurar" class="form-group-label">Capital que desea asegurar</label>
					<input id="capital_a_asegurar" type="text" name="capital_a_asegurar" value="<?php echo set_value('capital_a_asegurar'); ?>" class="form-control form-control-gray">
				</div>
			</div>																												


			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="anyo_inicio_actividad" class="form-group-label">Año de inicio de la actividad</label>
					<input id="anyo_inicio_actividad" type="number" min="1800" max="2050" name="anyo_inicio_actividad" value="<?php echo set_value('anyo_inicio_actividad'); ?>" class="form-control form-control-gray">
				</div>
			</div>	


																


		</div>
	</fieldset>




                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>


					<div class="offset-top-100">
						<h5 class="text-info-dr">DESCARGA DE DOCUMENTOS</h5>
						<hr class="divider divider-lg-left divider-primary divider-80">
						<div class="range">
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/directivos_y_altos_cargos/D_O aig1.pdf', img('public/images/pdf_file.png') . ' D&O AIG', 'target="_blank"'); ?></div>
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/directivos_y_altos_cargos/GUIA D&O.pdf', img('public/images/pdf_file.png') .' Qué es un seguro D&O', 'target="_blank"'); ?></div>

							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/directivos_y_altos_cargos/seguro-directivos-formulario.pdf', img('public/images/pdf_file.png') .' Cuestionario de solicitud', 'target="_blank"'); ?></div>

							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/directivos_y_altos_cargos/bajar-informacion-axa.pdf', img('public/images/pdf_file.png') .' Condicionado AXA', 'target="_blank"'); ?></div>														
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/directivos_y_altos_cargos/seguro-directivos-condiciones-allianz.pdf', img('public/images/pdf_file.png') .' Condicionado ALLIANZ', 'target="_blank"'); ?></div>														
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/directivos_y_altos_cargos/seguro-directivos-condiciones-reale.pdf', img('public/images/pdf_file.png') .' Condicionado REALE', 'target="_blank"'); ?></div>														
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/directivos_y_altos_cargos/condicionado-zurich-d-o-egi-mayo-2010-clog.pdf', img('public/images/pdf_file.png') .' Condicionado ZURICH', 'target="_blank"'); ?></div>														
													
							

							
						</div>
					</div>

					</div>


					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/directivos_altos_cargos2.png', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de '  . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

						<p class="offset-top-30">Ten bien cubierta tu Responsabilidad Civil como Directivo, Administrador, Alto Cargo, Gerente, Consejo de Administración y Empleados Supervisores o Codemandados ante cualquier conflicto o reclamación que recibas por parte de Accionistas, Acreedores, Empleados, Clientes, Administración Pública, por faltas errores o negligencias cometidas en la gestión empresarial. Esta póliza es extensiva a empresas filiales o participadas.</p>

						<p class="offset-top-30">También contamos con la póliza de Responsabilidad Civil D & O para Cargos de la Administración Pública.</p>
			
						<p class="text-red offset-top-20">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
