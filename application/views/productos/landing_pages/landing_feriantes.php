


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">


      <div class="cell-sm-12">
                <div class="form-group">
                  <label class="form-group-label">Seguro que solicita</label>
                  <!--Select 2-->
                                        <select id="producto" name="producto" class="form-control">
                                            <option value="">Tipos de seguros en los que está interesado &#x25BC;</option>
                                            <option value="Responsabilidad civil" <?php echo  set_select('producto', 'Responsabilidad civil'); ?> >Responsabilidad civil</option>
                                            <option value="Daños a la atracción" <?php echo  set_select('producto', 'Daños a la atracción'); ?> >Daños a la atracción (robo e incendio)</option>
                                            <option value="Seguro vehículo" <?php echo  set_select('producto', 'Seguro vehículo'); ?> >Seguro vehículo</option>
                                            <option value="Accidentes personales" <?php echo  set_select('producto', 'Accidentes personales'); ?> >Accidentes personales</option>
                                            <option value="Varios seguros" <?php echo  set_select('producto', 'Varios seguros'); ?> >Varios seguros</option>
                                            <option value="Otros" <?php echo  set_select('producto', 'Otros'); ?> >Otros</option>
                                        </select> 
                </div>
      </div>










																			
		</div>
	</fieldset>
                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>  
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>


					</div>

					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/feriantes2.jpg', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de ' . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

            <p class="offset-top-30">Cubra todos sus riesgos a través de nuestras pólizas especializadas en el sector de los industriales feriantes y ferias.</p>
					 <ul>
            <li>Seguro de Responsabilidad Civil Industriales Feriantes</li>     
            <li>Seguro Colectivo de Accidentes Feriantes</li>     
            <li>Seguro Vida Feriantes</li>     
            <li>Seguro vehículos industriales Feriantes</li>     
            <li>Seguro viviendas fijas o móviles</li>     
            <li>Seguro Almacenes y naves industriales feriantes</li>     
           </ul>

            <p class="text-red offset-top-30">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
