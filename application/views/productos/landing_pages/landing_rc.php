


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">

			<div class="cell-sm-6">
				<div class="form-group">
					<label for="poblacion" class="form-group-label">Población</label>
					<input id="poblacion" type="text" name="poblacion" value="<?php echo set_value('poblacion'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-6">
				<div class="form-group">
					<label for="actividad_de_la_empresa" class="form-group-label">Actividad de la empresa</label>
					<input id="actividad_de_la_empresa" type="text" name="actividad_de_la_empresa" value="<?php echo set_value('actividad_de_la_empresa'); ?>" class="form-control form-control-gray">
				</div>
			</div>			

			<div class="cell-sm-6 offset-top-20">
				<div class="form-group">
					<label for="coberturas" class="form-group-label">Coberturas</label>
				
					<select name="coberturas" id="coberturas" class="form-control form-control-gray">
					
	                    <option value="Centro ciudad">Centro ciudad &#x25BC;</option>
	                    <option value="Explotacion (actividad)">Explotacion (actividad)</option>
	                    <option value="Patronal">Patronal</option>
	                    <option value="Profesional">Profesional</option>
	                    <option value="Producto">Producto</option>
	                    <option value="Locativa">Locativa</option>
	                    <option value="Garantía">Garantía</option>
	                    <option value="Probadores">Probadores</option>
	                    <option value="Cruzada">Cruzada</option>
	                    <option value="Union y mezcla">Union y mezcla</option>
	                    <option value="Retirada producto">Retirada producto</option>
	                    <option value="Transporte">Transporte</option>
	                    <option value="Ampl. EEUU y Canadá">Ampl. EEUU y Canadá</option>
	                    <option value="Defensa y fianzas">Defensa y fianzas</option>
	                    <option value="Reclamación">Reclamación</option>
	                    <option value="Otra">Otra</option>
	                  
					</select>
				</div>
			</div>	

			<div class="cell-sm-6">
				<div class="form-group">
					<label for="capitales_asegurados" class="form-group-label">Capitales asegurados</label>
				
					<select name="capitales_asegurados" id="capitales_asegurados" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
	                    <option value="150.253 €">150.253 €</option>
	                    <option value="300.506 €">300.506 €</option>
	                    <option value="601.012,1 €">601.012,1 €</option>
	                    <option value="1.202.024 €">1.202.024 €</option>
	                    <option value="6.010.120 €">6.010.120 €</option>
	                    <option value="Otro">Otro</option>
					</select>
				</div>
			</div>	

			<div class="cell-sm-6 offset-top-20">
				<div class="form-group">
					<label for="limite_por_victima" class="form-group-label">Límite por víctima</label>
				
					<select name="limite_por_victima" id="limite_por_victima" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
	                      <option value="60.000€">60.000€</option>
	                      <option value="90.000€">90.000€</option>
	                      <option value="150.000€">150.000€</option>                      
	                      <option value="200.000€">200.000€</option>
	                      <option value="250.000€">250.000€</option>
	                      <option value="300.000€">300.000€</option>
					</select>
				</div>
			</div>	

			<div class="cell-sm-6 offset-top-20">
				<div class="form-group">
					<label for="fecha_vencimiento_poliza_actual" class="form-group-label">Fecha vencimiento poliza actual</label>
	                <div class='input-group date' id='datetimepicker1'>
	                    <input type='text' class="form-control form-control-gray" id="fecha_vencimiento_poliza_actual" name="fecha_vencimiento_poliza_actual" value="<?php echo set_value('fecha_vencimiento_poliza_actual'); ?>" />
	                    <span class="input-group-addon">
	                        <span class="glyphicon glyphicon-calendar"></span>
	                    </span>
	                </div>	
				</div>
			</div>									

			<div class="cell-sm-6 offset-top-20">
				<div class="form-group">
					<label for="importe_facturacion_ejercicio_anterior" class="form-group-label">Importe facturación ejercicio anterior (€)</label>
					<input id="importe_facturacion_ejercicio_anterior" type="text" name="importe_facturacion_ejercicio_anterior" value="<?php echo set_value('importe_facturacion_ejercicio_anterior'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-6 offset-top-20">
				<div class="form-group">
					<label for="prevision_ejercicio_actual" class="form-group-label">Previsión ejercicio actual (€)</label>
					<input id="prevision_ejercicio_actual" type="text" name="prevision_ejercicio_actual" value="<?php echo set_value('prevision_ejercicio_actual'); ?>" class="form-control form-control-gray">
				</div>
			</div>	


	
			<div class="cell-sm-6 offset-top-20">
				<div class="form-group">
					<label for="urge_cotizacion" class="form-group-label">¿Urge cotización?</label>
				
					<select name="urge_cotizacion" id="urge_cotizacion" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="Sí">Sí</option>
						<option value="No">No</option>
					</select>
				</div>
			</div>	

		</div>
	</fieldset>


                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>


					</div>


					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/rc2.png', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de '  . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

						<p class="offset-top-30">Cubre los daños que puedas ocasionar a terceros y a tus empleados en el ejercicio de tu actividad:</p>
						<ul>
							<li>R.C actividad o explotación</li>
							<li>R.C patronal</li>
							<li>R.C producto</li>
							<li>R.C locativa</li>
							<li>R.C garantía</li>
							<li>R.C probadores</li>
							<li>R.C cruzada</li>
							<li>R.C trabajo realizado</li>
							<li>R.C unión y mezcla</li>
							<li>R.C EEUU y Canada</li>
							<li>R.C transporte mercancias</li>
		
						</ul>
						<p class="text-red offset-top-20"></p>
					</div>
			</div>
    	</div>
	</section>
</main>
