


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">





			<div class="cell-sm-12">
                <div class="form-group">
                  <label class="form-group-label">Clases de actos festivos</label>
                  <!--Select 2-->
                  <select data-minimum-results-for-search="Infinity" class="form-control select-filter" id="producto" name="producto">
                                            <option value="">Selecciona una opción &#x25BC;</option>
                                            <option value="Fallas" <?php echo set_select('producto', 'Fallas'); ?> >Fallas</option>
                                            <option value="Hogueras" <?php echo set_select('producto', 'Hogueras'); ?> >Hogueras</option>
                                            <option value="Dragones de fuego" <?php echo set_select('producto', 'Dragones de fuego'); ?> >Dragones de fuego</option>
                                            <option value="Corda - Cordaes" <?php echo set_select('producto', 'Corda - Cordaes'); ?> >Corda - Cordaes</option>
                                            <option value="Toros de fuego" <?php echo set_select('producto', 'Toros de fuego'); ?> >Toros de fuego</option>
                                            <option value="Actos festivos sin pirotecnia" <?php echo set_select('producto', 'Actos festivos sin pirotecnia'); ?> >Actos festivos sin pirotecnia</option>
                                            <option value="Actos festivos con pirotecnia" <?php echo set_select('producto', 'Actos festivos con pirotecnia'); ?> >Actos festivos con pirotecnia</option>
                   
                  </select>
                </div>
			</div>
																			
		</div>
	</fieldset>
                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>  
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>


					</div>

					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/fallas.jpg', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de ' . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

            <p class="offset-top-30">La mejor calidad - precio para el seguro de fallas, seguros para todo tipo de festejo popular.</p>
						<p class="offset-top-30">Disfruta las mejores coberturas con nuestras pólizas de responsabilidad civil en Seguro para fallas, seguro para hogueras, seguros para festejos o fiestas populares, seguro para cordaes, seguro para toros de fuego, seguro para moros y cristianos.</p>

            <p class="text-red offset-top-30">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
