


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">
			<div class="cell-sm-4">
				<div class="form-group">
					<label for="domicilio_para_asegurar" class="form-group-label">Domicilio Riesgo para asegurar</label>
					<input id="domicilio_para_asegurar" type="text" name="domicilio_para_asegurar" value="<?php echo set_value('domicilio_para_asegurar'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-4">
				<div class="form-group">
					<label for="zona_emplazamiento" class="form-group-label">Zona emplazamiento</label>
				
					<select name="zona_emplazamiento" id="zona_emplazamiento" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
	                    <option value="Centro ciudad">Centro ciudad</option>
	                    <option value="Centro comercial">Centro comercial</option>
	                    <option value="Zona artesanal">Zona artesanal</option>
	                    <option value="Zona industrial">Zona industrial</option>
	                    <option value="Población + 2000 habitantes">Población + 2000 habitantes</option>
	                    <option value="Población - 2000 habitantes">Población - 2000 habitantes</option>
	                    <option value="Escaparate de hotel">Escaparate de hotel</option>
					</select>
				</div>
			</div>	

			<div class="cell-sm-4">
				<div class="form-group">
					<label for="clase_de_actividad" class="form-group-label">Clase de actividad</label>
				
					<select name="clase_de_actividad" id="clase_de_actividad" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
	                    <option value="Mayorista">Mayorista</option>
	                    <option value="Minorista">Minorista</option>
	                    <option value="Fabricante">Fabricante</option>
	                    <option value="Taller">Taller</option>
	                    <option value="Otro">Otro</option>
					</select>
				</div>
			</div>	

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="ubicacion_riesgo" class="form-group-label">Ubicación del riesgo</label>
				
					<select name="ubicacion_riesgo" id="ubicacion_riesgo" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
	                    <option value="Planta baja">Planta baja</option>
	                    <option value="Primer piso">Primer piso</option>
	                    <option value="Entre plantas">Entre plantas</option>
	                    <option value="Nave industrial">Nave industrial</option>
					</select>
				</div>
			</div>							

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="anyo_inicio_actividad" class="form-group-label">Año inicio actividad</label>
					<input id="anyo_inicio_actividad" type="number" min="1800" max="2050" value="2000" name="anyo_inicio_actividad" value="<?php echo set_value('anyo_inicio_actividad'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="fecha_ultimo_siniestro" class="form-group-label">Fecha último siniestro</label>
	                <div class='input-group date' id='datetimepicker1'>
	                    <input type='text' class="form-control form-control-gray" id="fecha_ultimo_siniestro" name="fecha_ultimo_siniestro" value="<?php echo set_value('fecha_ultimo_siniestro'); ?>" />
	                    <span class="input-group-addon">
	                        <span class="glyphicon glyphicon-calendar"></span>
	                    </span>
	                </div>	
				</div>
			</div>	

	
			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="asistencia_ferias" class="form-group-label">Asistencia a ferias</label>
				
					<select name="asistencia_ferias" id="asistencia_ferias" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="Sí">Sí</option>
						<option value="No">No</option>
					</select>
				</div>
			</div>	




			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="valor_total_continente" class="form-group-label">Valor total continente</label>
					<input id="valor_total_continente" type="text" name="valor_total_continente" value="<?php echo set_value('valor_total_continente'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="valor_total_contenido" class="form-group-label">Valor total contenido</label>
					<input id="valor_total_contenido" type="text" name="valor_total_contenido" value="<?php echo set_value('valor_total_contenido'); ?>" class="form-control form-control-gray">
				</div>
			</div>			

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="valor_existencia_en_local" class="form-group-label">Valor existencia en el local</label>
					<input id="valor_existencia_en_local" type="text" name="valor_existencia_en_local" value="<?php echo set_value('valor_existencia_en_local'); ?>" class="form-control form-control-gray">
				</div>
			</div>	

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="valor_en_caja_fuerte" class="form-group-label">Valor en caja fuerte</label>
					<input id="valor_en_caja_fuerte" type="text" name="valor_en_caja_fuerte" value="<?php echo set_value('valor_en_caja_fuerte'); ?>" class="form-control form-control-gray">
				</div>
			</div>	

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="valor_en_escaparates" class="form-group-label">Valor en escaparates</label>
					<input id="valor_en_escaparates" type="text" name="valor_en_escaparates" value="<?php echo set_value('valor_en_escaparates'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="valor_en_vitrinas_hoteles" class="form-group-label">Valor en vitrinas hoteles</label>
					<input id="valor_en_vitrinas_hoteles" type="text" name="valor_en_vitrinas_hoteles" value="<?php echo set_value('valor_en_vitrinas_hoteles'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="valor_joyas_en_casa" class="form-group-label">Valor de joyas en casa</label>
					<input id="valor_joyas_en_casa" type="text" name="valor_joyas_en_casa" value="<?php echo set_value('valor_joyas_en_casa'); ?>" class="form-control form-control-gray">
				</div>
			</div>			

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="valor_de_muestrario" class="form-group-label">Valor de muestrario</label>
					<input id="valor_de_muestrario" type="text" name="valor_de_muestrario" value="<?php echo set_value('valor_de_muestrario'); ?>" class="form-control form-control-gray">
				</div>
			</div>	

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="viajantes" class="form-group-label">Viajantes</label>
				
					<select name="viajantes" id="viajantes" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="Sí">Sí</option>
						<option value="No">No</option>
					</select>
				</div>
			</div>	

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="hace_envios" class="form-group-label">Hace envios</label>
				
					<select name="hace_envios" id="hace_envios" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="Sí">Sí</option>
						<option value="No">No</option>
					</select>
				</div>
			</div>																						



																


		</div>
	</fieldset>



	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Medidas de seguridad</legend>

		<div class="range">


			<div class="cell-sm-4">
				<div class="form-group">
					<label for="alarma" class="form-group-label">Alarma</label>
				
					<select name="alarma" id="alarma" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
	                    <option value="Conectada central de alarma">Conectada central de alarma</option>
	                    <option value="Conectada a la policia">Conectada a la policia</option>
	                    <option value="Conectada a casa">Conectada a casa</option>
	                    <option value="No conectada">No conectada</option>
					</select>
				</div>
			</div>	

			<div class="cell-sm-4">
				<div class="form-group">
					<label for="tipo_de_alarma" class="form-group-label">Tipo de alarma</label>
				
					<select name="tipo_de_alarma" id="tipo_de_alarma" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
	                    <option value="Detención perimétrica">Detención perimétrica</option>
	                    <option value="Volumetrica">Volumetrica</option>
	                    <option value="Antisismico">Antisismico</option>
	                    <option value="Detención puntual">Detención puntual</option>
					</select>
				</div>
			</div>				


	

			<div class="cell-sm-4">
				<div class="form-group">
					<label for="pulsador_anti_panico" class="form-group-label">Pulsador anti-panico</label>
				
					<select name="pulsador_anti_panico" id="pulsador_anti_panico" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
	                    <option value="Pulsadores fijos">Pulsadores fijos</option>
	                    <option value="Pulsadores móviles">Pulsadores móviles</option>
	                    <option value="Móviles/Fijos">Móviles/Fijos</option>
					</select>
				</div>
			</div>	

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="clase_caja_fuerte" class="form-group-label">Clase de caja fuerte</label>
				
					<select name="clase_caja_fuerte" id="clase_caja_fuerte" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
	                    <option value="Caja fuerte">Caja fuerte</option>
	                    <option value="Camara acorazada">Camara acorazada</option>
	                    <option value="Caja fuerte y acorazada">Caja fuerte y acorazada</option>
	                    <option value="Bunker">Bunker</option>
					</select>
				</div>
			</div>				

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="seguridad_escaparate" class="form-group-label">Seguridad escaparate</label>
				
					<select name="seguridad_escaparate" id="seguridad_escaparate" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
                    <option value="Persiana ciega">Persiana ciega</option>
                    <option value="Persiana nido de abeja">Persiana nido de abeja</option>
                    <option value="Persiana con rejas">Persiana con rejas</option>
                    <option value="Otro">Otro</option>  
					</select>
				</div>
			</div>	


			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="cristales_en_puertas_y_escaparate" class="form-group-label">Cristales en puertas y escaparate</label>
				
					<select name="cristales_en_puertas_y_escaparate" id="cristales_en_puertas_y_escaparate" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
	                    <option value="Cristal simple">Cristal simple</option>
	                    <option value="Cristal antimotín o antidisturbios">Cristal antimotín o antidisturbios</option>
	                    <option value="Cristal antirrobo">Cristal antirrobo</option>
	                    <option value="Cristal antibala o blindado">Cristal antibala o blindado</option>
	                    <option value="No tiene vitrinas exteriores">No tiene vitrinas exteriores</option>
					</select>
				</div>
			</div>	

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="otros_dispositivos" class="form-group-label">Otros dispositivos</label>
				
					<select name="otros_dispositivos" id="otros_dispositivos" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
	                    <option value="Camara con grabación contínua">Camara con grabación contínua</option>
	                    <option value="Camara con grabación discontinua">Camara con grabación discontinua</option>
	                    <option value="Camaras simuladas">Camaras simuladas</option>
	                    <option value="Vigilante">Vigilante</option>
	                    <option value="Bunker">Bunker</option>
	                    <option value="Otro">Otro</option>  
					</select>
				</div>
			</div>	

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="jaula_acceso_al_local" class="form-group-label">Jaula acceso al local</label>
				
					<select name="jaula_acceso_al_local" id="jaula_acceso_al_local" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="Sí">Sí</option>
						<option value="No">No</option>
					</select>
				</div>
			</div>													
															

		</div>
	</fieldset>	

                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>


					<div class="offset-top-100">
						<h5 class="text-info-dr">DESCARGA DE DOCUMENTOS</h5>
						<hr class="divider divider-lg-left divider-primary divider-80">
						<div class="range">
							<div class="col-md-6"><?php echo anchor(base_url().'public/docs/joyeria/seguro-joyerias-formulario.pdf', img('public/images/pdf_file.png') . ' Seguro multirriesgo para joyeria - Formulario extendido', 'target="_blank"'); ?></div>
							<div class="col-md-6"><?php echo anchor(base_url().'public/docs/joyeria/seguro-joyerias-condiciones.pdf', img('public/images/pdf_file.png') .' Seguro multirriesgo para joyeria - Condiciones seguro', 'target="_blank"'); ?></div>

							<div class="col-md-6"><?php echo anchor(base_url().'public/docs/joyeria/seguro-joyerias-TR-formulario.pdf', img('public/images/pdf_file.png') .' Seguro todo riesgo para joyeria - Formulario extendido', 'target="_blank"'); ?></div>

							<div class="col-md-6"><?php echo anchor(base_url().'public/docs/joyeria/seguro-joyerias-TR-condiciones.pdf', img('public/images/pdf_file.png') .' Seguro todo riesgo para joyeria - Condiciones seguro', 'target="_blank"'); ?></div>														
							

							
						</div>
					</div>

					</div>


					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/joyeria2.png', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro para joyerias'); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

						<p class="offset-top-30">Si eres profesional del sector de la joyeria, esta póliza de seguros y sus coberturas cubrira todas tus necesidades que estas buscas y esperas de un Seguro Multirriesgo o a Todo Riesgo ( All - Risk ) . Esta dirigida a todos lo profesionales ligados al sector de la joyeria:</p>
						<ul>
							<li>Mayoristas</li>
							<li>Minoristas</li>
							<li>Talleres</li>
							<li>Almacenes</li>
							<li>Viajantes</li>
						</ul>
						<p class="offset-top-20">Con esta póliza podrás estar muy tranquilo,estar bien asegurado y contar con las máximas coberturas entre ellas:</p>
						<ul>
							<li>Robo Expoliación</li>
							<li>Hurto</li>
							<li>Viajantes</li>
							<li>Envios nacionales e internacionales</li>
							<li>Responsabilidad Civil</li>
						</ul>
						<p class="text-red offset-top-20">Para conseguir el mejor precio y coberturas es imprescindible que contestes todas las preguntas del cuestionario.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
