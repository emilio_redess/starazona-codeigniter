


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">






      <div class="cell-sm-4">
        <div class="form-group">
          <label for="tipo_rodaje" class=" form-group-label">Tipo de rodaje</label>
        
          <select name="tipo_rodaje" id="tipo_rodaje" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Largo">Largo</option>
            <option value="Corto">Corto</option>
            <option value="Reportaje">Reportaje</option>
            <option value="Publicidad">Publicidad</option>
            <option value="TV">TV</option>
            <option value="Otro">Otro</option>
          </select>
        </div>
      </div>  

      <div class="cell-sm-4">
        <div class="form-group">
          <label for="genero" class=" form-group-label">Genero</label>
        
          <select name="genero" id="genero" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Drama">Drama</option>
            <option value="Comedia">Comedia</option>
            <option value="Musical">Musical</option>
            <option value="Thriller">Thriller</option>
            <option value="Terror">Terror</option>
            <option value="Otro">Otro</option>
          </select>
        </div>
      </div>      

      <div class="cell-sm-4">
        <div class="form-group">
          <label for="capital_asegurada" class=" form-group-label">Capital asegurado R.C.</label>
        
          <select name="capital_asegurada" id="capital_asegurada" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="300.000">300.000 €</option>
            <option value="600.000">600.000 €</option>
            <option value="1.000.000">1.000.000 €</option>
            <option value="Otro">Otro</option>
          </select>
        </div>
      </div>      

   
         <div class="cell-sm-6 offset-top-20">
                <div class="form-group">
                  <label for="valor_rodaje" class="form-group-label">Valor rodaje (con pre- y post-producción)</label>
                  <!--Select 2-->
                  <input id="valor_rodaje" type="text" name="valor_rodaje" value="<?php echo set_value('valor_rodaje'); ?>" class="form-control form-control-gray">
                </div>
      </div>   

         <div class="cell-sm-6 offset-top-20">
                <div class="form-group">
                  <label for="valor_equipos" class="form-group-label">Valor equipos para asegurar</label>
                  <!--Select 2-->
                  <input id="valor_equipos" type="text" name="valor_equipos" value="<?php echo set_value('valor_equipos'); ?>" class="form-control form-control-gray">
                </div>
      </div>       



            <div class="cell-sm-6 offset-top-20">
                <div class="form-group">
                    <label for="fecha_inicio_rodaje" class="form-group-label">Fecha inicio rodaje</label>
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control form-control-gray" id="fecha_inicio_rodaje" name="fecha_inicio_rodaje" value="<?php echo set_value('fecha_inicio_rodaje'); ?>" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>  
                </div>
            </div>  

            <div class="cell-sm-6 offset-top-20">
                <div class="form-group">
                    <label for="fecha_fin_rodaje" class="form-group-label">Fecha fin rodaje</label>
                    <div class='input-group date' id='datetimepicker2'>
                        <input type='text' class="form-control form-control-gray" id="fecha_fin_rodaje" name="fecha_fin_rodaje" value="<?php echo set_value('fecha_fin_rodaje'); ?>" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>  
                </div>
            </div>   

    



      <div class="cell-sm-4 offset-top-20">
        <div class="form-group">
          <label for="efectos_especiales" class=" form-group-label">Efectos Especiales</label>
        
          <select name="efectos_especiales" id="efectos_especiales" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Si">Si</option>
            <option value="No">No</option>
          </select>
        </div>
      </div>      

        
      <div class="cell-sm-4 offset-top-20">
        <div class="form-group">
          <label for="contratar_colectivo_accidente" class=" form-group-label">Contratar colectivo de accidente</label>
        
          <select name="contratar_colectivo_accidente" id="contratar_colectivo_accidente" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Si">Si</option>
            <option value="No">No</option>
          </select>
        </div>
      </div>  



            <div class="cell-sm-4 offset-top-20">
        <div class="form-group">
          <label for="rodaje_lugares_historicos" class=" form-group-label">¿Se rueda en lugares históricos o con objetos históricos?</label>
        
          <select name="rodaje_lugares_historicos" id="rodaje_lugares_historicos" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Si">Si</option>
            <option value="No">No</option>
          </select>
        </div>
      </div>  



      <div class="cell-sm-4 offset-top-20">
        <div class="form-group">
          <label for="rodaje_exteriores" class=" form-group-label">¿Se van a realizar rodajes en exteriores?</label>
        
          <select name="rodaje_exteriores" id="rodaje_exteriores" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Si">Si</option>
            <option value="No">No</option>
          </select>
        </div>
      </div>  



      <div class="cell-sm-4 offset-top-20">
        <div class="form-group">
          <label for="escenas_peligrosas" class=" form-group-label">¿Se van a realizar escenas peligrosas?</label>
        
          <select name="escenas_peligrosas" id="escenas_peligrosas" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Si">Si</option>
            <option value="No">No</option>
          </select>
        </div>
      </div>  


      <div class="cell-sm-4 offset-top-20">
        <div class="form-group">
          <label for="rodaje_con_drones" class=" form-group-label">¿Se van a utilizar para el rodaje Drones?</label>
        
          <select name="rodaje_con_drones" id="rodaje_con_drones" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Si">Si</option>
            <option value="No">No</option>
          </select>
        </div>
      </div>                    





    



        

																			
		</div>
	</fieldset>
                    <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="experiencia_anterior_de_productora" class="form-group-label">Indicar brevemente la experiencia anterior de la productora o productor</label>
                      <textarea id="experiencia_anterior_de_productora" name="experiencia_anterior_de_productora" class="form-control form-control-gray"></textarea>
                    </div>
                </div>
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>

          <div class="offset-top-100">
            <h5 class="text-info-dr">DESCARGA DE DOCUMENTOS</h5>
            <hr class="divider divider-lg-left divider-primary divider-80">
            <div class="range">
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/cinematografia/seguro-cinematografia-formulario.pdf', img('public/images/pdf_file.png') . ' Formulario Extendido', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/cinematografia/seguro-cinematografia-condiciones.pdf', img('public/images/pdf_file.png') . ' Condiciones del seguro', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/cinematografia/CHUBB_Pequenas_Producciones_V11-13.pdf', img('public/images/pdf_file.png') . ' Póliza Pequeños Rodajes. Chubb', 'target="_blank"'); ?></div>

            

            </div>
          </div>
					</div>

					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/cinematografia.jpg', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de ' . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

                        <p class="offset-top-30">Ten cubierta tu trabajo e inversión ante cualquier incidente que pueda ocurrir en todo el proceso de rodaje incluso en las post-producciones.</p>
                        <p class="offset-top-30">Coberturas para equipos, personas, paralizaciones y vehículos.</p>
                        <p class="offset-top-30">Si quieres una oferta más personalizada y ajustada a tus necesidades baja el PDF del formulario extendido, rellénalo y envíalo por <a href="mailto:<?php echo EMAIL_CONTACTO; ?>">correo electrónico</a>.</p>
						<p class="text-red offset-top-30">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
