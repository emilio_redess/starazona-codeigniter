


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">
			<div class="cell-sm-6">
				<div class="form-group">
					<label for="fecha_nacimiento" class="form-group-label">Fecha de nacimiento</label>
					<input id="fecha_nacimiento" type="text" name="fecha_nacimiento" value="<?php echo set_value('fecha_nacimiento'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-6">
				<div class="form-group">
					<label for="sexo_animal" class="form-group-label">Sexo del animal</label>
				
					<select name="sexo_animal" id="sexo_animal" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="Macho">Macho</option>
						<option value="Hembra">Hembra</option>
					</select>
				</div>
			</div>	

			<div class="cell-sm-6 offset-top-20">
				<div class="form-group">
					<label for="valor_animal" class="form-group-label">Valor del animal (€)</label>
					<input id="valor_animal" type="text" name="valor_animal" value="<?php echo set_value('valor_animal'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-6 offset-top-20">
				<div class="form-group">
					<label for="uso_animal" class="form-group-label">Uso del animal</label>
				
					<select name="uso_animal" id="uso_animal" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="Compañía">Compañía</option>
						<option value="Vigilancia">Vigilancia</option>
						<option value="Paseo">Paseo</option>
						<option value="Deportivo">Deportivo</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
			</div>		

			<div class="cell-sm-6 offset-top-20">
				<div class="form-group">
					<label for="clase_animal" class="form-group-label">Clase de animal</label>
				
					<select name="clase_animal" id="clase_animal" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="Perro">Perro</option>
						<option value="Gato">Gato</option>
						<option value="Caballo">Caballo</option>
						<option value="Animal exótico">Animal exótico</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
			</div>	

			<div class="cell-sm-6 offset-top-20">
				<div class="form-group">
					<label for="raza_animal" class="form-group-label">Raza del animal</label>
					<input id="raza_animal" type="text" name="raza_animal" value="<?php echo set_value('raza_animal'); ?>" class="form-control form-control-gray">
				</div>
			</div>	

			<div class="cell-sm-6 offset-top-20">
				<div class="form-group">
					<label for="defecto_fisico_animal" class="form-group-label">¿Tiene algún defecto físico y/o ha sido objeto de alguna intervención quirúrgica y/o ha sufrido o sufre algún traumatismo, accidente o enfermedad?</label>
				
					<select name="defecto_fisico_animal" id="defecto_fisico_animal" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="Sí">Sí</option>
						<option value="No">No</option>
					</select>
				</div>
			</div>																	


		</div>
	</fieldset>



	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Garantías</legend>

		<div class="range">


			<div class="cell-sm-6">
				<div class="form-group">
					<label for="accidentes_animal" class="form-group-label">Accidentes, robo o extravío</label>
				
					<select name="accidentes_animal" id="accidentes_animal" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="100 €">100 €</option>
						<option value="200 €">200 €</option>
						<option value="300 €">300 €</option>
						<option value="400 €">400 €</option>
						<option value="500 €">500 €</option>
						<option value="600 €">600 €</option>
						<option value="700 €">700 €</option>
						<option value="800 €">800 €</option>
						<option value="900 €">900 €</option>
						<option value="1000 €">1000 €</option>
						<option value="2000 €">2000 €</option>
						<option value="3000 €">3000 €</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
			</div>	

			<div class="cell-sm-6">
				<div class="form-group">
					<label for="rc_animal" class="form-group-label">Responsabilidad civil</label>
				
					<select name="rc_animal" id="rc_animal" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="No incluir">No incluir</option>
						<option value="30.000 €">30.000 €</option>
						<option value="60.000 €">60.000 €</option>
						<option value="120.000 €">120.000 €</option>
						<option value="150.000 €">150.000 €</option>
						<option value="180.500 €">180.500 €</option>
						<option value="300.000 €">300.000 €</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
			</div>				


	

			<div class="cell-sm-6 offset-top-20">
				<div class="form-group">
					<label for="sacrificio_animal" class="form-group-label">Sacrificio y eliminación necesaria</label>
				
					<select name="sacrificio_animal" id="sacrificio_animal" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="Sí, hasta 150 €">Sí, hasta 150 €</option>
						<option value="No incluir">No incluir</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
			</div>	

			<div class="cell-sm-6 offset-top-20">
				<div class="form-group">
					<label for="residencia_animal" class="form-group-label">Estancia en residencia</label>
				
					<select name="residencia_animal" id="residencia_animal" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="150 €">150 €</option>
						<option value="300 €">300 €</option>
						<option value="No incluir">No incluir</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
			</div>				

			<div class="cell-sm-6 offset-top-20">
				<div class="form-group">
					<label for="gasto_veterinario_animal" class="form-group-label">Gasto veterinario por accidentes / enfermedad</label>
				
					<select name="gasto_veterinario_animal" id="gasto_veterinario_animal" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="300 €">300 €</option>
						<option value="600 €">600 €</option>
						<option value="910 €">910 €</option>
						<option value="No incluir">No incluir</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
			</div>					
															

		</div>
	</fieldset>	

                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>


					<div class="offset-top-100">
						<h5 class="text-info-dr">DESCARGA DE DOCUMENTOS</h5>
						<hr class="divider divider-lg-left divider-primary divider-80">
						<div class="range">
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/mascotas/seguro-mascotas-perros.pdf', img('public/images/pdf_file.png') . ' Condiciones del seguro de mascotas', 'target="_blank"'); ?></div>
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/mascotas/seguro-mascotas-caballos.pdf', img('public/images/pdf_file.png') .' Condiciones del seguro de caballos', 'target="_blank"'); ?></div>
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/mascotas/seguro-mascotas-exoticos.pdf', img('public/images/pdf_file.png') .' Condiciones del seguro de animales exóticos', 'target="_blank"'); ?></div>

							
						</div>
					</div>

					</div>


					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/mascotas2.png', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro para animales domesticos, peligrosos, exoticos y caballos'); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

						<p class="offset-top-30">Para tu compañero más fiel. Tu para él lo eres todo: su mejor amigo, su protector... y la persona en la que confía plenamente.</p>
						<p>Por eso, tuya es la responsabilidad de hacer que nada altere la tranquilidad de ambos. Seguro de Responsabilidad Civil, Accidente, robo, extravío, veterinario por accidente o enfermedad, estancia en residencia, sacrificio, eliminación restos, ..cualquier imprevisto debe estar cubierto.</p>
						<p class="text-red">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
