


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">


      <div class="cell-sm-6">
        <div class="form-group">
          <label for="tomador" class="form-group-label">Tomador</label>
          <input id="tomador" type="text" name="tomador" value="<?php echo set_value('tomador'); ?>" class="form-control form-control-gray">
        </div>
      </div>


      <div class="cell-sm-6">
        <div class="form-group">
          <label for="CIF_o_DNI" class="form-group-label">CIF o DNI</label>
          <input id="CIF_o_DNI" type="text" name="CIF_o_DNI" value="<?php echo set_value('CIF_o_DNI'); ?>" class="form-control form-control-gray">
        </div>
      </div>


      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="lugar" class="form-group-label">Lugar</label>
          <input id="lugar" type="text" name="lugar" value="<?php echo set_value('lugar'); ?>" class="form-control form-control-gray">
        </div>
      </div>    

      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="efecto_del_evento" class="form-group-label">Efecto del evento (mínimo 15 dias antes)</label>
          <input id="efecto_del_evento" type="text" name="efecto_del_evento" value="<?php echo set_value('efecto_del_evento'); ?>" class="form-control form-control-gray">
        </div>
      </div>     

      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="importe_cache" class="form-group-label">Importe cache (€)</label>
          <input id="importe_cache" type="text" name="importe_cache" value="<?php echo set_value('importe_cache'); ?>" class="form-control form-control-gray">
        </div>
      </div>        


      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="importe_produccion" class="form-group-label">Importe producción (€)</label>
          <input id="importe_produccion" type="text" name="importe_produccion" value="<?php echo set_value('importe_produccion'); ?>" class="form-control form-control-gray">
        </div>
      </div>               



			<div class="cell-sm-6 offset-top-20">
				<div class="form-group">
					<label for="escenario" class="form-group-label">Escenario</label>
				
					<select name="escenario" id="escenario" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Escenario cubierto">Escenario cubierto</option>
            <option value="Aire libre">Aire libre</option> 
					</select>
				</div>
			</div>	

      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="tipo_evento" class="form-group-label">Tipo evento</label>
        
          <select name="tipo_evento" id="tipo_evento" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
                    <option value="Evento musical">Evento musical</option>
                    <option value="Evento deportivo">Evento deportivo</option>
                    <option value="Teatro">Teatro</option>
                    <option value="Feria">Feria</option>
                    <option value="Varios">Varios</option>
                    <option value="Otro">Otro</option>
          </select>
        </div>
      </div>      

      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="coberturas" class="form-group-label">Coberturas que deseas contratar</label>
        
          <select name="coberturas" id="coberturas" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
                    <option value="Climatológicas + otras causas">Climatológicas + otras causas</option>
                    <option value="Incomparecencia + otras causas">Incomparecencia + otras causas</option>
                    <option value="Climatológicas+ incomparecencia + otras causas">Climatológicas+ incomparecencia + otras causas</option>
                    <option value="Hoyo en uno">Hoyo en uno</option>
                    <option value="Otro">Otro</option>
          </select>
        </div>
      </div>         
			



    

																			
		</div>
	</fieldset>
                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>  
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>

						

					</div>

					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/suspension_espectaculos.jpg', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de ' . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

						<p class="offset-top-30">Tendrás aseguradas las perdidas de tus espectáculos musicales, deportivos y de cualquier clase por: incidencia meteorológica, incomparecencia, cualquier causa.</p>
					
						<p class="text-red offset-top-30">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
