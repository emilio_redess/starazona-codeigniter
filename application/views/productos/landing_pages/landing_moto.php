<!-- Page Content-->
<main class="page-content">
	<section class="bg-image-06">
    	<div class="breadcrumb-wrapper">
      		<div class="shell context-dark section-30 section-lg-top-100">
		        <h1 class="offset-top-20 text-ubold">Seguro de moto</h1>
      		</div>
    	</div>
  	</section>

	<section class="section-80 section-md-120">
		    <div class="range">
		        <div class="inset-lg-left-80">
		          	<div class="col-sm-12">
						<iframe class="iframe_ws" src="http://www.stbseguros.com/seguros_de_moto/Motos.htm" frameborder="0" height="1000"></iframe>
					</div>			                 			    			       			     			        			   			                			      
				</div>
			</div>
	</section>
</main>
