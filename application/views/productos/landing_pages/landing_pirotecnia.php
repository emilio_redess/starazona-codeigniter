


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col- cell-sm-6">Información del seguro</legend>

		<div class="range">



      <div class="cell-sm-6">
        <div class="form-group">
          <label for="nombre_empresa" class=" form-group-label">Nombre empresa o persona física</label>
          <input id="nombre_empresa" type="text" name="nombre_empresa" value="<?php echo set_value('nombre_empresa'); ?>" class="form-control form-control-gray">
        </div>
      </div>

      <div class="cell-sm-6">
        <div class="form-group">
          <label for="poblacion" class=" form-group-label">Población</label>
          <input id="poblacion" type="text" name="poblacion" value="<?php echo set_value('poblacion'); ?>" class="form-control form-control-gray">
        </div>
      </div>      






      <div class="cell-sm-6 offset-top-20">
                <div class="form-group">
                  <label class="form-group-label">Número de empleados</label>
                  <!--Select 2-->
                  <input type="number" min="1" max="1000" value="1" class="form-control" name="numero_de_empleados" id="numero_de_empleados">
                </div>
      </div>


      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="facturacion" class=" form-group-label">Facturación</label>
          <input id="facturacion" type="text" name="facturacion" value="<?php echo set_value('facturacion'); ?>" class="form-control form-control-gray">
        </div>
      </div>         


      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="tiene_deposito_o_fabrica" class=" form-group-label">¿Tiene deposito o fábrica?</label>
        
          <select name="tiene_deposito_o_fabrica" id="tiene_deposito_o_fabrica" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Sí">Sí</option>
            <option value="No">No</option>
          </select>
        </div>
      </div>  



      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="actividad" class=" form-group-label">Actividad</label>
          <select name="actividad" id="actividad" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Fabricante con disparo">Fabricante con disparo</option>
            <option value="Fabricante sin disparo">Fabricante sin disparo</option>
            <option value="Almacenista">Almacenista</option>
            <option value="Importador pirotecnia">Importador pirotecnia</option>
            <option value="Almacen Explosivos">Almacen Explosivos</option>
            <option value="Fabrica cartuchera">Fabrica cartuchera</option>                                        
            <option value="Otra">Otra</option>
          </select>
        </div>
      </div>      

      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="tipo_de_seguro" class="form-group-label">Tipo de seguro</label>
          <select name="tipo_de_seguro" id="tipo_de_seguro" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Responsabilidad Civil">Responsabilidad Civil</option>
            <option value="Daños a las instalaciones">Daños a las instalaciones</option>
            <option value="Ampliacion EEUU y Canadá">Ampliacion EEUU y Canadá</option> 
          </select>
        </div>
      </div>          



			<div class="cell-sm-6 offset-top-20">
                <div class="form-group">
                  <label class="form-group-label">Capital asegurado responsabilidad civil</label>
                  <!--Select 2-->
                  <select data-minimum-results-for-search="Infinity" class="form-control select-filter" id="capital_asegurado" name="capital_asegurado">
                    <option value="">Selecciona una opción &#x25BC;</option>
                    <option value="300.600 €">300.600 €</option>
                    <option value="450.000 €">450.000 €</option>
                    <option value="601.000 €">601.000 €</option>
                    <option value="900.000 €">900.000 €</option>
                    <option value="1.200.000 €">1.200.000 €</option>
                    <option value="2.000.000 €">2.000.000 €</option>
                    <option value="3.000.000 €">3.000.000 €</option>
                    <option value="6.000.000 €">6.000.000 €</option>   
                   
                  </select>
                </div>
			</div>

      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="duracion_del_seguro" class=" form-group-label">Duración del seguro</label>
          <select name="duracion_del_seguro" id="duracion_del_seguro" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Temporal">Temporal</option>
            <option value="Anual">Anual</option>
          </select>
        </div>
      </div>        


																			
		</div>
	</fieldset>
                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class=" form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>  
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>

          <div class="offset-top-100">
            <h5 class="text-info-dr">DESCARGA DE DOCUMENTOS</h5>
            <hr class="divider divider-lg-left divider-primary divider-80">
            <div class="range">
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/pirotecnia/pirotecnia-telefonos-emergencia.pdf', img('public/images/pdf_file.png') . ' Teléfonos de emergencia', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/pirotecnia/categorias-productos-pirotecnicos.pdf', img('public/images/pdf_file.png') .' Categoria de productos pirotécnicos', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/pirotecnia/venta-y-uso-productos-pirotecnicos.pdf', img('public/images/pdf_file.png') .' Venta y uso de productos pirotécnicos', 'target="_blank"'); ?></div>

              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/pirotecnia/decreto_pirotecnia.pdf', img('public/images/pdf_file.png') .' Autorizaciones a menores en la CV', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/pirotecnia/ITC 2.pdf', img('public/images/pdf_file.png') .' ORDEN PRE 647/2014 25 ABRIL', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/pirotecnia/2014.05.16 Nota informativa fabricacion uso propio.pdf', img('public/images/pdf_file.png') .' Fabricación propia sin marcada CEE', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/pirotecnia/BOE-A-2015-12054.pdf', img('public/images/pdf_file.png') .' Reglamento Pirotecnia 989 /11/2015', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/pirotecnia/RD-1335-2012.pdf', img('public/images/pdf_file.png') .' Real Decreto 1335/2012', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/pirotecnia/BOE-A-2010-7333.pdf', img('public/images/pdf_file.png') .' Real Decreto 563/2010', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/pirotecnia/RD-230-1998.pdf', img('public/images/pdf_file.png') .' Real Decreto 230/1998', 'target="_blank"'); ?></div>
           
            </div>
          </div>
					</div>

					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/pirotecnia.jpg', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de ' . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

						<p class="offset-top-30">Cubre tu responsabilidad civil frente a terceros por los daños que puedas ocasionar a terceros o a tus empleados en el ejercicio de tu actividad en todas las vertientes de fabricación, transporte, montaje, disparo de fuegos artificiales, importación y exportación. Opción de ampliaciones del capital asegurado de forma temporal, para un solo día o para pirotécnicos invitados a realizar disparos en Festivales o Concursos en España.</p>

            <p class="text-red offset-top-30">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
