


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">







			<div class="cell-sm-6">
				<div class="form-group">
					<label for="tipologia_de_poliza" class="form-group-label">Tipología de poliza</label>
				
					<select name="tipologia_de_poliza" id="tipologia_de_poliza" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
	                	<option value="Individual">Individual</option>                    
	                	<option value="Colectivo">Colectivo</option>
	                	<option value="Convenio">Convenio</option>  
					</select>
				</div>
			</div>	

			<div class="cell-sm-6">
				<div class="form-group">
					<label for="modalidad_de_cobertura" class="form-group-label">Modalidad de cobertura</label>
				
					<select name="modalidad_de_cobertura" id="modalidad_de_cobertura" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
	                	<option value="Las 24 horas del día">Las 24 horas del día</option>
	                	<option value="Solo horas laborales + in itinere">Solo horas laborales + in itinere</option>
	                	<option value="Solo actividad">Solo actividad</option> 
					</select>
				</div>
			</div>				



    

																			
		</div>
	</fieldset>
                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>  
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>

						

					</div>

					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/accidentes.png', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de ' . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

						<p class="offset-top-30">Siéntete bien protegido junto a los tuyos, con nuestro seguro de accidentes personales, ante cualquier imprevisto que te pueda surgir, teniendo asegurada vuestra estabilidad económica.</p>
						<p class="offset-top-30">El futuro de tus empleados puede depender del seguro colectivo de accidentes o convenio colectivo de accidentes que contrates. Con nuestro Seguro de Accidentes personales, tu tranquilidad estará asegurada en todo momento En tu vida profesional, ante cualquier suceso en las horas laborales o los traslados al trabajo. Opción 24 horas.</p>
						<p class="offset-top-30">En tu vida privada estarás asegurado las 24 horas incluso cuando disfrutes de tu tiempo libre y ocio, practicando tus deportes favoritos o actividades diversas. También en nuestro seguro de accidentes personales tendrás la opción a tener los desplazamiento de tus hijos al extranjero por estudios protegido con el producto Seguro de Accidentes para estudiantes.</p>
						<p class="offset-top-30">Contamos con seguros de accidentes personales muy flexibles, con coberturas de lujo, para que decidas cómo cuidar a tu familia Dinos tus necesidades y nosotros te ofrecemos varias opciones y soluciones.</p>
						<p class="text-red offset-top-30">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
