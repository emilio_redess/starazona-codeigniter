


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">

			<div class="cell-sm-4">
				<div class="form-group">
					<label for="actividad" class="form-group-label">Actividad</label>
					<input id="actividad" type="text" name="actividad" value="<?php echo set_value('actividad'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-4">
				<div class="form-group">
					<label for="capital_a_asegurar" class="form-group-label">Capital a asegurar</label>
					<input id="capital_a_asegurar" type="text" name="capital_a_asegurar" value="<?php echo set_value('capital_a_asegurar'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-4">
				<div class="form-group">
					<label for="numero_empleados" class="form-group-label">Nº de empleados</label>
					<input id="numero_empleados" type="number" min="1" max="2000" name="numero_empleados" value="<?php echo set_value('numero_empleados'); ?>" class="form-control form-control-gray">
				</div>
			</div>

										



	


																			
		
		</div>
	</fieldset>

                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>




					</div>


					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/efectos_especiales.jpg', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de ' . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

						
						<p class="text-red">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
