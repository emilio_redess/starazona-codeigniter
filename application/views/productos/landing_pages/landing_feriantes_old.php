
<main>
	<input id="horario" type="hidden" />
	<div class="main animated fadeIn">
		<section>

			<article>
				<div class="container">
					<div class="slide slide_basico_feriantes">
						<div class="slide_left">
							<div class="uppercase">
							<?php echo anchor('seguros/feriantes',img(array('src'=>'public/images/logotipo_mobile.png', 'alt'=> 'Starazona','class' => 'slide_img_right')),array('class' => 'visible-xs')); ?>
								<!--<h4 class="visible-xs slide_left_naranja">Seguro para feriantes</h4> -->				
								<h4 class="visible-xs slide_left_azul"><i class="icon-phone"></i><?php echo TELEFONO_CONTACTO_OFICINA_VALENCIA; ?></h4>		
							</div>						
						</div>						

<!-- ----- formulario ----------------------------------------------------------------------------------------------------------------------------------------- -->

						<div class="slide_right clearfix animated bounceInUp">
							<div class="animated bounceInUp">
								<h4  class="uppercase">Seguro para <b>Feriantes</b></h4>
								<?php echo form_open_multipart('seguros/feriantes'); ?>
								<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro para feriantes" />
									<div class="form-group">
										<?php echo form_error('nombreApellidos'); ?>
										<input type="text" name="nombreApellidos" id="nombreApellidos" placeholder="Escribe tu nombre y apellidos"  maxlength="50" class="form-control" value="<?php echo set_value('nombreApellidos'); ?>">
									</div>

									<div class="form-group">
										<?php echo form_error('email'); ?>
										<input type="email" name="email" id="email" placeholder="Escribe tu email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="form-control" value="<?php echo set_value('email'); ?>">
									</div>

									<div class="form-group">
										<?php echo form_error('telefono'); ?>
										<input type="tel" name="telefono" id="telefono" placeholder="Escribe tu nº de teléfono" pattern="[0-9]{9}" maxlength="9" class="form-control input_phone only_numbers" value="<?php echo set_value('telefono'); ?>">
									</div>

									<div class="form-group">
									<?php echo form_error('producto'); ?>
                                        <select id="producto" name="producto" class="form-control">
                                            <option value="">Tipos de seguros en los que está interesado</option>
                                            <option value="Responsabilidad civil" <?php echo  set_select('producto', 'Responsabilidad civil'); ?> >Responsabilidad civil</option>
                                            <option value="Daños a la atracción" <?php echo  set_select('producto', 'Daños a la atracción'); ?> >Daños a la atracción (robo e incendio)</option>
                                            <option value="Seguro vehículo" <?php echo  set_select('producto', 'Seguro vehículo'); ?> >Seguro vehículo</option>
                                            <option value="Accidentes personales" <?php echo  set_select('producto', 'Accidentes personales'); ?> >Accidentes personales</option>
                                            <option value="Varios seguros" <?php echo  set_select('producto', 'Varios seguros'); ?> >Varios seguros</option>
                                            <option value="Otros" <?php echo  set_select('producto', 'Otros'); ?> >Otros</option>
                                        </select>										
									</div>									
							


																																																																																																										
		

									<div class="checkbox">
										<label>
											<input type="checkbox" > He leído y acepto la <a href="#" data-toggle="modal" data-target="#modal_privacy_policy">política de privacidad</a>
										</label>
									</div>
									<button type="submit" class="btn btn-warning btn-block">Calcula tu precio</button>
								</form>
							</div>						
						</div>
<!-- --------------------------------------------end formulario ------------------------------------------------------------------------------------------------------------------- -->
					</div>
				</div>
			</article>
	
<!-- ---------------------------------------------------------- seccion servicios -------------------------------------------------------------------------------- -->
<article>
				<div class="container">

					<div class="content_wrapper coverages_mobile">
					

									<h2>Garantía y Calidad</h2>
									<p>Cubra todos sus riesgos a través de nuestras pólizas especializadas en el sector de los industriales feriantes y ferias</p>
									<ul>
										<li>Seguro de Responsabilidad Civil Industriales Feriantes</li>
										<li>Seguro Colectivo de Accidentes Feriantes</li>
										<li>Seguro Vida Feriantes</li>
										<li>Seguro vehículos industriales Feriantes</li>
										<li>Seguro viviendas fijas o móviles</li>
										<li>Seguro Almacenes y naves industriales feriantes</li>

									</ul>




																			
					</div>

					<div class="content_wrapper coverages">
						<h2 class="title">Seguros para <b>Industriales Feriantes</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <i class="icon-phone"></i><?php echo TELEFONO_CONTACTO_OFICINA_VALENCIA; ?></h2>
						<div role="tabpanel" data-toggle="tab" class="wrapper_tabpanel clearfix">
							<div class="zoomin zoomin_margin">
								<?php echo img(array('src'=>'public/images/productos/feriantes2.jpg','class' => 'landing-img-zoom')); ?>
							</div>
						

							<div class="tab-content">

	
								<div role="tabpanel" class="animated fadeIn tab-pane active" id="tab2">
									<h2>Garantía y Calidad</h2>
									<p>Cubra todos sus riesgos a través de nuestras pólizas especializadas en el sector de los industriales feriantes y ferias</p>
									<ul>
										<li>Seguro de Responsabilidad Civil Industriales Feriantes</li>
										<li>Seguro Colectivo de Accidentes Feriantes</li>
										<li>Seguro Vida Feriantes</li>
										<li>Seguro vehículos industriales Feriantes</li>
										<li>Seguro viviendas fijas o móviles</li>
										<li>Seguro Almacenes y naves industriales feriantes</li>

									</ul>
								</div>

								<div class="tab_content_bottom">
									<div class="call_me">
										<p>No dudes en contactar con nosotros para resolver tus cuestiones:</p>
										<div class="content_bordered clearfix">
											<div class="col-sm-6">
												<h1><?php echo img('public/images/pixel.png'); ?> <?php echo TELEFONO_CONTACTO_OFICINA_VALENCIA; ?></h1>
											</div>
											<div class="col-sm-6">
												<button type="button" class="btn btn-warning callme_pop">Quiero que me llamen</button>
											</div>
										</div>
										<div class="quote"> * precio sujeto a condiciones de la póliza</div>
									</div>											
								</div>										
							</div>
						</div>
					</div>	
				</div>
			</article>
<!-- ---------------------------------------------------------- end seccion servicios -------------------------------------------------------------------------------- -->

			<article>
				<div class="container">
					<div class="content_wrapper things_to_know">
						<h2 class="title">Otros servicios que te podemos gestionar</h2>
						<div class="row">
							<div class="col-md-4">
								<p class="title">Correduría de seguros</p>
								<p>Somos expertos asesores en seguros. Amplia experiencia tanto en seguros de empresas como particulares.</p>
								<?php echo anchor("http://starazona.com/",'Enlace','target="_blank"') ?>
							</div>
							<div class="col-md-4">
								<p class="title">Todos nuestros productos</p>
								<p>Correduría Salvador Tarazona seguros.</p>
								<?php echo anchor("http://www.starazona.com/productos-correduria-de-seguros",'Enlace','target="_blank"') ?>
							</div>
							<div class="col-md-4">
								<p class="title">Seguros de hogar</p>
								<p>La correduría Starazona ofrece seguros de hogar.</p>
								<?php echo anchor("http://www.starazona.com/seguro-de-hogar/seguro-de-hogar",'Enlace','target="_blank"') ?>
							</div>
						</div>
					</div>
				</div>
			</article>					
		</section>
	</div>
</main>