


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">
			<div class="cell-sm-4">
				<div class="form-group">
					<label for="nif" class="form-group-label">CIF o NIF</label>
					<input id="nif" type="text" name="nif" value="<?php echo set_value('nif'); ?>" class="form-control form-control-gray">
				</div>
			</div>





			<div class="cell-sm-4">
				<div class="form-group">
					<label for="pagina_web" class="form-group-label">Página web</label>
					<input id="pagina_web" type="text" name="pagina_web" value="<?php echo set_value('pagina_web'); ?>" class="form-control form-control-gray">
				</div>
			</div>




																						
			<div class="cell-sm-4">
				<div class="form-group">
					<label for="provincia_del_solicitante" class="form-group-label">Provincia del solicitante</label>
				
					<select name="provincia_del_solicitante" id="provincia_del_solicitante" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
		                <option value="Álava">Álava</option>
		                <option value="Albacete">Albacete</option>
		                <option value="Alicante">Alicante</option>
		                <option value="Almería">Almería</option>
		                <option value="Asturias">Asturias</option>
		                <option value="Ávila">Ávila</option>
		                <option value="Badajoz">Badajoz</option>
		                <option value="Baleares (Islas)">Baleares (Islas)</option>
		                <option value="Barcelona">Barcelona</option>
		                <option value="Burgos">Burgos</option>
		                <option value="Cáceres">Cáceres</option>
		                <option value="Cádiz">Cádiz</option>
		                <option value="Cantabria">Cantabria</option>
		                <option value="Castellón">Castellón</option>
		                <option value="Ceuta">Ceuta</option>
		                <option value="Ciudad real">Ciudad real</option>
		                <option value="Córdoba">Córdoba</option>
		                <option value="Coruña (A)">Coruña (A)</option>
		                <option value="Cuenca">Cuenca</option>
		                <option value="Girona">Girona</option>
		                <option value="Granada">Granada</option>
		                <option value="Guadalajara">Guadalajara</option>
		                <option value="Guipúzcoa">Guipúzcoa</option>
		                <option value="Huelva">Huelva</option>
		                <option value="Huesca">Huesca</option>
		                <option value="Jaén">Jaén</option>
		                <option value="León">León</option>
		                <option value="Lleida">Lleida</option>
		                <option value="Lugo">Lugo</option>
		                <option value="Madrid">Madrid</option>
		                <option value="Málaga">Málaga</option>
		                <option value="Melilla">Melilla</option>
		                <option value="Murcia">Murcia</option>
		                <option value="Navarra">Navarra</option>
		                <option value="Orense">Orense</option>
		                <option value="Palencia">Palencia</option>
		                <option value="Palmas (las)">Palmas (las)</option>
		                <option value="Pontevedra">Pontevedra</option>
		                <option value="Rioja (la)">Rioja (la)</option>
		                <option value="Salamanca">Salamanca</option>
		                <option value="Segovia">Segovia</option>
		                <option value="Sevilla">Sevilla</option>
		                <option value="Soria">Soria</option>
		                <option value="Sta. Cruz Tenerife">Sta. Cruz Tenerife</option>
		                <option value="Tarragona">Tarragona</option>
		                <option value="Teruel">Teruel</option>
		                <option value="Toledo">Toledo</option>
		                <option value="Valencia">Valencia</option>
		                <option value="Valladolid">Valladolid</option>
		                <option value="Vizcaya">Vizcaya</option>
		                <option value="Zamora">Zamora</option>
		                <option value="Zaragoza">Zaragoza</option>
					</select>
				</div>
			</div>



			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="actividad_negocio" class="form-group-label">Actividad del negocio</label>
					<input id="actividad_negocio" type="text" name="actividad_negocio" value="<?php echo set_value('actividad_negocio'); ?>" class="form-control form-control-gray">
				</div>
			</div>			

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="ingresos_brutos" class="form-group-label">Ingresos brutos (€)</label>
					<input id="ingresos_brutos" type="text" name="ingresos_brutos" value="<?php echo set_value('ingresos_brutos'); ?>" class="form-control form-control-gray">
				</div>
			</div>	


																						
			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="zona_emplazamiento" class="form-group-label">Coberturas interesado</label>
				
					<select name="zona_emplazamiento" id="zona_emplazamiento" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
	                    <option value="Cyberedge (A-C)">Cyberedge (A-C)</option>
	                    <option value=" R.C. por actividades multimedia"> R.C. por actividades multimedia</option>
	                    <option value="Extorsion cibernetica">Extorsion cibernetica</option>
	                    <option value="perdida beneficios fallo seguridad">Perdida beneficios fallo seguridad</option>
					</select>
				</div>
			</div>	


																


		</div>
	</fieldset>




                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>


					<div class="offset-top-100">
						<h5 class="text-info-dr">DESCARGA DE DOCUMENTOS</h5>
						<hr class="divider divider-lg-left divider-primary divider-80">
						<div class="range">
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/riesgos_ciberneticos/CyberEdge Brochure Clientes.pdf', img('public/images/pdf_file.png') . ' CyberEdge Brochure Clientes', 'target="_blank"'); ?></div>
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/riesgos_ciberneticos/Cyber Manual de Ventas.pdf', img('public/images/pdf_file.png') .' Cyber Manual de ventas', 'target="_blank"'); ?></div>

							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/riesgos_ciberneticos/CyberEdge 2.0 Perfil de Producto.pdf', img('public/images/pdf_file.png') .' CyberEdge 2.0 Perfil de Producto', 'target="_blank"'); ?></div>

							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/riesgos_ciberneticos/CyberEdge Playbook.pdf', img('public/images/pdf_file.png') .' CyberEdge Playbook', 'target="_blank"'); ?></div>														
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/riesgos_ciberneticos/Quote-pad CyberEdge 2.0_ed.pdf', img('public/images/pdf_file.png') .' Quote-pad CyberEdge 2.0', 'target="_blank"'); ?></div>														
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/riesgos_ciberneticos/CyberEdge 2 0_Iberia.pdf', img('public/images/pdf_file.png') .' CyberEdge 2.0', 'target="_blank"'); ?></div>														
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/riesgos_ciberneticos/CyberEdge 5 razones para contratarlo.pdf', img('public/images/pdf_file.png') .' CyberEdge 5 razones para contratarlo', 'target="_blank"'); ?></div>														
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/riesgos_ciberneticos/CyberEdge Perfil Producto.pdf', img('public/images/pdf_file.png') .' CyberEdge Perfil Producto', 'target="_blank"'); ?></div>														
							<div class="col-md-4"><?php echo anchor(base_url().'public/docs/riesgos_ciberneticos/CyberEdge Quoted Pad 2 0_generalv01_ed.pdf', img('public/images/pdf_file.png') .' CyberEdge Quoted Pad 2.0', 'target="_blank"'); ?></div>														
							

							
						</div>
					</div>

					</div>


					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/riesgos_ciberneticos2.png', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro contra ataques ciberneticos'); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

						<p class="offset-top-30">Envíanos cumplimentado el cuestionario adjunto y en menos de 24 horas recibirás el presupuesto de seguro y las condiciones particulares de la póliza, con las coberturas y capitales asegurados.</p>
			
						<p class="text-red offset-top-20">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
