


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">



         <div class="cell-sm-4">
                <div class="form-group">
                  <label for="nombre_organizador" class="form-group-label">Nombre organizador</label>
                  <!--Select 2-->
                  <input id="nombre_organizador" type="text" name="nombre_organizador" value="<?php echo set_value('nombre_organizador'); ?>" class="form-control form-control-gray">
                </div>
      </div>



      <div class="cell-sm-4">
        <div class="form-group">
          <label for="tipo_de_acto" class=" form-group-label">Tipo de acto</label>
        
          <select name="tipo_de_acto" id="tipo_de_acto" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Bous al carrer">Bous al carrer</option>
            <option value="Corrida de toros">Corrida de toros</option>
            <option value="Bou en corda/ensogado">Bou en corda/ensogado</option>
            <option value="Bou embolat/toro embolado">Bou embolat/toro embolado</option>
            <option value="Sokamuturra">Sokamuturra</option>
            <option value="Toro borracho">Toro borracho</option>  
          </select>
        </div>
      </div>  

      <div class="cell-sm-4">
        <div class="form-group">
          <label for="cumple_normativa_toros_comunidad" class=" form-group-label">Cumple Normativa Toros Comunidad</label>
        
          <select name="cumple_normativa_toros_comunidad" id="cumple_normativa_toros_comunidad" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Si">Si</option>
            <option value="No">No</option>
          </select>
        </div>
      </div>    



            <div class="cell-sm-6 offset-top-20">
                <div class="form-group">
                    <label for="fecha_inicio" class="form-group-label">Fecha inicio</label>
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control form-control-gray" id="fecha_inicio" name="fecha_inicio" value="<?php echo set_value('fecha_inicio'); ?>" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>  
                </div>
            </div>  

            <div class="cell-sm-6 offset-top-20">
                <div class="form-group">
                    <label for="fecha_fin" class="form-group-label">Fecha fin</label>
                    <div class='input-group date' id='datetimepicker2'>
                        <input type='text' class="form-control form-control-gray" id="fecha_fin" name="fecha_fin" value="<?php echo set_value('fecha_fin'); ?>" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>  
                </div>
            </div>   

   

   
         <div class="cell-sm-4 offset-top-20">
                <div class="form-group">
                  <label for="numero_dias_de_sueltas" class="form-group-label">Nº días totales de sueltas</label>
                  <!--Select 2-->
                  <input id="numero_dias_de_sueltas" type="text" name="numero_dias_de_sueltas" value="<?php echo set_value('numero_dias_de_sueltas'); ?>" class="form-control form-control-gray">
                </div>
      </div>   

         <div class="cell-sm-4 offset-top-20">
                <div class="form-group">
                  <label for="poblacion_acto" class="form-group-label">Poblacion Acto</label>
                  <!--Select 2-->
                  <input id="poblacion_acto" type="text" name="poblacion_acto" value="<?php echo set_value('poblacion_acto'); ?>" class="form-control form-control-gray">
                </div>
      </div>  



          <div class="cell-sm-4 offset-top-20">
        <div class="form-group">
          <label for="provincia" class="form-group-label">Provincia</label>
        
          <select name="provincia" id="provincia" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
                    <option value="Álava">Álava</option>
                    <option value="Albacete">Albacete</option>
                    <option value="Alicante">Alicante</option>
                    <option value="Almería">Almería</option>
                    <option value="Asturias">Asturias</option>
                    <option value="Ávila">Ávila</option>
                    <option value="Badajoz">Badajoz</option>
                    <option value="Baleares (Islas)">Baleares (Islas)</option>
                    <option value="Barcelona">Barcelona</option>
                    <option value="Burgos">Burgos</option>
                    <option value="Cáceres">Cáceres</option>
                    <option value="Cádiz">Cádiz</option>
                    <option value="Cantabria">Cantabria</option>
                    <option value="Castellón">Castellón</option>
                    <option value="Ceuta">Ceuta</option>
                    <option value="Ciudad real">Ciudad real</option>
                    <option value="Córdoba">Córdoba</option>
                    <option value="Coruña (A)">Coruña (A)</option>
                    <option value="Cuenca">Cuenca</option>
                    <option value="Girona">Girona</option>
                    <option value="Granada">Granada</option>
                    <option value="Guadalajara">Guadalajara</option>
                    <option value="Guipúzcoa">Guipúzcoa</option>
                    <option value="Huelva">Huelva</option>
                    <option value="Huesca">Huesca</option>
                    <option value="Jaén">Jaén</option>
                    <option value="León">León</option>
                    <option value="Lleida">Lleida</option>
                    <option value="Lugo">Lugo</option>
                    <option value="Madrid">Madrid</option>
                    <option value="Málaga">Málaga</option>
                    <option value="Melilla">Melilla</option>
                    <option value="Murcia">Murcia</option>
                    <option value="Navarra">Navarra</option>
                    <option value="Orense">Orense</option>
                    <option value="Palencia">Palencia</option>
                    <option value="Palmas (las)">Palmas (las)</option>
                    <option value="Pontevedra">Pontevedra</option>
                    <option value="Rioja (la)">Rioja (la)</option>
                    <option value="Salamanca">Salamanca</option>
                    <option value="Segovia">Segovia</option>
                    <option value="Sevilla">Sevilla</option>
                    <option value="Soria">Soria</option>
                    <option value="Sta. Cruz Tenerife">Sta. Cruz Tenerife</option>
                    <option value="Tarragona">Tarragona</option>
                    <option value="Teruel">Teruel</option>
                    <option value="Toledo">Toledo</option>
                    <option value="Valencia">Valencia</option>
                    <option value="Valladolid">Valladolid</option>
                    <option value="Vizcaya">Vizcaya</option>
                    <option value="Zamora">Zamora</option>
                    <option value="Zaragoza">Zaragoza</option>
          </select>
        </div>
      </div> 

           

     
      <div class="cell-sm-4 offset-top-20">
        <div class="form-group">
          <label for="caracteristicas_lugar_del_acto" class=" form-group-label">Caracteristicas lugar del acto</label>
        
          <select name="caracteristicas_lugar_del_acto" id="caracteristicas_lugar_del_acto" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Circuito urbano con vallas">Circuito urbano con vallas</option>
            <option value="Circuito urbano sin vallas">Circuito urbano sin vallas</option>
            <option value="Circuito urbano con cuerda">Circuito urbano con cuerda</option>
            <option value="Plaza de toros">Plaza de toros</option>
            <option value="Otro">Otro</option>
          </select>
        </div>
      </div>      

        
      <div class="cell-sm-4 offset-top-20">
        <div class="form-group">
          <label for="capital_responsabilidad_civil" class=" form-group-label">Capital Responsabilidad civil</label>
        
          <select name="capital_responsabilidad_civil" id="capital_responsabilidad_civil" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="según decreto comunidad">según decreto comunidad</option>
            <option value="150.253">150.253€</option>
            <option value="180.000">180.000€</option>
            <option value="300.506">300.506€</option>
            <option value="450.000">450.000€</option>
            <option value="533.000">533.000€</option>
            <option value="600.000">600.000€</option>
            <option value="750.000">750.000€</option>
            <option value="1.200.000">1.200.000€</option>
            <option value="Otro">Otro</option>
          </select>
        </div>
      </div>  

      <div class="cell-sm-4 offset-top-20">
        <div class="form-group">
          <label for="limite_por_victima_responsabilidad_civil" class=" form-group-label">Límite por víctima R.Civil</label>
        
          <select name="limite_por_victima_responsabilidad_civil" id="limite_por_victima_responsabilidad_civil" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="según decreto comunidad">según decreto comunidad</option>
            <option value="60.100">60.100€</option>
            <option value="75.000">75.000€</option>
            <option value="90.000">90.000€</option>
            <option value="115.253">115.253€</option>
            <option value="150.000">150.000€</option>
            <option value="150.253">150.253€</option>
            <option value="Otro">Otro</option>
          </select>
        </div>
      </div>        



            <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="colectivo_de_accidente" class=" form-group-label">Colectivo de accidente</label>
        
          <select name="colectivo_de_accidente" id="colectivo_de_accidente" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Si">Si</option>
            <option value="No">No</option>
          </select>
        </div>
      </div>  



      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="seguro_cadafales" class=" form-group-label">Seguro Cadafales</label>
        
          <select name="seguro_cadafales" id="seguro_cadafales" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Si">Si</option>
            <option value="No">No</option>
          </select>
        </div>
      </div>  



      <div class="cell-sm-4 offset-top-20">
        <div class="form-group">
          <label for="muerte" class=" form-group-label">Muerte</label>
        
          <select name="muerte" id="muerte" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Según decreto comunidad">Según decreto comunidad</option>
            <option value="6.000">6.000€</option>
            <option value="12.000">12.000€</option>
            <option value="18.000">18.000€</option>
            <option value="24.000">24.000€</option>
            <option value="Ilimitado">Ilimitado</option>
          </select>
        </div>
      </div>  

      <div class="cell-sm-4 offset-top-20">
        <div class="form-group">
          <label for="invalidez" class=" form-group-label">Invalidez</label>
        
          <select name="invalidez" id="invalidez" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Según decreto comunidad">Según decreto comunidad</option>
            <option value="6.000">6.000€</option>
            <option value="12.000">12.000€</option>
            <option value="18.000">18.000€</option>
            <option value="24.000">24.000€</option>
            <option value="Otro">Otro</option>
          </select>
        </div>
      </div>        


      <div class="cell-sm-4 offset-top-20">
        <div class="form-group">
          <label for="gastos_de_curacion" class=" form-group-label">Gastos de curación</label>
        
          <select name="gastos_de_curacion" id="gastos_de_curacion" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="según decreto comunidad">Según decreto comunidad</option>
            <option value="6.000">6.000€</option>
            <option value="12.000">12.000€</option>
            <option value="18.000">18.000€</option>
            <option value="24.000">24.000€</option>
            <option value="225.000">225.000€</option> 
            <option value="Otro">Otro</option>
          </select>
        </div>
      </div>        
                  





    



        

																			
		</div>
	</fieldset>
                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>

          <div class="offset-top-100">
            <h5 class="text-info-dr">DESCARGA DE DOCUMENTOS</h5>
            <hr class="divider divider-lg-left divider-primary divider-80">
            <div class="range">
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/bous_carrer/2-decreto-24-2007-reglamento-bous-al-carrer-comunidad-valenciana.pdf', img('public/images/pdf_file.png') . ' Reglamento Bous al Carrer C. Valenciana', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/bous_carrer/120-2010.pdf', img('public/images/pdf_file.png') . ' Fiestas tradicionales con toros en Cataluña', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/bous_carrer/normativa-taurina-castilla-la-mancha.pdf', img('public/images/pdf_file.png') . ' Normativa taurina Castilla la Mancha', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/bous_carrer/10-1991.pdf', img('public/images/pdf_file.png') . ' Potestades administrativas en materia de espectáculos taurinos', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/bous_carrer/toros-por-comunidad.pdf', img('public/images/pdf_file.png') . ' Legislación toros por comunidades', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/bous_carrer/solicitud-auto-c-v.pdf', img('public/images/pdf_file.png') . ' Solicitud Autorización Comunidad Valenciana', 'target="_blank"'); ?></div>
           
         

            

            </div>
          </div>
					</div>

					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/bous_carrer.jpg', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de ' . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

            <p class="offset-top-30">Ten bien asegurado todos tus eventos taurinos, en todas sus modalidades, a través de una póliza de seguros para toros. Precios inmejorables con descuentos especiales por contratar varios días. Nuestro seguro de toros será tu seguro más seguro y parte del éxito de tu fiesta.</p>
                       
						<p class="text-red offset-top-30">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
