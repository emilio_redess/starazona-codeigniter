


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">



      <div class="cell-sm-6">
                <div class="form-group">
                  <label for="nombre_grupo_musical" class="form-group-label">Nombre grupo musical / artista</label>
                  <!--Select 2-->
                  <input id="nombre_grupo_musical" type="text" name="nombre_grupo_musical" value="<?php echo set_value('nombre_grupo_musical'); ?>" class="form-control form-control-gray">
                </div>
      </div>



      <div class="cell-sm-6">
        <div class="form-group">
          <label for="opciones" class=" form-group-label">Opciones</label>
        
          <select name="opciones" id="opciones" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Con fuegos artificiales o fx">Con fuegos artificiales o fx</option>
            <option value="Sin fuegos artifciales">Sin fuegos artifciales</option>
          </select>
        </div>
      </div>  

      <div class="cell-sm-6  offset-top-20">
        <div class="form-group">
          <label for="tipo_evento" class=" form-group-label">Tipo evento</label>
        
          <select name="tipo_evento" id="tipo_evento" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="Evento musical">Evento musical</option>
            <option value="Evento deportivo">Evento deportivo</option>
            <option value="Teatro">Teatro</option>
            <option value="Feria">Feria</option>
            <option value="Varios">Varios</option>
            <option value="Otro">Otro</option>
          </select>
        </div>
      </div>      

      <div class="cell-sm-6  offset-top-20">
        <div class="form-group">
          <label for="suma_asegurada" class=" form-group-label">Suma asegurada</label>
        
          <select name="suma_asegurada" id="suma_asegurada" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
            <option value="300.000 €">300.000 €</option>
            <option value="600.000 €">600.000 €</option>
            <option value="1.200.000 €">1.200.000 €</option>
            <option value="2.000.000 €">2.000.000 €</option>
            <option value="2.000.000 €">3.000.000 €</option>
            <option value="Otro">Otro</option>
          </select>
        </div>
      </div>      

      



            <div class="cell-sm-6 offset-top-20">
                <div class="form-group">
                    <label for="fecha_efecto_poliza" class="form-group-label">Fecha efecto póliza</label>
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control form-control-gray" id="fecha_efecto_poliza" name="fecha_efecto_poliza" value="<?php echo set_value('fecha_efecto_poliza'); ?>" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>  
                </div>
            </div>  

            <div class="cell-sm-6 offset-top-20">
                <div class="form-group">
                    <label for="fecha_vencimiento_poliza" class="form-group-label">Fecha vencimiento póliza</label>
                    <div class='input-group date' id='datetimepicker2'>
                        <input type='text' class="form-control form-control-gray" id="fecha_vencimiento_poliza" name="fecha_vencimiento_poliza" value="<?php echo set_value('fecha_vencimiento_poliza'); ?>" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>  
                </div>
            </div>   

            <div class="cell-sm-6 offset-top-20">
                <div class="form-group">
                  <label for="volumen_facturacion" class="form-group-label">Volumen de Facturación anual / evento</label>
                  <!--Select 2-->
                  <input id="volumen_facturacion" type="text" name="volumen_facturacion" value="<?php echo set_value('volumen_facturacion'); ?>" class="form-control form-control-gray">
                </div>
            </div>      


      <div class="cell-sm-6  offset-top-20">
        <div class="form-group">
          <label for="duracion_poliza" class=" form-group-label">Duración póliza</label>
        
          <select name="duracion_poliza" id="duracion_poliza" class="form-control form-control-gray">
            <option value="">selecciona una opcion...</option>
            <option value="Anual">Anual</option>
            <option value="Temporal">Temporal</option>
          </select>
        </div>
      </div> 

      <div class="cell-sm-6  offset-top-20">
        <div class="form-group">
          <label for="rc_locativa" class=" form-group-label">R.C Locativa</label>
        
          <select name="rc_locativa" id="rc_locativa" class="form-control form-control-gray">
            <option value="">selecciona una opcion...</option>
            <option value="Si">Si</option>
            <option value="No">No</option>
          </select>
        </div>
      </div>      

      <div class="cell-sm-6  offset-top-20">
        <div class="form-group">
          <label for="salida_a_extranjero" class=" form-group-label">Salida a Extranjero</label>
        
          <select name="salida_a_extranjero" id="salida_a_extranjero" class="form-control form-control-gray">
            <option value="">selecciona una opcion...</option>
            <option value="Si">Si</option>
            <option value="No">No</option>
          </select>
        </div>
      </div>         





    



        

																			
		</div>
	</fieldset>
                    <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>

					</div>

					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/rc_espectaculos.jpg', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de ' . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

                        <p class="offset-top-30">Ten cubierta la Responsabilidad Civil profesional de tu empresa y de tus empleados, con unas coberturas muy importantes.</p>
						<p class="text-red offset-top-30">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
