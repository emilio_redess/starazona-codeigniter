


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">


			<div class="cell-sm-6">
	            <div class="form-group">
	              <label class="form-group-label">Provincia del asegurado</label>
	              <!--Select 2-->
	              <select data-minimum-results-for-search="Infinity" class="form-control select-filter" id="provincia" name="provincia">
	                <option value="">Selecciona una opción &#x25BC;</option>
	                <option value="01">Álava</option>
	                <option value="02">Albacete</option>
	                <option value="03">Alicante</option>
	                <option value="04">Almería</option>
	                <option value="33">Asturias</option>
	                <option value="05">Ávila</option>
	                <option value="06">Badajoz</option>
	                <option value="07">Baleares (Islas)</option>
	                <option value="08">Barcelona</option>
	                <option value="09">Burgos</option>
	                <option value="10">Cáceres</option>
	                <option value="11">Cádiz</option>
	                <option value="39">Cantabria</option>
	                <option value="12">Castellón</option>
	                <option value="51">Ceuta</option>
	                <option value="13">Ciudad real</option>
	                <option value="14">Córdoba</option>
	                <option value="15">Coruña (A)</option>
	                <option value="16">Cuenca</option>
	                <option value="17">Girona</option>
	                <option value="18">Granada</option>
	                <option value="19">Guadalajara</option>
	                <option value="20">Guipúzcoa</option>
	                <option value="21">Huelva</option>
	                <option value="22">Huesca</option>
	                <option value="23">Jaén</option>
	                <option value="24">León</option>
	                <option value="25">Lleida</option>
	                <option value="27">Lugo</option>
	                <option value="28">Madrid</option>
	                <option value="29">Málaga</option>
	                <option value="52">Melilla</option>
	                <option value="30">Murcia</option>
	                <option value="31">Navarra</option>
	                <option value="32">Orense</option>
	                <option value="34">Palencia</option>
	                <option value="35">Palmas (las)</option>
	                <option value="36">Pontevedra</option>
	                <option value="26">Rioja (la)</option>
	                <option value="37">Salamanca</option>
	                <option value="40">Segovia</option>
	                <option value="41">Sevilla</option>
	                <option value="42">Soria</option>
	                <option value="38">Sta. Cruz Tenerife</option>
	                <option value="43">Tarragona</option>
	                <option value="44">Teruel</option>
	                <option value="45">Toledo</option>
	                <option value="46">Valencia</option>
	                <option value="47">Valladolid</option>
	                <option value="48">Vizcaya</option>
	                <option value="49">Zamora</option>
	                <option value="50">Zaragoza</option>
	              </select>
	            </div>
			</div>



			<div class="cell-sm-6">
	            <div class="form-group">
	              <label class="form-group-label">Número de personas</label>
	              <!--Select 2-->
	              <select data-minimum-results-for-search="Infinity" class="form-control select-filter" id="numeroPersonas" name="numeroPersonas">
	                <option value="">Selecciona una opción &#x25BC;</option>
	                <option value="1">1</option>
	                <option value="2">2</option>
	                <option value="3">3</option>
	                <option value="4">4</option>
	                <option value="5">5</option>
	                <option value="6">6</option>
	              </select>
	            </div>
			</div>

			<div class="cell-sm-6 offset-top-20">
              <div class="form-group edad_hide">
                  <label class="form-group-label">Edad 1</label>
                  <input type="number" min="0" max="99" value="50" class="form-control" name="edad-1" id="edad-1" data-indice="1">
              </div>
			</div>		

			<div class="cell-sm-6 offset-top-20">
              <div class="form-group edad_hide">
                  <label class="form-group-label">Edad 2</label>
                  <input type="number" min="0" max="99" value="50" class="form-control" name="edad-2" id="edad-2" data-indice="2">
              </div>
			</div>	

			<div class="cell-sm-6 offset-top-20">
              <div class="form-group edad_hide">
                  <label class="form-group-label">Edad 3</label>
                  <input type="number" min="0" max="99" value="50" class="form-control" name="edad-3" id="edad-3" data-indice="3">
              </div>
			</div>	

			<div class="cell-sm-6 offset-top-20">
              <div class="form-group edad_hide">
                  <label class="form-group-label">Edad 4</label>
                  <input type="number" min="0" max="99" value="50" class="form-control" name="edad-4" id="edad-4" data-indice="4">
              </div>
			</div>		

			<div class="cell-sm-6 offset-top-20">
              <div class="form-group edad_hide">
                  <label class="form-group-label">Edad 5</label>
                  <input type="number" min="0" max="99" value="50" class="form-control" name="edad-5" id="edad-5" data-indice="5">
              </div>
			</div>	

			<div class="cell-sm-6 offset-top-20">
              <div class="form-group edad_hide">
                  <label class="form-group-label">Edad 6</label>
                  <input type="number" min="0" max="99" value="50" class="form-control" name="edad-6" id="edad-6" data-indice="6">
              </div>
			</div>																			


		</div>
	</fieldset>


                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>




					</div>


					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/decesos.jpg', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de '  . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

						<p class="offset-top-30">Consigue el mejor precio y coberturas en tu seguro de decesos y el de toda tu familia.</p>
						<p>En caso de desear alguna cobertura especial o complementaria díselo a nuestros especialistas y gustosamente te informarán.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
