


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">



            <div class="cell-sm-6">
                <div class="form-group">
                    <label for="fecha_disparo" class="form-group-label">Fecha disparo</label>
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control form-control-gray" id="fecha_disparo" name="fecha_disparo" value="<?php echo set_value('fecha_disparo'); ?>" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>  
                </div>
            </div>  



			<div class="cell-sm-6">
                <div class="form-group">
                  <label for="franja_horaria" class="form-group-label">Franja horaria (Inicio - Fin)</label>
                  <!--Select 2-->
                  <input id="franja_horaria" type="text" name="franja_horaria" value="<?php echo set_value('franja_horaria'); ?>" class="form-control form-control-gray">
                </div>
			</div>

            <div class="cell-sm-6 offset-top-20">
                <div class="form-group">
                  <label for="kilos_reglamentados" class="form-group-label">Kgs. Reglamentados (mínimo 100 Kgs.)</label>
                  <!--Select 2-->
                  <input id="kilos_reglamentados" type="text" name="kilos_reglamentados" value="<?php echo set_value('kilos_reglamentados'); ?>" class="form-control form-control-gray">
                </div>
            </div>        

            <div class="cell-sm-6 offset-top-20">
                <div class="form-group">
                  <label for="capital_seguro" class="form-group-label">Capital Seguro R.C. € (Kgs. x 500 €)</label>
                  <!--Select 2-->
                  <input id="capital_seguro" type="text" name="capital_seguro" value="<?php echo set_value('capital_seguro'); ?>" class="form-control form-control-gray">
                </div>
            </div>  

            <div class="cell-sm-6 offset-top-20">
                <div class="form-group">
                  <label for="poblacion_evento" class="form-group-label">Población/Lugar del evento</label>
                  <!--Select 2-->
                  <input id="poblacion_evento" type="text" name="poblacion_evento" value="<?php echo set_value('poblacion_evento'); ?>" class="form-control form-control-gray">
                </div>
            </div>                              




      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="provincia" class="form-group-label">Provincia</label>
        
          <select name="provincia" id="provincia" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
                    <option value="Álava">Álava</option>
                    <option value="Albacete">Albacete</option>
                    <option value="Alicante">Alicante</option>
                    <option value="Almería">Almería</option>
                    <option value="Asturias">Asturias</option>
                    <option value="Ávila">Ávila</option>
                    <option value="Badajoz">Badajoz</option>
                    <option value="Baleares (Islas)">Baleares (Islas)</option>
                    <option value="Barcelona">Barcelona</option>
                    <option value="Burgos">Burgos</option>
                    <option value="Cáceres">Cáceres</option>
                    <option value="Cádiz">Cádiz</option>
                    <option value="Cantabria">Cantabria</option>
                    <option value="Castellón">Castellón</option>
                    <option value="Ceuta">Ceuta</option>
                    <option value="Ciudad real">Ciudad real</option>
                    <option value="Córdoba">Córdoba</option>
                    <option value="Coruña (A)">Coruña (A)</option>
                    <option value="Cuenca">Cuenca</option>
                    <option value="Girona">Girona</option>
                    <option value="Granada">Granada</option>
                    <option value="Guadalajara">Guadalajara</option>
                    <option value="Guipúzcoa">Guipúzcoa</option>
                    <option value="Huelva">Huelva</option>
                    <option value="Huesca">Huesca</option>
                    <option value="Jaén">Jaén</option>
                    <option value="León">León</option>
                    <option value="Lleida">Lleida</option>
                    <option value="Lugo">Lugo</option>
                    <option value="Madrid">Madrid</option>
                    <option value="Málaga">Málaga</option>
                    <option value="Melilla">Melilla</option>
                    <option value="Murcia">Murcia</option>
                    <option value="Navarra">Navarra</option>
                    <option value="Orense">Orense</option>
                    <option value="Palencia">Palencia</option>
                    <option value="Palmas (las)">Palmas (las)</option>
                    <option value="Pontevedra">Pontevedra</option>
                    <option value="Rioja (la)">Rioja (la)</option>
                    <option value="Salamanca">Salamanca</option>
                    <option value="Segovia">Segovia</option>
                    <option value="Sevilla">Sevilla</option>
                    <option value="Soria">Soria</option>
                    <option value="Sta. Cruz Tenerife">Sta. Cruz Tenerife</option>
                    <option value="Tarragona">Tarragona</option>
                    <option value="Teruel">Teruel</option>
                    <option value="Toledo">Toledo</option>
                    <option value="Valencia">Valencia</option>
                    <option value="Valladolid">Valladolid</option>
                    <option value="Vizcaya">Vizcaya</option>
                    <option value="Zamora">Zamora</option>
                    <option value="Zaragoza">Zaragoza</option>
          </select>
        </div>
      </div>



            <div class="cell-sm-6 offset-top-20">
                <div class="form-group">
                  <label for="pirotecnia_encargada" class="form-group-label">Pirotecnia encargada del espectáculo</label>
                  <!--Select 2-->
                  <input id="pirotecnia_encargada" type="text" name="pirotecnia_encargada" value="<?php echo set_value('pirotecnia_encargada'); ?>" class="form-control form-control-gray">
                </div>
            </div>    

																			
		</div>
	</fieldset>
                    <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>

          <div class="offset-top-100">
            <h5 class="text-info-dr">DESCARGA DE DOCUMENTOS</h5>
            <hr class="divider divider-lg-left divider-primary divider-80">
            <div class="range">
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/organizacion_pirotecnia/BOE-A-2015-12054.pdf', img('public/images/pdf_file.png') . ' Reglamento Pirotecnia 989 /11/2015', 'target="_blank"'); ?></div>

            

            </div>
          </div>
					</div>

					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/organizacion_pirotecnia.jpg', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de ' . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

                        <p class="offset-top-30">Seguro de responsabilidad civil que cubrirá los daños que ocasiones a terceros o empleados en el ejercicio de tu actividad pudiendo hacer coberturas anuales como temporales para tiendas y kioskos.</p>
						<p class="text-red offset-top-30">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
