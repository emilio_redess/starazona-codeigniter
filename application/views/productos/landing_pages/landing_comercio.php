


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">



			<div class="cell-sm-4">
				<div class="form-group">
					<label for="actividad" class="form-group-label">Actividad</label>
				
					<select name="actividad" id="actividad" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
	                    <option value="Alimentación">Alimentación</option>
	                    <option value="Automoción y maquinaria">Automoción y maquinaria</option>
	                    <option value="Electricidad y electrónica">Electricidad y electrónica</option>
	                    <option value="Enseñanza">Enseñanza</option>
	                    <option value="Hosteleria">Hosteleria</option>
	                    <option value="Limpieza">Limpieza</option>
	                    <option value="Materiales de construcción">Materiales de construcción</option>
	                    <option value="Metalurgia">Metalurgia</option>
	                    <option value="Muebles y decoración">Muebles y decoración</option>
	                    <option value="Ocio">Ocio</option>
	                    <option value="Sanidad">Sanidad</option>
	                    <option value="Textiles y pieles">Textiles y pieles</option>
	                    <option value="Diversos y locales vacíos">Diversos y locales vacíos</option>
					    <option value="Otra actividad">Otra actividad</option>
					</select>
				</div>
			</div>




			<div class="cell-sm-4">
				<div class="form-group">
					<label for="codigo_postal" class="form-group-label">Código postal del comercio</label>
					<input id="codigo_postal" type="text" name="codigo_postal" value="<?php echo set_value('codigo_postal'); ?>" class="form-control form-control-gray">
				</div>
			</div>





			<div class="cell-sm-4">
				<div class="form-group">
					<label for="metros_cuadrados" class="form-group-label">Metros cuadrados</label>
					<input id="metros_cuadrados" type="text" name="metros_cuadrados" value="<?php echo set_value('metros_cuadrados'); ?>" class="form-control form-control-gray">
				</div>
			</div>



			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="anyo_construccion" class="form-group-label">Año de construcción</label>
					<input id="anyo_construccion" type="number" min="1900" max="2050" name="anyo_construccion" value="<?php echo set_value('anyo_construccion'); ?>" class="form-control form-control-gray">
				</div>
			</div>			

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="volumen_facturacion" class="form-group-label">Volumen de facturación (€)</label>
					<input id="volumen_facturacion" type="text" name="volumen_facturacion" value="<?php echo set_value('volumen_facturacion'); ?>" class="form-control form-control-gray">
				</div>
			</div>	


			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="numero_empleados" class="form-group-label">Nº de empleados</label>
					<input id="numero_empleados" type="text" name="numero_empleados" value="<?php echo set_value('numero_empleados'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="continente" class="form-group-label">Continente</label>
					<input id="continente" type="text" name="continente" value="<?php echo set_value('continente'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="contenido" class="form-group-label">Contenido</label>
					<input id="contenido" type="text" name="contenido" value="<?php echo set_value('contenido'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-4 offset-top-20">
				<div class="form-group">
					<label for="existencias" class="form-group-label">Existencias</label>
					<input id="existencias" type="text" name="existencias" value="<?php echo set_value('existencias'); ?>" class="form-control form-control-gray">
				</div>
			</div>									

																						
	

		</div>
	</fieldset>

                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>


					</div>


					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/comercio2.png', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de '  . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

						<p class="offset-top-30">Ten bien cubierto tu negocio o despacho profesional a través de nuestra póliza de multirriesgo de comercio y oficinas, póliza con la cual podrás estar tranquilo en caso de cualquier accidente o contingencia adversa.</p>
			
						<p class="text-red offset-top-20">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
