


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">

			<div class="cell-sm-3">
				<div class="form-group">
					<label for="anyo_construccion" class="form-group-label">Año de construcción</label>
					<input id="anyo_construccion" type="text" name="anyo_construccion" value="<?php echo set_value('anyo_construccion'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-3">
				<div class="form-group">
					<label for="numero_edificios" class="form-group-label">Nº Edificios</label>
					<input id="numero_edificios" type="text" name="numero_edificios" value="<?php echo set_value('numero_edificios'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-3">
				<div class="form-group">
					<label for="continente" class="form-group-label">Continente</label>
					<input id="continente" type="text" name="continente" value="<?php echo set_value('continente'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-3">
				<div class="form-group">
					<label for="cp_comunidad" class="form-group-label">Código postal de la Comunidad</label>
					<input id="cp_comunidad" type="text" name="cp_comunidad" value="<?php echo set_value('cp_comunidad'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-3 offset-top-20">
				<div class="form-group">
					<label for="altura_comunidad" class="form-group-label">Alturas de la Comunidad</label>
					<input id="altura_comunidad" type="text" name="altura_comunidad" value="<?php echo set_value('altura_comunidad'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-3 offset-top-20">
				<div class="form-group">
					<label for="superficie" class="form-group-label">Superficie m²</label>
					<input id="superficie" type="text" name="superficie" value="<?php echo set_value('superficie'); ?>" class="form-control form-control-gray">
				</div>
			</div>

			<div class="cell-sm-3 offset-top-20">
				<div class="form-group">
					<label for="numero_viviendas" class="form-group-label">Nº de viviendas</label>
					<input id="numero_viviendas" type="text" name="numero_viviendas" value="<?php echo set_value('numero_viviendas'); ?>" class="form-control form-control-gray">
				</div>
			</div>																		

															




			<div class="cell-sm-3 offset-top-20">
				<div class="form-group">
					<label for="zona_ajardinada" class="form-group-label">Zona Ajardinada</label>
				
					<select name="zona_ajardinada" id="zona_ajardinada" class="form-control form-control-gray">
						<option value="">selecciona una opcion... &#x25BC;</option>
						<option value="Sí">Sí</option>
						<option value="No">No</option>
					</select>
				</div>
			</div>		


																			
		
		</div>
	</fieldset>

                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>




					</div>


					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/comunidades.jpg', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de ' . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

						<p class="offset-top-30">Ten bien cubierto el edificio de tu comunidad de vecinos y descansa tranquilo en caso de cualquier siniestro o imprevisto.</p>
						
						<p class="text-red">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde a todas las preguntas del Formulario de Comunidades y Edificios para poder ofrecerte la mejor póliza de seguros.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
