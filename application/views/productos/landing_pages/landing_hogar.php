


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">

      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="tipo_vivienda" class="form-group-label">Tipo de vivienda que habitas </label>
        
          <select name="tipo_vivienda" id="tipo_vivienda" class="form-control form-control-gray selectpicker">
            <option value="">selecciona una opcion... &#x25BC;</option>
                    <option value="Piso en altura">Piso en altura</option>
                    <option value="Piso en planta baja">Piso en planta baja</option>
                    <option value="Ático">Ático</option>
                    <option value="Unifamiliar">Unifamiliar</option>
                    <option value="Adosado">Adosado</option>
            </select>
            </div>
        </div>

      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="ubicacion_vivienda" class="form-group-label">Ubicación de la vivienda</label>
        
          <select name="ubicacion_vivienda" id="ubicacion_vivienda" class="form-control form-control-gray selectpicker">
            <option value="">selecciona una opcion... &#x25BC;</option>
                    <option value="Núcleo urbano">Núcleo urbano</option>
                    <option value="Urbanización">Urbanización</option>
                    <option value="Despoblado">Despoblado</option>
            </select>
            </div>
        </div>      

      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="propietario" class="form-group-label">¿Eres el propietario o el inquilino de la vivienda?</label>
        
          <select name="propietario" id="propietario" class="form-control form-control-gray selectpicker">
            <option value="">selecciona una opcion... &#x25BC;</option>
                    <option value="Propietario">Propietario</option>
                    <option value="Inquilino">Inquilino</option>
            </select>
            </div>
        </div> 


      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="uso_vivienda" class="form-group-label">Uso de la vivienda</label>
        
          <select name="uso_vivienda" id="uso_vivienda" class="form-control form-control-gray selectpicker">
            <option value="">selecciona una opcion... &#x25BC;</option>
                    <option value="Vivienda habitual">Vivienda habitual</option>
                    <option value="Vivienda secundaria">Vivienda secundaria</option>

            </select>
            </div>
        </div>    
                                      

			<div class="cell-sm-6 offset-top-20">
                <div class="form-group">
                  <label for="metros_cuadrados_vivienda" class="form-group-label">Metros cuadrados de la vivienda</label>
                  <!--Select 2-->
                  <input id="metros_cuadrados_vivienda" type="text" name="metros_cuadrados_vivienda" value="<?php echo set_value('metros_cuadrados_vivienda'); ?>" class="form-control form-control-gray">
                </div>
			</div>

      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="provincia" class="form-group-label">Provincia</label>
        
          <select name="provincia" id="provincia" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
                    <option value="Álava">Álava</option>
                    <option value="Albacete">Albacete</option>
                    <option value="Alicante">Alicante</option>
                    <option value="Almería">Almería</option>
                    <option value="Asturias">Asturias</option>
                    <option value="Ávila">Ávila</option>
                    <option value="Badajoz">Badajoz</option>
                    <option value="Baleares (Islas)">Baleares (Islas)</option>
                    <option value="Barcelona">Barcelona</option>
                    <option value="Burgos">Burgos</option>
                    <option value="Cáceres">Cáceres</option>
                    <option value="Cádiz">Cádiz</option>
                    <option value="Cantabria">Cantabria</option>
                    <option value="Castellón">Castellón</option>
                    <option value="Ceuta">Ceuta</option>
                    <option value="Ciudad real">Ciudad real</option>
                    <option value="Córdoba">Córdoba</option>
                    <option value="Coruña (A)">Coruña (A)</option>
                    <option value="Cuenca">Cuenca</option>
                    <option value="Girona">Girona</option>
                    <option value="Granada">Granada</option>
                    <option value="Guadalajara">Guadalajara</option>
                    <option value="Guipúzcoa">Guipúzcoa</option>
                    <option value="Huelva">Huelva</option>
                    <option value="Huesca">Huesca</option>
                    <option value="Jaén">Jaén</option>
                    <option value="León">León</option>
                    <option value="Lleida">Lleida</option>
                    <option value="Lugo">Lugo</option>
                    <option value="Madrid">Madrid</option>
                    <option value="Málaga">Málaga</option>
                    <option value="Melilla">Melilla</option>
                    <option value="Murcia">Murcia</option>
                    <option value="Navarra">Navarra</option>
                    <option value="Orense">Orense</option>
                    <option value="Palencia">Palencia</option>
                    <option value="Palmas (las)">Palmas (las)</option>
                    <option value="Pontevedra">Pontevedra</option>
                    <option value="Rioja (la)">Rioja (la)</option>
                    <option value="Salamanca">Salamanca</option>
                    <option value="Segovia">Segovia</option>
                    <option value="Sevilla">Sevilla</option>
                    <option value="Soria">Soria</option>
                    <option value="Sta. Cruz Tenerife">Sta. Cruz Tenerife</option>
                    <option value="Tarragona">Tarragona</option>
                    <option value="Teruel">Teruel</option>
                    <option value="Toledo">Toledo</option>
                    <option value="Valencia">Valencia</option>
                    <option value="Valladolid">Valladolid</option>
                    <option value="Vizcaya">Vizcaya</option>
                    <option value="Zamora">Zamora</option>
                    <option value="Zaragoza">Zaragoza</option>
          </select>
        </div>
      </div>


      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="anyo_construccion" class="form-group-label">Año de construcción de la vivienda</label>
        
          <select name="anyo_construccion" id="anyo_construccion" class="form-control form-control-gray">
            <option value="">selecciona una opcion... &#x25BC;</option>
<script type="text/javascript">
                      
                      var now = new Date(); 
                      var year = now.getFullYear();
                      var startYear = year;
                      var stopYear = 1900;
                      
        for (i=startYear;i>stopYear;i--)
        {
          lista = "<option value=\"" + i + "\">" + i + "</option>";
          document.write(lista);
        } 
</script>
        
          </select>
        </div>
      </div>


      <div class="cell-sm-6 offset-top-20">
        <div class="form-group">
          <label for="vivienda_rehabilitada" class="form-group-label">¿La vivienda ha sido rehabilitada?</label>
        
          <select name="vivienda_rehabilitada" id="vivienda_rehabilitada" class="form-control form-control-gray selectpicker">
            <option value="">selecciona una opcion...</option>
                    <option value="Si">Si</option>
                    <option value="No">No</option>
                  
            </select>
            </div>
        </div> 

            <div class="cell-sm-6 offset-top-20">
                <div class="form-group">
                  <label for="valor_continente" class="form-group-label">¿Que capital de CONTINENTE o Edificación deseas asegurar?</label>
                  <!--Select 2-->
                  <input id="valor_continente" type="text" name="valor_continente" value="<?php echo set_value('valor_continente'); ?>" class="form-control form-control-gray">
                </div>
            </div>

            <div class="cell-sm-6 offset-top-20">
                <div class="form-group">
                  <label for="valor_contenido" class="form-group-label">¿Que cantidad de CONTENIDO deseas asegurar?</label>
                  <!--Select 2-->
                  <input id="valor_contenido" type="text" name="valor_contenido" value="<?php echo set_value('valor_contenido'); ?>" class="form-control form-control-gray">
                </div>
            </div>            
															
		</div>
	</fieldset>
                    <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>

						<?php echo form_close(); ?>

					</div>

					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/hogar.jpg', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de ' . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

            <p class="offset-top-30">Contrata con nosotros tu seguro de hogar y ten bien cubierto tu patrimonio. Seguros de hogar para tu tranquilidad y de los tuyos. Desde hoy contratar nuestro seguro de hogar aumentará tu calidad de vida.</p>
					
				</div>
			</div>
    	</div>
	</section>
</main>
