


	<input id="tipo_seguro" name="tipo_seguro" type="hidden" value="Seguro de <?php echo $nombre_seguro; ?>" />
	<input id="correduria" name="correduria" type="hidden" value="<?php echo CODIGO_CORREDURIA; ?>" />

	<fieldset class=" cell-sm-12 form-group offset-top-60">
		<legend class="col-form-label cell-sm-6">Información del seguro</legend>

		<div class="range">


			<div class="cell-sm-6">
                <div class="form-group">
                  <label class="form-group-label">Capital para asegurar</label>
                  <!--Select 2-->
                  <select data-minimum-results-for-search="Infinity" class="form-control select-filter" id="capital" name="capital">
                    <option value="">Selecciona una opción &#x25BC;</option>
                    <option value="50000">50.000 €</option>
                    <option value="100000">100.000 €</option>
                    <option value="150000">150.000 €</option>
                    <option value="200000">200.000 €</option>
                    <option value="250000">250.000 €</option>
                    <option value="300000">300.000 €</option>
                    <option value="350000">350.000 €</option>
                    <option value="400000">400.000 €</option>
                    <option value="450000">450.000 €</option>
                    <option value="500000">500.000 €</option>
                   
                  </select>
                </div>
			</div>

			<div class="cell-sm-6">
                <div class="form-group">
                  <label class="form-group-label">Edad del asegurado</label>
                  <!--Select 2-->
                  <input type="number" min="0" max="99" value="50" class="form-control" name="edad" id="edad">
                </div>
			</div>
																			
		</div>
	</fieldset>
                  <div class="cell-md-12 offset-top-20">
                    <div class="form-group">
                      <label for="observaciones" class="form-group-label">Observaciones (opcional)</label>
                      <textarea id="observaciones" name="observaciones" class="form-control form-control-gray"></textarea>
                    </div>
                </div>  
                <div class="cell-md-12 offset-top-20">
                	<div class="form-group">
						<label class="form-check-label">
							<input type="checkbox"  class="form-check-input" name="politica_privacidad" id="politica_privacidad">
							Acepto la <?php echo anchor('politica_privacidad','política de protección de datos',array('class' => 'text_politica','target' => '_blank')); ?>
						</label>
                	</div>
                    <div class="offset-top-20 text-center text-md-left">
                      <button style="min-width: 140px;" type="submit" class="btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-envelope-o"></span><span>Enviar</span></button>
                    </div>
                  </div>


						<?php echo form_close(); ?>

          <div class="offset-top-100">
            <h5 class="text-info-dr">DESCARGA DE DOCUMENTOS</h5>
            <hr class="divider divider-lg-left divider-primary divider-80">
            <div class="range">
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/dependencia/baremo_dependencia.pdf', img('public/images/pdf_file.png') . ' Baremo dependencia R.D. 504/2007', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/dependencia/ficha_producto_dependencia.pdf', img('public/images/pdf_file.png') .' Ficha Producto AXA', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/dependencia/ley_dependencia_2006.pdf', img('public/images/pdf_file.png') .' Ley dependencia 2006', 'target="_blank"'); ?></div>

              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/dependencia/poliza_seguro_dependencia.pdf', img('public/images/pdf_file.png') .' Póliza Seguro Dependencia AXA', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/dependencia/101_preguntas_ley_dependencia.pdf', img('public/images/pdf_file.png') .' 101 preguntas ley dependencia', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/dependencia/preguntas_y_respuestas_vida_dependencia.pdf', img('public/images/pdf_file.png') .' Preguntas y Respuestas Vida Dependencia AXA', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/dependencia/libro_blanco.pdf', img('public/images/pdf_file.png') .' Libro blanco de la dependencia', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/dependencia/solicitud_seguro_vida_dependencia.pdf', img('public/images/pdf_file.png') .' Solicitud Seguro Vida Dependencia AXA', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/dependencia/GRADOS Y NIVELES DE DEPENDENCIA.pdf', img('public/images/pdf_file.png') .' Grados y niveles de dependencia', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/dependencia/ley dependencia por autonomia.pdf', img('public/images/pdf_file.png') .' Ley dependencia por autonomia', 'target="_blank"'); ?></div>
              <div class="col-md-4"><?php echo anchor(base_url().'public/docs/dependencia/guia-ventas-seguro-dependencia-2015.pdf', img('public/images/pdf_file.png') .' Guía de ventas', 'target="_blank"'); ?></div>

            </div>
          </div>
					</div>

					<div class="col-md-2">
						<?php echo img(array('src'=>'public/images/productos/dependencia2.png', 'width' => '100%')); ?>

						<h5 class="offset-top-30 strong text-info-dr"><?php echo strtoupper('Seguro de ' . $nombre_seguro); ?></h5>
						<hr class="divider divider-lg-left divider-primary divider-80">

						<p class="offset-top-30">Porque tu futuro comienza dentro de un momento, contrata tu seguro de Dependencia y asegura tu bienestar y el de las personas que conviven contigo o de tus familiares.</p>

            <p class="text-red offset-top-30">Utiliza este formulario para obtener rápidamente nuestra mejor oferta de seguro. Responde todos los campos, para poder conseguirte el mejor presupuesto.</p>
					</div>
			</div>
    	</div>
	</section>
</main>
