<div class="container texto_correo_enviado">
	<?php //echo img('public/images/logos_seguros.jpg'); ?>

	<p>Estimado usuario/a:</p>
	<p>Debido a la complejidad de este seguro, en breve uno de nuestros especialistas en la materia  le llamará de manera personal, para informarle y ofrecerle la mejor oferta. Si desea puede contactar con nosotros a través de nuestro número de teléfono <?php echo TELEFONO_CONTACTO_OFICINA_VALENCIA; ?>.</p>

	<div class="row" style="margin-top: 75px;margin-bottom:50px;">
		<div class="col-md-2"></div>
		<div class="col-md-6">
		<table class="table" style="text-align: center;">
		<thead>
		<tr>
			<th colspan="2" style="text-align: center;">HORARIO OFICINAS DE LUNES A VIERNES</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>MAÑANAS</td>
				<td>9:00 - 15:00</td>
			</tr>
<tr>
				<td>TARDES</td>
				<td>16:30 - 19:30</td>
			</tr>			
		</tbody>
		</table>
		</div>
		<div class="col-md-2"></div>
		</div>


		<?php
			$dia_actual = date("N");
			$hora_actual = date("H:i:s");
			$hora_apertura = "8:00:00";
			$hora_cierre = "19:25:00";
			if (($dia_actual < 6) && ( strtotime($hora_apertura) < strtotime($hora_actual)) && (strtotime($hora_actual) < strtotime($hora_cierre)) )
				$cadena_adicional = "En el caso de llamar o enviarnos un  mail fuera del horario de oficina, le llamaremos el siguiente día laborable.";
			else
				$cadena_adicional = "Lamentamos decirle que no podemos atenderle en este momento debido a que nuestras oficina están en horario de cierre. En la próxima jornada laboral hábil uno de nuestros especialistas se pondrá en contacto con usted para asesorarle y recomendarle la mejor oferta en seguros.";

			echo '<p>' . $cadena_adicional . '</p>';
		?>

	
	<!--<p>En el caso de llamar o enviarnos un  mail fuera del horario de oficina, le llamaremos el siguiente día laborable. </p>-->
	

	<div class="owl-carousel">
		<?php echo img(array('src'=>'public/images/logos_cias/27_zurich.gif','class' => 'item carousel_cias')); ?>
		<?php echo img(array('src'=>'public/images/logos_cias/3_allianz.png','class' => 'item carousel_cias')); ?>
		<?php echo img(array('src'=>'public/images/logos_cias/81_axa.png','class' => 'item carousel_cias')); ?>
		<?php echo img(array('src'=>'public/images/logos_cias/generali.png','class' => 'item carousel_cias')); ?>
		<?php echo img(array('src'=>'public/images/logos_cias/mapfre.png','class' => 'item carousel_cias')); ?>
		<?php echo img(array('src'=>'public/images/logos_cias/rsa.png','class' => 'item carousel_cias')); ?>
		<?php echo img(array('src'=>'public/images/logos_cias/plus_ultra.png','class' => 'item carousel_cias')); ?>
		<?php echo img(array('src'=>'public/images/logos_cias/helvetia.png','class' => 'item carousel_cias')); ?>
		<?php echo img(array('src'=>'public/images/logos_cias/34_catalana-occidente.gif','class' => 'item carousel_cias')); ?>
		<?php echo img(array('src'=>'public/images/logos_cias/fiatc.png','class' => 'item carousel_cias')); ?>
		<?php echo img(array('src'=>'public/images/logos_cias/mutua_madrilena.png','class' => 'item carousel_cias')); ?>
		<?php echo img(array('src'=>'public/images/logos_cias/arag.png','class' => 'item carousel_cias')); ?>
		<?php echo img(array('src'=>'public/images/logos_cias/05_aviva.jpg','class' => 'item carousel_cias')); ?>
		<?php echo img(array('src'=>'public/images/logos_cias/23_reale.gif','class' => 'item carousel_cias')); ?>
		<?php echo img(array('src'=>'public/images/logos_cias/61_asefa.png','class' => 'item carousel_cias')); ?>
		<?php echo img(array('src'=>'public/images/logos_cias/21_ocaso.gif','class' => 'item carousel_cias')); ?>
	</div>
</div>
