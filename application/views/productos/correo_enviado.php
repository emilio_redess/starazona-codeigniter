<!-- Page Content-->
<main class="page-content">

	<section class="section-80 section-md-120">
	  	<div class="shell shell-wide text-lg-left">
		    <div class="range">
		        <div class="inset-lg-left-80">
		          	<h5 class="text-info-dr">Hemos recibido los datos del formulario</h5>
		          	<hr class="divider divider-lg-left divider-primary divider-80">
					          
						<p>Estimado usuario/a:<br>Gracias por su interés en nuestros servicios a través de nuestra página web.</p>
						<p>Hemos recibido los datos introducidos en el formulario.</p> 
						<p>Pronto uno de nuestros agentes se pondrá en contacto con usted</p>


				</div>
			</div>
    	</div>
	</section>

</main>


