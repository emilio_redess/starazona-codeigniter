
      
<!-- Page Content-->
<main class="page-content">
	<section class="bg-image-06">
    	<div class="breadcrumb-wrapper">
      		<div class="shell context-dark section-30 section-lg-top-100">
		       
		        <h1 class="offset-top-20 text-ubold">Por qué elegirnos</h1>
		       
      		</div>
    	</div>
  	</section>

	<section class="section-80 section-md-120">
	  	<div class="shell shell-wide text-lg-left">
		    <div class="range">
		        <div class="inset-lg-left-80">

		          	<div class="col-sm-12">
						<?php echo img(array('src'=>'public/images/porque_elegirnos.png','alt'=> '','class' => 'img-responsive center-block')); ?>
					</div>


				<div class="col-sm-12 section-md-100">
					<p>Nuestra labor profesional se sustenta en una clara y marcada  vocación de servicio y atención al cliente,  desde la más rigurosa independencia respecto a las entidades aseguradoras en las que colocamos los riesgos. Siempre tenemos en cuenta quien es nuestro cliente como  a quien hay que defender.</p>

					<p>La actividad de la Mediación en Seguros está sujeta al control administrativo por parte de la Dirección General de Seguros y Fondos de Pensión, Ministerio de Economía (Ley de Mediación 26/2006 de 17 de Julio y anteriores ). Ejercemos la actividad de mediadores de seguros, habilitados por la titulación específica que nos otorga el Ministerio de Economía. </p>


					<p>Analizamos de forma objetiva las muchas posibilidades que ofrece el mercado asegurador,  para ofrecer a nuestros clientes coberturas de seguro en las que la relación precio-calidad sea óptima y adecuada a sus necesidades y características. A su vez estamos todas las personas que pertenecemos a esta empresa ( dirección y empleados ) realizando constantes cursos que nos permite el poder reciclarnos y estar al día en todo lo relativo a nuestra actividad, como pueden ser conocer nuevos productos, estar al día de la nueva legislación aplicándola de forma inmediata y de forma rigurosa a los contratos de los clientes y adquisición de nuevos conocimientos como intentar estar a la última en el mundo de la Tecnología ligada al sector del seguro. </p>

					<p>Elegimos para trabajar todas aquellas compañías que pueden resultar adecuadas a los intereses de nuestros clientes, teniendo muy en cuenta su solvencia, funcionamiento y del servicio que presta. Carecemos de vínculos de participación con compañías aseguradoras o de cualquier entidad financiera.</p>

					<p>Representamos y defendemos  a nuestros clientes ante las entidades aseguradoras y personalmente nos ocupamos diariamente  del seguimiento de las pólizas suscritas,  al objeto de que la eficacia y coberturas  de las mismas no se vean mermadas o carentes  por el paso del tiempo.</p>

					<p>Asesoramos, asistimos y tramitamos los posibles siniestros vigilando que las resoluciones de las entidades aseguradoras se ciñan en todo caso a los términos pactados en las pólizas y con los derechos del asegurado o perjudicado.</p>

					<p>Nuestra Correduría de Seguros para tranquilidad de nuestros clientes,  dispone de una póliza de  Responsabilidad Civil Profesional con una garantía de 3.000.000 € que respalda nuestra responsabilidad frente a los posibles daños o negligencias que podamos ocasionar, de forma involuntaria, a nuestros clientes o terceros, para  poder resarcir los posibles perjuicios causados. Esta póliza está contratada con la Compañía de Seguros Zurich y reasegurada en el Pool Español de Corredores de Seguros. En la actualidad superamos el capital asegurado, que nos obliga la legislación en 5 veces. Permítanos demostrarle cuánto podemos hacer por usted y la tranquilidad que tendrá estando asesorado y asegurado a través nuestro.</p>
					<p></p>

					<p>Para cualquier duda puede escribirme a mi mail personal y encantado le contestaré inmediatamente.:</p>
				
					<p><a href="mailto:sftb@starazona.com">sftb@starazona.com</a></p>
					<p>Salvador Francisco Tarazona Baixauli</p>
					<p>Administrador Único</p>
				</div>
				</div>
			</div>
    	</div>
	</section>
</main>
