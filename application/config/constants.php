<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


/*
* Constants personalizadas
*/
define("CODIGO_CORREDURIA", "STZ");

define("CODIGO_SALUD", "SAL");
define("CODIGO_VIDA", "VID");
define("CODIGO_DECESOS", "DEC");
define("CODIGO_HOGAR", "HOG");


define("URL_CURL_SERVER", "http://www.tarificadoronline.com/index.php/ws-api/");
define("URL_LOCAL_CURL", "http://localhost/webs/tarificador-server/index.php/ws-api/");

define("URL_CURL", "http://www.tarificadoronline.com/index.php/ws-api/");

define("TELEFONO_CONTACTO", "96 396 35 00");
define("TELEFONO_CONTACTO_OFICINA_VALENCIA", "96 373 45 50");
define("TELEFONO_CONTACTO_OFICINA_SEDAVI", "96 396 35 00");
define("TELEFONO_CONTACTO_OFICINA_MASSANASSA", "96 125 22 92");
define("TELEFONO_CONTACTO_OFICINA_SILLA", "96 311 60 01");
define("EMAIL_CONTACTO", "info@starazona.com");
define("EMAIL_CONTACTO2", "seguros@starazona.com");

define("DIRECCION_CONTACTO", "Avda. Blasco Ibáñez 127, 46470 Massanassa");
define("DIRECCION_CONTACTO_SEDAVI", "Avda. L'Albufera 33, 46910 Sedavi");
define("DIRECCION_CONTACTO_VALENCIA", "Esc. Ant. Sacramento 11, sc. izq. nº1, 46013 Valencia");
define("DIRECCION_CONTACTO_SILLA", "C/ Valencia 105, 46600 Silla");

define("DIRECCION_CONTACTO_PARTE1", "Avda. Blasco Ibáñez 127,");
define("DIRECCION_CONTACTO_PARTE2", "46470 Massanassa");


define("DIRECCION_FACEBOOK","https://www.facebook.com/salvadortarazonacorreduriadeseguros/");
define("DIRECCION_LINKEDIN","http://www.linkedin.com/pub/stb-seguros/40/286/a87");




define("ASISTENCIA_ZURICH","93 416 50 40");
define("ASISTENCIA_GENERALI","91 112 34 43");
define("ASISTENCIA_PELAYO","91 749 68 00");
define("ASISTENCIA_ALLIANZ","900 117 117");
define("ASISTENCIA_MAPFRE","915 816 300");
define("ASISTENCIA_AXA","900 300 188");
define("ASISTENCIA_REALE","900 101 399");
define("ASISTENCIA_CASER","915 95 54 55");
define("ASISTENCIA_PLUS_ULTRA","917 83 83 83");
define("ASISTENCIA_CATALANA_OCCIDENTE","932 220 212");
define("ASISTENCIA_SEGUROS_BILBAO","946 421 241");
define("ASISTENCIA_HELVETIA","900 252 252");
define("ASISTENCIA_FIATC","900 30 20 20");
define("ASISTENCIA_LIBERTY","900 101 369");
define("ASISTENCIA_PREVENTIVA","91 516 05 00");
define("ASISTENCIA_SANTALUCIA","900 24 20 20");
define("ASISTENCIA_ACTIVE","96 351 98 85");
define("ASISTENCIA_POLICIA","112");
define("ASISTENCIA_GUARDIA_CIVIL","062");

define("GOOGLE_MAPS_API_KEY","AIzaSyARH0YIgAON0-UW75Uj0qknMdbuNcmwwZQ");




//////////////////////////////////// constantes para el envio de emails   //////////////////////////
define("EMAIL_RECIPIENTE", "info@starazona.com");
//define("EMAIL_RECIPIENTE", "emilio@redess.es");
define("EMAIL_BCC", "");
define("EMAIL_CC", "");

define("EMAIL_FROM", "no-reply@starazona.com");
define("EMAIL_FROMNAME", "Starazona");

define("EMAIL_RUTA_IMAGENES", "http://www.starazona.com/public/images/");
define("EMAIL_PROTOCOL", "smtp");
define("EMAIL_SMTP_PORT", 25);

define("EMAIL_SMTP_HOST", "smtp.starazona.com");
define("EMAIL_SMTP_USER", "no-reply@starazona.com");
define("EMAIL_SMTP_PASSWORD", "asdf+1234");

define("EMAIL_SMTP_TIMEOUT", "4");
define("EMAIL_SMTP_MAILTYPE", "html");
define("EMAIL_SMTP_CHARSET", "utf-8");
