<?php

class Facebook_articulos_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function agregar_articulo($url) {

		$sql = "INSERT INTO articulos (articulo_url) VALUES (?)";
		$query = $this->db->query($sql,array($url));

		$id = $this->db->insert_id(); // Will return the last insert id.
		return array("id" => $id, "success" => $query, "queryString" => $sql);
	}	

	public function get_articulo($id) {

		$sql = "SELECT * FROM articulos WHERE id = ?";
		$query = $this->db->query($sql,array($id));
		return $query->row();
	}	

	public function cargar_recientes($val) {

		$sql = "SELECT * FROM articulos order by id desc LIMIT ?";
		$query = $this->db->query($sql,array($val));
		return $query;
	}

	public function insert_articulo_image($articulo_id,$file_name) {

		$sql = "UPDATE articulos SET imagen = ? WHERE id = ?";
		$query = $this->db->query($sql,array($file_name,$articulo_id));
		return $query;
	}
}