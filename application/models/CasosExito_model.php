<?php

class CasosExito_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}



	public function get_articulos() {

		$sql = "SELECT * FROM casosexito_articulos WHERE activo = 1";
		$query = $this->db->query($sql);
		return $query;		
	}



	public function get_articulo($id) {

		$sql = "SELECT * FROM casosexito_articulos WHERE id = ?";
		$query = $this->db->query($sql,array($id));
		return $query->row();
	}	

	
}