<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Helper extends CI_Controller {

	public function index()
	{
		
	}

	public function ajax_llamame()
	{
		$nombre = $this->input->post('nombre');
		$telefono = $this->input->post('telefono');
		$horario = $this->input->post('horario');

		if ($telefono != ""){

			// Datos ok, enviar correo
			$subject = "Un usuario quiere que le llames";
			$recipient = EMAIL_RECIPIENTE;
			$from = EMAIL_FROM;
			$fromName = EMAIL_FROMNAME;
			$view = "emails/llamame.php";

			$datosCorreo = array("nombre" => $nombre, "telefono" => $telefono, "horario" => $horario);

			$respuesta = $this->send_emails->smtp_mail($subject,$recipient,"","",$from,$fromName,$view,$datosCorreo);
			///////////////////////////////////////////////////////////////////////////////////////////
			     
			     if (!$respuesta) {
			     	$msg = "Ha habido un error al intentar enviar el email";
			     	$status = false;
				    //show_error($this->email->print_debugger()); 
				}else {
				    $msg = 'Se ha enviado el e-mail correctamente!';
				    $status = true;
				}

			///////////////////////////////////////////////////////////////////////////////////////////

		}else{
			$status = false;
			$msg = "No se han podido enviar sus datos:\n- Debe indicar un teléfono de contacto\n";
		}


		$data = array(    
			"status" => $status,
    		"msg" => $msg
    	);

		echo json_encode($data);
	}






}
