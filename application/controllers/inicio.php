<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

    public function __construct() {
		parent::__construct();
 
		// Cambiamos el delimitador de los mensajes del formulario para que usen los estilos de bootstrap
		//$this->form_validation->set_error_delimiters('<div class="alerta-formulario" role="alert">', '</div>');
		
    }


	public function index()
	{
		$data['page_title'] = 'correduria de seguros Valencia, Barcelona, Sevilla, Madrid, Bilbao, comparador de seguros vida, decesos y medicos';
	    $data['page_keywords'] = 'correduria de seguros, corredor de seguros Valencia, Barcelona, Sevilla, Madrid, Bilbao, comparador de seguros, seguro de vida, seguro de decesos, seguros de coche, seguro de hogar';
	    $data['page_description'] = 'Somos una correduria de seguros de Valencia especializada más de 30 años en todo tipo de seguro. El mejor precio en seguros de vida, decesos, coche, hogar, salud';

    	$opciones_menu = ['inicio' => '1','sobre nosotros' => '0','contacto' => '0','productos' => '0','casos exito' => '0'];

    	$data['opciones_menu'] = $opciones_menu;

		$data['jsArray'] = array('public/js/carousel_form.js');	

		$this->load->view('templates/header',$data);
		$this->load->view('templates/menu');
		$this->load->view('templates/carousel');
		$this->load->view('home');
		$this->load->view('templates/footer');
	}
}
