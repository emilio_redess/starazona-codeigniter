<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CasosExito extends CI_Controller {

    public function __construct() {
		parent::__construct();

		$this->load->model('casosExito_model');
		// Cambiamos el delimitador de los mensajes del formulario para que usen los estilos de bootstrap
		$this->form_validation->set_error_delimiters('<div class="alerta-formulario" role="alert">', '</div>');
    }

	public function index()
	{

		$data['page_title'] = 'correduria de seguros Valencia, Barcelona, Sevilla, Madrid, Bilbao, comparador de seguros vida, decesos y medicos';
	    $data['page_keywords'] = 'correduria de seguros, corredor de seguros Valencia, Barcelona, Sevilla, Madrid, Bilbao, comparador de seguros, seguro de vida, seguro de decesos, seguros de coche, seguro de hogar';
	    $data['page_description'] = 'Somos una correduria de seguros de Valencia especializada más de 30 años en todo tipo de seguro. El mejor precio en seguros de vida, decesos, coche, hogar, salud';

    	$opciones_menu = ['inicio' => '0','sobre nosotros' => '0','contacto' => '1','productos' => '0','casos exito' => '1'];

    	$data['opciones_menu'] = $opciones_menu;

		$articulos = $this->casosExito_model->get_articulos();




	}
	
	public function articulo($id)
	{
		
		$articulo = $this->casosExito_model->get_articulo($id);


/*
		$data["articulo_url"] = $articulo->articulo_url;
		$this->load->view('facebook/articulo_redirect',$data);
		*/
	}
}
