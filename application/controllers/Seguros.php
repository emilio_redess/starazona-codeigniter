<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seguros extends CI_Controller {

    public function __construct() {
		parent::__construct();

		// Cambiamos el delimitador de los mensajes del formulario para que usen los estilos de bootstrap
		$this->form_validation->set_error_delimiters('<div class="alerta-formulario" role="alert">', '</div>');

    }

	public function index()
	{
		$data['page_title'] = 'correduria de seguros, corredor de seguros, comparador de seguros, seguro de vida';
	    $data['page_keywords'] = 'correduria de seguros, corredor de seguros, comparador de seguros, seguro de vida, seguros de coche, seguro de hogar';
	    $data['page_description'] = 'correduria de seguros especializada más de 30 años en todo tipo de seguro. Consigue el mejor precio en seguro de vida, coche, hogar, salud';

	    $opciones_menu = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];
	    $data['opciones_menu'] = $opciones_menu;
	    $data['jsArray'] = array('public/js/isotope_init.js');
	    $data['filtro'] = 0;
	    $data['filtro_selected'] = ['todos' => '1','particulares' => '0','empresas' => '0','vida' => '0','espectaculos' => '0'];

		$this->load->view('templates/header',$data);
		$this->load->view('templates/menu');
		$this->load->view('productos_list');

		$this->load->view('templates/footer');
	}

/*
*  0 -> todos los productos
*  1 -> Seguros particulares
*  2 -> Seguros para empresas
*  3 -> Seguros vida e inversion
*  4 -> Seguros para espectaculos
*/

	public function filtro($categoria = 0)
	{
		$data['page_title'] = 'correduria de seguros, corredor de seguros, comparador de seguros, seguro de vida';
	    $data['page_keywords'] = 'correduria de seguros, corredor de seguros, comparador de seguros, seguro de vida, seguros de coche, seguro de hogar';
	    $data['page_description'] = 'correduria de seguros especializada más de 30 años en todo tipo de seguro. Consigue el mejor precio en seguro de vida, coche, hogar, salud';

	    $opciones_menu = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];
	    $data['opciones_menu'] = $opciones_menu;
	    $data['jsArray'] = array('public/js/isotope_init.js');
	    $data['filtro'] = $categoria;


	    switch ($categoria) {
	    	case '1':
	    		$data['filtro_selected'] = ['todos' => '0','particulares' => '1','empresas' => '0','vida' => '0','espectaculos' => '0'];
	    		break;

	    	case '2':
	    		$data['filtro_selected'] = ['todos' => '0','particulares' => '0','empresas' => '1','vida' => '0','espectaculos' => '0'];
	    		break;

	    	case '3':
	    		$data['filtro_selected'] = ['todos' => '0','particulares' => '0','empresas' => '0','vida' => '1','espectaculos' => '0'];
	    		break;

	    	case '4':
	    		$data['filtro_selected'] = ['todos' => '0','particulares' => '0','empresas' => '0','vida' => '0','espectaculos' => '1'];
	    		break;	    			    			    		
	    	
	    	default:
	    		$data['filtro'] = 0;
	    		$data['filtro_selected'] = ['todos' => '1','particulares' => '0','empresas' => '0','vida' => '0','espectaculos' => '0'];
	    		break;
	    }


		$this->load->view('templates/header',$data);
		$this->load->view('templates/menu');
		$this->load->view('productos_list');

		$this->load->view('templates/footer');
	}



    /*
    *	Funcion para mostrar el formulario de contratacion (envia un email a la correduria)
    *
    *
    */

	public function contratar ($ramo = NULL,$codtar = NULL,$nombreCia = NULL)
	{

		$data['page_title'] = 'correduria de seguros Valencia, Barcelona, Sevilla, Madrid, Bilbao, comparador de seguros vida, decesos y medicos';
	    $data['page_keywords'] = 'correduria de seguros, corredor de seguros Valencia, Barcelona, Sevilla, Madrid, Bilbao, comparador de seguros, seguro de vida, seguro de decesos, seguros de coche, seguro de hogar';
	    $data['page_description'] = 'Somos una correduria de seguros de Valencia especializada más de 30 años en todo tipo de seguro. El mejor precio en seguros de vida, decesos, coche, hogar, salud';

		$opciones_menu = ['inicio' => '1','sobre nosotros' => '0','contacto' => '0','productos' => '0','casos exito' => '0'];

    	$data['opciones_menu'] = $opciones_menu;	    

		// Form validation
		$this->form_validation->set_rules('nombre', 'nombre', 'required');
		$this->form_validation->set_rules('telefono', 'telefono', 'required');
		//$this->form_validation->set_rules('horario', 'horario de llamada', 'required');
	

		if ($this->form_validation->run() == FALSE)
		{

			$data['ramo'] = $ramo;
			$data['codtar'] = $codtar;
			$data['nombreCia'] = $nombreCia;


			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('contratar');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$nombre = $this->input->post('nombre');
			$horario = $this->input->post('horario');
			$telefono = $this->input->post('telefono');

			///////////////////////// enviamos el correo  /////////////////////////////////////////////////
			$subject = "Un usuario está interesado en un seguro de " . $ramo;
			$recipient = EMAIL_CONTACTO;
			$cc = EMAIL_CC;
			$bcc = EMAIL_BCC;
			$from = EMAIL_FROM;
			$fromName = EMAIL_FROMNAME;
			$view = "emails/contratar.php";

			$datosCorreo = array("codtar" => $codtar, "nombre" => $nombre, "telefono" => $telefono, "horario" => $horario, "cia" => $nombreCia, "ramo" => $ramo);



			$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('contratar_ok');
			$this->load->view('templates/footer');
		}
	}    



	/******************************** coche y moto ponen un iframe *******************************************************************/
	public function coche()
	{
		$data['page_title'] = 'Seguros de Coche, Seguros de Moto, Comparador de Seguros, Seguro coche barato, Seguro coche Renault';
	    $data['page_keywords'] = 'seguros de coches, seguro de coche, seguros de coche, seguro coche, seguros barato de coche, seguro de coche barato, seguro para coche, presupuesto seguro coche, comparativa seguro coche, comparador seguros de coches';
	    $data['page_description'] = 'Contr&aacute;tanos tu seguro de coche y deja de pagar m&aacute;s por menos';

	    $opciones_menu = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];
	    $data['opciones_menu'] = $opciones_menu;

		$this->load->view('templates/header',$data);
		$this->load->view('templates/menu');
		$this->load->view('productos/landing_pages/landing_coche');

		$this->load->view('templates/footer');
	}


	// para la version movil, en vez de cargar un iframe, ponemos un formulario
	public function coche_mb()
	{
		$data['page_title'] = 'Seguros de Coche, Seguros de Moto, Comparador de Seguros, Seguro coche barato, Seguro coche Renault';
	    $data['page_keywords'] = 'seguros de coches, seguro de coche, seguros de coche, seguro coche, seguros barato de coche, seguro de coche barato, seguro para coche, presupuesto seguro coche, comparativa seguro coche, comparador seguros de coches';
	    $data['page_description'] = 'Contr&aacute;tanos tu seguro de coche y deja de pagar m&aacute;s por menos';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['callback'] = "coche_mb";
	    $data['nombre_seguro'] = "coche";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');	  
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');	  

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_coche_mb');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}	
	}

	public function moto()
	{
		$data['page_title'] = 'seguro moto, seguros de moto, seguro de moto barato, comparador motos';
	    $data['page_keywords'] = 'seguro moto, seguro de moto, seguro de moto barato, comparador seguros de moto, seguro de motocicleta, seguro ciclomotor, comparar seguro de moto.';
	    $data['page_description'] = 'contrata tu seguro de moto online y consigue con nuestro comparador de seguros de motos  más de 15 ofertas baratas  de seguros de motos y ciclomotores.';

	    $opciones_menu = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];
	    $data['opciones_menu'] = $opciones_menu;

		$this->load->view('templates/header',$data);
		$this->load->view('templates/menu');
		$this->load->view('productos/landing_pages/landing_moto');

		$this->load->view('templates/footer');
		//$this->load->view('templates/modals');
		//$this->load->view('templates/scripts');
	}



	// para la version movil, en vez de cargar un iframe, ponemos un formulario
	public function moto_mb()
	{
		$data['page_title'] = 'seguro moto, seguros de moto, seguro de moto barato, comparador motos';
	    $data['page_keywords'] = 'seguro moto, seguro de moto, seguro de moto barato, comparador seguros de moto, seguro de motocicleta, seguro ciclomotor, comparar seguro de moto.';
	    $data['page_description'] = 'contrata tu seguro de moto online y consigue con nuestro comparador de seguros de motos  más de 15 ofertas baratas  de seguros de motos y ciclomotores.';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['callback'] = "moto_mb";
	    $data['nombre_seguro'] = "moto";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');


 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_moto_mb');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}	
	}


	/**********************************************************************************************************************************/	
	public function feriantes()
	{
	    $data['page_title'] = 'seguro para feriantes, seguros ferias, seguro industriales feriantes, seguro feriantes, seguro de ferias, seguro de feria';

	    $data['page_keywords'] = 'seguro feriantes, seguro industrial feriante, seguro feria, seguros para feriantes, seguro castillos hinchables, seguros toros mecánicos, seguros norias, seguro montaña rusa, seguros empresarios feriantes, seguros para empresarios feriantes, seguro para feria';

	    $data['page_description'] = 'Seguros para feriantes, somos expertos en seguros de atracciones de ferias';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];	  
	    
	    $data['callback'] = "feriantes";
	    $data['nombre_seguro'] = "feriantes";	      

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');	
		$this->form_validation->set_rules('producto', 'tipos de seguros', 'required');	
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_feriantes');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}	

	public function feriantes_old()   // no activada. De momento este seguro es un enlace a la web antigua //
	{
	    $data['page_title'] = 'seguro para feriantes, seguros ferias, seguro industriales feriantes, seguro feriantes, seguro de ferias, seguro de feria';

	    $data['page_keywords'] = 'seguro feriantes, seguro industrial feriante, seguro feria, seguros para feriantes, seguro castillos hinchables, seguros toros mecánicos, seguros norias, seguro montaña rusa, seguros empresarios feriantes, seguros para empresarios feriantes, seguro para feria';

	    $data['page_description'] = 'Seguros para feriantes, somos expertos en seguros de atracciones de ferias';

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre y apellidos', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('telefono', 'telefono', 'required');
		$this->form_validation->set_rules('producto', 'tipos de seguros', 'required');
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');


		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('templates/head',$data);
			$this->load->view('templates/header');

			$this->load->view('seguros/landing_pages/landing_feriantes');

			$this->load->view('templates/footer');
			$this->load->view('templates/modals');
			$this->load->view('templates/scripts');
		}
		else // Formulario validado correctamente.
		{
			$cod_correduria = CODIGO_CORREDURIA;
			$nombreApellidos = $this->input->post('nombreApellidos');
			$email = $this->input->post('email');
			$telefono = $this->input->post('telefono');
			$producto = $this->input->post('producto');
			$tipo_seguro = $this->input->post('tipo_seguro');

			/****************************** Llamada CURL para crear tarificacion en el webservice  *******************************/
			$params = array(
			   "correduria" => CODIGO_CORREDURIA,
			   "nombreApellidos" => $nombreApellidos,
			   "email" => $email,
			   "telefono" => $telefono,
			   "tipo_seguro" => $tipo_seguro,
			   "Producto_interesado" => $producto
			);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);		

			if ($output_arr['success'] === TRUE){

				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_contacto.php";

				$datosCorreo = array("nombreApellidos" => $nombreApellidos, "email" => $email, "telefono" => $telefono, "tipo_seguro" => $tipo_seguro, "producto_interesado" => $producto);


				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);

				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['telefono_contacto'] = TELEFONO_CONTACTO_OFICINA_VALENCIA;

				$this->load->view('templates/head',$data);
				$this->load->view('templates/header');

				$this->load->view('seguros/resultados/correo_enviado',$data);

				$this->load->view('templates/footer');
				$this->load->view('templates/modals');
				$this->load->view('templates/scripts');
			}else{

				$this->load->view('templates/head',$data);
				$this->load->view('templates/header');

				$this->load->view('error');

				$this->load->view('templates/footer');
				$this->load->view('templates/modals');
				$this->load->view('templates/scripts');				
			}				
		}
	}

	public function fallas_old()   // no activada. De momento este seguro es un enlace a la web antigua //
	{
	    $data['page_title'] = 'Comparador seguros de fallas, Seguros fallas, Seguro para fallas, Seguro correfocs, Seguros para fallas de Valencia, Seguros diables, Seguro cordaes, seguro correfocs, seguro hogueras, Seguro Besties, Assegurances Correfocs, Assegurances Besties, Seguro Trabucaire, Seguro Passacaglia, Seguro bruixes, Assegurances bruixes, seguro fiesta del dragon, seguro dragon chino, seguro gigantes, cabezudos, Assegurances gegants, seguro cohetes, asseguranca castellers';

	    $data['page_keywords'] = 'Seguro para fallas, seguro falla, seguro correfocs y diables, seguro hoguera, seguro dimonis,  Seguros o assegurances, Organizaciones falleras, Hogueras, Festejos en general, Correfocs, Cordaes, Toros de fuego, Trabucaires, Seguro Besties, Seguro Bestiari, Assegurances Correfocs, Assegurances Besties, Seguro Trabucaire, assegurances trabucaires, Seguro Passacaglia, Assegurances Passacaglia, Seguro bruixes, Assegurances bruixes, Seguro Fades, Assegurances Fades, seguro para dragones de fuego, seguro Cuques, assegurances Cuques, seguro fiesta del dragon, seguro dragon chino';

	    $data['page_description'] = 'Comparador seguros de fallas';

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre y apellidos', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('telefono', 'telefono', 'required');
		$this->form_validation->set_rules('producto', 'clases de actos festivos', 'required');
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');


		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('templates/head',$data);
			$this->load->view('templates/header');

			$this->load->view('seguros/landing_pages/landing_fallas');

			$this->load->view('templates/footer');
			$this->load->view('templates/modals');
			$this->load->view('templates/scripts');
		}
		else // Formulario validado correctamente.
		{
			$cod_correduria = CODIGO_CORREDURIA;
			$nombreApellidos = $this->input->post('nombreApellidos');
			$email = $this->input->post('email');
			$telefono = $this->input->post('telefono');
			$producto = $this->input->post('producto');
			$tipo_seguro = $this->input->post('tipo_seguro');

			/****************************** Llamada CURL para crear tarificacion en el webservice  *******************************/
			$params = array(
			   "correduria" => CODIGO_CORREDURIA, 
			   "nombreApellidos" => $nombreApellidos,
			   "email" => $email,
			   "telefono" => $telefono,
			   "tipo_seguro" => $tipo_seguro,
			   "Clase_de_actos_festivos" => $producto
			);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);		

			if ($output_arr['success'] === TRUE){

				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_contacto.php";

				$datosCorreo = array("nombreApellidos" => $nombreApellidos, "email" => $email, "telefono" => $telefono, "tipo_seguro" => $tipo_seguro, "producto_interesado" => $producto);


				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['telefono_contacto'] = TELEFONO_CONTACTO_OFICINA_VALENCIA;

				$this->load->view('templates/head',$data);
				$this->load->view('templates/header');

				$this->load->view('seguros/resultados/correo_enviado',$data);

				$this->load->view('templates/footer');
				$this->load->view('templates/modals');
				$this->load->view('templates/scripts');
			}else{

				$this->load->view('templates/head',$data);
				$this->load->view('templates/header');

				$this->load->view('error');

				$this->load->view('templates/footer');
				$this->load->view('templates/modals');
				$this->load->view('templates/scripts');				
			}				
		}
	}	


	public function correfocs()
	{
	    $data['page_title'] = 'Seguro, assegurances per correfocs, diables, besties, gegants, capgrands, gigantes y cabezudos, trabucaires, cercaviles';

	    $data['page_keywords'] = 'seguro, assegurança, segur, assegurances, para correfocs, diables, besties, bestiaris, gegants, capgrands, gigantes y cabezudos, trabucaires, arcabuceros, bruixes, fades, dragones de fuego, cuques, cercaviles, passacaglia';

	    $data['page_description'] = 'Seguro o assegurança para correfocs, diables, besties, bestiaris, cercaviles, gegants, capgrands, gigantes y cabezudos, trabucaires';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];	  
	    
	    $data['nombre_seguro'] = "correfocs";	      
	    $data['callback'] = "correfocs";	      

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');			
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_correfocs');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}	



	/*
	*	Producto: seguro de salud
	*	Resultado: precio obtenido a traves de webservice
	*
	*/
	public function salud($carousel = FALSE)
	{
	    $data['page_title'] = 'Seguros de salud en Valencia, Madrid, Barcelona, Bilbao, Sevilla, comparador y comparativa seguros medicos, salud u hospitalarios';
	    $data['page_keywords'] = 'Seguros de salud en Valencia, Madrid, Barcelona, Bilbao, Sevilla, Comparador de Seguro de salud, comparador de seguros medicos, comparativa de seguros de salud, comparar seguros médicos, comparar seguro de salud, Seguros de salud barato, seguro de salud barato comparador seguro de salud';
	    $data['page_description'] = 'Seguros de salud en Valencia, Madrid, Barcelona, Bilbao, Sevilla, Consigue el mejor precio con tus seguros medicos y salud con nuestro comparador y comparativa de seguros médicos y baratos con Sanitas, Adeslas,Mapfre, Santalucia';

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre y apellidos', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('telefono', 'telefono', 'required');
		$this->form_validation->set_rules('provincia', 'provincia', 'required');
		$this->form_validation->set_rules('numeroPersonas', 'numero de personas', 'required');
		$this->form_validation->set_rules('autonomo', 'contratante del seguro', 'required');
		$this->form_validation->set_rules('edad-1', 'Edad del asegurado', 'required');
		if ($carousel)	
			$this->form_validation->set_rules('salud_politica', 'politica de privacidad', 'required');
		else
			$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');

		if ($this->form_validation->run() == FALSE)
		{


			$data['jsArray'] = array('public/js/carousel_form.js');	


			if ($carousel)
			{

				$data['opciones_menu'] = ['inicio' => '1','sobre nosotros' => '0','contacto' => '0','productos' => '0','casos exito' => '0'];
    
				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('templates/carousel');
				$this->load->view('home');
				$this->load->view('templates/footer');

			}else{

			    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];
		
			    $data['nombre_seguro'] = "salud";
			    $data['callback'] = "salud";

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('templates/header_producto_formulario');
				$this->load->view('productos/landing_pages/landing_' . $data['nombre_seguro']);
				$this->load->view('templates/footer');
			}


		}
		else // Formulario validado correctamente.
		{
			$cod_correduria = CODIGO_CORREDURIA;
			$nombreApellidos = $this->input->post('nombreApellidos');
			$email = $this->input->post('email');
			$telefono = $this->input->post('telefono');
			$prefijoProvincia = $this->input->post('provincia');
			$numAseg = $this->input->post('numeroPersonas');
			$autonomo = $this->input->post('autonomo');	


			/****************************** Llamada CURL para crear tarificacion en el webservice  *******************************/
			$params = array(
			   "correduria" => CODIGO_CORREDURIA,
			   "nombreApellidos" => $nombreApellidos,
			   "email" => $email,
			   "telefono" => $telefono,
			   "provinciaPrefijo" => $prefijoProvincia,
			   "saludContratante" => $autonomo,
			   "numPersonas" => $numAseg
			);



			for ($i = 1; $i <= $numAseg; $i++) {

				$params['edad' . $i] = $this->input->post('edad-'. $i);
			}

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_salud/salud",$params);  // hay que meter los productos de salud de starazona en el tarificador. de momento no tiene ninguno.

			if ($output_arr['success'] === TRUE){
				// Decodificamos el JSON de respuesta
				$arrayRespuesta = json_decode($output_arr['output'],true);

				
				$id = $arrayRespuesta['id'];
				$mensaje = $arrayRespuesta['message'];

				$data['id'] = $id;
				$data['mensaje'] = $mensaje;
				$data['datosraw'] = $output_arr['output'];



				if ($arrayRespuesta['success'] == TRUE) {

					// La tarificacion ha sido creada correctamente y tenemos su id en $id.
					// Ahora hacemos otra llamada al servidor REST mediante una llamada GET para obtener 
					// los datos de dicha tarificacion
					$output_arr = $this->curl_functions->httpGet(URL_CURL . "tarificador_seguro_salud/salud/id/" . $id);

					if ($output_arr['success'] === TRUE){
						// Decodificamos el JSON de respuesta
						$arrayRespuesta = json_decode($output_arr['output'],true);

						// normalizamos las primas ( quitando decimales innecesarios, redondeando si hace falta, y dividiendo cada prima por el numero de personas para mostrar el precio medio por persona)
						$primas = $this->primas->valor_medio_salud($arrayRespuesta['primas'], $numAseg);
						$primas = $this->primas->decimales_primas_salud($primas, 0);

						$data['codigoTarificacion'] = CODIGO_CORREDURIA . "-" . $arrayRespuesta['codtar'] . "-" . CODIGO_SALUD;
						$data['codtar'] = $arrayRespuesta['codtar'];
						$data['nombreApellidos'] = $arrayRespuesta['nombreApellidos'];						
						$data['email'] = $arrayRespuesta['email'];
						$data['telefono'] = $arrayRespuesta['telefono'];
						$data['primas'] = $primas;
						$data['numeroPersonas'] = $arrayRespuesta['numeroPersonas'];
						$data['salud_provincia_id'] = $arrayRespuesta['saludProvinciaId'];
						$data['salud_contratante'] = $arrayRespuesta['saludContratante'];
						$data['salud_edad1'] = $arrayRespuesta['saludEdad1'];
						$data['salud_edad2'] = $arrayRespuesta['saludEdad2'];
						$data['salud_edad3'] = $arrayRespuesta['saludEdad3'];
						$data['salud_edad4'] = $arrayRespuesta['saludEdad4'];
						$data['salud_edad5'] = $arrayRespuesta['saludEdad5'];
						$data['salud_edad6'] = $arrayRespuesta['saludEdad6'];
						$data['salud_edad7'] = $arrayRespuesta['saludEdad7'];
						$data['salud_edad8'] = $arrayRespuesta['saludEdad8'];
						$data['salud_edad9'] = $arrayRespuesta['saludEdad9'];
						$data['salud_edad10'] = $arrayRespuesta['saludEdad10'];
					

						
						/////////////////////////////////////////////////////////// enviamos el correo con las primas //////////////////////////////////////////////////////////////////////////////
						
						$subject = "Los precios de tu seguro de salud";
						$recipient = $arrayRespuesta['email'];
						$cc = EMAIL_CC;
						$bcc = EMAIL_BCC;
						$from = EMAIL_FROM;
						$fromName = EMAIL_FROMNAME;
						$view = "emails/primas_salud.php";

						$datosCorreo = array("codtar" => $data['codigoTarificacion'], "nombreApellidos" => $nombreApellidos, "email" => $email, "telefono" => $telefono, "primas" => $primas);



						$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
						
						//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////						
						
						$opciones_menu = ['inicio' => '1','sobre nosotros' => '0','contacto' => '0','productos' => '0','casos exito' => '0'];
						$data['opciones_menu'] = $opciones_menu;

						$this->load->view('templates/header',$data);
						$this->load->view('templates/menu');
						$this->load->view('parrilla_salud',$data);
						$this->load->view('templates/footer');


					}else{
						$data['datosraw'] = $output_arr['output'];
						$data['num_error'] = 'error_1';

						$this->load->view('templates/header');
						$this->load->view('error',$data);
						$this->load->view('templates/footer');
					}

				} else { 
					$data['num_error'] = 'error_2';
					$data['mensaje'] = $output_arr;

					$this->load->view('templates/header');
					$this->load->view('error',$data);
					$this->load->view('templates/footer');
				}

			}else{
				$data['mensaje'] = $output_arr['output'];
				$data['num_error'] = 'error_3';

				$this->load->view('templates/header');
				$this->load->view('error',$data);
				$this->load->view('templates/footer');				
			}			
		}

	}



/* tipo: 
* 0 --> vida
* 1 --> vida-hipoteca
* 2 --> vida-bomberos
* 3 --> vida-policia
* 4 --> vida-guardia_civil
*/

public function vida($carousel = FALSE,$tipo = 0)
{
	    $data['page_title'] = 'Seguros de vida en Valencia, Madrid, Barcelona, Bilbao, Sevilla, comparador y comparativa seguros de vida, Comparar seguro de vida';
	    $data['page_keywords'] = 'Seguros de vida en Valencia, Madrid, Barcelona, Bilbao, Sevilla, Comparador de seguro de vida, Comparativa seguros de vida, Comparar seguros de vida, seguros vida, comparar seguro vida, Seguros de vida baratos';
	    $data['page_description'] = 'Seguros de vida en Valencia, Madrid, Barcelona, Bilbao, Sevilla, Consigue tu comparativa de seguro de vida más completa. Tu presupuesto mas barato online con nuestro comparador de seguros de vida. Seguros de Vida para toda tu familia';

		// Form validation

		if ($carousel){
			$this->form_validation->set_rules('vida_nombreApellidos', 'nombre y apellidos', 'required');
			$this->form_validation->set_rules('vida_email', 'email', 'required');
			$this->form_validation->set_rules('vida_telefono', 'telefono', 'required');
			$this->form_validation->set_rules('vida_capital', 'capital para asegurar', 'required');
			$this->form_validation->set_rules('vida_edad', 'Edad del asegurado', 'required'); 				
			$this->form_validation->set_rules('vida_politica', 'politica de privacidad', 'required');
		}
		else{
			$this->form_validation->set_rules('nombreApellidos', 'nombre y apellidos', 'required');
			$this->form_validation->set_rules('email', 'email', 'required');
			$this->form_validation->set_rules('telefono', 'telefono', 'required');
			$this->form_validation->set_rules('capital', 'capital para asegurar', 'required');
			$this->form_validation->set_rules('edad', 'Edad del asegurado', 'required'); 				
			$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');
		}


			switch ($tipo) {
				case '0':
					$data['nombre_seguro'] = "vida";
					break;

				case '1':
					$data['nombre_seguro'] = "vida hipoteca";
					break;

				case '2':
					$data['nombre_seguro'] = "vida para bomberos";
					break;

				case '3':
					$data['nombre_seguro'] = "vida para policias";
					break;	

				case '4':
					$data['nombre_seguro'] = "vida para guardia civil";
					break;																				
				
				default:
					$data['nombre_seguro'] = "vida";
					break;
			}		

		if ($this->form_validation->run() == FALSE)
		{
			$data['jsArray'] = array('public/js/carousel_form.js');	



			if ($carousel)
			{

				$data['opciones_menu'] = ['inicio' => '1','sobre nosotros' => '0','contacto' => '0','productos' => '0','casos exito' => '0'];
    
				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('templates/carousel');
				$this->load->view('home');
				$this->load->view('templates/footer');

			}else{

			    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];
		

			    $data['callback'] = 'vida';
			    

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('templates/header_producto_formulario');
				$this->load->view('productos/landing_pages/landing_vida');
				$this->load->view('templates/footer');
			}
		}
		else // Formulario validado correctamente.
		{
			$cod_correduria = CODIGO_CORREDURIA;
			if ($carousel)
			{
				$nombreApellidos = $this->input->post('vida_nombreApellidos');
				$email = $this->input->post('vida_email');
				$telefono = $this->input->post('vida_telefono');
				$capital = $this->input->post('vida_capital');
				$edad = $this->input->post('vida_edad');
			}else{
				$nombreApellidos = $this->input->post('nombreApellidos');
				$email = $this->input->post('email');
				$telefono = $this->input->post('telefono');
				$capital = $this->input->post('capital');
				$edad = $this->input->post('edad');

			}


			/****************************** Llamada CURL para crear tarificacion en el webservice  *******************************/
			$params = array(
			   "correduria" => CODIGO_CORREDURIA,
			   "nombreApellidos" => $nombreApellidos,
			   "email" => $email,
			   "telefono" => $telefono,
			   "capital" => $capital,
			   "edad" => $edad
			);


			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_vida/vida",$params);			

			if ($output_arr['success'] === TRUE){
				// Decodificamos el JSON de respuesta
				$arrayRespuesta = json_decode($output_arr['output'],true);
				
				$id = $arrayRespuesta['id'];
				$mensaje = $arrayRespuesta['message'];

				$data['id'] = $id;
				$data['mensaje'] = $mensaje;
				$data['datosraw'] = $output_arr['output'];

				if ($arrayRespuesta['success'] == TRUE) {

					// La tarificacion ha sido creada correctamente y tenemos su id en $id.
					// Ahora hacemos otra llamada al servidor REST mediante una llamada GET para obtener 
					// los datos de dicha tarificacion					
					$output_arr = $this->curl_functions->httpGet(URL_CURL . "tarificador_seguro_vida/vida/id/" . $id);

					if ($output_arr['success'] === TRUE){
						// Decodificamos el JSON de respuesta
						$arrayRespuesta = json_decode($output_arr['output'],true);

						// normalizamos las primas ( quitando decimales innecesarios, redondeando si hace falta, y dividiendo cada prima por el numero de personas para mostrar el precio medio por persona)
						$primas = $this->primas->decimales_primas_vida($arrayRespuesta['primas'], 0);

						$data['codigoTarificacion'] = CODIGO_CORREDURIA . "-" . $arrayRespuesta['codtar'] . "-" . CODIGO_VIDA;
						$data['codtar'] = $arrayRespuesta['codtar'];
						$data['nombreApellidos'] = $arrayRespuesta['nombreApellidos'];						
						$data['email'] = $arrayRespuesta['email'];
						$data['telefono'] = $arrayRespuesta['telefono'];
						$data['primas'] = $primas;
						$data['capital'] = $arrayRespuesta['capital'];
						$data['edad'] = $arrayRespuesta['edad'];
						
						///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
						$subject = "Los precios de tu seguro de " . $data['nombre_seguro'];
						$recipient = $arrayRespuesta['email'];
						$cc = EMAIL_CC;
						$bcc = EMAIL_BCC;
						$from = EMAIL_FROM;
						$fromName = EMAIL_FROMNAME;
						$view = "emails/primas_vida.php";

						$datosCorreo = array("codtar" => $data['codigoTarificacion'], "nombreApellidos" => $nombreApellidos, "email" => $email, "telefono" => $telefono, "primas" => $primas);

						$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
						////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////						
						

						$opciones_menu = ['inicio' => '1','sobre nosotros' => '0','contacto' => '0','productos' => '0','casos exito' => '0'];
						$data['opciones_menu'] = $opciones_menu;

						$this->load->view('templates/header',$data);
						$this->load->view('templates/menu');
						$this->load->view('parrilla_vida',$data);
						$this->load->view('templates/footer');

					}else{
						$data['datosraw'] = $output_arr['output'];
						$data['num_error'] = 'error_1';

						$this->load->view('templates/header');
						$this->load->view('error',$data);
						$this->load->view('templates/footer');
					}

				} else {
					$data['num_error'] = 'error_2';
					$data['mensaje'] = $output_arr;

					$this->load->view('templates/header');
					$this->load->view('error',$data);
					$this->load->view('templates/footer');
				}

			}else{
				$data['mensaje'] = $output_arr['output'];
				$data['num_error'] = 'error_3';

				$this->load->view('templates/header');
				$this->load->view('error',$data);
				$this->load->view('templates/footer');				
			}		
		}	
	}



public function decesos($carousel = FALSE)
{

	    $data['page_title'] = 'seguros de decesos';
	    $data['page_keywords'] = 'seguros de decesos, Comparador de seguros de decesos, comparar seguro de decesos, seguro de deceso, seguros de decesos, seguro entierro, seguro de los muertos, seguros de decesos baratos, Comparativa de seguros de decesos';
	    $data['page_description'] = 'seguros de decesos, Comparador seguros decesos, Comparar seguro de decesos, seguros de entierro, seguro de los muertos, seguros funerarios. Tu mejor precio por comparar el seguro de decesos';

		// Form validation

		if ($carousel){	
			$this->form_validation->set_rules('decesos_nombreApellidos', 'nombre y apellidos', 'required');
			$this->form_validation->set_rules('decesos_email', 'email', 'required');
			$this->form_validation->set_rules('decesos_telefono', 'telefono', 'required');
			$this->form_validation->set_rules('decesos_provincia', 'provincia', 'required');
			$this->form_validation->set_rules('decesos_numeroPersonas', 'numero de personas', 'required');
			$this->form_validation->set_rules('decesos_edad-1', 'Edad del asegurado', 'required');			
			$this->form_validation->set_rules('decesos_politica', 'politica de privacidad', 'required');
		}else{
			$this->form_validation->set_rules('nombreApellidos', 'nombre y apellidos', 'required');
			$this->form_validation->set_rules('email', 'email', 'required');
			$this->form_validation->set_rules('telefono', 'telefono', 'required');
			$this->form_validation->set_rules('provincia', 'provincia', 'required');
			$this->form_validation->set_rules('numeroPersonas', 'numero de personas', 'required');
			$this->form_validation->set_rules('edad-1', 'Edad del asegurado', 'required');				
			$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			$data['jsArray'] = array('public/js/carousel_form.js');

			if ($carousel)
			{

				$data['opciones_menu'] = ['inicio' => '1','sobre nosotros' => '0','contacto' => '0','productos' => '0','casos exito' => '0'];
    
				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('templates/carousel');
				$this->load->view('home');
				$this->load->view('templates/footer');

			}else{

			    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];
		
			    $data['nombre_seguro'] = "decesos";
			    $data['callback'] = "decesos";

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('templates/header_producto_formulario');
				$this->load->view('productos/landing_pages/landing_' . $data['nombre_seguro']);
				$this->load->view('templates/footer');
			}
		}
		else // Formulario validado correctamente.
		{
			$cod_correduria = CODIGO_CORREDURIA;
			if ($carousel)
			{			
				$nombreApellidos = $this->input->post('decesos_nombreApellidos');
				$email = $this->input->post('decesos_email');
				$telefono = $this->input->post('decesos_telefono');
				$prefijoProvincia = $this->input->post('decesos_provincia');
				$numAseg = $this->input->post('decesos_numeroPersonas');
		}else{
				$nombreApellidos = $this->input->post('nombreApellidos');
				$email = $this->input->post('email');
				$telefono = $this->input->post('telefono');
				$prefijoProvincia = $this->input->post('provincia');
				$numAseg = $this->input->post('numeroPersonas');
		}
	
			/****************************** Llamada CURL para crear tarificacion en el webservice  *******************************/
			$params = array(
			   "correduria" => CODIGO_CORREDURIA,
			   "nombreApellidos" => $nombreApellidos,
			   "email" => $email,
			   "telefono" => $telefono,
			   "provincia" => $prefijoProvincia,
			   "numPersonas" => $numAseg,
			   "overwrite_capitales" => '0',
			   "nuevo_capital" => '0'
			);


			for ($i = 1; $i <= $numAseg; $i++) {
				if ($carousel)
					$params['edad' . $i] = $this->input->post('decesos_edad-'. $i);
				else
					$params['edad' . $i] = $this->input->post('edad-'. $i);
			}

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_decesos/decesos",$params);			

			if ($output_arr['success'] === TRUE){
				// Decodificamos el JSON de respuesta
				$arrayRespuesta = json_decode($output_arr['output'],true);

				
				$id = $arrayRespuesta['id'];
				$mensaje = $arrayRespuesta['message'];

				$data['id'] = $id;
				$data['mensaje'] = $mensaje;
				$data['datosraw'] = $output_arr['output'];



				if ($arrayRespuesta['success'] == TRUE) { 

					// La tarificacion ha sido creada correctamente y tenemos su id en $id.
					// Ahora hacemos otra llamada al servidor REST mediante una llamada GET para obtener 
					// los datos de dicha tarificacion
					$output_arr = $this->curl_functions->httpGet(URL_CURL . "tarificador_seguro_decesos/decesos/id/" . $id);

					if ($output_arr['success'] === TRUE){
						// Decodificamos el JSON de respuesta
						$arrayRespuesta = json_decode($output_arr['output'],true);

						// normalizamos las primas ( quitando decimales innecesarios, redondeando si hace falta, y dividiendo cada prima por el numero de personas para mostrar el precio medio por persona)
						$primas = $this->primas->valor_medio_decesos($arrayRespuesta['primas'], $numAseg);
						$primas = $this->primas->decimales_primas_decesos($primas, 0);

						$data['codigoTarificacion'] = CODIGO_CORREDURIA . "-" . $arrayRespuesta['codtar'] . "-" . CODIGO_DECESOS;
						$data['codtar'] = $arrayRespuesta['codtar'];
						$data['nombreApellidos'] = $arrayRespuesta['nombreApellidos'];
						$data['email'] = $arrayRespuesta['email'];
						$data['telefono'] = $arrayRespuesta['telefono'];
						$data['primas'] = $primas;
						$data['numeroPersonas'] = $numAseg;
						
						$data['salud_edad1'] = $this->input->post('decesos_edad-1');
						$data['salud_edad2'] = $this->input->post('decesos_edad-2');
						$data['salud_edad3'] = $this->input->post('decesos_edad-3');
						$data['salud_edad4'] = $this->input->post('decesos_edad-4');
		
						///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
						$subject = "Los precios de tu seguro de decesos";
						$recipient = $arrayRespuesta['email'];
						$cc = EMAIL_CC;
						$bcc = EMAIL_BCC;
						$from = EMAIL_FROM;
						$fromName = EMAIL_FROMNAME;
						$view = "emails/primas_decesos.php";

						$datosCorreo = array("codtar" => $data['codigoTarificacion'], "nombreApellidos" => $nombreApellidos, "email" => $email, "telefono" => $telefono, "primas" => $primas);

						$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
						////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////						
						
						$opciones_menu = ['inicio' => '1','sobre nosotros' => '0','contacto' => '0','productos' => '0','casos exito' => '0'];
						$data['opciones_menu'] = $opciones_menu;

						$this->load->view('templates/header',$data);
						$this->load->view('templates/menu');
						$this->load->view('parrilla_decesos',$data);
						$this->load->view('templates/footer');

					}else{
						$data['datosraw'] = $output_arr['output'];
						$data['num_error'] = 'error_1';

						$this->load->view('templates/header');
						$this->load->view('error',$data);
						$this->load->view('templates/footer');
					}

				} else { 
					$data['num_error'] = 'error_2';
					$data['mensaje'] = $output_arr;

					$this->load->view('templates/header');
					$this->load->view('error',$data);
					$this->load->view('templates/footer');
				}

			}else{   
				$data['mensaje'] = $output_arr['output'];
				$data['num_error'] = 'error_3';

				$this->load->view('templates/header');
				$this->load->view('error',$data);
				$this->load->view('templates/footer');				
			}
		}	
	}	

	public function mascotas()
	{
		$data['page_title'] = 'Comparador seguros de perros, caballos, mascotas y animales peligrosos';
	    $data['page_keywords'] = 'Seguros para Perros, Seguros para perros peligrosos, Seguros para mascotas, Seguro para Caballos, seguros para animales, seguro para gatos';
	    $data['page_description'] = 'Seguros online para perros, caballos, gatos y animales domésticos o peligrosos.';

	    $opciones_menu = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];
	    $data['opciones_menu'] = $opciones_menu;

	    $data['nombre_seguro'] = "mascotas";
	    $data['callback'] = "mascotas";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');  

		$this->form_validation->set_rules('fecha_nacimiento', 'fecha de nacimiento', 'required');	    
		$this->form_validation->set_rules('sexo_animal', 'sexo del animal', 'required');	    
		$this->form_validation->set_rules('valor_animal', 'valor del animal', 'required');	    
		$this->form_validation->set_rules('uso_animal', 'uso del animal', 'required');	    
		$this->form_validation->set_rules('clase_animal', 'clase de animal', 'required');	    
		$this->form_validation->set_rules('raza_animal', 'raza del animal', 'required');	    
		$this->form_validation->set_rules('defecto_fisico_animal', 'defecto del animal', 'required');	    
		$this->form_validation->set_rules('accidentes_animal', 'accidentes, robo o extravío', 'required');	    
		$this->form_validation->set_rules('rc_animal', 'responsabilidad civil', 'required');	    
		$this->form_validation->set_rules('sacrificio_animal', 'sacrificio y eliminación necesaria', 'required');	    
		$this->form_validation->set_rules('residencia_animal', 'estancia en residencia', 'required');	    
		$this->form_validation->set_rules('gasto_veterinario_animal', 'gasto veterinario por accidentes / enfermedad', 'required');	  

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_' . $data['nombre_seguro']);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value) 
				$params[$key] = $value;
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}

	public function barcos()
	{
		$data['page_title'] = 'Comparador seguros de barcos, seguro barcos, seguros embarcaciones';
	    $data['page_keywords'] = 'seguros de barcos, seguro barco, seguros embarcaciones de recreo, seguros motos de agua, seguro velero, seguros de barcos online';
	    $data['page_description'] = 'Comparar Online tu Seguros de Barcos, Velero y moto de agua. Ahórrate mucho dinero en el seguro de embarcación de recreo. Contrátanos tu seguro de barco';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "barcos";
	    $data['callback'] = "barcos";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');	 
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');



		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_' . $data['nombre_seguro']);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value) 
				$params[$key] = $value;
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}	




	public function comunidades()
	{
		$data['page_title'] = 'seguro de comunidades, seguro comunidades, seguro comunidad, seguro para comunidad';
	    $data['page_keywords'] = 'seguro de comunidades, seguro comunidades, seguro de comunidad, seguro comunidad, seguros para comunidades, seguros de comunidades, comparativa seguro comunidades';
	    $data['page_description'] = 'Con un seguro de comunidad ten cubiertos todos tus riesgos y los de tus vecinos.';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "comunidades";
	    $data['callback'] = "comunidades";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_' . $data['nombre_seguro']);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value) 
				$params[$key] = $value;
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}		

	public function empresas()
	{
		$data['page_title'] = 'Comparador seguros de empresa, Seguro empresas, Ayuntamiento, pymes, seguro empresa';
	    $data['page_keywords'] = 'Seguro empresas, Seguros industriales, Seguro industrial,  Seguros pyme, seguro para empresas, seguros fabricas de muebles, seguros fabricas de alimentación, seguros fábrica de componentes eléctricos, seguros fábrica de muebles, seguros fabricas productos químicos, seguros tapiceras, textil, seguros salas de baile, seguro ayuntamientos, seguro discoteca, seguros para discotecas,  seguro empresa azulejera, seguro azulejera, seguro empresas cartonaje, seguro para papeleras.';
	    $data['page_description'] = 'Seguros para empresas e industrias. Un ámplio abanico de coberturas para poder tener bien cubierta tu empresa, industria, fábrica, almacén, ayuntamiento';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "empresas/pyme";
	    $data['callback'] = "empresas";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required'); 

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_empresas');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value) 
				$params[$key] = $value;
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}		

	public function rc()
	{
		$data['page_title'] = 'Seguros de Responsabilidad Civil, Seguro RC ayuntamientos';
	    $data['page_keywords'] = 'seguro de  responsabilidad civil, seguro responsabilidad civil profesional, seguro responsabilidad civil cazadores, Seguros de Responsabilidad Civil, Seguro RC, seguro responsabilidad civil medioambiental, comparativa seguro responsabilidad civil, seguro de responsabilidad civil  para ayuntamientos, instituciones públicas';
	    $data['page_description'] = 'Contrata un seguro de responsabilidad civil para tu empresa o para cubrir tu profesión. La mejor inversión para tu tranquilidad.';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "responsabilidad civil";
	    $data['callback'] = "rc";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_rc');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value) 
				$params[$key] = $value;
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}	

	public function joyeria()
	{
		$data['page_title'] = 'Comparador seguros de joyerias, seguro joyerías, seguro para joyerías, seguro joyería';
	    $data['page_keywords'] = 'seguros para joyería, seguro para joyería, seguro para joyerías, seguro joyería, todo riesgo joyerías, seguro joyerías';
	    $data['page_description'] = 'Seguro de joyerías. Solicítanos el presupuesto para tu seguro todo riesgo de joyerías y consigue los precios más baratos y con las mejores coberturas';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "joyeria";
	    $data['callback'] = "joyeria";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required'); 

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_' . $data['nombre_seguro']);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value) 
				$params[$key] = $value;
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}	


	public function riesgos_ciberneticos()
	{
		$data['page_title'] = 'seguro contra ataques informaticos, seguro contra ataques ciberneticos, seguro contra ataques piratas informaticos, seguro contra hackers, seguro contra malware, seguro contra virus informaticos';
	    $data['page_keywords'] = 'seguro contra ataques informaticos, seguro contra ataques ciberneticos, seguro contra ataques piratas informaticos, seguro contra hackers, seguro contra malware, seguro contra virus informaticos';
	    $data['page_description'] = 'Contrata para tu tranquilidad un seguro contra ataques informaticos, seguro contra ataques ciberneticos, seguro contra ataques piratas informaticos';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "riesgos cibernéticos";
	    $data['callback'] = "riesgos_ciberneticos";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');	
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');  

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_riesgos_ciberneticos');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value) 
				$params[$key] = $value;
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}		


	public function comercio()
	{
		$data['page_title'] = 'Comparador seguros de comercio, Seguro de Comercio, Seguro Comercios, Seguro comercio, Seguro Tienda, Seguro Tiendas';
	    $data['page_keywords'] = 'seguro de comercio, seguro comercio, seguro tienda, seguro negocio, seguro tienda, seguro bar, seguro boutique, seguro peluquería, seguros comercio, seguro oficinas';
	    $data['page_description'] = 'Con el Seguro de Comercio, cubre tu oficina, negocio o despacho';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "comercio";
	    $data['callback'] = "comercio";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');	 
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required'); 

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_' . $data['nombre_seguro']);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value) 
				$params[$key] = $value;
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}



	public function directivos_altos_cargos()
	{
		$data['page_title'] = 'Seguro de Directivos y Altos Cargos, Seguro Empresa, Seguro D & O, responsabilidad civil directivos, administradores concursales, Responsabilidad civil, altos cargos, directivos, empresa, privada, ZUrich, AIG, Reale';
	    $data['page_keywords'] = 'Seguro de directivos  altos cargos D & O, seguro directivos, seguro altos cargos, seguro empresa, seguro d & o, seguro d & o administradores concursales, seguro para directivos, seguro responsabilidad civil directivos, seguro altos cargos y directivos, seguro de directivos y altos cargos';
	    $data['page_description'] = 'Ten cubierta tu responsabilidad civil ante cualquier conflicto frente a proveedores, socios, administración publica filial o participada';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "RC directivos y altos cargos";
	    $data['callback'] = "directivos_altos_cargos";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');	 
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required'); 

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_directivos_altos_cargos');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value) 
				$params[$key] = $value;
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}	
	

	public function dependencia()
	{
		$data['page_title'] = 'seguro dependencia, comparador seguros privados dependencia, comparativa, comparar';
	    $data['page_keywords'] = 'Seguro dependencia, dependencia, comparador comparativa seguros de dependencia,  comparar seguro de dependencia o dependiente, Seguro ayuda, Seguro ayuda domicilio, Seguro fallecimiento, Seguro muerte, Seguro deceso, Seguro asistencia familiar, Seguro teleasistencia, Ley de dependencia';
	    $data['page_description'] = 'Por si mañana necesitas ayuda contrata hoy tu seguro de dependencia, haz una comparativa con nuestro comparador de seguro de dependencia.';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "dependencia";
	    $data['callback'] = "dependencia";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');	
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');  

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_' . $data['nombre_seguro']);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}


	public function jubilacion()
	{
		$data['page_title'] = 'Seguro jubilación, seguro pensiones, pías, primas únicas, inversión';
	    $data['page_keywords'] = 'seguro de jubilación, seguro de pensiones, seguro inversión, seguro pías, primas únicas muy rentables, seguro de máximos rendimientos e intereses';
	    $data['page_description'] = 'Consigue con nuestros seguros de jubilación, pensiones, primas únicas e inversiónes la máxima tranquilidad y rendimientos para tus ahorros e inversiones';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "jubilacion y pensiones";
	    $data['callback'] = "jubilacion";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');	 
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required'); 

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_jubilacion');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}


	public function fusiones()
	{
		$data['page_title'] = 'Seguro de fusiones, compra, venta y adquisiciones, seguro M. & A.';
	    $data['page_keywords'] = 'seguro de fusiones, adquisiciones, relevos generacionales, seguro de venta de empresas, Seguro M&A, seguro de compras de empresas y negocios';
	    $data['page_description'] = 'Dar entrada a nuevos socios una póliza seguro de fusiones y adquisiciones m&a es lo que anda buscando para que todo salga bien';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "fusiones y adquisiciones";
	    $data['callback'] = "fusiones";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');	  
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_fusiones');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}	

	public function dacion_pago()
	{
		$data['page_title'] = 'Seguro dacion en pagos, dacion en pagos, seguro dacion en pago preventiva, dacion en pago reinicia, seguro hipoteca';
	    $data['page_keywords'] = 'dacion en pago, Seguro dacion en pago, dación en pago, seguro hipoteca, dacion pago, dacion en pago hipoteca, seguro dacion en pago preventiva, seguro reinicia preventiva, inmueble en dacion de pago, dacion en pago hipoteca, seguro hipoteca con dacion en pago';
	    $data['page_description'] = 'Seguro dacion en pago para que en caso de subasta de tu casa puedas pagar la deuda diferencial. Dacion en pago preventiva';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "dacion en pago";
	    $data['callback'] = "dacion_pago";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');  

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_dacion');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}	

	public function caucion()
	{
		$data['page_title'] = 'Seguro de caución';
	    $data['page_keywords'] = 'seguro de caucion';
	    $data['page_description'] = 'seguro de caución';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "caución";
	    $data['callback'] = "caucion";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');	  
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_caucion');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}	

	public function empresas_seguridad()
	{
		$data['page_title'] = 'Seguro para empresa de seguridad';
	    $data['page_keywords'] = 'Seguros empresa de seguridad, seguros empresas de vigilancia, seguro responsabilidad civil y caucion empresa seguridad, vigilancia y receptoras, seguro de responsabilidad civil empresas de transporte de explosivos o mercancías peligrosas, seguro detectives, seguro de detective privado, seguro de responsabilidad civil detective privado';
	    $data['page_description'] = 'Seguros para empresas de seguridad, seguros para empresas de vigilancia, seguro para empresas instaladoras';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "empresas de seguridad";
	    $data['callback'] = "empresas_seguridad";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_empresas_seguridad');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}	

	public function secuestro_extorsion()
	{
		$data['page_title'] = 'Seguro de secuestro, extorsion, detencion ilegal negociación y rescate';
	    $data['page_keywords'] = 'seguro de secuestro y extorsion, seguro contra el secuestro y la extorsión, seguro de secuestro, seguro contra extorsiones, seguro detención ilegal';
	    $data['page_description'] = 'Seguro de secuestro, extorsión, detención ilegal y rescate, su seguro o de sus empleados por sí van a algún lugar o país conflictivo. Seguro de secuestro';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "secuestro y extorsion";
	    $data['callback'] = "secuestro_extorsion";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');  

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_secuestro_extorsion');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}			

	public function accidentes()
	{
		$data['page_title'] = 'Seguro individual de accidentes, particulares, convenio, colectivo';
	    $data['page_keywords'] = 'seguro individual de accidentes, seguro de accidentes, seguro colectivo de accidentes, seguros personales de accidentes, seguro accidente convenio colectivo, seguro accidentes empresas, seguro trabajadores';
	    $data['page_description'] = 'Tu seguro individual de accidentes mas barato y completo online. Los mejores precios en seguros colectivos de accidentes o convenio para trabajadores';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "accidentes";
	    $data['callback'] = "accidentes";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_accidentes');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}	

	public function artistas_falleros()
	{
		$data['page_title'] = 'Comparador seguros de fallas, Seguros fallas, Seguro para fallas, Seguro correfocs, Seguros para fallas de Valencia, Seguros diables, Seguro cordaes, seguro correfocs, seguro hogueras, Seguro Besties, Assegurances Correfocs, Assegurances Besties, Seguro Trabucaire, Seguro Passacaglia, Seguro bruixes, Assegurances bruixes, seguro fiesta del dragon, seguro dragon chino, seguro gigantes, cabezudos, Assegurances gegants, seguro cohetes, asseguranca castellers';
	    $data['page_keywords'] = 'Seguro para fallas, seguro falla, seguro correfocs y diables, seguro hoguera, seguro dimonis,  Seguros o assegurances, Organizaciones falleras, Hogueras, Festejos en general, Correfocs, Cordaes, Toros de fuego, Trabucaires, Seguro Besties, Seguro Bestiari, Assegurances Correfocs, Assegurances Besties, Seguro Trabucaire, assegurances trabucaires, Seguro Passacaglia, Assegurances Passacaglia, Seguro bruixes, Assegurances bruixes, Seguro Fades, Assegurances Fades, seguro para dragones de fuego, seguro Cuques, assegurances Cuques, seguro fiesta del dragon, seguro dragon chino';
	    $data['page_description'] = 'Seguro para fallas, correfocs y diables, seguro hoguera, seguro dimonis, póliza  que cubre la responsabilidad civil en todas sus vertientes';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "artistas falleros";
	    $data['callback'] = "artistas_falleros";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');  

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_artistas_falleros');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}			


	public function suspension_espectaculos()
	{
		$data['page_title'] = 'Seguro de suspensión de espectaculos, eventos conciertos y festivales';
	    $data['page_keywords'] = 'Seguro de suspensión de espectáculo y eventos, seguro suspensión conciertos, seguro paralización, seguros contingencias, seguro para contingencias, seguro de hoyo a hoyo';
	    $data['page_description'] = 'Seguros suspensión y paralización para tus  espectáculos, eventos, giras musicales, conciertos, bolos';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "suspension de espectaculos";
	    $data['callback'] = "suspension_espectaculos";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_suspension_espectaculos');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}	

	public function tienda_pirotecnia()
	{
		$data['page_title'] = 'Seguro tiendas pirotecnia, Seguro kioscos pirotecnia, Seguros Quioscos pirotecnia, Seguro venta pirotecnia, seguro petardos,  seguro comercio pirotecnia, seguros, kioscos, Quiosco, tienda, comercio, petardos, cohetes, pirotecnia, fuegos artificiales, responsabilidad civil, aseguranca, Assegurances, segur';
	    $data['page_keywords'] = 'seguro tienda pirotecnia, seguro para tienda pirotecnia, seguro kiosco pirotecnia, seguro  para kiosco pirotecnia,seguro responsabilidad civil tienda pirotecnia, seguro responsabilidad civil, asseguranca pirotecnia, assegurances tenda kiosco o kiosco  pirotecnia, seguros, kioscos, Quiosco, tienda, comercio, petardos, cohetes, pirotecnia, fuegos artificiales, responsabilidad civil, aseguranca, Assegurances, segur,seguro responsabilidad civil kiosco pirotecnia, seguro disparo Pirotecnia, seguro disparo cohetes';
	    $data['page_description'] = 'Seguro tienda y kiosco de pirotecnia. Seguro para venta de pirotecnia, petardos, cohetes, Quiosco de pirotecnia';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "tiendas y kioscos de pirotecnia";
	    $data['callback'] = "tienda_pirotecnia";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');  

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_tienda_pirotecnia');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}	

	public function organizacion_pirotecnia()
	{
		$data['page_title'] = 'Seguro responsabilidad civil organización fuegos artificiales, seguro organizador fuegos artificiales, seguro organizador  pirotecnia, Seguro responsabilidad civil organización pirotecnia, seguro organización pirotecnia, seguro organizador fuegos artificiales, seguro organización, fuegos artificiales, pirotecnia, espectáculos pirotécnicos, seguro, seguros, póliza, organización, organizador, pirotecnia, fuegos artificiales, espectáculos, cohetes, cordaes';
	    $data['page_keywords'] = 'seguro responsabilidad civil organizador pirotecnia, seguro para organizador fuegos artificiales, seguro para organizador de pirotecnia, seguro organizador eventos, seguro organizador fuegos artificiales, seguro organización fuegos artificiales, seguro pirotecnia, seguro, seguros, póliza, organización, organizador, pirotecnia, fuegos artificiales, espectáculos, cohetes, cordaes, seguro responsabilidad civil pirotecnia, seguro de responsabilidad civil para pirotecnia, seguro responsabilidad civil organizador fuegos artificiales, seguro rc organizador, seguro rc pirotecnia, seguro rc pirotecnia, seguro responsabilidad civil pirotecnia, seguro disparo pirotecnia, seguro disparo de fuegos artificiales, seguro explosivos, seguro dinamita, seguro almacén explosivos, seguro, seguros, asegurar';
	    $data['page_description'] = 'Seguro organización fuegos artificiales, pirotecnia, espectaculos pirotécnicos. Poliza que cubre la responsabilidad civil del organizador de pirotecnia, seguro, seguros, organización, organizador, pirotecnia, fuegos artificiales, espectáculos, fuegos artificiales en todas sus vertientes de los actos festivos organizados por asociaciones privadas, publicas o ludicas como: Organizaciones falleras, Hogueras, Festejos en general, Correfocs, Cordaes, cohetes, Toros de fuego, Trabucaires, Toros en la calle';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "organizacion de pirotecnia";
	    $data['callback'] = "organizacion_pirotecnia";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');	 
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required'); 

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_organizacion_pirotecnia');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}		

	public function pirotecnia()
	{
		$data['page_title'] = 'Seguros Responsabilidad Civil Pirotecnia, R.C. Pirotecnia, Seguros Pirotécnicos, Seguro Pirotécnicos,seguro pirotecnia, Seguro fuegos artificiales, seguro mascletá, seguro espectáculos pirotécnicos, Seguro para petardos, Seguros petardos, seguro explosivos, seguro para depósitos explosivos, pirotecnia, pirotécnicos, fuegos artificiales,  mascleta, Amics del coet, Amigos del cohete, seguro voladuras, Explosivos';
	    $data['page_keywords'] = 'Responsabilidad Civil Pirotecnia, R.C. Pirotecnia, Seguros Pirotécnicos, Seguro Pirotécnicos , seguro pirotecnia, Seguro fuegos artificiales, seguros espectáculos pirotécnicos, seguro mascleta,  seguros para petardos, seguros petardos,  seguro explosivos, seguros explosivos, seguro para explosivos, seguros para explosivos, seguro transporte explosivos, seguro depósitos explosivos, seguro responsabilidad civil explosivos, seguro para tirar cohetes, seguro pirotecnia, seguro de pirotecnia, seguros pirotecnia, seguros para pirotecnia, seguros de pirotecnia, seguro para pirotecnia, seguro organizador pirotecnia, seguro para organizador fuegos artificiales, seguro para organizador de pirotecnia, seguro organizador eventos, seguro organizador fuegos artificiales, seguro organización fuegos artificiales, seguro pirotecnia, seguro responsabilidad civil pirotecnia, seguro de responsabilidad civil para pirotecnia, seguro responsabilidad civil organizador fuegos artificiales, seguro rc organizador, seguro rc pirotecnia, seguro rc pirotecnia, seguro responsabilidad civil pirotecnia, seguro disparo pirotecnia, seguro disparo de fuegos artificiales, seguro explosivos, seguro dinamita, seguro explosivos, amics del coet, amigos del cohete, seguro, seguros';
	    $data['page_description'] = 'Seguro de Responsabilidad civil pirotecnia, pirotécnicos, fuegos artificiales, mascleta';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "pirotecnia";
	    $data['callback'] = "pirotecnia";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');  

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_pirotecnia');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}		


	public function rc_espectaculos()
	{
		$data['page_title'] = 'Seguros de Responsabilidad Civil Espectáculos, eventos, conciertos';
	    $data['page_keywords'] = 'seguro espectaculos, seguro eventos, seguros eventos, seguro para eventos, Seguros espectáculos, seguro responsabilidad civil eventos, Seguro para eventos, seguro evento, seguros para eventos , seguro para festivales';
	    $data['page_description'] = 'Como profesional que rees es fundamental que cuentes con un sguro de responsabilidad civil para tus conciertos, espectáculos y eventos';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "RC espectaculos";
	    $data['callback'] = "rc_espectaculos";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required'); 

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_rc_espectaculos');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}	

	public function cinematografia()
	{
		$data['page_title'] = 'Seguro Cinematografía, Seguro Cine, Seguro Rodajes, seguro audiovisuales, Seguro Producción Cinematográfica, seguro audiovisuales';
	    $data['page_keywords'] = 'seguro cinematografía, seguro  cine, seguro rodajes , seguro audiovisuales, seguro producciones cinematográficas, responsabilidad civil cine, seguros para el Cine, seguro todo riesgo cine y audiovisuales, Seguros para Cine, seguros de cine, responsabilidad civil cines, seguro rodajes, seguro para rodajes, seguro de rodajes, seguro para rodajes cinematográficos, seguros rodaje, seguros para rodajes, seguros de rodajes, seguro empresas efectos especiales';
	    $data['page_description'] = 'Contrata el seguro de responsabilidad civil o de daños para tus rodajes cinematográficos o audiovisuales';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "cinematografia";
	    $data['callback'] = "cinematografia";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');	  
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_cinematografia');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}	

	public function bous_carrer()
	{
		$data['page_title'] = 'Comparador seguros de bous al carrer, seguro bous al carrer, seguro bous, seguro toros, seguro vaquillas, assegurances bous al carrer, bou embolat, en corda, sokamuturra';
	    $data['page_keywords'] = 'seguro bous al carrer, seguro bous, assegurances bous al carrer, toros, seguro vaquilla,  seguros para vaquillas, seguro toros, sokamuturra, seguro bous en corda, toro ensogado, toro embolado, corrida de toros,  seguro cadafales, seguro eventos taurinos, seguros taurinos, toro borracho, ensogados, toro de fuego';
	    $data['page_description'] = 'Contrate su seguro de bous al carrer o seguro de vaquillas. Especialistas más de 25 años en la contratación de seguros de bous al carrer  y vaquillas';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "bous al carrer";
	    $data['callback'] = "bous_carrer";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');	
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_bous_carrer');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}		

	public function efectos_especiales()
	{
		$data['page_title'] = 'Seguro responsabilidad civil , efectos especiales, película de  cine, televisión, publicidad, audiovisuales, conciertos, eventos, rodajes, impactos, armas';
	    $data['page_keywords'] = 'Seguro  responsabilidad civil para efectos especiales,  película , cine, televisión, publicidad, audiovisuales, seguro empresas de efectos especiales, seguro empresas  fx, , conciertos, eventos, discotecas, explosiones, fuegos, climatológicos, tiroteos, armas, especialistas, humo, ficticios, maquillaje f/x. Efectos Especiales para Eventos, Conciertos,  Espectáculos, confeti, pirotecnia, lanzallamas, efectos co2, burbujas, aromas, lluvia, nieve, niebla, hielo, impactos, cañones de espuma, armas';
	    $data['page_description'] = 'seguros para empresas de efectos especiales, seguro,  rodajes,  RESPONSABILIDAD CIVIL,  cine, televisión, publicidad, audiovisuales, rodajes  conciertos y eventos. Efectos mecánicos, atmosféricos, pirotécnia, ficticios, impactos, armas';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "efectos especiales";
	    $data['callback'] = "efectos_especiales";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_efectos_especiales');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}	

	public function hogar()
	{
		$data['page_title'] = 'Comparador seguros de hogar,Seguro de Hogar, Seguro hogar Barato';
	    $data['page_keywords'] = 'seguro de hogar, seguro de casa, seguro de hogar barato, seguro de hogar Mapfre, el mejor seguro de hogar, seguro de hogar más barato, mejor seguro de hogar, presupuesto seguro de hogar, seguro de hogar BBVA, calcular seguro de hogar, seguro para casa';
	    $data['page_description'] = 'Consigue el mejor precio  online para tu seguro de hogar con  nuestro comparador de seguros de hogar baratos.En 1 minuto el mejor precio y cobertura';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];

	    $data['nombre_seguro'] = "hogar";
	    $data['callback'] = "hogar";

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');  

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_hogar');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}


	public function fallas()
	{
	    $data['page_title'] = 'Comparador seguros de fallas, Seguros fallas, Seguro para fallas, Seguro correfocs, Seguros para fallas de Valencia, Seguros diables, Seguro cordaes, seguro correfocs, seguro hogueras, Seguro Besties, Assegurances Correfocs, Assegurances Besties, Seguro Trabucaire, Seguro Passacaglia, Seguro bruixes, Assegurances bruixes, seguro fiesta del dragon, seguro dragon chino, seguro gigantes, cabezudos, Assegurances gegants, seguro cohetes, asseguranca castellers';

	    $data['page_keywords'] = 'Seguro para fallas, seguro falla, seguro correfocs y diables, seguro hoguera, seguro dimonis,  Seguros o assegurances, Organizaciones falleras, Hogueras, Festejos en general, Correfocs, Cordaes, Toros de fuego, Trabucaires, Seguro Besties, Seguro Bestiari, Assegurances Correfocs, Assegurances Besties, Seguro Trabucaire, assegurances trabucaires, Seguro Passacaglia, Assegurances Passacaglia, Seguro bruixes, Assegurances bruixes, Seguro Fades, Assegurances Fades, seguro para dragones de fuego, seguro Cuques, assegurances Cuques, seguro fiesta del dragon, seguro dragon chino';

	    $data['page_description'] = 'Comparador seguros de fallas';

	    $data['opciones_menu'] = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '1','casos exito' => '0'];	  
	    
	    $data['nombre_seguro'] = "fallas";	      
	    $data['callback'] = "fallas";	      

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre', 'required');	        
		$this->form_validation->set_rules('email', 'email', 'required');	    
		$this->form_validation->set_rules('telefono', 'teléfono', 'required');	
		$this->form_validation->set_rules('producto', 'clases de actos festivos', 'required');	
		$this->form_validation->set_rules('politica_privacidad', 'politica de privacidad', 'required');

 

		if ($this->form_validation->run() == FALSE)
		{		  
			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('templates/header_producto_formulario');
			$this->load->view('productos/landing_pages/landing_fallas');
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$formValues = $this->input->post(NULL, TRUE);
			
			$params = array();

			foreach($formValues as $key => $value){
				$params[$key] = $value;
                        }
			
			// se elimina del array el checkbox de la politica de privacidad para no enviarla al ws
			unset($params["politica_privacidad"]);

			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_formularios/formulario",$params);

			if ($output_arr['success'] === TRUE){
				///////////////////////// enviamos el correo con las primas /////////////////////////////////////////////////
				
				$subject = "Formulario de contacto";
				$recipient = EMAIL_RECIPIENTE;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_producto_formulario.php";

				$datosCorreo = array("nombreApellidos" => $params["nombreApellidos"], "email" => $params["email"], "telefono" => $params["telefono"], "tipo_seguro" => $params["tipo_seguro"]);
				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				$data['items'] = $params;

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('productos/correo_enviado');
				$this->load->view('templates/footer');

			}else{

				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('error');
				$this->load->view('templates/footer');				
			}		
		}		
	}	





}
