<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacto extends CI_Controller {

    public function __construct() {
		parent::__construct();

		// Cambiamos el delimitador de los mensajes del formulario para que usen los estilos de bootstrap
		//$this->form_validation->set_error_delimiters('<div class="alerta-formulario" role="alert">', '</div>');
    }

	public function loc($oficina = 'SED')
	{
		$data['page_title'] = 'correduria de seguros Valencia, Barcelona, Sevilla, Madrid, Bilbao, comparador de seguros vida, decesos y medicos';
	    $data['page_keywords'] = 'correduria de seguros, corredor de seguros Valencia, Barcelona, Sevilla, Madrid, Bilbao, comparador de seguros, seguro de vida, seguro de decesos, seguros de coche, seguro de hogar';
	    $data['page_description'] = 'Somos una correduria de seguros de Valencia especializada más de 30 años en todo tipo de seguro. El mejor precio en seguros de vida, decesos, coche, hogar, salud';

    	$opciones_menu = ['inicio' => '0','sobre nosotros' => '0','contacto' => '1','productos' => '0','casos exito' => '0'];

    	$data['opciones_menu'] = $opciones_menu;

		
		// Form validation
		$this->form_validation->set_rules('name', 'nombre', 'required');
		$this->form_validation->set_rules('surname', 'apellidos', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('phone', 'telefono', 'required');
		$this->form_validation->set_rules('message', 'mensaje', 'required');



		if ($this->form_validation->run() == FALSE)
		{


			switch ($oficina) {
				case 'VAL':
					$data['lng'] = '-0.358096';
					$data['lat'] = '39.454112';
					break;

				case 'SED':
					$data['lat'] = '39.423022';
					$data['lng'] = '-0.385206';
					break;		

				case 'MAS':
					$data['lat'] = '39.413255';
					$data['lng'] = '-0.399447';
					break;

				case 'SIL':
					$data['lat'] = '39.366834';
					$data['lng'] = '-0.410288';
					break;						
				
				default:
					$data['lng'] = '-0.358096';
					$data['lat'] = '39.454112';
					break;
			}




			$this->load->view('templates/header',$data);
			$this->load->view('templates/menu');
			$this->load->view('contacto',$data);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$nombre = $this->input->post('name');
			$apellidos = $this->input->post('surname');
			$email = $this->input->post('email');
			$telefono = $this->input->post('phone');
			$mensaje = $this->input->post('message');

			//$return_arr = $this->registro_model->nuevo_registro($nombre,$apellidos,$telefono,$email,$cp);  	
			
			//if ($return_arr["success"] === TRUE){

				///////////////////////// enviamos el correo  /////////////////////////////////////////////////
				$subject = "Contacto Starazona";
				$recipient = EMAIL_CONTACTO;
				$cc = EMAIL_CC;
				$bcc = EMAIL_BCC;
				$from = EMAIL_FROM;
				$fromName = EMAIL_FROMNAME;
				$view = "emails/plantilla_contacto.php";

				$datosCorreo = array("nombre" => $nombre, "apellidos" => $apellidos, "email" => $email, "telefono" => $telefono, "mensaje" => $mensaje);


				$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				$data['nombre'] = $nombre;		
				$data['apellidos'] = $apellidos;		
				$data['email'] = $email;		
				$data['telefono'] = $telefono;
				$data['mensaje'] = $mensaje;	



				$this->load->view('templates/header',$data);
				$this->load->view('templates/menu');
				$this->load->view('resultado_contacto');
				$this->load->view('templates/footer');
			
		
		}
	}
}
