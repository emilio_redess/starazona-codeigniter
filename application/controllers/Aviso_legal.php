<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aviso_legal extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['page_title'] = 'correduria de seguros Valencia, Barcelona, Sevilla, Madrid, Bilbao, comparador de seguros vida, decesos y medicos';
	    $data['page_keywords'] = 'correduria de seguros, corredor de seguros Valencia, Barcelona, Sevilla, Madrid, Bilbao, comparador de seguros, seguro de vida, seguro de decesos, seguros de coche, seguro de hogar';
	    $data['page_description'] = 'Somos una correduria de seguros de Valencia especializada más de 30 años en todo tipo de seguro. El mejor precio en seguros de vida, decesos, coche, hogar, salud';

    	$opciones_menu = ['inicio' => '0','sobre nosotros' => '0','contacto' => '0','productos' => '0','casos exito' => '0'];

    	$data['opciones_menu'] = $opciones_menu;

		$this->load->view('templates/header',$data);
		$this->load->view('templates/menu');
		$this->load->view('aviso_legal');
		$this->load->view('templates/footer');
	}
}