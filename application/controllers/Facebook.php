<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facebook extends CI_Controller {

    public function __construct() {
		parent::__construct();

		$this->load->model('facebook_articulos_model');
		// Cambiamos el delimitador de los mensajes del formulario para que usen los estilos de bootstrap
		$this->form_validation->set_error_delimiters('<div class="alerta-formulario" role="alert">', '</div>');
    }

	public function index()
	{

	}
	
	public function articulo($id)
	{
		$articulo = $this->facebook_articulos_model->get_articulo($id);

		$data["articulo_url"] = $articulo->articulo_url;
		$this->load->view('facebook/articulo_redirect',$data);
	}
}
