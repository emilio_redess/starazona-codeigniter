<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


/**
 * CodeIgniter clase para hacer operaciones varias sobre las primas devueltas por el WS
 *
 *
 * @package         CodeIgniter
 * @subpackage      Libraries
 * @category        Libraries
 * @author          Emilio Valls
 */

class Primas {


   /**
     * Funcion que devuelve el valor medio de las primas segun el numero de asegurados
     * 
     *
     * @access  public
     * @param   primas    array: La clave es la id del producto asociado a la correduria.
     *                             y cada valor es un array con las siguientes claves => valores
     *                           nombreProducto => string
     *                           nombreProducto_cm => string
     *                           nombreProducto_re => string
     *                           logoProducto => string
     *                           nombreCia => string
     *                           formaPagoProducto => string
     *                           prima_cm => float
     *                           prima_re => float
     * @param numAseg integer
     * @return  array
     */
    function valor_medio_salud ($primas,$numAseg)
    {
        $primas_aux = array();
        foreach ($primas as $k => $v) {
           
            $primas_aux[$k] = array(
                "nombreProducto" => $v["nombreProducto"],
                "nombreProducto_cm" => $v["nombreProducto_cm"],
                "nombreProducto_re" => $v["nombreProducto_re"],
                "logoProducto" => $v["logoProducto"],
                "nombreCia" => $v["nombreCia"],
                "formaPagoProducto" => $v["formaPagoProducto"],
                "prima_cm" => $v["prima_cm"] / $numAseg,
                "prima_re" => $v["prima_re"] / $numAseg
            );
        }

        return $primas_aux;
    }

    function valor_medio_decesos ($primas,$numAseg)
    {
        $primas_aux = array();
        foreach ($primas as $k => $v) {
           
            $primas_aux[$k] = array(
                "nombreProducto" => $v["nombreProducto"],
                "idproducto" => $v["idproducto"],
                "idproducto_general" => $v["idproducto_general"],
                "logoProducto" => $v["logoProducto"],
                "nombreCia" => $v["nombreCia"],
                "formaPagoProducto" => $v["formaPagoProducto"],
                "prima_12m" => $v["prima_12m"] / $numAseg,
                "capital" => $v["capital"],
                "coberturas" => $v["coberturas"]                
            );
        }

        return $primas_aux;
    }    

   /**
     * Funcion para hacer operaciones varias sobre las primas devueltas por el WS
     * 
     *
     * @access  public
     * @param   primas    array: La clave es la id del producto asociado a la correduria.
     *                           y cada valor es un array con las siguientes claves => valores
     *                           nombreProducto => string
     *                           nombreProducto_cm => string
     *                           nombreProducto_re => string
     *                           logoProducto => string
     *                           nombreCia => string
     *                           formaPagoProducto => string
     *                           prima_cm => float
     *                           prima_re => float
     *
     * @param numeroDecimales integer                               

     * @return  array
     */
    function decimales_primas_salud ($primas,$numeroDecimales)
    {
        $primas_aux = array();
        foreach ($primas as $k => $v) {
           
            $primas_aux[$k] = array(
                "nombreProducto" => $v["nombreProducto"],
                "nombreProducto_cm" => $v["nombreProducto_cm"],
                "nombreProducto_re" => $v["nombreProducto_re"],
                "logoProducto" => $v["logoProducto"],
                "nombreCia" => $v["nombreCia"],
                "formaPagoProducto" => $v["formaPagoProducto"],
                "prima_cm" => round($v["prima_cm"], $numeroDecimales, PHP_ROUND_HALF_UP),
                "prima_re" => round($v["prima_re"], $numeroDecimales, PHP_ROUND_HALF_UP)
            );
        }

        return $primas_aux;

    }

    function decimales_primas_vida ($primas,$numeroDecimales)
    {
        $primas_aux = array();
        foreach ($primas as $k => $v) {
           
            $primas_aux[$k] = array(
                "nombreProducto" => $v["nombreProducto"],
                "idProducto" => $v["idproducto"],
                "idproducto_general" => $v["idproducto_general"],
                "logoProducto" => $v["logoProducto"],
                "nombreCia" => $v["nombreCia"],
                "formaPagoProducto" => $v["formaPagoProducto"],
                "prima_f" => round($v["prima_f"], $numeroDecimales, PHP_ROUND_HALF_UP),
                "prima_fi" => round($v["prima_fi"], $numeroDecimales, PHP_ROUND_HALF_UP)
            );
        }

        return $primas_aux;

    }   

    function decimales_primas_decesos ($primas,$numeroDecimales)
    {
        $primas_aux = array();
        foreach ($primas as $k => $v) {
           
            $primas_aux[$k] = array(
                "nombreProducto" => $v["nombreProducto"],
                "idproducto" => $v["idproducto"],
                "idproducto_general" => $v["idproducto_general"],
                "logoProducto" => $v["logoProducto"],
                "nombreCia" => $v["nombreCia"],
                "formaPagoProducto" => $v["formaPagoProducto"],
                "prima_12m" => round($v["prima_12m"], $numeroDecimales, PHP_ROUND_HALF_UP),
                "capital" => $v["capital"],
                "coberturas" => $v["coberturas"]  
            );
        }

        return $primas_aux;

    }       



}