<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


/**
 * CodeIgniter clase para el envio de emails
 *
 *
 * @package         CodeIgniter
 * @subpackage      Libraries
 * @category        Libraries
 * @author          Emilio Valls
 */

class Send_emails {

   /**
     * Funcion para hacer el envio de emails
     *
     * @access  public
     * @param   subject    subject del email
     * @param   recipient    destino del email
     * @param   from    from del email
     * @param   fromName    Nombre del que envia el email
     * @param   view    vista donde está el body del email
     * @return  array
     */
    public function smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datos_correo){

        $ci =& get_instance();

        $config = Array(        
            'protocol' => EMAIL_PROTOCOL,
            'smtp_host' => EMAIL_SMTP_HOST,
            'smtp_port' => EMAIL_SMTP_PORT,
            'smtp_user' => EMAIL_SMTP_USER,
            'smtp_pass' => EMAIL_SMTP_PASSWORD,
            'smtp_timeout' => EMAIL_SMTP_TIMEOUT,
            'mailtype'  => EMAIL_SMTP_MAILTYPE, 
            'charset'   => EMAIL_SMTP_CHARSET
        );
        $ci->load->library('email', $config);
        $ci->email->set_newline("\r\n");
        $ci->email->from($from, $fromName);
        $ci->email->to($recipient);
        if ($cc != "") 
            $ci->email->cc($cc);
        if ($bcc != "")
            $ci->email->bcc($bcc);
        $ci->email->subject($subject); 



        $data = array();
        foreach ($datos_correo as $k => $v) {
            $data[$k] = $v;
        }


        $body = $ci->load->view($view,$data,TRUE);

        $ci->email->message($body);   

        if (!$ci->email->send()) {
            $msg = "Ha habido un error al intentar enviar el email";
            $status = false;
            //show_error($this->email->print_debugger()); 
        }else {
            $msg = 'Your e-mail has been sent!';
            $status = true;
        }

        return $status;
    }





}